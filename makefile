###################################
# Makefile of NAGASH              #
# Author : Wenhao Ma              #
# Last Modified: 2021/7/26        #
###################################

# This is quite a general makefile which includes automatic deduction of dependencies.
# Users can change the name of the .so file in order to apply to other projects
# as well as change the flags and compile options to suit their own needs.

# normal compile flags
CXX = g++
ROOT_FLAGS = $(shell root-config --cflags --libs) -lMinuit -lMinuit2
CXX_FLAGS = -std=c++17 -lpthread -lm -O3 -fPIC 
CXX_INCLUDE = -I./include
CXX_LIB = -L./lib -lNAGASH -Wl,-rpath=$(shell pwd)/lib $(patsubst lib%.so,-l%,$(notdir $(wildcard ./lib/*.so)))

# for auto generation of dependency file 
OBJDIR := obj
DEPDIR := $(OBJDIR)/.deps
DEPFLAGS = -MT $@ -MD -MP -MF $(DEPDIR)/$*.d
COMPILE.o = $(CXX) $(DEPFLAGS) $(ROOT_FLAGS) $(CXX_FLAGS) $(CXX_INCLUDE) -c $< -o ./$(OBJDIR)/$@
COMPILE.so = $(CXX) $(ROOT_FLAGS) $(CXX_FLAGS) $(CXX_INCLUDE) -shared -fPIC -o $@ $^
COMPILE.exe = $(CXX) $(ROOT_FLAGS) $(CXX_FLAGS) $(CXX_LIB) $(CXX_INCLUDE) -o $@ $^

# list of objects needed to be built
SRCS = $(notdir $(wildcard ./src/*.cxx ./control/*.cxx))
OBJECTS = $(patsubst %.cxx,%.o,$(notdir $(wildcard ./src/*.cxx ./control/*.cxx)))
LIBS = ./lib/libWY.so
EXECUTABLES = $(patsubst %.cxx,./bin/%,$(notdir $(wildcard ./control/*.cxx)))

VPATH = ./control:./src:./include:./obj

# build all objects
release : 
	@mkdir -p $(OBJDIR) bin lib
	-make $(OBJECTS)
	-make $(LIBS)
	-make $(EXECUTABLES)

# build .o file
%.o : %.cxx $(DEPDIR)/%.d | $(DEPDIR)
	@$(COMPILE.o)

# for dependency files
$(DEPDIR): ; @mkdir -p $@

DEPFILES := $(SRCS:%.cxx=$(DEPDIR)/%.d)
$(DEPFILES):
include $(wildcard $(DEPFILES))

# build LIBS
./lib/libWY.so : $(patsubst %.cxx,./obj/%.o,$(notdir $(wildcard ./src/*.cxx)))
	@$(COMPILE.so)

# build executable file 
./bin/% : ./$(OBJDIR)/%.o
	@$(COMPILE.exe)

# phony objects
.PHONY : clean
clean :
	-rm ./bin/* ./obj/* ./lib/*
