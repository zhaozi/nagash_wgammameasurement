#ifndef WYPLOTGROUP_SOURCE
#define WYPLOTGROUP_SOURCE

#include "WYPlotGroup.h"

WYPlotGroup::WYPlotGroup(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname, int channel) : PlotGroup(MSG, c, rname, fname)
{
  ELECTRON_FABS_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("ELECTRON_FABS_ETA_BINDIV");
  ELECTRON_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("ELECTRON_ETA_BINDIV");
  MUON_FABS_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("MUON_FABS_ETA_BINDIV");
  MUON_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("MUON_ETA_BINDIV");
  PHOTON_FABS_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("PHOTON_FABS_ETA_BINDIV");
  PHOTON_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("PHOTON_ETA_BINDIV");
  LEPTON_FABS_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("LEPTON_FABS_ETA_BINDIV");
  LEPTON_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("LEPTON_ETA_BINDIV");
  LEPTON_PT_BINDIV = ConfigUser()->GetPar<vector<double>>("LEPTON_PT_BINDIV");
  PHOTON_PT_BINDIV = ConfigUser()->GetPar<vector<double>>("PHOTON_PT_BINDIV");
  YJETCR_MET_BINDIV = ConfigUser()->GetPar<vector<double>>("YJETCR_MET_BINDIV");
  YJETCR_MT_BINDIV = ConfigUser()->GetPar<vector<double>>("YJETCR_MT_BINDIV");
  MET_BINDIV = ConfigUser()->GetPar<vector<double>>("MET_BINDIV");
  MT_BINDIV = ConfigUser()->GetPar<vector<double>>("MT_BINDIV");
  PHI_BINDIV = ConfigUser()->GetPar<vector<double>>("PHI_BINDIV");
  DELTAR_BINDIV = ConfigUser()->GetPar<vector<double>>("DELTAR_BINDIV");
  LY_MASS_BINDIV = ConfigUser()->GetPar<vector<double>>("LY_MASS_BINDIV");
  WJET_PHOTON_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("WJET_PHOTON_ETA_BINDIV");
  WJET_PHOTON_PT_BINDIV = ConfigUser()->GetPar<vector<double>>("WJET_PHOTON_PT_BINDIV");
  WEY_YJET_ELECTRON_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("WEY_YJET_ELECTRON_ETA_BINDIV");
  WEY_YJET_ELECTRON_PT_BINDIV = ConfigUser()->GetPar<vector<double>>("WEY_YJET_ELECTRON_PT_BINDIV");
  WUY_YJET_MUON_ETA_BINDIV = ConfigUser()->GetPar<vector<double>>("WUY_YJET_MUON_ETA_BINDIV");
  WUY_YJET_MUON_PT_BINDIV = ConfigUser()->GetPar<vector<double>>("WUY_YJET_MUON_PT_BINDIV");

  BASELINE_PASS_WECY = ConfigUser()->GetPar<bool>("BASELINE_PASS_WECY");                                                                                                           
  BASELINE_PASS_WMUY = ConfigUser()->GetPar<bool>("BASELINE_PASS_WMUY");

  BookWminusEYChannelPlots(channel);
  BookWplusEYChannelPlots(channel);
  BookWminusUYChannelPlots(channel);
  BookWplusUYChannelPlots(channel);
  
  ChannelNumber = BookPlot1D("ChannelNumber", 1001000, -1000, 1000000);
  
 if (channel == -999)
 {
  WJETCR_LT_PhotonEta_PhotonPt_2D = BookBasicPlot2D("WJETCR_LT_PhotonEta_PhotonPt_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV, channel);
  WJETCR_TL_PhotonEta_PhotonPt_2D = BookBasicPlot2D("WJETCR_TL_PhotonEta_PhotonPt_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV, channel);
  WJETCR_LL_PhotonEta_PhotonPt_2D = BookBasicPlot2D("WJETCR_LL_PhotonEta_PhotonPt_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV, channel);
  WJETCR_LT_PhotonEta_PhotonPt_SignalRemoved_2D = BookPlot2D("WJETCR_LT_PhotonEta_PhotonPt_SignalRemoved_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WJETCR_LT_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkType("Delta");
  WJETCR_LT_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkPlot(0, WJETCR_LT_PhotonEta_PhotonPt_2D.Channel_Map["Data"]);
  WJETCR_LT_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkPlot(1, WJETCR_LT_PhotonEta_PhotonPt_2D.Channel_Map["Signal"]);
  WJETCR_TL_PhotonEta_PhotonPt_SignalRemoved_2D = BookPlot2D("WJETCR_TL_PhotonEta_PhotonPt_SignalRemoved_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WJETCR_TL_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkType("Delta");
  WJETCR_TL_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkPlot(0, WJETCR_TL_PhotonEta_PhotonPt_2D.Channel_Map["Data"]);
  WJETCR_TL_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkPlot(1, WJETCR_TL_PhotonEta_PhotonPt_2D.Channel_Map["Signal"]);
  WJETCR_LL_PhotonEta_PhotonPt_SignalRemoved_2D = BookPlot2D("WJETCR_LL_PhotonEta_PhotonPt_SignalRemoved_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WJETCR_LL_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkType("Delta");
  WJETCR_LL_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkPlot(0, WJETCR_LL_PhotonEta_PhotonPt_2D.Channel_Map["Data"]);
  WJETCR_LL_PhotonEta_PhotonPt_SignalRemoved_2D->SetLinkPlot(1, WJETCR_LL_PhotonEta_PhotonPt_2D.Channel_Map["Signal"]);
  WJETCR_LT_PhotonEta_PhotonPt_BKGRemoved_2D = BookPlot2D("WJETCR_LT_PhotonEta_PhotonPt_BKGRemoved_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WJETCR_LT_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkType("Delta");
  WJETCR_LT_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkPlot(0, WJETCR_LT_PhotonEta_PhotonPt_SignalRemoved_2D);
  WJETCR_LT_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkPlot(1, WJETCR_LT_PhotonEta_PhotonPt_2D.Channel_Map["BKG"]);
  WJETCR_TL_PhotonEta_PhotonPt_BKGRemoved_2D = BookPlot2D("WJETCR_TL_PhotonEta_PhotonPt_BKGRemoved_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WJETCR_TL_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkType("Delta");
  WJETCR_TL_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkPlot(0, WJETCR_TL_PhotonEta_PhotonPt_SignalRemoved_2D);
  WJETCR_TL_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkPlot(1, WJETCR_TL_PhotonEta_PhotonPt_2D.Channel_Map["BKG"]);
  WJETCR_LL_PhotonEta_PhotonPt_BKGRemoved_2D = BookPlot2D("WJETCR_LL_PhotonEta_PhotonPt_BKGRemoved_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WJETCR_LL_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkType("Delta");
  WJETCR_LL_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkPlot(0, WJETCR_LL_PhotonEta_PhotonPt_SignalRemoved_2D);
  WJETCR_LL_PhotonEta_PhotonPt_BKGRemoved_2D->SetLinkPlot(1, WJETCR_LL_PhotonEta_PhotonPt_2D.Channel_Map["BKG"]);
  WJETCR_SF_PhotonEta_PhotonPt_2D = BookPlot2D("WJETCR_SF_PhotonEta_PhotonPt_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WJETCR_SF_PhotonEta_PhotonPt_2D->SetLinkType("Ratio");
  WJETCR_SF_PhotonEta_PhotonPt_2D->SetLinkPlot(0, WJETCR_LT_PhotonEta_PhotonPt_BKGRemoved_2D);
  WJETCR_SF_PhotonEta_PhotonPt_2D->SetLinkPlot(1, WJETCR_LL_PhotonEta_PhotonPt_BKGRemoved_2D);

  WEY_WJETCR_TT_PhotonEta_PhotonPt_2D = BookBasicPlot2D("WEY_WJETCR_TT_PhotonEta_PhotonPt_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV, channel);
  WUY_WJETCR_TT_PhotonEta_PhotonPt_2D = BookBasicPlot2D("WUY_WJETCR_TT_PhotonEta_PhotonPt_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV, channel);
  WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D = BookPlot2D("WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->SetLinkType("Ratio");
  WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->SetLinkPlot(0, WEY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["Wjets"]);
  WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->SetLinkPlot(1, WEY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["WPlusJet"]);
  WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D = BookPlot2D("WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D", WJET_PHOTON_ETA_BINDIV, WJET_PHOTON_PT_BINDIV);
  WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->SetLinkType("Ratio");
  WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->SetLinkPlot(0, WUY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["Wjets"]);
  WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->SetLinkPlot(1, WUY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["WPlusJet"]);
  
  WEY_YJETCR_LT_LeptonPt = BookBasicPlot1D("WEY_YJETCR_LT_LeptonPt", LEPTON_PT_BINDIV, channel);
  WEY_YJETCR_LT_LeptonEta = BookBasicPlot1D("WEY_YJETCR_LT_LeptonEta", ELECTRON_ETA_BINDIV, channel);
  WEY_YJETCR_LT_PhotonPt = BookBasicPlot1D("WEY_YJETCR_LT_PhotonPt", PHOTON_PT_BINDIV, channel);
  WEY_YJETCR_LT_PhotonEta = BookBasicPlot1D("WEY_YJETCR_LT_PhotonEta", PHOTON_ETA_BINDIV, channel);
  WEY_YJETCR_LT_MET = BookBasicPlot1D("WEY_YJETCR_LT_MET", YJETCR_MET_BINDIV, channel);
  WEY_YJETCR_LT_MT = BookBasicPlot1D("WEY_YJETCR_LT_MT", YJETCR_MT_BINDIV, channel);
  WEY_YJETCR_LT_LYMass = BookBasicPlot1D("WEY_YJETCR_LT_LYMass", LY_MASS_BINDIV, channel);
  WEY_YJETCR_TL_LeptonPt = BookBasicPlot1D("WEY_YJETCR_TL_LeptonPt", LEPTON_PT_BINDIV, channel);
  WEY_YJETCR_TL_LeptonEta = BookBasicPlot1D("WEY_YJETCR_TL_LeptonEta", ELECTRON_ETA_BINDIV, channel);
  WEY_YJETCR_TL_PhotonPt = BookBasicPlot1D("WEY_YJETCR_TL_PhotonPt", PHOTON_PT_BINDIV, channel);
  WEY_YJETCR_TL_PhotonEta = BookBasicPlot1D("WEY_YJETCR_TL_PhotonEta", PHOTON_ETA_BINDIV, channel);
  WEY_YJETCR_TL_MET = BookBasicPlot1D("WEY_YJETCR_TL_MET", YJETCR_MET_BINDIV, channel);
  WEY_YJETCR_TL_MT = BookBasicPlot1D("WEY_YJETCR_TL_MT", YJETCR_MT_BINDIV, channel);
  WEY_YJETCR_TL_LYMass = BookBasicPlot1D("WEY_YJETCR_TL_LYMass", LY_MASS_BINDIV, channel);
  WEY_YJETCR_LL_LeptonPt = BookBasicPlot1D("WEY_YJETCR_LL_LeptonPt", LEPTON_PT_BINDIV, channel);
  WEY_YJETCR_LL_LeptonEta = BookBasicPlot1D("WEY_YJETCR_LL_LeptonEta", ELECTRON_ETA_BINDIV, channel);
  WEY_YJETCR_LL_PhotonPt = BookBasicPlot1D("WEY_YJETCR_LL_PhotonPt", PHOTON_PT_BINDIV, channel);
  WEY_YJETCR_LL_PhotonEta = BookBasicPlot1D("WEY_YJETCR_LL_PhotonEta", PHOTON_ETA_BINDIV, channel);
  WEY_YJETCR_LL_MET = BookBasicPlot1D("WEY_YJETCR_LL_MET", YJETCR_MET_BINDIV, channel);
  WEY_YJETCR_LL_MT = BookBasicPlot1D("WEY_YJETCR_LL_MT", YJETCR_MT_BINDIV, channel);
  WEY_YJETCR_LL_LYMass = BookBasicPlot1D("WEY_YJETCR_LL_LYMass", LY_MASS_BINDIV, channel);
  
  WEY_YJETCR_LT_ElectronEta_ElectronPt_2D = BookBasicPlot2D("WEY_YJETCR_LT_ElectronEta_ElectronPt_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV, channel);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_2D = BookBasicPlot2D("WEY_YJETCR_TL_ElectronEta_ElectronPt_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV, channel);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_2D = BookBasicPlot2D("WEY_YJETCR_LL_ElectronEta_ElectronPt_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV, channel);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_SignalRemoved_2D = BookPlot2D("WEY_YJETCR_LT_ElectronEta_ElectronPt_SignalRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_LT_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkPlot(0, WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Data"]);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkPlot(1, WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_SignalRemoved_2D = BookPlot2D("WEY_YJETCR_TL_ElectronEta_ElectronPt_SignalRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_TL_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkPlot(0, WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Data"]);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkPlot(1, WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_SignalRemoved_2D = BookPlot2D("WEY_YJETCR_LL_ElectronEta_ElectronPt_SignalRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_LL_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkPlot(0, WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Data"]);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_SignalRemoved_2D->SetLinkPlot(1, WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_BKGRemoved_2D = BookPlot2D("WEY_YJETCR_LT_ElectronEta_ElectronPt_BKGRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_LT_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkPlot(0, WEY_YJETCR_LT_ElectronEta_ElectronPt_SignalRemoved_2D);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkPlot(1, WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_BKGRemoved_2D = BookPlot2D("WEY_YJETCR_TL_ElectronEta_ElectronPt_BKGRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_TL_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkPlot(0, WEY_YJETCR_TL_ElectronEta_ElectronPt_SignalRemoved_2D);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkPlot(1, WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_BKGRemoved_2D = BookPlot2D("WEY_YJETCR_LL_ElectronEta_ElectronPt_BKGRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_LL_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkPlot(0, WEY_YJETCR_LL_ElectronEta_ElectronPt_SignalRemoved_2D);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_BKGRemoved_2D->SetLinkPlot(1, WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_WjetsRemoved_2D = BookPlot2D("WEY_YJETCR_LT_ElectronEta_ElectronPt_WjetsRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_LT_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkPlot(0, WEY_YJETCR_LT_ElectronEta_ElectronPt_BKGRemoved_2D);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkPlot(1, WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D = BookPlot2D("WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkPlot(0, WEY_YJETCR_TL_ElectronEta_ElectronPt_BKGRemoved_2D);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkPlot(1, WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D = BookPlot2D("WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkType("Delta");
  WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkPlot(0, WEY_YJETCR_LL_ElectronEta_ElectronPt_BKGRemoved_2D);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D->SetLinkPlot(1, WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]);
  WEY_YJETCR_SF_ElectronEta_ElectronPt_2D = BookPlot2D("WEY_YJETCR_SF_ElectronEta_ElectronPt_2D", WEY_YJET_ELECTRON_ETA_BINDIV, WEY_YJET_ELECTRON_PT_BINDIV);
  WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->SetLinkType("Ratio");
  WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->SetLinkPlot(0, WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D); // It's LT in the NOTE of 7TeV 
  WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->SetLinkPlot(1, WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D);

  WUY_YJETCR_LT_LeptonPt = BookBasicPlot1D("WUY_YJETCR_LT_LeptonPt", LEPTON_PT_BINDIV, channel);
  WUY_YJETCR_LT_LeptonEta = BookBasicPlot1D("WUY_YJETCR_LT_LeptonEta", MUON_ETA_BINDIV, channel);
  WUY_YJETCR_LT_PhotonPt = BookBasicPlot1D("WUY_YJETCR_LT_PhotonPt", PHOTON_PT_BINDIV, channel);
  WUY_YJETCR_LT_PhotonEta = BookBasicPlot1D("WUY_YJETCR_LT_PhotonEta", PHOTON_ETA_BINDIV, channel);
  WUY_YJETCR_LT_MET = BookBasicPlot1D("WUY_YJETCR_LT_MET", YJETCR_MET_BINDIV, channel);
  WUY_YJETCR_LT_MT = BookBasicPlot1D("WUY_YJETCR_LT_MT", YJETCR_MT_BINDIV, channel);
  WUY_YJETCR_LT_LYMass = BookBasicPlot1D("WUY_YJETCR_LT_LYMass", LY_MASS_BINDIV, channel);
  WUY_YJETCR_TL_LeptonPt = BookBasicPlot1D("WUY_YJETCR_TL_LeptonPt", LEPTON_PT_BINDIV, channel);
  WUY_YJETCR_TL_LeptonEta = BookBasicPlot1D("WUY_YJETCR_TL_LeptonEta", MUON_ETA_BINDIV, channel);
  WUY_YJETCR_TL_PhotonPt = BookBasicPlot1D("WUY_YJETCR_TL_PhotonPt", PHOTON_PT_BINDIV, channel);
  WUY_YJETCR_TL_PhotonEta = BookBasicPlot1D("WUY_YJETCR_TL_PhotonEta", PHOTON_ETA_BINDIV, channel);
  WUY_YJETCR_TL_MET = BookBasicPlot1D("WUY_YJETCR_TL_MET", YJETCR_MET_BINDIV, channel);
  WUY_YJETCR_TL_MT = BookBasicPlot1D("WUY_YJETCR_TL_MT", YJETCR_MT_BINDIV, channel);
  WUY_YJETCR_TL_LYMass = BookBasicPlot1D("WUY_YJETCR_TL_LYMass", LY_MASS_BINDIV, channel);
  WUY_YJETCR_LL_LeptonPt = BookBasicPlot1D("WUY_YJETCR_LL_LeptonPt", LEPTON_PT_BINDIV, channel);
  WUY_YJETCR_LL_LeptonEta = BookBasicPlot1D("WUY_YJETCR_LL_LeptonEta", MUON_ETA_BINDIV, channel);
  WUY_YJETCR_LL_PhotonPt = BookBasicPlot1D("WUY_YJETCR_LL_PhotonPt", PHOTON_PT_BINDIV, channel);
  WUY_YJETCR_LL_PhotonEta = BookBasicPlot1D("WUY_YJETCR_LL_PhotonEta", PHOTON_ETA_BINDIV, channel);
  WUY_YJETCR_LL_MET = BookBasicPlot1D("WUY_YJETCR_LL_MET", YJETCR_MET_BINDIV, channel);
  WUY_YJETCR_LL_MT = BookBasicPlot1D("WUY_YJETCR_LL_MT", YJETCR_MT_BINDIV, channel);
  WUY_YJETCR_LL_LYMass = BookBasicPlot1D("WUY_YJETCR_LL_LYMass", LY_MASS_BINDIV, channel);
  
  WUY_YJETCR_LT_MuonEta_MuonPt_2D = BookBasicPlot2D("WUY_YJETCR_LT_MuonEta_MuonPt_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV, channel);
  WUY_YJETCR_TL_MuonEta_MuonPt_2D = BookBasicPlot2D("WUY_YJETCR_TL_MuonEta_MuonPt_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV, channel);
  WUY_YJETCR_LL_MuonEta_MuonPt_2D = BookBasicPlot2D("WUY_YJETCR_LL_MuonEta_MuonPt_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV, channel);
  WUY_YJETCR_LT_MuonEta_MuonPt_SignalRemoved_2D = BookPlot2D("WUY_YJETCR_LT_MuonEta_MuonPt_SignalRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_LT_MuonEta_MuonPt_SignalRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_LT_MuonEta_MuonPt_SignalRemoved_2D->SetLinkPlot(0, WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Data"]);
  WUY_YJETCR_LT_MuonEta_MuonPt_SignalRemoved_2D->SetLinkPlot(1, WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Signal"]);
  WUY_YJETCR_TL_MuonEta_MuonPt_SignalRemoved_2D = BookPlot2D("WUY_YJETCR_TL_MuonEta_MuonPt_SignalRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_TL_MuonEta_MuonPt_SignalRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_TL_MuonEta_MuonPt_SignalRemoved_2D->SetLinkPlot(0, WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Data"]);
  WUY_YJETCR_TL_MuonEta_MuonPt_SignalRemoved_2D->SetLinkPlot(1, WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Signal"]);
  WUY_YJETCR_LL_MuonEta_MuonPt_SignalRemoved_2D = BookPlot2D("WUY_YJETCR_LL_MuonEta_MuonPt_SignalRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_LL_MuonEta_MuonPt_SignalRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_LL_MuonEta_MuonPt_SignalRemoved_2D->SetLinkPlot(0, WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Data"]);
  WUY_YJETCR_LL_MuonEta_MuonPt_SignalRemoved_2D->SetLinkPlot(1, WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Signal"]);
  WUY_YJETCR_LT_MuonEta_MuonPt_BKGRemoved_2D = BookPlot2D("WUY_YJETCR_LT_MuonEta_MuonPt_BKGRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_LT_MuonEta_MuonPt_BKGRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_LT_MuonEta_MuonPt_BKGRemoved_2D->SetLinkPlot(0, WUY_YJETCR_LT_MuonEta_MuonPt_SignalRemoved_2D);
  WUY_YJETCR_LT_MuonEta_MuonPt_BKGRemoved_2D->SetLinkPlot(1, WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["BKG"]);
  WUY_YJETCR_TL_MuonEta_MuonPt_BKGRemoved_2D = BookPlot2D("WUY_YJETCR_TL_MuonEta_MuonPt_BKGRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_TL_MuonEta_MuonPt_BKGRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_TL_MuonEta_MuonPt_BKGRemoved_2D->SetLinkPlot(0, WUY_YJETCR_TL_MuonEta_MuonPt_SignalRemoved_2D);
  WUY_YJETCR_TL_MuonEta_MuonPt_BKGRemoved_2D->SetLinkPlot(1, WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["BKG"]);
  WUY_YJETCR_LL_MuonEta_MuonPt_BKGRemoved_2D = BookPlot2D("WUY_YJETCR_LL_MuonEta_MuonPt_BKGRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_LL_MuonEta_MuonPt_BKGRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_LL_MuonEta_MuonPt_BKGRemoved_2D->SetLinkPlot(0, WUY_YJETCR_LL_MuonEta_MuonPt_SignalRemoved_2D);
  WUY_YJETCR_LL_MuonEta_MuonPt_BKGRemoved_2D->SetLinkPlot(1, WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["BKG"]);
  WUY_YJETCR_LT_MuonEta_MuonPt_WjetsRemoved_2D = BookPlot2D("WUY_YJETCR_LT_MuonEta_MuonPt_WjetsRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_LT_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_LT_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkPlot(0, WUY_YJETCR_LT_MuonEta_MuonPt_BKGRemoved_2D);
  WUY_YJETCR_LT_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkPlot(1, WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Wjets"]);
  WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D = BookPlot2D("WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkPlot(0, WUY_YJETCR_TL_MuonEta_MuonPt_BKGRemoved_2D);
  WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkPlot(1, WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Wjets"]);
  WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D = BookPlot2D("WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkType("Delta");
  WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkPlot(0, WUY_YJETCR_LL_MuonEta_MuonPt_BKGRemoved_2D);
  WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D->SetLinkPlot(1, WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Wjets"]);
  WUY_YJETCR_SF_MuonEta_MuonPt_2D = BookPlot2D("WUY_YJETCR_SF_MuonEta_MuonPt_2D", WUY_YJET_MUON_ETA_BINDIV, WUY_YJET_MUON_PT_BINDIV);
  WUY_YJETCR_SF_MuonEta_MuonPt_2D->SetLinkType("Ratio");
  WUY_YJETCR_SF_MuonEta_MuonPt_2D->SetLinkPlot(0, WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D);
  WUY_YJETCR_SF_MuonEta_MuonPt_2D->SetLinkPlot(1, WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D);
 }
}

void WYPlotGroup::BookWminusEYChannelPlots(int channel)
{
  ElectronPt = BookBasicPlot1D("ElectronPt", LEPTON_PT_BINDIV, channel);
  ElectronEta = BookBasicPlot1D("ElectronEta", ELECTRON_ETA_BINDIV, channel);
  PhotonPt_WminusEY = BookBasicPlot1D("PhotonPt_WminusEY", PHOTON_PT_BINDIV, channel);
  PhotonEta_WminusEY = BookBasicPlot1D("PhotonEta_WminusEY", PHOTON_ETA_BINDIV, channel);
  LYMass_WminusEY = BookBasicPlot1D("LYMass_WminusEY", LY_MASS_BINDIV, channel);
  FabsEtaElectron_WminusLarger = BookBasicPlot1D("FabsEtaElectron_WminusLarger", ELECTRON_FABS_ETA_BINDIV, channel);
  FabsEtaElectron_WminusSmaller = BookBasicPlot1D("FabsEtaElectron_WminusSmaller", ELECTRON_FABS_ETA_BINDIV, channel);
  MET_WminusEY = BookBasicPlot1D("MET_WminusEY", MET_BINDIV, channel);
  MT_WminusEY = BookBasicPlot1D("MT_WminusEY", MT_BINDIV, channel);
  LYDeltaPhi_WminusEY = BookBasicPlot1D("LYDeltaPhi_WminusEY", PHI_BINDIV, channel);
  LYDeltaR_WminusEY = BookBasicPlot1D("LYDeltaR_WminusEY", DELTAR_BINDIV, channel);
  
  FabsEtaElectron_WminusLarger_WjetsRemoved = BookPlot1D("FabsEtaElectron_WminusLarger_WjetsRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaElectron_WminusLarger_WjetsRemoved->SetLinkType("Delta");
  FabsEtaElectron_WminusLarger_WjetsRemoved->SetLinkPlot(0, FabsEtaElectron_WminusLarger.Channel_Map["Data"]);
  FabsEtaElectron_WminusLarger_WjetsRemoved->SetLinkPlot(1, FabsEtaElectron_WminusLarger.Channel_Map["Wjets"]);
  FabsEtaElectron_WminusLarger_YjetsRemoved = BookPlot1D("FabsEtaElectron_WminusLarger_YjetsRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaElectron_WminusLarger_YjetsRemoved->SetLinkType("Delta");
  FabsEtaElectron_WminusLarger_YjetsRemoved->SetLinkPlot(0, FabsEtaElectron_WminusLarger_WjetsRemoved);
  FabsEtaElectron_WminusLarger_YjetsRemoved->SetLinkPlot(1, FabsEtaElectron_WminusLarger.Channel_Map["Yjets"]);
  FabsEtaElectron_WminusLarger_BKGRemoved = BookPlot1D("FabsEtaElectron_WminusLarger_BKGRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaElectron_WminusLarger_BKGRemoved->SetLinkType("Delta");
  FabsEtaElectron_WminusLarger_BKGRemoved->SetLinkPlot(0, FabsEtaElectron_WminusLarger_YjetsRemoved);
  FabsEtaElectron_WminusLarger_BKGRemoved->SetLinkPlot(1, FabsEtaElectron_WminusLarger.Channel_Map["BKG"]);

  FabsEtaElectron_WminusSmaller_WjetsRemoved = BookPlot1D("FabsEtaElectron_WminusSmaller_WjetsRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaElectron_WminusSmaller_WjetsRemoved->SetLinkType("Delta");
  FabsEtaElectron_WminusSmaller_WjetsRemoved->SetLinkPlot(0, FabsEtaElectron_WminusSmaller.Channel_Map["Data"]);
  FabsEtaElectron_WminusSmaller_WjetsRemoved->SetLinkPlot(1, FabsEtaElectron_WminusSmaller.Channel_Map["Wjets"]);
  FabsEtaElectron_WminusSmaller_YjetsRemoved = BookPlot1D("FabsEtaElectron_WminusSmaller_YjetsRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaElectron_WminusSmaller_YjetsRemoved->SetLinkType("Delta");
  FabsEtaElectron_WminusSmaller_YjetsRemoved->SetLinkPlot(0, FabsEtaElectron_WminusSmaller_WjetsRemoved);
  FabsEtaElectron_WminusSmaller_YjetsRemoved->SetLinkPlot(1, FabsEtaElectron_WminusSmaller.Channel_Map["Yjets"]);
  FabsEtaElectron_WminusSmaller_BKGRemoved = BookPlot1D("FabsEtaElectron_WminusSmaller_BKGRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaElectron_WminusSmaller_BKGRemoved->SetLinkType("Delta");
  FabsEtaElectron_WminusSmaller_BKGRemoved->SetLinkPlot(0, FabsEtaElectron_WminusSmaller_YjetsRemoved);
  FabsEtaElectron_WminusSmaller_BKGRemoved->SetLinkPlot(1, FabsEtaElectron_WminusSmaller.Channel_Map["BKG"]);

  AsymmetryFabsEtaElectron_Wminus = BookBasicPlot1D("AsymmetryFabsEtaElectron_Wminus", ELECTRON_FABS_ETA_BINDIV, channel);
  AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->SetLinkType("Asymmetry");
  AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->SetLinkPlot(0, FabsEtaElectron_WminusLarger_BKGRemoved);
  AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->SetLinkPlot(1, FabsEtaElectron_WminusSmaller_BKGRemoved);
  AsymmetryFabsEtaElectron_Wminus.Channel_Map["Signal"]->SetLinkType("Asymmetry");
  AsymmetryFabsEtaElectron_Wminus.Channel_Map["Signal"]->SetLinkPlot(0, FabsEtaElectron_WminusLarger.Channel_Map["Signal"]);
  AsymmetryFabsEtaElectron_Wminus.Channel_Map["Signal"]->SetLinkPlot(1, FabsEtaElectron_WminusSmaller.Channel_Map["Signal"]);
}

void WYPlotGroup::BookWplusEYChannelPlots(int channel)
{
  PositronPt = BookBasicPlot1D("PositronPt", LEPTON_PT_BINDIV, channel);
  PositronEta = BookBasicPlot1D("PositronEta", ELECTRON_ETA_BINDIV, channel);
  PhotonPt_WplusEY = BookBasicPlot1D("PhotonPt_WplusEY", PHOTON_PT_BINDIV, channel);
  PhotonEta_WplusEY = BookBasicPlot1D("PhotonEta_WplusEY", PHOTON_ETA_BINDIV, channel);
  LYMass_WplusEY = BookBasicPlot1D("LYMass_WplusEY", LY_MASS_BINDIV, channel);
  FabsEtaPositron_WplusLarger = BookBasicPlot1D("FabsEtaPositron_WplusLarger", ELECTRON_FABS_ETA_BINDIV, channel);
  FabsEtaPositron_WplusSmaller = BookBasicPlot1D("FabsEtaPositron_WplusSmaller", ELECTRON_FABS_ETA_BINDIV, channel);
  MET_WplusEY = BookBasicPlot1D("MET_WplusEY", MET_BINDIV, channel);
  MT_WplusEY = BookBasicPlot1D("MT_WplusEY", MT_BINDIV, channel);
  LYDeltaPhi_WplusEY = BookBasicPlot1D("LYDeltaPhi_WplusEY", PHI_BINDIV, channel);
  LYDeltaR_WplusEY = BookBasicPlot1D("LYDeltaR_WplusEY", DELTAR_BINDIV, channel);

  FabsEtaPositron_WplusLarger_WjetsRemoved = BookPlot1D("FabsEtaPositron_WplusLarger_WjetsRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaPositron_WplusLarger_WjetsRemoved->SetLinkType("Delta");
  FabsEtaPositron_WplusLarger_WjetsRemoved->SetLinkPlot(0, FabsEtaPositron_WplusLarger.Channel_Map["Data"]);
  FabsEtaPositron_WplusLarger_WjetsRemoved->SetLinkPlot(1, FabsEtaPositron_WplusLarger.Channel_Map["Wjets"]);
  FabsEtaPositron_WplusLarger_YjetsRemoved = BookPlot1D("FabsEtaPositron_WplusLarger_YjetsRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaPositron_WplusLarger_YjetsRemoved->SetLinkType("Delta");
  FabsEtaPositron_WplusLarger_YjetsRemoved->SetLinkPlot(0, FabsEtaPositron_WplusLarger_WjetsRemoved);
  FabsEtaPositron_WplusLarger_YjetsRemoved->SetLinkPlot(1, FabsEtaPositron_WplusLarger.Channel_Map["Yjets"]);
  FabsEtaPositron_WplusLarger_BKGRemoved = BookPlot1D("FabsEtaPositron_WplusLarger_BKGRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaPositron_WplusLarger_BKGRemoved->SetLinkType("Delta");
  FabsEtaPositron_WplusLarger_BKGRemoved->SetLinkPlot(0, FabsEtaPositron_WplusLarger_YjetsRemoved);
  FabsEtaPositron_WplusLarger_BKGRemoved->SetLinkPlot(1, FabsEtaPositron_WplusLarger.Channel_Map["BKG"]);

  FabsEtaPositron_WplusSmaller_WjetsRemoved = BookPlot1D("FabsEtaPositron_WplusSmaller_WjetsRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaPositron_WplusSmaller_WjetsRemoved->SetLinkType("Delta");
  FabsEtaPositron_WplusSmaller_WjetsRemoved->SetLinkPlot(0, FabsEtaPositron_WplusSmaller.Channel_Map["Data"]);
  FabsEtaPositron_WplusSmaller_WjetsRemoved->SetLinkPlot(1, FabsEtaPositron_WplusSmaller.Channel_Map["Wjets"]);
  FabsEtaPositron_WplusSmaller_YjetsRemoved = BookPlot1D("FabsEtaPositron_WplusSmaller_YjetsRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaPositron_WplusSmaller_YjetsRemoved->SetLinkType("Delta");
  FabsEtaPositron_WplusSmaller_YjetsRemoved->SetLinkPlot(0, FabsEtaPositron_WplusSmaller_WjetsRemoved);
  FabsEtaPositron_WplusSmaller_YjetsRemoved->SetLinkPlot(1, FabsEtaPositron_WplusSmaller.Channel_Map["Yjets"]);
  FabsEtaPositron_WplusSmaller_BKGRemoved = BookPlot1D("FabsEtaPositron_WplusSmaller_BKGRemoved", ELECTRON_FABS_ETA_BINDIV);
  FabsEtaPositron_WplusSmaller_BKGRemoved->SetLinkType("Delta");
  FabsEtaPositron_WplusSmaller_BKGRemoved->SetLinkPlot(0, FabsEtaPositron_WplusSmaller_YjetsRemoved);
  FabsEtaPositron_WplusSmaller_BKGRemoved->SetLinkPlot(1, FabsEtaPositron_WplusSmaller.Channel_Map["BKG"]);

  AsymmetryFabsEtaPositron_Wplus = BookBasicPlot1D("AsymmetryFabsEtaPositron_Wplus", ELECTRON_FABS_ETA_BINDIV, channel);
  AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->SetLinkType("Asymmetry");
  AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->SetLinkPlot(0, FabsEtaPositron_WplusLarger_BKGRemoved);
  AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->SetLinkPlot(1, FabsEtaPositron_WplusSmaller_BKGRemoved);
  AsymmetryFabsEtaPositron_Wplus.Channel_Map["Signal"]->SetLinkType("Asymmetry");
  AsymmetryFabsEtaPositron_Wplus.Channel_Map["Signal"]->SetLinkPlot(0, FabsEtaPositron_WplusLarger.Channel_Map["Signal"]);
  AsymmetryFabsEtaPositron_Wplus.Channel_Map["Signal"]->SetLinkPlot(1, FabsEtaPositron_WplusSmaller.Channel_Map["Signal"]);
}

void WYPlotGroup::BookWminusUYChannelPlots(int channel)
{
  MuonPt = BookBasicPlot1D("MuonPt", LEPTON_PT_BINDIV, channel);
  MuonEta = BookBasicPlot1D("MuonEta", MUON_ETA_BINDIV, channel);
  PhotonPt_WminusUY = BookBasicPlot1D("PhotonPt_WminusUY", PHOTON_PT_BINDIV, channel);
  PhotonEta_WminusUY = BookBasicPlot1D("PhotonEta_WminusUY", PHOTON_ETA_BINDIV, channel);
  LYMass_WminusUY = BookBasicPlot1D("LYMass_WminusUY", LY_MASS_BINDIV, channel);
  FabsEtaMuon_WminusLarger = BookBasicPlot1D("FabsEtaMuon_WminusLarger", MUON_FABS_ETA_BINDIV, channel);
  FabsEtaMuon_WminusSmaller = BookBasicPlot1D("FabsEtaMuon_WminusSmaller", MUON_FABS_ETA_BINDIV, channel);
  MET_WminusUY = BookBasicPlot1D("MET_WminusUY", MET_BINDIV, channel);
  MT_WminusUY = BookBasicPlot1D("MT_WminusUY", MT_BINDIV, channel);
  LYDeltaPhi_WminusUY = BookBasicPlot1D("LYDeltaPhi_WminusUY", PHI_BINDIV, channel);
  LYDeltaR_WminusUY = BookBasicPlot1D("LYDeltaR_WminusUY", DELTAR_BINDIV, channel);
  
  FabsEtaMuon_WminusLarger_WjetsRemoved = BookPlot1D("FabsEtaMuon_WminusLarger_WjetsRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaMuon_WminusLarger_WjetsRemoved->SetLinkType("Delta");
  FabsEtaMuon_WminusLarger_WjetsRemoved->SetLinkPlot(0, FabsEtaMuon_WminusLarger.Channel_Map["Data"]);
  FabsEtaMuon_WminusLarger_WjetsRemoved->SetLinkPlot(1, FabsEtaMuon_WminusLarger.Channel_Map["Wjets"]);
  FabsEtaMuon_WminusLarger_YjetsRemoved = BookPlot1D("FabsEtaMuon_WminusLarger_YjetsRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaMuon_WminusLarger_YjetsRemoved->SetLinkType("Delta");
  FabsEtaMuon_WminusLarger_YjetsRemoved->SetLinkPlot(0, FabsEtaMuon_WminusLarger_WjetsRemoved);
  FabsEtaMuon_WminusLarger_YjetsRemoved->SetLinkPlot(1, FabsEtaMuon_WminusLarger.Channel_Map["Yjets"]);
  FabsEtaMuon_WminusLarger_BKGRemoved = BookPlot1D("FabsEtaMuon_WminusLarger_BKGRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaMuon_WminusLarger_BKGRemoved->SetLinkType("Delta");
  FabsEtaMuon_WminusLarger_BKGRemoved->SetLinkPlot(0, FabsEtaMuon_WminusLarger_YjetsRemoved);
  FabsEtaMuon_WminusLarger_BKGRemoved->SetLinkPlot(1, FabsEtaMuon_WminusLarger.Channel_Map["BKG"]);

  FabsEtaMuon_WminusSmaller_WjetsRemoved = BookPlot1D("FabsEtaMuon_WminusSmaller_WjetsRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaMuon_WminusSmaller_WjetsRemoved->SetLinkType("Delta");
  FabsEtaMuon_WminusSmaller_WjetsRemoved->SetLinkPlot(0, FabsEtaMuon_WminusSmaller.Channel_Map["Data"]);
  FabsEtaMuon_WminusSmaller_WjetsRemoved->SetLinkPlot(1, FabsEtaMuon_WminusSmaller.Channel_Map["Wjets"]);
  FabsEtaMuon_WminusSmaller_YjetsRemoved = BookPlot1D("FabsEtaMuon_WminusSmaller_YjetsRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaMuon_WminusSmaller_YjetsRemoved->SetLinkType("Delta");
  FabsEtaMuon_WminusSmaller_YjetsRemoved->SetLinkPlot(0, FabsEtaMuon_WminusSmaller_WjetsRemoved);
  FabsEtaMuon_WminusSmaller_YjetsRemoved->SetLinkPlot(1, FabsEtaMuon_WminusSmaller.Channel_Map["Yjets"]);
  FabsEtaMuon_WminusSmaller_BKGRemoved = BookPlot1D("FabsEtaMuon_WminusSmaller_BKGRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaMuon_WminusSmaller_BKGRemoved->SetLinkType("Delta");
  FabsEtaMuon_WminusSmaller_BKGRemoved->SetLinkPlot(0, FabsEtaMuon_WminusSmaller_YjetsRemoved);
  FabsEtaMuon_WminusSmaller_BKGRemoved->SetLinkPlot(1, FabsEtaMuon_WminusSmaller.Channel_Map["BKG"]);

  AsymmetryFabsEtaMuon_Wminus = BookBasicPlot1D("AsymmetryFabsEtaMuon_Wminus", MUON_FABS_ETA_BINDIV, channel);
  AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->SetLinkType("Asymmetry");
  AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->SetLinkPlot(0, FabsEtaMuon_WminusLarger_BKGRemoved);
  AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->SetLinkPlot(1, FabsEtaMuon_WminusSmaller_BKGRemoved);
  AsymmetryFabsEtaMuon_Wminus.Channel_Map["Signal"]->SetLinkType("Asymmetry");
  AsymmetryFabsEtaMuon_Wminus.Channel_Map["Signal"]->SetLinkPlot(0, FabsEtaMuon_WminusLarger.Channel_Map["Signal"]);
  AsymmetryFabsEtaMuon_Wminus.Channel_Map["Signal"]->SetLinkPlot(1, FabsEtaMuon_WminusSmaller.Channel_Map["Signal"]);
}

void WYPlotGroup::BookWplusUYChannelPlots(int channel)
{
  AntiMuonPt = BookBasicPlot1D("AntiMuonPt", LEPTON_PT_BINDIV, channel);
  AntiMuonEta = BookBasicPlot1D("AntiMuonEta", MUON_ETA_BINDIV, channel);
  PhotonPt_WplusUY = BookBasicPlot1D("PhotonPt_WplusUY", PHOTON_PT_BINDIV, channel);
  PhotonEta_WplusUY = BookBasicPlot1D("PhotonEta_WplusUY", PHOTON_ETA_BINDIV, channel);
  LYMass_WplusUY = BookBasicPlot1D("LYMass_WplusUY", LY_MASS_BINDIV, channel);
  FabsEtaAntiMuon_WplusLarger = BookBasicPlot1D("FabsEtaAntiMuon_WplusLarger", MUON_FABS_ETA_BINDIV, channel);
  FabsEtaAntiMuon_WplusSmaller = BookBasicPlot1D("FabsEtaAntiMuon_WplusSmaller", MUON_FABS_ETA_BINDIV, channel);
  MET_WplusUY = BookBasicPlot1D("MET_WplusUY", MET_BINDIV, channel);
  MT_WplusUY = BookBasicPlot1D("MT_WplusUY", MT_BINDIV, channel);
  LYDeltaPhi_WplusUY = BookBasicPlot1D("LYDeltaPhi_WplusUY", PHI_BINDIV, channel);
  LYDeltaR_WplusUY = BookBasicPlot1D("LYDeltaR_WplusUY", DELTAR_BINDIV, channel);
  
  FabsEtaAntiMuon_WplusLarger_WjetsRemoved = BookPlot1D("FabsEtaAntiMuon_WplusLarger_WjetsRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaAntiMuon_WplusLarger_WjetsRemoved->SetLinkType("Delta");
  FabsEtaAntiMuon_WplusLarger_WjetsRemoved->SetLinkPlot(0, FabsEtaAntiMuon_WplusLarger.Channel_Map["Data"]);
  FabsEtaAntiMuon_WplusLarger_WjetsRemoved->SetLinkPlot(1, FabsEtaAntiMuon_WplusLarger.Channel_Map["Wjets"]);
  FabsEtaAntiMuon_WplusLarger_YjetsRemoved = BookPlot1D("FabsEtaAntiMuon_WplusLarger_YjetsRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaAntiMuon_WplusLarger_YjetsRemoved->SetLinkType("Delta");
  FabsEtaAntiMuon_WplusLarger_YjetsRemoved->SetLinkPlot(0, FabsEtaAntiMuon_WplusLarger_WjetsRemoved);
  FabsEtaAntiMuon_WplusLarger_YjetsRemoved->SetLinkPlot(1, FabsEtaAntiMuon_WplusLarger.Channel_Map["Yjets"]);
  FabsEtaAntiMuon_WplusLarger_BKGRemoved = BookPlot1D("FabsEtaAntiMuon_WplusLarger_BKGRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaAntiMuon_WplusLarger_BKGRemoved->SetLinkType("Delta");
  FabsEtaAntiMuon_WplusLarger_BKGRemoved->SetLinkPlot(0, FabsEtaAntiMuon_WplusLarger_YjetsRemoved);
  FabsEtaAntiMuon_WplusLarger_BKGRemoved->SetLinkPlot(1, FabsEtaAntiMuon_WplusLarger.Channel_Map["BKG"]);

  FabsEtaAntiMuon_WplusSmaller_WjetsRemoved = BookPlot1D("FabsEtaAntiMuon_WplusSmaller_WjetsRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaAntiMuon_WplusSmaller_WjetsRemoved->SetLinkType("Delta");
  FabsEtaAntiMuon_WplusSmaller_WjetsRemoved->SetLinkPlot(0, FabsEtaAntiMuon_WplusSmaller.Channel_Map["Data"]);
  FabsEtaAntiMuon_WplusSmaller_WjetsRemoved->SetLinkPlot(1, FabsEtaAntiMuon_WplusSmaller.Channel_Map["Wjets"]);
  FabsEtaAntiMuon_WplusSmaller_YjetsRemoved = BookPlot1D("FabsEtaAntiMuon_WplusSmaller_YjetsRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaAntiMuon_WplusSmaller_YjetsRemoved->SetLinkType("Delta");
  FabsEtaAntiMuon_WplusSmaller_YjetsRemoved->SetLinkPlot(0, FabsEtaAntiMuon_WplusSmaller_WjetsRemoved);
  FabsEtaAntiMuon_WplusSmaller_YjetsRemoved->SetLinkPlot(1, FabsEtaAntiMuon_WplusSmaller.Channel_Map["Yjets"]);
  FabsEtaAntiMuon_WplusSmaller_BKGRemoved = BookPlot1D("FabsEtaAntiMuon_WplusSmaller_BKGRemoved", MUON_FABS_ETA_BINDIV);
  FabsEtaAntiMuon_WplusSmaller_BKGRemoved->SetLinkType("Delta");
  FabsEtaAntiMuon_WplusSmaller_BKGRemoved->SetLinkPlot(0, FabsEtaAntiMuon_WplusSmaller_YjetsRemoved);
  FabsEtaAntiMuon_WplusSmaller_BKGRemoved->SetLinkPlot(1, FabsEtaAntiMuon_WplusSmaller.Channel_Map["BKG"]);

  AsymmetryFabsEtaAntiMuon_Wplus = BookBasicPlot1D("AsymmetryFabsEtaAntiMuon_Wplus", MUON_FABS_ETA_BINDIV, channel);
  AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->SetLinkType("Asymmetry");
  AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->SetLinkPlot(0, FabsEtaAntiMuon_WplusLarger_BKGRemoved);
  AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->SetLinkPlot(1, FabsEtaAntiMuon_WplusSmaller_BKGRemoved);
  AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Signal"]->SetLinkType("Asymmetry");
  AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Signal"]->SetLinkPlot(0, FabsEtaAntiMuon_WplusLarger.Channel_Map["Signal"]);
  AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Signal"]->SetLinkPlot(1, FabsEtaAntiMuon_WplusSmaller.Channel_Map["Signal"]);
}

void WYPlotGroup::FillWminusEYChannelEvent(const WYEvent event)
{
  ElectronPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
  ElectronEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
  PhotonPt_WminusEY.Fill(event, event.PhotonPt.at(0), event.GetWeight());
  PhotonEta_WminusEY.Fill(event, event.PhotonEta.at(0), event.GetWeight());
  LYMass_WminusEY.Fill(event, event.GetLYMass(), event.GetWeight());
  if (fabs(event.LeptonEta.at(0)) > fabs(event.PhotonEta.at(0)))
  {
    FabsEtaElectron_WminusLarger.Fill(event, fabs(event.LeptonEta.at(0)), event.GetWeight());
  }
  else
  {
    FabsEtaElectron_WminusSmaller.Fill(event, fabs(event.LeptonEta.at(0)), event.GetWeight());
  }
  MET_WminusEY.Fill(event, event.MET, event.GetWeight());
  MT_WminusEY.Fill(event, event.GetMT(), event.GetWeight());
  LYDeltaPhi_WminusEY.Fill(event, event.GetLYDeltaPhi(), event.GetWeight());
  LYDeltaR_WminusEY.Fill(event, event.GetLYDeltaR(), event.GetWeight());
}

void WYPlotGroup::FillWplusEYChannelEvent(const WYEvent event)
{ 
  PositronPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
  PositronEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
  PhotonPt_WplusEY.Fill(event, event.PhotonPt.at(0), event.GetWeight());
  PhotonEta_WplusEY.Fill(event, event.PhotonEta.at(0), event.GetWeight());
  LYMass_WplusEY.Fill(event, event.GetLYMass(), event.GetWeight());
  if (fabs(event.LeptonEta.at(0)) > fabs(event.PhotonEta.at(0)))
  { 
    FabsEtaPositron_WplusLarger.Fill(event, fabs(event.LeptonEta.at(0)), event.GetWeight());
  }
  else
  { 
    FabsEtaPositron_WplusSmaller.Fill(event, fabs(event.LeptonEta.at(0)), event.GetWeight());
  }
  MET_WplusEY.Fill(event, event.MET, event.GetWeight());
  MT_WplusEY.Fill(event, event.GetMT(), event.GetWeight());
  LYDeltaPhi_WplusEY.Fill(event, event.GetLYDeltaPhi(), event.GetWeight());
  LYDeltaR_WplusEY.Fill(event, event.GetLYDeltaR(), event.GetWeight());
}

void WYPlotGroup::FillWminusUYChannelEvent(const WYEvent event)
{ 
  MuonPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
  MuonEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
  PhotonPt_WminusUY.Fill(event, event.PhotonPt.at(0), event.GetWeight());
  PhotonEta_WminusUY.Fill(event, event.PhotonEta.at(0), event.GetWeight());
  LYMass_WminusUY.Fill(event, event.GetLYMass(), event.GetWeight());
  if (fabs(event.LeptonEta.at(0)) > fabs(event.PhotonEta.at(0)))
  { 
    FabsEtaMuon_WminusLarger.Fill(event, fabs(event.LeptonEta.at(0)), event.GetWeight());
  }
  else
  { 
    FabsEtaMuon_WminusSmaller.Fill(event, fabs(event.LeptonEta.at(0)), event.GetWeight());
  }
  MET_WminusUY.Fill(event, event.MET, event.GetWeight());
  MT_WminusUY.Fill(event, event.GetMT(), event.GetWeight());
  LYDeltaPhi_WminusUY.Fill(event, event.GetLYDeltaPhi(), event.GetWeight());
  LYDeltaR_WminusUY.Fill(event, event.GetLYDeltaR(), event.GetWeight());
}

void WYPlotGroup::FillWplusUYChannelEvent(const WYEvent event)
{ 
  AntiMuonPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
  AntiMuonEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
  PhotonPt_WplusUY.Fill(event, event.PhotonPt.at(0), event.GetWeight());
  PhotonEta_WplusUY.Fill(event, event.PhotonEta.at(0), event.GetWeight());
  LYMass_WplusUY.Fill(event, event.GetLYMass(), event.GetWeight());
  if (fabs(event.LeptonEta.at(0)) > fabs(event.PhotonEta.at(0)))
  { 
    FabsEtaAntiMuon_WplusLarger.Fill(event, fabs(event.LeptonEta.at(0)), event.GetWeight());
  }
  else
  { 
    FabsEtaAntiMuon_WplusSmaller.Fill(event, fabs(event.LeptonEta.at(0)), event.GetWeight());
  }
  MET_WplusUY.Fill(event, event.MET, event.GetWeight());
  MT_WplusUY.Fill(event, event.GetMT(), event.GetWeight());
  LYDeltaPhi_WplusUY.Fill(event, event.GetLYDeltaPhi(), event.GetWeight());
  LYDeltaR_WplusUY.Fill(event, event.GetLYDeltaR(), event.GetWeight());
}

void WYPlotGroup::FillWEYWPlusJetChannelEvent(const WYEvent event)
{
    WEY_WJETCR_TT_PhotonEta_PhotonPt_2D.Fill(event, event.PhotonEta.at(0), event.PhotonPt.at(0), event.GetWeight());
}

void WYPlotGroup::FillWUYWPlusJetChannelEvent(const WYEvent event)
{
    WUY_WJETCR_TT_PhotonEta_PhotonPt_2D.Fill(event, event.PhotonEta.at(0), event.PhotonPt.at(0), event.GetWeight());
}

void WYPlotGroup::FillWJETCRChannelEvent(const WYEvent event)
{ 
  //MSGUser()->MSG_DEBUG("Fill WJETCR Channel Event : PhotonEta = ", event.PhotonEta.at(0), " Pt = ", event.PhotonPt.at(0), " w = ", event.GetWeight());
  if (event.ID == WYEventType::WECY_WJETCR_LT ||
      event.ID == WYEventType::WMUY_WJETCR_LT)
    WJETCR_LT_PhotonEta_PhotonPt_2D.Fill(event, event.PhotonEta.at(0), event.PhotonPt.at(0), event.GetWeight());
  else if (event.ID == WYEventType::WECY_WJETCR_TL ||
           event.ID == WYEventType::WMUY_WJETCR_TL)
    WJETCR_TL_PhotonEta_PhotonPt_2D.Fill(event, event.PhotonEta.at(0), event.PhotonPt.at(0), event.GetWeight());
  else if (event.ID == WYEventType::WECY_WJETCR_LL ||
           event.ID == WYEventType::WMUY_WJETCR_LL)
    WJETCR_LL_PhotonEta_PhotonPt_2D.Fill(event, event.PhotonEta.at(0), event.PhotonPt.at(0), event.GetWeight());
  else MSGUser()->MSG_ERROR("Fill WJETCR ChannelEvent : Unfilled WYEventType = ", (int)event.ID);
}

void WYPlotGroup::FillWEYYJETCRChannelEvent(const WYEvent event)
{
  if (event.ID == WYEventType::WECY_YJETCR_LT)
  {
    WEY_YJETCR_LT_LeptonPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
    WEY_YJETCR_LT_LeptonEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
    WEY_YJETCR_LT_PhotonPt.Fill(event, event.PhotonPt.at(0), event.GetWeight());
    WEY_YJETCR_LT_PhotonEta.Fill(event, event.PhotonEta.at(0), event.GetWeight());
    WEY_YJETCR_LT_MET.Fill(event, event.MET, event.GetWeight());
    WEY_YJETCR_LT_MT.Fill(event, event.GetMT(), event.GetWeight());
    WEY_YJETCR_LT_LYMass.Fill(event, event.GetLYMass(), event.GetWeight());
    WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Fill(event, event.LeptonEta.at(0) , event.LeptonPt.at(0), event.GetWeight());
  }
  else if (event.ID == WYEventType::WECY_YJETCR_TL)
  {
    WEY_YJETCR_TL_LeptonPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
    WEY_YJETCR_TL_LeptonEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
    WEY_YJETCR_TL_PhotonPt.Fill(event, event.PhotonPt.at(0), event.GetWeight());
    WEY_YJETCR_TL_PhotonEta.Fill(event, event.PhotonEta.at(0), event.GetWeight());
    WEY_YJETCR_TL_MET.Fill(event, event.MET, event.GetWeight());
    WEY_YJETCR_TL_MT.Fill(event, event.GetMT(), event.GetWeight());
    WEY_YJETCR_TL_LYMass.Fill(event, event.GetLYMass(), event.GetWeight());
    WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Fill(event, event.LeptonEta.at(0) , event.LeptonPt.at(0), event.GetWeight());
  }
  else if (event.ID == WYEventType::WECY_YJETCR_LL)
  {
    WEY_YJETCR_LL_LeptonPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
    WEY_YJETCR_LL_LeptonEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
    WEY_YJETCR_LL_PhotonPt.Fill(event, event.PhotonPt.at(0), event.GetWeight());
    WEY_YJETCR_LL_PhotonEta.Fill(event, event.PhotonEta.at(0), event.GetWeight());
    WEY_YJETCR_LL_MET.Fill(event, event.MET, event.GetWeight());
    WEY_YJETCR_LL_MT.Fill(event, event.GetMT(), event.GetWeight());
    WEY_YJETCR_LL_LYMass.Fill(event, event.GetLYMass(), event.GetWeight());
    WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Fill(event, event.LeptonEta.at(0) , event.LeptonPt.at(0), event.GetWeight());
  }
  else MSGUser()->MSG_ERROR("Fill WEY YJETCR ChannelEvent : Unfilled WYEventType = ", (int)event.ID);
}

void WYPlotGroup::FillWUYYJETCRChannelEvent(const WYEvent event)
{ 
  if (event.ID == WYEventType::WMUY_YJETCR_LT)
  {
    WUY_YJETCR_LT_LeptonPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
    WUY_YJETCR_LT_LeptonEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
    WUY_YJETCR_LT_PhotonPt.Fill(event, event.PhotonPt.at(0), event.GetWeight());
    WUY_YJETCR_LT_PhotonEta.Fill(event, event.PhotonEta.at(0), event.GetWeight());
    WUY_YJETCR_LT_MET.Fill(event, event.MET, event.GetWeight());
    WUY_YJETCR_LT_MT.Fill(event, event.GetMT(), event.GetWeight());
    WUY_YJETCR_LT_LYMass.Fill(event, event.GetLYMass(), event.GetWeight());
    WUY_YJETCR_LT_MuonEta_MuonPt_2D.Fill(event, event.LeptonEta.at(0) , event.LeptonPt.at(0), event.GetWeight());
  }
  else if (event.ID == WYEventType::WMUY_YJETCR_TL)
  {
    WUY_YJETCR_TL_LeptonPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
    WUY_YJETCR_TL_LeptonEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
    WUY_YJETCR_TL_PhotonPt.Fill(event, event.PhotonPt.at(0), event.GetWeight());
    WUY_YJETCR_TL_PhotonEta.Fill(event, event.PhotonEta.at(0), event.GetWeight());
    WUY_YJETCR_TL_MET.Fill(event, event.MET, event.GetWeight());
    WUY_YJETCR_TL_MT.Fill(event, event.GetMT(), event.GetWeight());
    WUY_YJETCR_TL_LYMass.Fill(event, event.GetLYMass(), event.GetWeight());
    WUY_YJETCR_TL_MuonEta_MuonPt_2D.Fill(event, event.LeptonEta.at(0) , event.LeptonPt.at(0), event.GetWeight());
  }
  else if (event.ID == WYEventType::WMUY_YJETCR_LL)
  {
    WUY_YJETCR_LL_LeptonPt.Fill(event, event.LeptonPt.at(0), event.GetWeight());
    WUY_YJETCR_LL_LeptonEta.Fill(event, event.LeptonEta.at(0), event.GetWeight());
    WUY_YJETCR_LL_PhotonPt.Fill(event, event.PhotonPt.at(0), event.GetWeight());
    WUY_YJETCR_LL_PhotonEta.Fill(event, event.PhotonEta.at(0), event.GetWeight());
    WUY_YJETCR_LL_MET.Fill(event, event.MET, event.GetWeight());
    WUY_YJETCR_LL_MT.Fill(event, event.GetMT(), event.GetWeight());
    WUY_YJETCR_LL_LYMass.Fill(event, event.GetLYMass(), event.GetWeight());
    WUY_YJETCR_LL_MuonEta_MuonPt_2D.Fill(event, event.LeptonEta.at(0) , event.LeptonPt.at(0), event.GetWeight());
  }
  else MSGUser()->MSG_ERROR("Fill WUY YJETCR ChannelEvent : Unfilled WYEventType = ", (int)event.ID);
}

WYBasicPlot1D WYPlotGroup::BookBasicPlot1D(TString name, vector<double> bindivx, int channel)
{
  WYBasicPlot1D basicplot;
  if (channel == -999)
  {
    basicplot.Channel_Map.emplace("WevY", BookPlot1D(TString::Format("%s_%s", "WevY", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("WuvY", BookPlot1D(TString::Format("%s_%s", "WuvY", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("WtvY", BookPlot1D(TString::Format("%s_%s", "WtvY", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("ZY", BookPlot1D(TString::Format("%s_%s", "ZY", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("WPlusJet", BookPlot1D(TString::Format("%s_%s", "WPlusJet", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Zee", BookPlot1D(TString::Format("%s_%s", "Zee", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Zmumu", BookPlot1D(TString::Format("%s_%s", "Zmumu", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Ztautau", BookPlot1D(TString::Format("%s_%s", "Ztautau", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Diboson", BookPlot1D(TString::Format("%s_%s", "Diboson", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Top", BookPlot1D(TString::Format("%s_%s", "Top", name.TString::Data()), bindivx));

    basicplot.Channel_Map.emplace("Wjets", BookPlot1D(TString::Format("%s_%s", "Wjets", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Yjets", BookPlot1D(TString::Format("%s_%s", "Yjets", name.TString::Data()), bindivx));

    basicplot.Channel_Map.emplace("Data", BookPlot1D(TString::Format("%s_%s", "Data", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Signal", BookPlot1D(TString::Format("%s_%s", "Signal", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("BKG", BookPlot1D(TString::Format("%s_%s", "BKG", name.TString::Data()), bindivx));
  }
  else if (channel == -1)
  {
    basicplot.Channel_Map.emplace("Data", BookPlot1D(TString::Format("%s_%s", "Data", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Signal", BookPlot1D(TString::Format("%s_%s", "Signal", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("BKG", BookPlot1D(TString::Format("%s_%s", "BKG", name.TString::Data()), bindivx));
  }
  else if (channel == 1026)
  {
    basicplot.Channel_Map.emplace("Wjets", BookPlot1D(TString::Format("%s_%s", "Wjets", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Yjets", BookPlot1D(TString::Format("%s_%s", "Yjets", name.TString::Data()), bindivx));
  }
  else if (channel > 0)
  {
    basicplot.Channel_Map.emplace(GetChannelName(channel), BookPlot1D(TString::Format("%s_%s", GetChannelName(channel), name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Data", BookPlot1D(TString::Format("%s_%s", "Data", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("Signal", BookPlot1D(TString::Format("%s_%s", "Signal", name.TString::Data()), bindivx));
    basicplot.Channel_Map.emplace("BKG", BookPlot1D(TString::Format("%s_%s", "BKG", name.TString::Data()), bindivx));
  }
  return basicplot;
}

WYBasicPlot2D WYPlotGroup::BookBasicPlot2D(TString name, vector<double> bindivx, vector<double> bindivy, int channel)
{ 
  WYBasicPlot2D basicplot;
  if (channel == -999)
  { 
    basicplot.Channel_Map.emplace("WevY", BookPlot2D(TString::Format("%s_%s", "WevY", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("WuvY", BookPlot2D(TString::Format("%s_%s", "WuvY", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("WtvY", BookPlot2D(TString::Format("%s_%s", "WtvY", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("ZY", BookPlot2D(TString::Format("%s_%s", "ZY", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("WPlusJet", BookPlot2D(TString::Format("%s_%s", "WPlusJet", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Zee", BookPlot2D(TString::Format("%s_%s", "Zee", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Zmumu", BookPlot2D(TString::Format("%s_%s", "Zmumu", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Ztautau", BookPlot2D(TString::Format("%s_%s", "Ztautau", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Diboson", BookPlot2D(TString::Format("%s_%s", "Diboson", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Top", BookPlot2D(TString::Format("%s_%s", "Top", name.TString::Data()), bindivx, bindivy));
    
    basicplot.Channel_Map.emplace("Wjets", BookPlot2D(TString::Format("%s_%s", "Wjets", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Yjets", BookPlot2D(TString::Format("%s_%s", "Yjets", name.TString::Data()), bindivx, bindivy));
    
    basicplot.Channel_Map.emplace("Data", BookPlot2D(TString::Format("%s_%s", "Data", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Signal", BookPlot2D(TString::Format("%s_%s", "Signal", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("BKG", BookPlot2D(TString::Format("%s_%s", "BKG", name.TString::Data()), bindivx, bindivy));
  }
  else if (channel == -1)
  { 
    basicplot.Channel_Map.emplace("Data", BookPlot2D(TString::Format("%s_%s", "Data", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Signal", BookPlot2D(TString::Format("%s_%s", "Signal", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("BKG", BookPlot2D(TString::Format("%s_%s", "BKG", name.TString::Data()), bindivx, bindivy));
  }
  else if (channel == 1026)
  {
    basicplot.Channel_Map.emplace("Wjets", BookPlot2D(TString::Format("%s_%s", "Wjets", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Yjets", BookPlot2D(TString::Format("%s_%s", "Yjets", name.TString::Data()), bindivx, bindivy));
  }
  else if (channel > 0)
  { 
    basicplot.Channel_Map.emplace(GetChannelName(channel), BookPlot2D(TString::Format("%s_%s", GetChannelName(channel), name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Data", BookPlot2D(TString::Format("%s_%s", "Data", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("Signal", BookPlot2D(TString::Format("%s_%s", "Signal", name.TString::Data()), bindivx, bindivy));
    basicplot.Channel_Map.emplace("BKG", BookPlot2D(TString::Format("%s_%s", "BKG", name.TString::Data()), bindivx, bindivy));
  }
  return basicplot;
}                                                                                                                                                                                   

void WYPlotGroup::FillEvent(WYEvent &event)
{
    MSGUser()->MSG_DEBUG("Fill Event @ 0 ID = ", (int)event.ID);
    TString name = event.GetChannelName();
    if (name.TString::EqualTo("UNKNOWN"))
        return;
    ChannelNumber->Fill(event.ChannelNumber, event.GetWeight());

    MSGUser()->MSG_DEBUG("Fill Event @ 1 Weight = ", event.GetWeight());
    if (BASELINE_PASS_WECY)
    {
        if (event.ID == WYEventType::WECY ||
            event.ID == WYEventType::WECY_WJETCR_REWEIGHTED ||
            event.ID == WYEventType::WECY_YJETCR_REWEIGHTED)
        {
            if (event.GetChannelName() == "WPlusJet" ||
                event.GetChannelName() == "Wjets")
            {
                FillWEYWPlusJetChannelEvent(event);
            }
            if (event.LeptonID[0] == LeptonType::ELECTRON)
                FillWminusEYChannelEvent(event);
            if (event.LeptonID[0] == LeptonType::POSITRON)
                FillWplusEYChannelEvent(event);
        }
        else if (event.ID == WYEventType::WECY_WJETCR_LT ||
                 event.ID == WYEventType::WECY_WJETCR_TL ||
                 event.ID == WYEventType::WECY_WJETCR_LL)
        {
            FillWJETCRChannelEvent(event);
        }
        else if (event.ID == WYEventType::WECY_YJETCR_LT ||
                 event.ID == WYEventType::WECY_YJETCR_TL ||
                 event.ID == WYEventType::WECY_YJETCR_LL)
        {
            FillWEYYJETCRChannelEvent(event);
        }
    }

    if (BASELINE_PASS_WMUY)
    {
        if (event.ID == WYEventType::WMUY ||
            event.ID == WYEventType::WMUY_WJETCR_REWEIGHTED ||
            event.ID == WYEventType::WMUY_YJETCR_REWEIGHTED)
        {
            if (event.GetChannelName() == "WPlusJet" ||
                event.GetChannelName() == "Wjets")
            {
                FillWUYWPlusJetChannelEvent(event);
            }
            if (event.LeptonID[0] == LeptonType::MUON)
                FillWminusUYChannelEvent(event);
            if (event.LeptonID[0] == LeptonType::ANTIMUON)
                FillWplusUYChannelEvent(event);
        }
        else if (event.ID == WYEventType::WMUY_WJETCR_LT ||
                 event.ID == WYEventType::WMUY_WJETCR_TL ||
                 event.ID == WYEventType::WMUY_WJETCR_LL)
        {
            FillWJETCRChannelEvent(event);
        }
        else if (event.ID == WYEventType::WMUY_YJETCR_LT ||
                 event.ID == WYEventType::WMUY_YJETCR_TL ||
                 event.ID == WYEventType::WMUY_YJETCR_LL)
        {
            FillWUYYJETCRChannelEvent(event);
        }
    }
    MSGUser()->MSG_DEBUG("Fill Event @ end");
}

TString WYPlotGroup::GetChannelName(int channel)
{
    TString name = "UNKNOWN";
    if (channel == -1)
    {
        name = "Data";
    }
    else if (channel == 1026)
    {
        /*if (ID == WYEventType::WECY_WJETCR_REWEIGHTED ||                   
            ID == WYEventType::WMUY_WJETCR_REWEIGHTED)
            name = "Wjets";
        if (ID == WYEventType::WECY_YJETCR_REWEIGHTED ||
            ID == WYEventType::WMUY_YJETCR_REWEIGHTED)*/
            name = "Yjets";
    }
    else if (channel == 700398 || // Zuu Y 
             channel == 700399 || // Zee Y 
             channel == 700400)   // Ztt Y 
        name = "ZY";
    else if (channel == 700402) // Wuv Y 
        name = "WuvY";
    else if (channel == 700403) // Wev Y
        name = "WevY";
    else if (channel == 700404) // Wtv Y
        name = "WtvY";
    else if (channel == 361100 || // W+ ev
             channel == 361101 || // W+ uv
             channel == 361102 || // W+ tv
             channel == 361103 || // W- ev
             channel == 361104 || // W- uv
             channel == 361105)   // W- tv
        name = "WPlusJet";
    else if (channel == 361106) // Z ee
        name = "Zee";
    else if (channel == 361107) // Z uu
        name = "Zmumu";
    else if (channel == 361108) // Z tt
        name = "Ztautau";
    else if (channel == 364100 ||
             channel == 364101 ||
             channel == 364102 ||
             channel == 364103 ||
             channel == 364104 ||
             channel == 364105)
        name = "Zmumu"; // Sherpa
    else if (channel == 364114 || 
             channel == 364115 || 
             channel == 364116 ||                                     
             channel == 364117 || 
             channel == 364118 || 
             channel == 364119)
        name = "Zee"; // Sherpa
    else if (channel == 364128 || 
             channel == 364129 || 
             channel == 364130 || 
             channel == 364131 || 
             channel == 364132 || 
             channel == 364133) 
        name = "Ztautau"; // Sherpa
    else if (channel == 363356 || // ZqqZll - Sherpa
             channel == 363357 || // WqqZvv - Sherpa
             channel == 363358 || // WqqZll - Sherpa
             channel == 363489 || // WlvZqq - Sherpa
             channel == 364250 || // llll - Sherpa
             channel == 364253 || // lllv - Sherpa
             channel == 364254 || // llvv - Sherpa
             channel == 364255)   // lvvv - Sherpa
        name = "Diboson";
    else if (channel == 410470 || // ttbar
             channel == 410644 || // single-top s-channel lepton top
             channel == 410645 || // single-top s-channel lepton anti-top
             channel == 410646 || // Wt DR inclusive top
             channel == 410647 || // Wt DR inclusive anti-top
             channel == 410658 || // t-channel BW50 lepton top
             channel == 410659)   // t-channel BW50 lepton antitop
        name = "Top";

    return name;
}

void WYPlotGroup::AddLYFakePlots(shared_ptr<WYPlotGroup> LYFakePlotGroup)
{
  TString VarName[2] = {"Wjets", "Yjets"};
  TString VarNameLast[2] = {"Data", "BKG"};
  for (int i = 0; i < 2; i++)
  {
    FabsEtaElectron_WminusLarger.Channel_Map[VarName[i]]->GetNominalHist()->Add(LYFakePlotGroup->FabsEtaElectron_WminusLarger.Channel_Map[VarName[i]]->GetNominalHist());
    FabsEtaPositron_WplusLarger.Channel_Map[VarName[i]]->GetNominalHist()->Add(LYFakePlotGroup->FabsEtaPositron_WplusLarger.Channel_Map[VarName[i]]->GetNominalHist());
    FabsEtaMuon_WminusLarger.Channel_Map[VarName[i]]->GetNominalHist()->Add(LYFakePlotGroup->FabsEtaMuon_WminusLarger.Channel_Map[VarName[i]]->GetNominalHist());
    FabsEtaAntiMuon_WplusLarger.Channel_Map[VarName[i]]->GetNominalHist()->Add(LYFakePlotGroup->FabsEtaAntiMuon_WplusLarger.Channel_Map[VarName[i]]->GetNominalHist());
    FabsEtaElectron_WminusSmaller.Channel_Map[VarName[i]]->GetNominalHist()->Add(LYFakePlotGroup->FabsEtaElectron_WminusSmaller.Channel_Map[VarName[i]]->GetNominalHist());
    FabsEtaPositron_WplusSmaller.Channel_Map[VarName[i]]->GetNominalHist()->Add(LYFakePlotGroup->FabsEtaPositron_WplusSmaller.Channel_Map[VarName[i]]->GetNominalHist());
    FabsEtaMuon_WminusSmaller.Channel_Map[VarName[i]]->GetNominalHist()->Add(LYFakePlotGroup->FabsEtaMuon_WminusSmaller.Channel_Map[VarName[i]]->GetNominalHist());
    FabsEtaAntiMuon_WplusSmaller.Channel_Map[VarName[i]]->GetNominalHist()->Add(LYFakePlotGroup->FabsEtaAntiMuon_WplusSmaller.Channel_Map[VarName[i]]->GetNominalHist());
  }
  for (int i = 0; i < 2; i++)
  {
    LYFakePlotGroup->FabsEtaElectron_WminusLarger.Channel_Map[VarName[i]]->GetNominalHist()->Add(FabsEtaElectron_WminusLarger.Channel_Map[VarName[i]]->GetNominalHist());
    LYFakePlotGroup->FabsEtaPositron_WplusLarger.Channel_Map[VarName[i]]->GetNominalHist()->Add(FabsEtaPositron_WplusLarger.Channel_Map[VarName[i]]->GetNominalHist());
    LYFakePlotGroup->FabsEtaMuon_WminusLarger.Channel_Map[VarName[i]]->GetNominalHist()->Add(FabsEtaMuon_WminusLarger.Channel_Map[VarName[i]]->GetNominalHist());
    LYFakePlotGroup->FabsEtaAntiMuon_WplusLarger.Channel_Map[VarName[i]]->GetNominalHist()->Add(FabsEtaAntiMuon_WplusLarger.Channel_Map[VarName[i]]->GetNominalHist());
    LYFakePlotGroup->FabsEtaElectron_WminusSmaller.Channel_Map[VarName[i]]->GetNominalHist()->Add(FabsEtaElectron_WminusSmaller.Channel_Map[VarName[i]]->GetNominalHist());
    LYFakePlotGroup->FabsEtaPositron_WplusSmaller.Channel_Map[VarName[i]]->GetNominalHist()->Add(FabsEtaPositron_WplusSmaller.Channel_Map[VarName[i]]->GetNominalHist());
    LYFakePlotGroup->FabsEtaMuon_WminusSmaller.Channel_Map[VarName[i]]->GetNominalHist()->Add(FabsEtaMuon_WminusSmaller.Channel_Map[VarName[i]]->GetNominalHist());
    LYFakePlotGroup->FabsEtaAntiMuon_WplusSmaller.Channel_Map[VarName[i]]->GetNominalHist()->Add(FabsEtaAntiMuon_WplusSmaller.Channel_Map[VarName[i]]->GetNominalHist());
  }
  FabsEtaElectron_WminusLarger_WjetsRemoved->SetUnprocessed();
  FabsEtaElectron_WminusLarger_YjetsRemoved->SetUnprocessed();
  FabsEtaElectron_WminusLarger_BKGRemoved->SetUnprocessed();
  FabsEtaElectron_WminusSmaller_WjetsRemoved->SetUnprocessed();
  FabsEtaElectron_WminusSmaller_YjetsRemoved->SetUnprocessed();
  FabsEtaElectron_WminusSmaller_BKGRemoved->SetUnprocessed();
  AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->SetUnprocessed();
  AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->Process();

  FabsEtaPositron_WplusLarger_WjetsRemoved->SetUnprocessed();
  FabsEtaPositron_WplusLarger_YjetsRemoved->SetUnprocessed();
  FabsEtaPositron_WplusLarger_BKGRemoved->SetUnprocessed();
  FabsEtaPositron_WplusSmaller_WjetsRemoved->SetUnprocessed();
  FabsEtaPositron_WplusSmaller_YjetsRemoved->SetUnprocessed();
  FabsEtaPositron_WplusSmaller_BKGRemoved->SetUnprocessed();
  AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->SetUnprocessed();
  AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->Process();
  
  FabsEtaMuon_WminusLarger_WjetsRemoved->SetUnprocessed();
  FabsEtaMuon_WminusLarger_YjetsRemoved->SetUnprocessed();
  FabsEtaMuon_WminusLarger_BKGRemoved->SetUnprocessed();
  FabsEtaMuon_WminusSmaller_WjetsRemoved->SetUnprocessed();
  FabsEtaMuon_WminusSmaller_YjetsRemoved->SetUnprocessed();
  FabsEtaMuon_WminusSmaller_BKGRemoved->SetUnprocessed();
  AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->SetUnprocessed();
  AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->Process();
  
  FabsEtaAntiMuon_WplusLarger_WjetsRemoved->SetUnprocessed();
  FabsEtaAntiMuon_WplusLarger_YjetsRemoved->SetUnprocessed();
  FabsEtaAntiMuon_WplusLarger_BKGRemoved->SetUnprocessed();
  FabsEtaAntiMuon_WplusSmaller_WjetsRemoved->SetUnprocessed();
  FabsEtaAntiMuon_WplusSmaller_YjetsRemoved->SetUnprocessed();
  FabsEtaAntiMuon_WplusSmaller_BKGRemoved->SetUnprocessed();
  AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->SetUnprocessed();
  AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->Process();
  
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusSmaller_WjetsRemoved->GetBinContent(3) = ", FabsEtaElectron_WminusSmaller_WjetsRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusSmaller_WjetsRemoved->GetBinError(3) = ", FabsEtaElectron_WminusSmaller_WjetsRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusLarger_WjetsRemoved->GetBinContent(3) = ", FabsEtaElectron_WminusLarger_WjetsRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusLarger_WjetsRemoved->GetBinError(3) = ", FabsEtaElectron_WminusLarger_WjetsRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusLarger_YjetsRemoved->GetBinContent(3) = ", FabsEtaElectron_WminusLarger_YjetsRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusLarger_YjetsRemoved->GetBinError(3) = ", FabsEtaElectron_WminusLarger_YjetsRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusSmaller_YjetsRemoved->GetBinContent(3) = ", FabsEtaElectron_WminusSmaller_YjetsRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusSmaller_YjetsRemoved->GetBinError(3) = ", FabsEtaElectron_WminusSmaller_YjetsRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusLarger_BKGRemoved->GetBinContent(3) = ", FabsEtaElectron_WminusLarger_BKGRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusLarger_BKGRemoved->GetBinError(3) = ", FabsEtaElectron_WminusLarger_BKGRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusSmaller_BKGRemoved->GetBinContent(3) = ", FabsEtaElectron_WminusSmaller_BKGRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaElectron_WminusSmaller_BKGRemoved->GetBinError(3) = ", FabsEtaElectron_WminusSmaller_BKGRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("AsymmetryFabsEtaElectron_Wminus.Channel_Map[Data]->GetBinContent(3) = ", AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("AsymmetryFabsEtaElectron_Wminus.Channel_Map[Data]->GetBinError(3) = ", AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->GetNominalHist()->GetBinError(3));
  
  MSGUser()->MSG_INFO("FabsEtaPositron_WplusLarger_BKGRemoved->GetBinContent(3) = ", FabsEtaPositron_WplusLarger_BKGRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaPositron_WplusLarger_BKGRemoved->GetBinError(3) = ", FabsEtaPositron_WplusLarger_BKGRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaPositron_WplusSmaller_BKGRemoved->GetBinContent(3) = ", FabsEtaPositron_WplusSmaller_BKGRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaPositron_WplusSmaller_BKGRemoved->GetBinError(3) = ", FabsEtaPositron_WplusSmaller_BKGRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("AsymmetryFabsEtaPositron_Wplus.Channel_Map[Data]->GetBinContent(3) = ", AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("AsymmetryFabsEtaPositron_Wplus.Channel_Map[Data]->GetBinError(3) = ", AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->GetNominalHist()->GetBinError(3));
  
  MSGUser()->MSG_INFO("FabsEtaMuon_WminusLarger_BKGRemoved->GetBinContent(3) = ", FabsEtaMuon_WminusLarger_BKGRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaMuon_WminusLarger_BKGRemoved->GetBinError(3) = ", FabsEtaMuon_WminusLarger_BKGRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaMuon_WminusSmaller_BKGRemoved->GetBinContent(3) = ", FabsEtaMuon_WminusSmaller_BKGRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaMuon_WminusSmaller_BKGRemoved->GetBinError(3) = ", FabsEtaMuon_WminusSmaller_BKGRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("AsymmetryFabsEtaMuon_Wminus.Channel_Map[Data]->GetBinContent(3) = ", AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("AsymmetryFabsEtaMuon_Wminus.Channel_Map[Data]->GetBinError(3) = ", AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->GetNominalHist()->GetBinError(3));
  
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusLarger_WjetsRemoved->GetBinContent(3) = ", FabsEtaAntiMuon_WplusLarger_WjetsRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusLarger_WjetsRemoved->GetBinError(3) = ", FabsEtaAntiMuon_WplusLarger_WjetsRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusSmaller_WjetsRemoved->GetBinContent(3) = ", FabsEtaAntiMuon_WplusSmaller_WjetsRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusSmaller_WjetsRemoved->GetBinError(3) = ", FabsEtaAntiMuon_WplusSmaller_WjetsRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusLarger_YjetsRemoved->GetBinContent(3) = ", FabsEtaAntiMuon_WplusLarger_YjetsRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusLarger_YjetsRemoved->GetBinError(3) = ", FabsEtaAntiMuon_WplusLarger_YjetsRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusSmaller_YjetsRemoved->GetBinContent(3) = ", FabsEtaAntiMuon_WplusSmaller_YjetsRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusSmaller_YjetsRemoved->GetBinError(3) = ", FabsEtaAntiMuon_WplusSmaller_YjetsRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusLarger_BKGRemoved->GetBinContent(3) = ", FabsEtaAntiMuon_WplusLarger_BKGRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusLarger_BKGRemoved->GetBinError(3) = ", FabsEtaAntiMuon_WplusLarger_BKGRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusSmaller_BKGRemoved->GetBinContent(3) = ", FabsEtaAntiMuon_WplusSmaller_BKGRemoved->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("FabsEtaAntiMuon_WplusSmaller_BKGRemoved->GetBinError(3) = ", FabsEtaAntiMuon_WplusSmaller_BKGRemoved->GetNominalHist()->GetBinError(3));
  MSGUser()->MSG_INFO("AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map[Data]->GetBinContent(3) = ", AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->GetNominalHist()->GetBinContent(3));
  MSGUser()->MSG_INFO("AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map[Data]->GetBinError(3) = ", AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->GetNominalHist()->GetBinError(3));
}

void WYPlotGroup::ResetWPlusJetMCSF(shared_ptr<WYPlotGroup> LYFakePlotGroup)
{
  LYFakePlotGroup->WEY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["WPlusJet"]->Combine(WEY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["WPlusJet"]);
  LYFakePlotGroup->WUY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["WPlusJet"]->Combine(WUY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["WPlusJet"]);
  WEY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["Wjets"]->Combine(LYFakePlotGroup->WEY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["Wjets"]);
  WUY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["Wjets"]->Combine(LYFakePlotGroup->WUY_WJETCR_TT_PhotonEta_PhotonPt_2D.Channel_Map["Wjets"]);
  WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->SetUnprocessed();
  WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->Process();
  WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->SetUnprocessed();
  WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->Process();
  LYFakePlotGroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->Scale(0);
  LYFakePlotGroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->Combine(WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D);
  LYFakePlotGroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->Scale(0);
  LYFakePlotGroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->Combine(WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D);
} 

void WYPlotGroup::ResetYJETCRSF(shared_ptr<WYPlotGroup> LYFakePlotGroup)
{
  LYFakePlotGroup->WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Data"]->Combine(WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Data"]);
  LYFakePlotGroup->WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]->Combine(WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]);
  LYFakePlotGroup->WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]->Combine(WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]);
  LYFakePlotGroup->WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Data"]->Combine(WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Data"]);
  LYFakePlotGroup->WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]->Combine(WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]);
  LYFakePlotGroup->WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]->Combine(WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]);
  LYFakePlotGroup->WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Data"]->Combine(WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Data"]);
  LYFakePlotGroup->WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]->Combine(WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Signal"]);
  LYFakePlotGroup->WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]->Combine(WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["BKG"]);
  LYFakePlotGroup->WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Data"]->Combine(WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Data"]);
  LYFakePlotGroup->WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Signal"]->Combine(WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Signal"]);
  LYFakePlotGroup->WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["BKG"]->Combine(WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["BKG"]);
  LYFakePlotGroup->WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Data"]->Combine(WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Data"]);
  LYFakePlotGroup->WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Signal"]->Combine(WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Signal"]);
  LYFakePlotGroup->WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["BKG"]->Combine(WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["BKG"]);
  LYFakePlotGroup->WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Data"]->Combine(WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Data"]);
  LYFakePlotGroup->WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Signal"]->Combine(WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Signal"]);
  LYFakePlotGroup->WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["BKG"]->Combine(WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["BKG"]);
  
  WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]->Combine(LYFakePlotGroup->WEY_YJETCR_LT_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]);
  WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]->Combine(LYFakePlotGroup->WEY_YJETCR_TL_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]);
  WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]->Combine(LYFakePlotGroup->WEY_YJETCR_LL_ElectronEta_ElectronPt_2D.Channel_Map["Wjets"]);
  WEY_YJETCR_LT_ElectronEta_ElectronPt_WjetsRemoved_2D->SetUnprocessed();
  WEY_YJETCR_LT_ElectronEta_ElectronPt_WjetsRemoved_2D->Process();
  WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D->SetUnprocessed();
  WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D->Process();
  WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D->SetUnprocessed();
  WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D->Process();
  WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->SetUnprocessed();
  WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->Process();
  
  WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Wjets"]->Combine(LYFakePlotGroup->WUY_YJETCR_LT_MuonEta_MuonPt_2D.Channel_Map["Wjets"]);
  WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Wjets"]->Combine(LYFakePlotGroup->WUY_YJETCR_TL_MuonEta_MuonPt_2D.Channel_Map["Wjets"]);
  WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Wjets"]->Combine(LYFakePlotGroup->WUY_YJETCR_LL_MuonEta_MuonPt_2D.Channel_Map["Wjets"]);
  WUY_YJETCR_LT_MuonEta_MuonPt_WjetsRemoved_2D->SetUnprocessed();
  WUY_YJETCR_LT_MuonEta_MuonPt_WjetsRemoved_2D->Process();
  WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D->SetUnprocessed();
  WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D->Process();
  WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D->SetUnprocessed();
  WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D->Process();
  WUY_YJETCR_SF_MuonEta_MuonPt_2D->SetUnprocessed();
  WUY_YJETCR_SF_MuonEta_MuonPt_2D->Process();
}

void WYPlotGroup::RescaleYjets(double scaleWEY, double scaleWUY)
{
  ElectronPt.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  ElectronEta.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  PhotonPt_WminusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  PhotonEta_WminusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  LYMass_WminusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  LYDeltaPhi_WminusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  LYDeltaR_WminusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  MET_WminusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  MT_WminusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  FabsEtaElectron_WminusLarger.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  FabsEtaElectron_WminusSmaller.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);

  PositronPt.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  PositronEta.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  PhotonPt_WplusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  PhotonEta_WplusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  LYMass_WplusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  LYDeltaPhi_WplusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  LYDeltaR_WplusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  MET_WplusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  MT_WplusEY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  FabsEtaPositron_WplusLarger.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);
  FabsEtaPositron_WplusSmaller.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWEY);

  MuonPt.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  MuonEta.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  PhotonPt_WminusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  PhotonEta_WminusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  LYMass_WminusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  LYDeltaPhi_WminusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  LYDeltaR_WminusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  MET_WminusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  MT_WminusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  FabsEtaMuon_WminusLarger.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  FabsEtaMuon_WminusSmaller.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);

  AntiMuonPt.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  AntiMuonEta.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  PhotonPt_WplusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  PhotonEta_WplusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  LYMass_WplusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  LYDeltaPhi_WplusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  LYDeltaR_WplusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  MET_WplusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  MT_WplusUY.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  FabsEtaAntiMuon_WplusLarger.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
  FabsEtaAntiMuon_WplusSmaller.Channel_Map["Yjets"]->GetNominalHist()->Scale(scaleWUY);
}

#endif

