#include "WYLoopEvent.h"
#include "WYEvent.h"
#include "MCSetsInfo.h"
#include "TTreeCache.h"

using namespace NAGASH;
using namespace std;

NAGASH::StatusCode WYLoopEvent::InitializeNomalization()
{
   if (InputRootFileName().Contains("MC16"))
   {
      string MCINFO_FILENAME;
      MCINFO_FILENAME = ConfigUser()->GetPar<TString>("MCINFO_FILENAME");
      MCSetsInfo info(MSGUser(), MCINFO_FILENAME);
      ChannelNumber_FromFile = info.GetChannelNumber(InputRootFileName().Data());
      std::ifstream infile;
      if (InputRootFileName().Contains("MC16a"))
      {
         infile.open(ConfigUser()->GetPar<TString>("SUMOFWEIGHTS_MC16a_FILENAME"));
      }
      else if (InputRootFileName().Contains("MC16d"))
         infile.open(ConfigUser()->GetPar<TString>("SUMOFWEIGHTS_MC16d_FILENAME"));
      else if (InputRootFileName().Contains("MC16e"))
         infile.open(ConfigUser()->GetPar<TString>("SUMOFWEIGHTS_MC16e_FILENAME"));
      int channelnumber;
      double sumw;
      while (!infile.eof())
      {
         infile >> channelnumber >> sumw;
         if (channelnumber == ChannelNumber_FromFile)
         {
            SumOfWeights_FromFile = sumw;
            MSGUser()->MSG_DEBUG("ChannelNumber_FromFile = ", ChannelNumber_FromFile, " channelnumber = ", channelnumber, " sumw = ", sumw);
         }
      }

      double XSection = info.GetXsecPb(InputRootFileName().Data());
      double Lumi;
      Lumi = 32988.1 + 3219.56 + 43587.3 + 58450.1; // ZHZH For Run2
      /*if (InputRootFileName().Contains("MC16a"))
         Lumi = 32988.1 + 3219.56;
      else if (InputRootFileName().Contains("MC16d"))
         Lumi = 43587.3;
      else if (InputRootFileName().Contains("MC16e"))
         Lumi = 58450.1;*/

      NormalizationFactor = XSection * Lumi / SumOfWeights_FromFile;
      MSGUser()->MSG_INFO("ChannelNumber  = ", ChannelNumber_FromFile, " XSection = ", XSection, " SumOfWeights = ", SumOfWeights_FromFile, " NormalizationFactor = ", NormalizationFactor);
   }
   else
   {
      NormalizationFactor = 1;
      ChannelNumber_FromFile = -1;
   }
   return NAGASH::StatusCode::SUCCESS;
}

NAGASH::StatusCode WYLoopEvent::InitializeUser()
{
   InitializeParameters();
   InitializeNomalization();

   MyDataSet = ResultGroupUser()->BookResult<WYDataSet>("AllEvents");
   MyPlots = ResultGroupUser()->BookResult<WYPlotGroup>("WYPlots", ConfigUser()->GetPar<TString>("FILLPLOTS_IN_EVENTLOOP_FILENAME"));
   //MyPlots = ResultGroupUser()->BookResult<WYPlotGroup>("WYPlots", ConfigUser()->GetPar<TString>("FILLPLOTS_IN_EVENTLOOP_FILENAME"), ChannelNumber_FromFile);
   
   DefaultEvent.Reserve();
   return NAGASH::StatusCode::SUCCESS;
}

NAGASH::StatusCode WYLoopEvent::InitializeParameters()
{
   MUON_BASELINE_PTMIN = ConfigUser()->GetPar<double>("MUON_BASELINE_PTMIN");
   MUON_BASELINE_FABS_ETAMAX = ConfigUser()->GetPar<double>("MUON_BASELINE_FABS_ETAMAX");
   MUON_BASELINE_FABS_DZ0MAX = ConfigUser()->GetPar<double>("MUON_BASELINE_FABS_DZ0MAX");
   MUON_BASELINE_FABS_D0SIGMAX = ConfigUser()->GetPar<double>("MUON_BASELINE_FABS_D0SIGMAX");
   MUON_BASELINE_ISOMAX = ConfigUser()->GetPar<double>("MUON_BASELINE_ISOMAX");

   ELEC_BASELINE_PTMIN = ConfigUser()->GetPar<double>("ELEC_BASELINE_PTMIN");
   ELEC_BASELINE_FABS_ETAMAX = ConfigUser()->GetPar<double>("ELEC_BASELINE_FABS_ETAMAX");
   ELEC_BASELINE_CRACK_FABS_ETAMAX = ConfigUser()->GetPar<double>("ELEC_BASELINE_CRACK_FABS_ETAMAX");
   ELEC_BASELINE_CRACK_FABS_ETAMIN = ConfigUser()->GetPar<double>("ELEC_BASELINE_CRACK_FABS_ETAMIN");
   ELEC_BASELINE_FABS_DZ0MAX = ConfigUser()->GetPar<double>("ELEC_BASELINE_FABS_DZ0MAX");
   ELEC_BASELINE_FABS_D0SIGMAX = ConfigUser()->GetPar<double>("ELEC_BASELINE_FABS_D0SIGMAX");

   FWDELEC_BASELINE_PTMIN = ConfigUser()->GetPar<double>("FWDELEC_BASELINE_PTMIN");
   FWDELEC_BASELINE_FABS_ETAMAX = ConfigUser()->GetPar<double>("FWDELEC_BASELINE_FABS_ETAMAX");
   FWDELEC_BASELINE_FABS_ETAMIN = ConfigUser()->GetPar<double>("FWDELEC_BASELINE_FABS_ETAMIN");
   FWDELEC_BASELINE_CRACK_FABS_ETAMAX = ConfigUser()->GetPar<double>("FWDELEC_BASELINE_CRACK_FABS_ETAMAX");
   FWDELEC_BASELINE_CRACK_FABS_ETAMIN = ConfigUser()->GetPar<double>("FWDELEC_BASELINE_CRACK_FABS_ETAMIN");
  
   JET_BASELINE_FABS_ETAMAX = ConfigUser()->GetPar<double>("JET_BASELINE_FABS_ETAMAX");
   JET_BASELINE_PTMIN = ConfigUser()->GetPar<double>("JET_BASELINE_PTMIN");
   JET_BASELINE_OVERLEP = ConfigUser()->GetPar<double>("JET_BASELINE_OVERLEP");
   
   PHOTON_BASELINE_PTMIN = ConfigUser()->GetPar<double>("PHOTON_BASELINE_PTMIN");
   PHOTON_BASELINE_FABS_ETAMAX = ConfigUser()->GetPar<double>("PHOTON_BASELINE_FABS_ETAMAX");
   PHOTON_BASELINE_CRACK_FABS_ETAMAX = ConfigUser()->GetPar<double>("PHOTON_BASELINE_CRACK_FABS_ETAMAX");
   PHOTON_BASELINE_CRACK_FABS_ETAMIN = ConfigUser()->GetPar<double>("PHOTON_BASELINE_CRACK_FABS_ETAMIN");

   WECY_BASELINE_ZVETO_DELTAMASSMIN = ConfigUser()->GetPar<double>("WECY_BASELINE_ZVETO_DELTAMASSMIN");
   WMUY_BASELINE_ZVETO_DELTAMASSMIN = ConfigUser()->GetPar<double>("WMUY_BASELINE_ZVETO_DELTAMASSMIN");
   WECY_BASELINE_ELEC_PTMIN = ConfigUser()->GetPar<double>("WECY_BASELINE_ELEC_PTMIN");
   WMUY_BASELINE_MUON_PTMIN = ConfigUser()->GetPar<double>("WMUY_BASELINE_MUON_PTMIN");
   WY_BASELINE_METMIN = ConfigUser()->GetPar<double>("WY_BASELINE_METMIN");
   WY_BASELINE_MWTMIN = ConfigUser()->GetPar<double>("WY_BASELINE_MWTMIN");
   WY_BASELINE_LY_DELTARMIN = ConfigUser()->GetPar<double>("WY_BASELINE_LY_DELTARMIN");

   BASELINE_PASS_WECY = ConfigUser()->GetPar<bool>("BASELINE_PASS_WECY");
   BASELINE_PASS_WMUY = ConfigUser()->GetPar<bool>("BASELINE_PASS_WMUY");
   
   EstimateYjetsMode = ConfigUser()->GetPar<int>("EstimateYjetsMode");

   return NAGASH::StatusCode::SUCCESS;
}

bool WYLoopEvent::PreSelection()
{
   MSGUser()->MSG_DEBUG("PassTrigger: ", PassTrigger,
                        " TriggerMatched: ", TriggerMatched,
                        " WithBadJet: ", WithBadJet,
                        " NPhotons: ", NPhotons,
                        " NElectrons + NMuons:", NElectrons + NMuons);
   if (!PassTrigger)
      return false;
   // if (!TriggerMatched)
   //    return false;
   if (WithBadJet)
      return false;
   if (NElectrons + NMuons < 1 || NPhotons < 1)
      return false;
   MSGUser()->MSG_DEBUG("Pass Preselection!");

   return true;
}

void WYLoopEvent::CountParticleNumbers()
{
   ElectronCount += NElectronsLast;
   ForwardElectronCount += NForwardElectronsLast;
   MuonCount += NMuonsLast;
   JetCount += NJetsLast;
   PhotonCount += NPhotonsLast;
   TruthJetCount += NTruthJetsLast;
   TruthParticleCount += NTruthParticlesLast;

   NElectronsLast = NElectrons;
   NForwardElectronsLast = NForwardElectrons;
   NMuonsLast = NMuons;
   NJetsLast = NJets;
   NPhotonsLast = NPhotons;
   NTruthJetsLast = NTruthJets;
   NTruthParticlesLast = NTruthParticles;

   RetrieveEventInfo();
}

void WYLoopEvent::RebuildFullEvent()
{
   DefaultEvent.Clean();

   DefaultEvent.WithBadJet = WithBadJet;
   DefaultEvent.ChannelNumber = ChannelNumber_FromFile;

   if (ChannelNumber_FromFile != -1)
   {
      // Check Channel Number
      if (ChannelNumber != ChannelNumber_FromFile)
         MSGUser()->MSG_ERROR("ChannelNumber identified from Filename is not consistent with the one stored in the EventInfo_Tree!(", ChannelNumber, ",", ChannelNumber_FromFile, ")");
   }
   DefaultEvent.MET = MET_MET;
   DefaultEvent.MET_Phi = MET_Phi;
   DefaultEvent.mc_EventWeight = mc_evtwt * NormalizationFactor;
   DefaultEvent.mc_PileupWeight = mc_pileupwt;

   for (int i = 0; i < NTruthParticles; i++)
   {
      RetrieveParticleTruth(i);
      DefaultEvent.Truth_ID->emplace_back(Truth_ID);
      DefaultEvent.Truth_Status->emplace_back(Truth_Status);
      DefaultEvent.Truth_MotherID->emplace_back(Truth_MotherID);
      DefaultEvent.Truth_Px->emplace_back(Truth_Px);
      DefaultEvent.Truth_Py->emplace_back(Truth_Py);
      DefaultEvent.Truth_Pz->emplace_back(Truth_Pz);
      DefaultEvent.Truth_E->emplace_back(Truth_E);
   }

   for (int i = 0; i < NElectrons; i++)
   {
      RetrieveElectron(i);
      DefaultEvent.Electron_Pt->emplace_back(Electron_Calibrated_Pt);
      DefaultEvent.Electron_Eta->emplace_back(Electron_Eta);
      DefaultEvent.Electron_Phi->emplace_back(Electron_Phi);
      DefaultEvent.Electron_Charge->emplace_back(Electron_Charge);
      DefaultEvent.Electron_PassIDLoose->emplace_back(Electron_PassIDLoose);
      DefaultEvent.Electron_PassID->emplace_back(Electron_PassIDTight);
      DefaultEvent.Electron_Track_d0sig->emplace_back(Electron_Track_d0sig);
      DefaultEvent.Electron_Track_dz0->emplace_back(Electron_Track_dz0);
      DefaultEvent.Electron_MatchedTrigger->emplace_back(Electron_MatchedTrigger);
      
      RetrieveElectronSF(i);
      DefaultEvent.mc_Electron_RecoSF->emplace_back(mc_Electron_RecoSF);
      DefaultEvent.mc_Electron_IDSF->emplace_back(mc_Electron_TightIDSF);
      //DefaultEvent.mc_Electron_IDSFLoose->emplace_back(mc_Electron_LooseIDSF);
      DefaultEvent.Electron_PassIso->emplace_back(Electron_PassIso_FCTight); 
      DefaultEvent.mc_Electron_IsoSF->emplace_back(mc_Electron_TightID_FCTightIsoSF);
      DefaultEvent.mc_Electron_TriggerSF->emplace_back(mc_Electron_TightID_FCTightIso_TriggerSF);
      DefaultEvent.mc_Electron_TriggerEff->emplace_back(mc_Electron_TightID_FCTightIso_TriggerEff);
   }

   for (int i = 0; i < NForwardElectrons; i++)
   {
      RetrieveForwardElectron(i);
      DefaultEvent.ForwardElectron_Pt->emplace_back(ForwardElectron_Calibrated_Pt);
      DefaultEvent.ForwardElectron_Eta->emplace_back(ForwardElectron_Eta);
      DefaultEvent.ForwardElectron_Phi->emplace_back(ForwardElectron_Phi);
      DefaultEvent.ForwardElectron_PassID->emplace_back(ForwardElectron_PassIDTight);
      DefaultEvent.ForwardElectron_PassIDLoose->emplace_back(ForwardElectron_PassIDLoose);

      RetrieveForwardElectronSF(i);
      DefaultEvent.mc_ForwardElectron_IDSF->emplace_back(mc_ForwardElectron_TightIDSF);
   }

   for (int i = 0; i < NMuons; i++)
   {
      RetrieveMuon(i);
      DefaultEvent.Muon_Original_Pt->emplace_back(Muon_Original_Pt);
      DefaultEvent.Muon_Pt->emplace_back(Muon_Original_Pt);
      DefaultEvent.Muon_Eta->emplace_back(Muon_Eta);
      DefaultEvent.Muon_Phi->emplace_back(Muon_Phi);
      DefaultEvent.Muon_Charge->emplace_back(Muon_Charge);
      DefaultEvent.Muon_PassID->emplace_back(Muon_PassIDTight);
      DefaultEvent.Muon_PassIDLoose->emplace_back(Muon_PassIDLoose);
      DefaultEvent.Muon_Track_dz0->emplace_back(Muon_Track_dz0);
      DefaultEvent.Muon_Track_d0sig->emplace_back(Muon_Track_d0Significance);
      DefaultEvent.Muon_MatchedTrigger->emplace_back(Muon_MatchedTrigger);
      DefaultEvent.Muon_PassIso->emplace_back(Muon_PassIso_FCTight);
      
      RetrieveMuonIsolation(i);
      DefaultEvent.Muon_ptcone30->emplace_back(Muon_Isolation_ptcone30);

      RetrieveMuonSF(i);
      DefaultEvent.mc_Muon_IsoSF->emplace_back(mc_Muon_FCTightIsoSF);
      DefaultEvent.mc_Muon_RecoSF->emplace_back(mc_Muon_TightRecoSF);
      DefaultEvent.mc_Muon_TriggerSF->emplace_back(mc_Muon_RecoTight_TriggerSF);
      DefaultEvent.mc_Muon_TriggerEff->emplace_back(mc_Muon_RecoTight_TriggerEff);
      DefaultEvent.mc_Muon_TTVASF->emplace_back(mc_Muon_TTVASF);
   }
   
   for (int i = 0; i < NJets; i++)
   {
      RetrieveJet(i);
      DefaultEvent.Jet_Pt->emplace_back(Jet_Calibrated_Pt);
      DefaultEvent.Jet_Eta->emplace_back(Jet_Eta);
      DefaultEvent.Jet_Phi->emplace_back(Jet_Phi);
      DefaultEvent.Jet_M->emplace_back(Jet_M);
      DefaultEvent.Jet_PassJvt->emplace_back(Jet_PassJvt);
      DefaultEvent.Jet_isBad->emplace_back(Jet_isBad);

      RetrieveJetSF(i);
      DefaultEvent.Jet_JvtSF->emplace_back(Jet_JvtSF);
   }
   
   for (int i = 0; i < NPhotons; i++)
   {
      RetrievePhoton(i);
      DefaultEvent.Photon_Pt->emplace_back(Photon_Calibrated_Pt);
      DefaultEvent.Photon_Eta->emplace_back(Photon_Eta);
      DefaultEvent.Photon_Phi->emplace_back(Photon_Phi);
      DefaultEvent.Photon_PassCleaning->emplace_back(Photon_PassCleaning);
      DefaultEvent.Photon_PassID->emplace_back(Photon_PassIDTight);
      DefaultEvent.Photon_PassIDLoose->emplace_back(Photon_PassIDLoose);
      DefaultEvent.Photon_PassIso->emplace_back(Photon_PassIso_FCTight);

      RetrievePhotonSF(i);
      DefaultEvent.mc_Photon_IDSF->emplace_back(mc_Photon_TightIDSF);
      DefaultEvent.mc_Photon_IsoSF->emplace_back(mc_Photon_FCTightIsoSF);
   }
}

NAGASH::StatusCode WYLoopEvent::Execute()
{
   // In this function, User do what they want for each entry in the TTree
   CountParticleNumbers();
   if (IsUselessMC(ChannelNumber))
      return NAGASH::StatusCode::SUCCESS;
   if (!PreSelection())
      return NAGASH::StatusCode::SUCCESS;
   RebuildFullEvent();

   //DefaultEvent.COut();
   WYEvent SelectedEvent;
   //MSGUser()->MSG_DEBUG("Ex @ 2");
   bool isSelected = WYLoopEvent::BaseLineSelection(DefaultEvent, SelectedEvent);
   //MSGUser()->MSG_DEBUG("Ex @ 3");
   if (isSelected)
   {
      //MSGUser()->MSG_INFO("Ex @ ID = ", (int)SelectedEvent.ID);
      //MSGUser()->MSG_INFO("SelectedEvent");
      /*cout << " " << endl;
      if (SelectedEvent.ID == WYEventType::WECY ||
          SelectedEvent.ID == WYEventType::WMUY)
      {
         if (SelectedEvent.LeptonID[0] == LeptonType::ELECTRON) cout << " ELECTRON " << endl;
         else if (SelectedEvent.LeptonID[0] == LeptonType::POSITRON) cout << " POSITRON +++++++++++" << endl;
         else if (SelectedEvent.LeptonID[0] == LeptonType::MUON) cout << " MUON " << endl;
         else if (SelectedEvent.LeptonID[0] == LeptonType::ANTIMUON) cout << " ANTIMUON +++++++++++" << endl;
         else cout << " Lepton ID " << (int)SelectedEvent.LeptonID[0] << endl;
         cout << " " << endl;
         SelectedEvent.COut();
         cout << " " << endl;
         DefaultEvent.COut();
      }
      cout << " " << endl;
      */
      if ((SelectedEvent.ID == WYEventType::WECY_WJETCR_TL ||
           SelectedEvent.ID == WYEventType::WMUY_WJETCR_TL)&&
           SelectedEvent.GetChannelName() != "UNKNOWN")
      {
         MyDataSet->PushBack(SelectedEvent);
      }
      if (EstimateYjetsMode == 1)
      {
         if (SelectedEvent.ID == WYEventType::WECY_YJETCR_LT ||
             SelectedEvent.ID == WYEventType::WMUY_YJETCR_LT ||
           ((SelectedEvent.ID == WYEventType::WECY_YJETCR_TL || 
             SelectedEvent.ID == WYEventType::WMUY_YJETCR_TL ||
             SelectedEvent.ID == WYEventType::WECY_YJETCR_LL ||
             SelectedEvent.ID == WYEventType::WMUY_YJETCR_LL) &&
             SelectedEvent.GetChannelName() == "WPlusJet"))
         {
            MyDataSet->PushBack(SelectedEvent);
         }
      }
      else if (EstimateYjetsMode == 2)
      {
         if (SelectedEvent.ID == WYEventType::WECY_YJETCR_LL ||
             SelectedEvent.ID == WYEventType::WMUY_YJETCR_LL)
         {
            MyDataSet->PushBack(SelectedEvent);
         }
      }
      MyPlots->FillEvent(SelectedEvent);
   }

   return NAGASH::StatusCode::SUCCESS;
}

bool WYLoopEvent::BaseLineSelection(WYFullEvent fullevent, WYEvent &event)
{
   MSGUser()->MSG_DEBUG("BaseLineSelection : @ 0");
   event.ChannelNumber = fullevent.ChannelNumber;
   event.MET = fullevent.MET;
   event.METPhi = fullevent.MET_Phi;

   vector<int> iGoodElectron;
   vector<int> iGoodPositron;
   vector<int> iGoodMuon;
   vector<int> iGoodAntiMuon;
   vector<int> iGoodFwd;
   vector<int> iGoodJet;
   vector<int> iGoodPhoton;
   vector<int> iQuark;
   for (auto i = 0; i < fullevent.Muon_Eta->size(); i++)
   {
      if (MuonBaseLineSelection(fullevent.Muon_Pt->at(i),
                                fullevent.Muon_Eta->at(i),
                                fullevent.Muon_Track_dz0->at(i),
                                fullevent.Muon_Track_d0sig->at(i),
                                fullevent.Muon_PassIDLoose->at(i)) == false)
         continue;
      if (fullevent.Muon_Charge->at(i) < 0)
         iGoodMuon.push_back(i);
      if (fullevent.Muon_Charge->at(i) > 0)
         iGoodAntiMuon.push_back(i);
      //MSGUser()->MSG_INFO("GoodMuon = ", iGoodMuon.size(), " GoodAntiMuon = ", iGoodAntiMuon.size());  
   }
   for (int i = 0; i < fullevent.Electron_Eta->size(); i++)
   {
      if (ElectronBaseLineSelection(fullevent.Electron_Pt->at(i),
                                    fullevent.Electron_Eta->at(i),
                                    fullevent.Electron_Track_dz0->at(i),
                                    fullevent.Electron_Track_d0sig->at(i),
                                    fullevent.Electron_PassIDLoose->at(i)) == false)
         continue;

      if (fullevent.Electron_Charge->at(i) < 0)
         iGoodElectron.push_back(i);
      if (fullevent.Electron_Charge->at(i) > 0)
         iGoodPositron.push_back(i);
   }
   for (int i = 0; i < fullevent.ForwardElectron_Eta->size(); i++)
   {
      if (ForwardElectronBaseLineSelection(fullevent.ForwardElectron_Pt->at(i),
                                           fullevent.ForwardElectron_Eta->at(i)) == false)
         continue;
      iGoodFwd.push_back(i);
   }
   /*int iFwdPtLeading = -1;
   if (iGoodFwd.size() > 0)
   {
      double ptmax = -1;
      for (int i = 0; i < iGoodFwd.size(); i++)
      {
         if (ptmax < fullevent.ForwardElectron_Pt->at(iGoodFwd[i]))
         {
            ptmax = fullevent.ForwardElectron_Pt->at(iGoodFwd[i]);
            iFwdPtLeading = iGoodFwd[i];
         }
      }
   }*/
   for (int i = 0; i < fullevent.Jet_Pt->size(); i++)
   {
      if (JetBaseLineSelection(fullevent.Jet_Pt->at(i),
                               fullevent.Jet_Eta->at(i),
                               fullevent.Jet_PassJvt->at(i)) == false)
         continue;
      iGoodJet.push_back(i);
   }
   int iLeadingJet = -1;
   if (iGoodJet.size() > 0)
   {
      iLeadingJet = iGoodJet[0];
      for (int i = 0; i < iGoodJet.size(); i++)
      {
         if (fullevent.Jet_Pt->at(iGoodJet[i]) > fullevent.Jet_Pt->at(iLeadingJet))
         {
            iLeadingJet = iGoodJet[i];
         }
      }
   }
   for (int i = 0; i < fullevent.Photon_Pt->size(); i++)
   {
      if (PhotonBaseLineSelection(fullevent.Photon_Pt->at(i),
                                  fullevent.Photon_Eta->at(i),
                                  fullevent.Photon_PassCleaning->at(i),
                                  fullevent.Photon_PassIDLoose->at(i)) == false)
         continue;
      iGoodPhoton.push_back(i);
   }

   if (fullevent.WithBadJet)
      return false;
   if (TriggerMatch(fullevent.Electron_MatchedTrigger, fullevent.Muon_MatchedTrigger) == false)
      return false;

   event.ID = EventIdentification(iGoodElectron.size(),
                                  iGoodPositron.size(),
                                  iGoodMuon.size(),
                                  iGoodAntiMuon.size(),
                                  iGoodFwd.size(),
                                  iGoodJet.size(),
                                  iGoodPhoton.size());
   if (event.ID == WYEventType::UNKNOWN)
      return false;
   MSGUser()->MSG_DEBUG("Base Line Selected!   nElectron = ", iGoodElectron.size(), "   nPositron = ",
                        iGoodPositron.size(), "   nMuon = ", iGoodMuon.size(), "   nAntiMuon = ",
                        iGoodAntiMuon.size(), "   nFwdElectron = ", iGoodFwd.size(), "   nJet = ", iGoodJet.size(), "   nPhoton = ", iGoodPhoton.size());

   if (event.ID == WYEventType::WECY)
   {
      if (iGoodElectron.size() == 1) 
      {
         if (fullevent.Electron_Pt->at(iGoodElectron[0]) < WECY_BASELINE_ELEC_PTMIN)
            return false;
         event.LeptonID[0] = ElectronSelection(fullevent.Electron_Charge->at(iGoodElectron[0]),
                                               fullevent.Electron_PassID->at(iGoodElectron[0]),
                                               fullevent.Electron_PassIso->at(iGoodElectron[0]));
      }
      else if (iGoodPositron.size() == 1) 
      {
         if (fullevent.Electron_Pt->at(iGoodPositron[0]) < WECY_BASELINE_ELEC_PTMIN)
            return false;
         event.LeptonID[0] = ElectronSelection(fullevent.Electron_Charge->at(iGoodPositron[0]),
                                               fullevent.Electron_PassID->at(iGoodPositron[0]),
                                               fullevent.Electron_PassIso->at(iGoodPositron[0]));
      }
      else
         MSGUser()->MSG_ERROR("Event Pass Base Line WECY should have 1 electron or 1 positron!");
      MSGUser()->MSG_DEBUG("WECY : @ 2 LeptonID[0] = ", (int)event.LeptonID[0]);
      
      event.PhotonID[0] = PhotonSelection(//fullevent.Photon_PassCleaning->at(iGoodPhoton[0]),
                                          fullevent.Photon_PassID->at(iGoodPhoton[0]),
                                          fullevent.Photon_PassIso->at(iGoodPhoton[0]));
      MSGUser()->MSG_DEBUG("WECY : @ 3 PhotonID[0] = ", (int)event.PhotonID[0]);

      if (event.LeptonID[0] == LeptonType::UNKNOWN ||
          event.PhotonID[0] == PhotonType::UNKNOWN)
         return false;
      
      if (event.LeptonID[0] == LeptonType::ELECTRON ||
          event.LeptonID[0] == LeptonType::POSITRON)
      {
         if (event.PhotonID[0] == PhotonType::PHOTON)
            event.ID = WYEventType::WECY;
         else if (event.PhotonID[0] == PhotonType::FAKEPHOTON_TL)
            event.ID = WYEventType::WECY_WJETCR_TL;
         else if (event.PhotonID[0] == PhotonType::FAKEPHOTON_LT)
            event.ID = WYEventType::WECY_WJETCR_LT;
         else if (event.PhotonID[0] == PhotonType::FAKEPHOTON_LL)
            event.ID = WYEventType::WECY_WJETCR_LL;
         else 
            return false;
      }
      else if ((event.LeptonID[0] == LeptonType::FAKEELECTRON ||
                event.LeptonID[0] == LeptonType::FAKEPOSITRON) &&
                event.PhotonID[0] == PhotonType::PHOTON)
      {
         if (fullevent.MET < WY_BASELINE_METMIN)
            event.ID = WYEventType::WECY_YJETCR_LL; 
         else if (fullevent.MET >= WY_BASELINE_METMIN)
            event.ID = WYEventType::WECY_YJETCR_LT;
         else 
            return false;
      }
      else 
         return false;
      
      if (event.ID == WYEventType::WECY)
         if (fullevent.MET < WY_BASELINE_METMIN)
            event.ID = WYEventType::WECY_YJETCR_TL;

      if (iGoodElectron.size() == 1) 
      {
         event.SetLepton(fullevent.Electron_Pt->at(iGoodElectron[0]),
                         fullevent.Electron_Eta->at(iGoodElectron[0]),
                         fullevent.Electron_Phi->at(iGoodElectron[0]));
      }
      else if (iGoodPositron.size() == 1) 
      {
         event.SetLepton(fullevent.Electron_Pt->at(iGoodPositron[0]),
                         fullevent.Electron_Eta->at(iGoodPositron[0]),
                         fullevent.Electron_Phi->at(iGoodPositron[0]));
      }
      else
         MSGUser()->MSG_ERROR("Event Pass Base Line WECY should have 1 electron or 1 positron!");
      MSGUser()->MSG_DEBUG("WECY : @ 5");
         
      event.SetPhoton(fullevent.Photon_Pt->at(iGoodPhoton[0]),
                      fullevent.Photon_Eta->at(iGoodPhoton[0]),
                      fullevent.Photon_Phi->at(iGoodPhoton[0]));
      
      MSGUser()->MSG_DEBUG("WECY : @ 6 event.GetMT() = ", event.GetMT());
      MSGUser()->MSG_DEBUG("WECY : @ 6 event.GetLYDeltaR() = ", event.GetLYDeltaR());
      if (fabs(event.GetLYMass()-91.19) < WECY_BASELINE_ZVETO_DELTAMASSMIN)
         return false;
      if (event.GetLYDeltaR() < WY_BASELINE_LY_DELTARMIN)
         return false;
      
      if (event.ID == WYEventType::WECY_YJETCR_TL ||
          event.ID == WYEventType::WECY_YJETCR_LL)
      {
         if (event.GetMT() >= WY_BASELINE_MWTMIN)
            return false;
      } // Creatively added by ZHZH
      else 
      {
         if (event.GetMT() < WY_BASELINE_MWTMIN)
            return false;
      }
      
      if (event.ChannelNumber > 0)
      {
         if (iGoodElectron.size() == 1) 
         {
            MSGUser()->MSG_DEBUG("WECY : @ 1 Electron ID = ", (int)event.ID);
            event.LeptonWeight[0] = GetElectronWeight(event.LeptonID[0],
                                                      fullevent.mc_Electron_RecoSF->at(iGoodElectron[0]),
                                                      fullevent.mc_Electron_IDSF->at(iGoodElectron[0]),
                                                      fullevent.mc_Electron_IsoSF->at(iGoodElectron[0]));
            event.EventWeight = GetEventWeight(fullevent.mc_EventWeight,
                                               fullevent.mc_PileupWeight,
                                               event.ID,
                                               fullevent.mc_Electron_TriggerSF->at(iGoodElectron[0]));
         }
         else if (iGoodPositron.size() == 1) 
         {
            MSGUser()->MSG_DEBUG("WECY : @ 1 Positron ID = ", (int)event.ID);
            event.LeptonWeight[0] = GetElectronWeight(event.LeptonID[0],
                                                      fullevent.mc_Electron_RecoSF->at(iGoodPositron[0]),
                                                      fullevent.mc_Electron_IDSF->at(iGoodPositron[0]),
                                                      fullevent.mc_Electron_IsoSF->at(iGoodPositron[0]));
            event.EventWeight = GetEventWeight(fullevent.mc_EventWeight,
                                               fullevent.mc_PileupWeight,
                                               event.ID,
                                               fullevent.mc_Electron_TriggerSF->at(iGoodPositron[0]));
         }
         MSGUser()->MSG_DEBUG("WECY : @ 2 LeptonWeight = ", event.LeptonWeight[0]);

         if (0) // for DEBUG
         {   
            TLorentzVector TruthParticle(0,0,0,0);
            for (int i = 0; i < fullevent.Truth_ID->size(); i++)
            {
               TruthParticle.SetPxPyPzE(fullevent.Truth_Px->at(i),
                                        fullevent.Truth_Py->at(i),
                                        fullevent.Truth_Pz->at(i),
                                        fullevent.Truth_E->at(i));
               /*if (fabs(event.PhotonP4(0).DeltaR(TruthParticle)) < 0.1)
               {
                  MSGUser()->MSG_INFO("PHOTOON Pt = ", event.PhotonPt[0]);
                  MSGUser()->MSG_INFO("TRUTH   Pt = ", TruthParticle.Pt());
                  MSGUser()->MSG_INFO("PHOTOON Eta = ", event.PhotonEta[0]);
                  MSGUser()->MSG_INFO("TRUTH   Eta = ", TruthParticle.Eta());
                  MSGUser()->MSG_INFO("PHOTOON Phi = ", event.PhotonPhi[0]);
                  MSGUser()->MSG_INFO("TRUTH   Phi = ", TruthParticle.Phi());
                  MSGUser()->MSG_INFO("TRUTH ID = ", fullevent.Truth_ID->at(i));
                  MSGUser()->MSG_INFO("TRUTH MotherID = ", fullevent.Truth_MotherID->at(i));
                  MSGUser()->MSG_INFO("TRUTH Status = ", fullevent.Truth_Status->at(i));
               }*/
               if (fullevent.Truth_ID->at(i) == 12 &&
                   fullevent.Truth_Status->at(i) == 1)
               {
                  MSGUser()->MSG_INFO("TRUTH Electron   Pt = ", TruthParticle.Pt());
                  MSGUser()->MSG_INFO("TRUTH Electron   Eta = ", TruthParticle.Eta());
                  MSGUser()->MSG_INFO("TRUTH Electron   Phi = ", TruthParticle.Phi());
               }
               else if (fullevent.Truth_ID->at(i) == -12 &&
                        fullevent.Truth_Status->at(i) == 1)
               {
                  MSGUser()->MSG_INFO("TRUTH Positron   Pt = ", TruthParticle.Pt());
                  MSGUser()->MSG_INFO("TRUTH Positron   Eta = ", TruthParticle.Eta());
                  MSGUser()->MSG_INFO("TRUTH Positron   Phi = ", TruthParticle.Phi());
               }
               else if (fullevent.Truth_ID->at(i) == 22 &&
                        fullevent.Truth_Status->at(i) == 1)
               {
                  MSGUser()->MSG_INFO("TRUTH PHOTON   Pt = ", TruthParticle.Pt());
                  MSGUser()->MSG_INFO("TRUTH PHOTON   Eta = ", TruthParticle.Eta());
                  MSGUser()->MSG_INFO("TRUTH PHOTON   Phi = ", TruthParticle.Phi());
               }
            }
         }
      }
   }
   else if (event.ID == WYEventType::WMUY)
   {
      MSGUser()->MSG_DEBUG("WMUY : @ 1");
      if (iGoodMuon.size() == 1)
      {
         if (fullevent.Muon_Pt->at(iGoodMuon[0]) < WMUY_BASELINE_MUON_PTMIN)
            return false;
         event.LeptonID[0] = MuonSelection(fullevent.Muon_Charge->at(iGoodMuon[0]),
                                           fullevent.Muon_PassID->at(iGoodMuon[0]),
                                           fullevent.Muon_PassIso->at(iGoodMuon[0]),
                                           fullevent.Muon_ptcone30->at(iGoodMuon[0])/1000./fullevent.Muon_Original_Pt->at(iGoodMuon[0]));
      }
      else if (iGoodAntiMuon.size() == 1)
      {
         if (fullevent.Muon_Pt->at(iGoodAntiMuon[0]) < WMUY_BASELINE_MUON_PTMIN)
            return false;
         event.LeptonID[0] = MuonSelection(fullevent.Muon_Charge->at(iGoodAntiMuon[0]),
                                           fullevent.Muon_PassID->at(iGoodAntiMuon[0]),
                                           fullevent.Muon_PassIso->at(iGoodAntiMuon[0]),
                                           fullevent.Muon_ptcone30->at(iGoodAntiMuon[0])/1000./fullevent.Muon_Original_Pt->at(iGoodAntiMuon[0]));
      }
      else
         MSGUser()->MSG_ERROR("Event Pass Base Line WMUY should have 1 muon or 1 antimuon!");
      MSGUser()->MSG_DEBUG("WMUY : @ 2 LeptonID[0] = ", (int)event.LeptonID[0]);
      
      event.PhotonID[0] = PhotonSelection(//fullevent.Photon_PassCleaning->at(iGoodPhoton[0]),
                                          fullevent.Photon_PassID->at(iGoodPhoton[0]),
                                          fullevent.Photon_PassIso->at(iGoodPhoton[0]));
      MSGUser()->MSG_DEBUG("WMUY : @ 3 PhotonID[0] = ", (int)event.PhotonID[0]);

      if (event.LeptonID[0] == LeptonType::UNKNOWN ||
          event.PhotonID[0] == PhotonType::UNKNOWN)
         return false;
      MSGUser()->MSG_DEBUG("WMUY : @ 4 MET = ", fullevent.MET);
      
      if (event.LeptonID[0] == LeptonType::MUON ||
          event.LeptonID[0] == LeptonType::ANTIMUON)
      {
         if (event.PhotonID[0] == PhotonType::PHOTON)
            event.ID = WYEventType::WMUY;
         else if (event.PhotonID[0] == PhotonType::FAKEPHOTON_TL)
            event.ID = WYEventType::WMUY_WJETCR_TL;
         else if (event.PhotonID[0] == PhotonType::FAKEPHOTON_LT)
            event.ID = WYEventType::WMUY_WJETCR_LT;
         else if (event.PhotonID[0] == PhotonType::FAKEPHOTON_LL)
            event.ID = WYEventType::WMUY_WJETCR_LL;
         else
            return false;
      }
      else if ((event.LeptonID[0] == LeptonType::FAKEMUON ||
                event.LeptonID[0] == LeptonType::FAKEANTIMUON) &&
                event.PhotonID[0] == PhotonType::PHOTON)
      {
         if (fullevent.MET < WY_BASELINE_METMIN)
            event.ID = WYEventType::WMUY_YJETCR_LL;
         else if (fullevent.MET >= WY_BASELINE_METMIN)
            event.ID = WYEventType::WMUY_YJETCR_LT;
         else
            return false;
      }
      else
         return false;
      
      MSGUser()->MSG_DEBUG("WMUY : @ 5");
      if (event.ID == WYEventType::WMUY)
         if (fullevent.MET < WY_BASELINE_METMIN)
            event.ID = WYEventType::WMUY_YJETCR_TL;
      
      if (iGoodMuon.size() == 1) 
      {
         event.SetLepton(fullevent.Muon_Pt->at(iGoodMuon[0]),
                         fullevent.Muon_Eta->at(iGoodMuon[0]),
                         fullevent.Muon_Phi->at(iGoodMuon[0]));
      }
      else if (iGoodAntiMuon.size() == 1) 
      {
         event.SetLepton(fullevent.Muon_Pt->at(iGoodAntiMuon[0]),
                         fullevent.Muon_Eta->at(iGoodAntiMuon[0]),
                         fullevent.Muon_Phi->at(iGoodAntiMuon[0]));
      }
      else
         MSGUser()->MSG_ERROR("Event Pass Base Line WMUY should have 1 muon or 1 antimuon!");

      event.SetPhoton(fullevent.Photon_Pt->at(iGoodPhoton[0]),
                      fullevent.Photon_Eta->at(iGoodPhoton[0]),
                      fullevent.Photon_Phi->at(iGoodPhoton[0]));

      MSGUser()->MSG_DEBUG("WMUY : @ 6 MT = ", event.GetMT());
      MSGUser()->MSG_DEBUG("WMUY : @ 6 LYDeltaR = ", event.GetLYDeltaR());
      
      if (fabs(event.GetLYMass()-91.19) < WMUY_BASELINE_ZVETO_DELTAMASSMIN)
         return false;
      if (event.GetLYDeltaR() < WY_BASELINE_LY_DELTARMIN)
         return false;
      
      if (event.ID == WYEventType::WMUY_YJETCR_TL ||
          event.ID == WYEventType::WMUY_YJETCR_LL)
      {
         if (event.GetMT() >= WY_BASELINE_MWTMIN)
            return false;
      }
      else 
      {
         if (event.GetMT() < WY_BASELINE_MWTMIN)
            return false;
      }

      MSGUser()->MSG_DEBUG("WMUY : @ 7");

      if (event.ChannelNumber > 0)
      {
         if (iGoodMuon.size() == 1) 
         {
            event.LeptonWeight[0] = GetMuonWeight(event.LeptonID[0],
                                                  fullevent.mc_Muon_RecoSF->at(iGoodMuon[0]),
                                                  fullevent.mc_Muon_TTVASF->at(iGoodMuon[0]),
                                                  fullevent.mc_Muon_IsoSF->at(iGoodMuon[0]));
            event.EventWeight = GetEventWeight(fullevent.mc_EventWeight,
                                               fullevent.mc_PileupWeight,
                                               event.ID,
                                               fullevent.mc_Muon_TriggerSF->at(iGoodMuon[0]));
         }         
         else if (iGoodAntiMuon.size() == 1) 
         {
            event.LeptonWeight[0] = GetMuonWeight(event.LeptonID[0],
                                                  fullevent.mc_Muon_RecoSF->at(iGoodAntiMuon[0]),
                                                  fullevent.mc_Muon_TTVASF->at(iGoodAntiMuon[0]),
                                                  fullevent.mc_Muon_IsoSF->at(iGoodAntiMuon[0]));
            event.EventWeight = GetEventWeight(fullevent.mc_EventWeight,
                                               fullevent.mc_PileupWeight,
                                               event.ID,
                                               fullevent.mc_Muon_TriggerSF->at(iGoodAntiMuon[0]));
         }         
         if (0) // for DEBUG
         {   
            TLorentzVector TruthParticle(0,0,0,0);
            for (int i = 0; i < fullevent.Truth_ID->size(); i++)
            {
               TruthParticle.SetPxPyPzE(fullevent.Truth_Px->at(i),
                                        fullevent.Truth_Py->at(i),
                                        fullevent.Truth_Pz->at(i),
                                        fullevent.Truth_E->at(i));
               /*if (fabs(event.PhotonP4(0).DeltaR(TruthParticle)) < 0.1 &&
                   fullevent.Truth_Status->at(i) == 1)
               {
                  MSGUser()->MSG_INFO("PHOTOON Pt = ", event.PhotonPt[0]);
                  MSGUser()->MSG_INFO("PHOTOON Eta = ", event.PhotonEta[0]);
                  MSGUser()->MSG_INFO("PHOTOON Phi = ", event.PhotonPhi[0]);
                  MSGUser()->MSG_INFO("TRUTH   Pt = ", TruthParticle.Pt());
                  MSGUser()->MSG_INFO("TRUTH   Eta = ", TruthParticle.Eta());
                  MSGUser()->MSG_INFO("TRUTH   Phi = ", TruthParticle.Phi());
                  MSGUser()->MSG_INFO("TRUTH ID = ", fullevent.Truth_ID->at(i));
                  //MSGUser()->MSG_INFO("TRUTH MotherID = ", fullevent.Truth_MotherID->at(i));
                  //MSGUser()->MSG_INFO("TRUTH Status = ", fullevent.Truth_Status->at(i));
               }*/
               if (fullevent.Truth_ID->at(i) == 13 &&
                   fullevent.Truth_Status->at(i) == 1)
               {
                  MSGUser()->MSG_INFO("TRUTH Muon   Pt = ", TruthParticle.Pt());
                  MSGUser()->MSG_INFO("TRUTH Muon   Eta = ", TruthParticle.Eta());
                  MSGUser()->MSG_INFO("TRUTH Muon   Phi = ", TruthParticle.Phi());
               }
               else if (fullevent.Truth_ID->at(i) == -13 &&
                        fullevent.Truth_Status->at(i) == 1)
               {
                  MSGUser()->MSG_INFO("TRUTH AntiMuon   Pt = ", TruthParticle.Pt());
                  MSGUser()->MSG_INFO("TRUTH AntiMuon   Eta = ", TruthParticle.Eta());
                  MSGUser()->MSG_INFO("TRUTH AntiMuon   Phi = ", TruthParticle.Phi());
               }
               else if (fullevent.Truth_ID->at(i) == 22 &&
                        fullevent.Truth_Status->at(i) == 1)
               {
                  MSGUser()->MSG_INFO("TRUTH PHOTON   Pt = ", TruthParticle.Pt());
                  MSGUser()->MSG_INFO("TRUTH PHOTON   Eta = ", TruthParticle.Eta());
                  MSGUser()->MSG_INFO("TRUTH PHOTON   Phi = ", TruthParticle.Phi());
               }
            }
         }
      }
   }

   return true;
}

bool WYLoopEvent::ElectronBaseLineSelection(double pt, double eta, double dz0, double d0sig, bool passid)
{
   if (pt < ELEC_BASELINE_PTMIN)
      return false;
   if (fabs(eta) > ELEC_BASELINE_FABS_ETAMAX)
      return false;
   if (fabs(eta) > ELEC_BASELINE_CRACK_FABS_ETAMIN &&
       fabs(eta) < ELEC_BASELINE_CRACK_FABS_ETAMAX)
      return false;
   if (fabs(dz0) > ELEC_BASELINE_FABS_DZ0MAX)
      return false;
   if (fabs(d0sig) > ELEC_BASELINE_FABS_D0SIGMAX)
      return false;
   if (passid == false)
      return false;
   return true;
}

bool WYLoopEvent::MuonBaseLineSelection(double pt, double eta, double dz0, double d0sig, bool passid)
{
   //MSGUser()->MSG_INFO("pt = ", pt, " eta = ", eta, " dz0 = ", dz0,  " d0sig = ", d0sig, " passid = ", (int)passid);
   if (pt < MUON_BASELINE_PTMIN)
      return false;
   if (fabs(eta) > MUON_BASELINE_FABS_ETAMAX)
      return false;
   if (fabs(dz0) > MUON_BASELINE_FABS_DZ0MAX)
      return false;
   if (fabs(d0sig) > MUON_BASELINE_FABS_D0SIGMAX)
      return false;
   if (passid == false)
      return false;
   return true;
}

bool WYLoopEvent::ForwardElectronBaseLineSelection(double pt, double eta)
{
   if (pt <= FWDELEC_BASELINE_PTMIN)
      return false;
   if (fabs(eta) >= FWDELEC_BASELINE_FABS_ETAMAX)
      return false;
   if (fabs(eta) <= FWDELEC_BASELINE_FABS_ETAMIN)
      return false;
   if (fabs(eta) >= FWDELEC_BASELINE_CRACK_FABS_ETAMIN &&
       fabs(eta) <= FWDELEC_BASELINE_CRACK_FABS_ETAMAX)
      return false;
   return true;
}

bool WYLoopEvent::JetBaseLineSelection(double pt, double eta, bool passjvt)
{
   if (pt < JET_BASELINE_PTMIN)
      return false;
   if (fabs(eta) > JET_BASELINE_FABS_ETAMAX)
      return false;
   if (passjvt == false)
      return false;
   return true;
}

bool WYLoopEvent::PhotonBaseLineSelection(double pt, double eta, bool passcleaning, bool passid)
{
   if (pt < PHOTON_BASELINE_PTMIN)
      return false;
   if (fabs(eta) > PHOTON_BASELINE_FABS_ETAMAX)
      return false;
   if (fabs(eta) > PHOTON_BASELINE_CRACK_FABS_ETAMIN &&
       fabs(eta) < PHOTON_BASELINE_CRACK_FABS_ETAMAX)
      return false;
   if (passcleaning == false)
      return false;
   if (passid == false)
      return false;
   return true;
}

bool WYLoopEvent::TriggerMatch(vector<bool> *el, vector<bool> *mu)
{
   if (el->size() > 0)
   {
      for (int i = 0; i < el->size(); i++)
         if ((*el)[i] == true)
            return true;
   }
   if (mu->size() > 0)
   {
      for (int i = 0; i < mu->size(); i++)
         if ((*mu)[i] == true)
            return true;
   }
   return false;
}

LeptonType WYLoopEvent::ElectronSelection(int charge, bool passid, bool passiso)
{
   if (passid && passiso)
   {
      if (charge > 0)
         return LeptonType::POSITRON;
      else
         return LeptonType::ELECTRON;
   }
   if (passid && !passiso) 
   {
      if (charge > 0)
         return LeptonType::FAKEPOSITRON;
      else
         return LeptonType::FAKEELECTRON;
   }

   return LeptonType::UNKNOWN;
}

LeptonType WYLoopEvent::MuonSelection(int charge, bool passid, bool passiso, double iso)
{
   if (passid && passiso)
   {
      if (charge > 0)
         return LeptonType::ANTIMUON;
      else
         return LeptonType::MUON;
   }
   if (passid && !passiso && iso < MUON_BASELINE_ISOMAX)
   {
      if (charge > 0)
         return LeptonType::FAKEANTIMUON;
      else
         return LeptonType::FAKEMUON;
   }

   return LeptonType::UNKNOWN;
}

LeptonType WYLoopEvent::ForwardElectronSelection(bool passid)
{
   if (passid)
      return LeptonType::FWDELECTRON;
   else
      return LeptonType::FAKEFWDELECTRON;
}

PhotonType WYLoopEvent::PhotonSelection(bool passid, bool passiso)
{
   if (passid && passiso)
      return PhotonType::PHOTON;
   else if (passid && !passiso)
      return PhotonType::FAKEPHOTON_TL;
   else if (!passid && passiso)
      return PhotonType::FAKEPHOTON_LT;
   else if (!passid && !passiso)
      return PhotonType::FAKEPHOTON_LL;

   return PhotonType::UNKNOWN;
}

WYEventType WYLoopEvent::EventIdentification(int nelectron, int npositron, int nmuon, int nantimuon, int nfwd, int njet, int nphoton)
{
   if (nelectron == 1 && npositron == 0 && nmuon == 0 && nantimuon == 0 && nphoton == 1 && BASELINE_PASS_WECY)
      return WYEventType::WECY;
   if (nelectron == 0 && npositron == 1 && nmuon == 0 && nantimuon == 0 && nphoton == 1 && BASELINE_PASS_WECY)
      return WYEventType::WECY;
   if (nelectron == 0 && npositron == 0 && nmuon == 1 && nantimuon == 0 && nphoton == 1 && BASELINE_PASS_WMUY)
      return WYEventType::WMUY;
   if (nelectron == 0 && npositron == 0 && nmuon == 0 && nantimuon == 1 && nphoton == 1 && BASELINE_PASS_WMUY)
      return WYEventType::WMUY;

   return WYEventType::UNKNOWN;
}

double WYLoopEvent::GetElectronWeight(LeptonType type, double recosf, double idsf, double isosf)
{
   if (type == LeptonType::ELECTRON || type == LeptonType::POSITRON)
   {
      return recosf * idsf * isosf;
   }
   else if (type == LeptonType::FAKEELECTRON || type == LeptonType::FAKEPOSITRON)
   {
      return recosf * idsf;
   }
   return 0;
}

double WYLoopEvent::GetMuonWeight(LeptonType type, double recosf, double ttvasf, double isosf)
{
   if (type == LeptonType::MUON || type == LeptonType::ANTIMUON)
      return recosf * ttvasf * isosf;
   else if (type == LeptonType::FAKEMUON || type == LeptonType::FAKEANTIMUON)
      return recosf * ttvasf;
   else
      return 0;
}

double WYLoopEvent::GetPhotonWeight(PhotonType type, double idsf, double isosf)
{
   if (type == PhotonType::PHOTON)
      return idsf * isosf;
   else if (type == PhotonType::FAKEPHOTON_TL)
      return idsf;
   else if (type == PhotonType::FAKEPHOTON_LT)
      return isosf;
   else if (type == PhotonType::FAKEPHOTON_LL)
      return 1;
   else
      return 0;
}

double WYLoopEvent::GetEventWeight(double evt, double pileup, WYEventType type, double sf)
{
   if (type == WYEventType::UNKNOWN)
      return 0;
   else
      return evt * pileup * sf;
}

bool WYLoopEvent::LeptonTruthMatch(TLorentzVector lepton, TLorentzVector truthlepton)
{
      //if (fabs(lepton.DeltaR(truthlepton)) < TRUTH_MATCH_DRMAX)
      if (fabs(lepton.DeltaR(truthlepton)) < 0.1)
         return true;
      return false;
}

bool WYLoopEvent::LeptonTruthMatch(WYEvent event)
{
   /*if ((event.ID == WYEventType::ZEECF_BKG_MUE ||
        event.ID == WYEventType::ZEECF_BKG_WJETCR_MUE) &&
       (event.LeptonID[0] == LeptonType::MUON ||
        event.LeptonID[0] == LeptonType::ANTIMUON) &&
       (event.LeptonID[1] == LeptonType::FWDELECTRON ||
        event.LeptonID[1] == LeptonType::FAKEFWDELECTRON))
   {
      if (fabs(event.LeptonP4(1).DeltaR(event.Truth_LeptonP4(1))) < TRUTH_MATCH_DRMAX)
         return true;
      else
         return false;
   }
   if (event.ID == WYEventType::ELECTRON_FAKE_CR && event.LeptonID[1] == LeptonType::UNKNOWN)
   {
      if (fabs(event.LeptonP4(0).DeltaR(event.Truth_LeptonP4(0))) < TRUTH_MATCH_DRMAX)
         return true;
      else
         return false;
   }
   else if (fabs(event.LeptonP4(0).DeltaR(event.Truth_LeptonP4(0))) < TRUTH_MATCH_DRMAX &&
            fabs(event.LeptonP4(1).DeltaR(event.Truth_LeptonP4(1))) < TRUTH_MATCH_DRMAX)
      return true;
   else if (fabs(event.LeptonP4(0).DeltaR(event.Truth_LeptonP4(1))) < TRUTH_MATCH_DRMAX &&
            fabs(event.LeptonP4(1).DeltaR(event.Truth_LeptonP4(0))) < TRUTH_MATCH_DRMAX)
      return true;
   else
      return false;*/
   return false;
}

bool WYLoopEvent::IsUselessMC(int channelnum)
{
   if (ChannelNumber == 361600 || ChannelNumber == 361601 || ChannelNumber == 361602 ||
       ChannelNumber == 361603 || ChannelNumber == 361604 || ChannelNumber == 361605 ||
       ChannelNumber == 361606 || ChannelNumber == 361607 || ChannelNumber == 361608 ||
       ChannelNumber == 361609 || ChannelNumber == 361610 ||
       ChannelNumber == 363360 || ChannelNumber == 363359 || ChannelNumber == 426131)
      return true;
   else
      return false;
}

NAGASH::StatusCode WYLoopEvent::Finalize()
{
   // In this function, User can do something after process the entries
   return NAGASH::StatusCode::SUCCESS;
}
