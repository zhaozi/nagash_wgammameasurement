#include "LYFakeBKG.h"

StatusCode LYFakeBKG::Process(const ConfigTool &config,
                              shared_ptr<ResultGroup> result,
                              shared_ptr<WYDataSet> AllSample,
                              shared_ptr<WYPlotGroup> WYplotgroup)
{
  LYFakeBKG myjob(config);
  result = myjob.ResultGroupUser();
  myjob.RunReweighting(AllSample, WYplotgroup);
  return StatusCode::SUCCESS;
}

void LYFakeBKG::RunReweighting(shared_ptr<WYDataSet> AllSample,
                               shared_ptr<WYPlotGroup> WYplotgroup)
{
  auto titlegurad = MSGUser()->StartTitleWithGuard("RunReweighting");
  MSGUser()->MSG_INFO("Run Fake-LY-BKG Reweighting Start");
  
  EstimateYjetsMode = ConfigUser()->GetPar<int>("EstimateYjetsMode");
  MyPlots = ResultGroupUser()->BookResult<WYPlotGroup>("LYFake", ConfigUser()->GetPar<TString>("FILLPLOTS_IN_LYFAKEBKG_FILENAME"));
  
  WYEvent event;
  MSGUser()->MSG_INFO("DataVec.size() = ", AllSample->DataVec.size());
  MSGUser()->MSG_INFO("SignalVec.size() = ", AllSample->SignalVec.size());
  MSGUser()->MSG_INFO("BKGVec.size() = ", AllSample->BKGVec.size());
  for (int i = 0; i < AllSample->DataVec.size(); i++)
  {
    event.Clone(AllSample->DataVec.at(i));
    if (event.ID == WYEventType::WECY_WJETCR_TL)
    {
      event.EventWeight = event.EventWeight * WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      event.ID = WYEventType::WECY_WJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_WJETCR_TL)
    {
      event.EventWeight = event.EventWeight * WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      event.ID = WYEventType::WMUY_WJETCR_REWEIGHTED;
    }
    else continue;
    
    if (event.PhotonID[0] == PhotonType::FAKEPHOTON_TL) 
      event.PhotonID[0] = PhotonType::PHOTON;
  
    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }
  
  for (int i = 0; i < AllSample->SignalVec.size(); i++)
  {
    event.Clone(AllSample->SignalVec.at(i));
    if (event.ID == WYEventType::WECY_WJETCR_TL)
    {
      event.EventWeight = - event.EventWeight * WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      event.ID = WYEventType::WECY_WJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_WJETCR_TL)
    {
      event.EventWeight = - event.EventWeight * WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      event.ID = WYEventType::WMUY_WJETCR_REWEIGHTED;
    }
    else continue;
    
    if (event.PhotonID[0] == PhotonType::FAKEPHOTON_TL) 
      event.PhotonID[0] = PhotonType::PHOTON;
   
    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }
  
  for (int i = 0; i < AllSample->BKGVec.size(); i++)
  {
    event.Clone(AllSample->BKGVec.at(i));
    if (event.ID == WYEventType::WECY_WJETCR_TL)
    {
      event.EventWeight = - event.EventWeight * WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      event.ID = WYEventType::WECY_WJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_WJETCR_TL)
    {
      event.EventWeight = - event.EventWeight * WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WJETCR_SF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      event.ID = WYEventType::WMUY_WJETCR_REWEIGHTED;
    }
    else continue;
     
    if (event.PhotonID[0] == PhotonType::FAKEPHOTON_TL) 
      event.PhotonID[0] = PhotonType::PHOTON;
   
    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }
 
  // Reset WPlusJet MC SF
  WYplotgroup->ResetWPlusJetMCSF(MyPlots);

if (EstimateYjetsMode == 1) // Estimate Yjets BKG by ABCD method
{
  for (int i = 0; i < AllSample->BKGVec.size(); i++)
  {
    event.Clone(AllSample->BKGVec.at(i));
     
    if (event.ID == WYEventType::WECY_YJETCR_LT ||
        event.ID == WYEventType::WECY_YJETCR_TL ||
        event.ID == WYEventType::WECY_YJETCR_LL)
    {
      if (event.GetChannelName() == "WPlusJet")
      {
        event.EventWeight = event.EventWeight * WYplotgroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      }
      else continue;
    }
    else if (event.ID == WYEventType::WMUY_YJETCR_LT ||
             event.ID == WYEventType::WMUY_YJETCR_TL ||
             event.ID == WYEventType::WMUY_YJETCR_LL)
    {
      if (event.GetChannelName() == "WPlusJet")
      {
        event.EventWeight = event.EventWeight * WYplotgroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      }
      else continue;
    }
    else continue;
    event.ReweightedWPlusJet();
    MyPlots->FillEvent(event);
  }

  // Reset YJET Control Region SF
  WYplotgroup->ResetYJETCRSF(MyPlots);

  for (int i = 0; i < AllSample->DataVec.size(); i++)
  {
    event.Clone(AllSample->DataVec.at(i));
    if (event.ID == WYEventType::WECY_YJETCR_LT)
    {
      event.EventWeight = event.EventWeight * WYplotgroup->WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->GetBinContent(WYplotgroup->WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->GetNominalHist()->FindBin(event.LeptonEta[0], event.LeptonPt[0]));
      event.ID = WYEventType::WECY_YJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_YJETCR_LT)
    {
      event.EventWeight = event.EventWeight * WYplotgroup->WUY_YJETCR_SF_MuonEta_MuonPt_2D->GetBinContent(WYplotgroup->WUY_YJETCR_SF_MuonEta_MuonPt_2D->GetNominalHist()->FindBin(event.LeptonEta[0], event.LeptonPt[0]));
      event.ID = WYEventType::WMUY_YJETCR_REWEIGHTED;
    }
    else continue;

    if (event.LeptonID[0] == LeptonType::FAKEELECTRON) 
      event.LeptonID[0] = LeptonType::ELECTRON;
    else if (event.LeptonID[0] == LeptonType::FAKEPOSITRON) 
      event.LeptonID[0] = LeptonType::POSITRON;
    else if (event.LeptonID[0] == LeptonType::FAKEMUON) 
      event.LeptonID[0] = LeptonType::MUON;
    else if (event.LeptonID[0] == LeptonType::FAKEANTIMUON) 
      event.LeptonID[0] = LeptonType::ANTIMUON;
  
    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }
  
  for (int i = 0; i < AllSample->SignalVec.size(); i++)
  {
    event.Clone(AllSample->SignalVec.at(i));
    if (event.ID == WYEventType::WECY_YJETCR_LT)
    {
      event.EventWeight = - event.EventWeight * WYplotgroup->WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->GetBinContent(WYplotgroup->WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->GetNominalHist()->FindBin(event.LeptonEta[0], event.LeptonPt[0]));
      event.ID = WYEventType::WECY_YJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_YJETCR_LT)
    {
      event.EventWeight = - event.EventWeight * WYplotgroup->WUY_YJETCR_SF_MuonEta_MuonPt_2D->GetBinContent(WYplotgroup->WUY_YJETCR_SF_MuonEta_MuonPt_2D->GetNominalHist()->FindBin(event.LeptonEta[0], event.LeptonPt[0]));
      event.ID = WYEventType::WMUY_YJETCR_REWEIGHTED;
    }
    else continue;
    
    if (event.LeptonID[0] == LeptonType::FAKEELECTRON) 
      event.LeptonID[0] = LeptonType::ELECTRON;
    else if (event.LeptonID[0] == LeptonType::FAKEPOSITRON) 
      event.LeptonID[0] = LeptonType::POSITRON;
    else if (event.LeptonID[0] == LeptonType::FAKEMUON) 
      event.LeptonID[0] = LeptonType::MUON;
    else if (event.LeptonID[0] == LeptonType::FAKEANTIMUON) 
      event.LeptonID[0] = LeptonType::ANTIMUON;
   
    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }

  for (int i = 0; i < AllSample->BKGVec.size(); i++)
  {
    event.Clone(AllSample->BKGVec.at(i));
     
    if (event.ID == WYEventType::WECY_YJETCR_LT)
    {
      if (event.GetChannelName() == "WPlusJet")
      {
        event.EventWeight = event.EventWeight * WYplotgroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      }
      event.EventWeight = - event.EventWeight * WYplotgroup->WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->GetBinContent(WYplotgroup->WEY_YJETCR_SF_ElectronEta_ElectronPt_2D->GetNominalHist()->FindBin(event.LeptonEta[0], event.LeptonPt[0]));
      event.ID = WYEventType::WECY_YJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_YJETCR_LT)
    {
      if (event.GetChannelName() == "WPlusJet")
      {
        event.EventWeight = event.EventWeight * WYplotgroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      }
      event.EventWeight = - event.EventWeight * WYplotgroup->WUY_YJETCR_SF_MuonEta_MuonPt_2D->GetBinContent(WYplotgroup->WUY_YJETCR_SF_MuonEta_MuonPt_2D->GetNominalHist()->FindBin(event.LeptonEta[0], event.LeptonPt[0]));
      event.ID = WYEventType::WMUY_YJETCR_REWEIGHTED;
    }
    else continue;
    
    if (event.LeptonID[0] == LeptonType::FAKEELECTRON) 
      event.LeptonID[0] = LeptonType::ELECTRON;
    else if (event.LeptonID[0] == LeptonType::FAKEPOSITRON) 
      event.LeptonID[0] = LeptonType::POSITRON;
    else if (event.LeptonID[0] == LeptonType::FAKEMUON) 
      event.LeptonID[0] = LeptonType::MUON;
    else if (event.LeptonID[0] == LeptonType::FAKEANTIMUON) 
      event.LeptonID[0] = LeptonType::ANTIMUON;

    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }
}
else if (EstimateYjetsMode == 2) // Estimate Yjets BKG by template fitting method
{
  for (int i = 0; i < AllSample->BKGVec.size(); i++)
  {
    event.Clone(AllSample->BKGVec.at(i));
     
    if (event.ID == WYEventType::WECY_YJETCR_LL)
    {
      if (event.GetChannelName() == "WPlusJet")
      {
        event.EventWeight = event.EventWeight * WYplotgroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      }
      else continue;
    }
    else if (event.ID == WYEventType::WMUY_YJETCR_LL)
    {
      if (event.GetChannelName() == "WPlusJet")
      {
        event.EventWeight = event.EventWeight * WYplotgroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      }
      else continue;
    }
    else continue;
    event.ReweightedWPlusJet();
    //MSGUser()->MSG_DEBUG("ChannelName = ", event.GetChannelName(), " w = ", event.GetWeight());
    MyPlots->FillEvent(event);
  }
  MSGUser()->MSG_INFO("WEY Wjets After Loop WPlusJet : ", MyPlots->WEY_YJETCR_LL_LeptonEta.Channel_Map["Wjets"]->GetNominalHist()->Integral());
  MSGUser()->MSG_INFO("WEY Wjets After Loop WPlusJet : ", MyPlots->WUY_YJETCR_LL_LeptonEta.Channel_Map["Wjets"]->GetNominalHist()->Integral());

  for (int i = 0; i < AllSample->DataVec.size(); i++)
  {
    event.Clone(AllSample->DataVec.at(i));
    if (event.ID == WYEventType::WECY_YJETCR_LL)
    {
      event.ID = WYEventType::WECY_YJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_YJETCR_LL)
    {
      event.ID = WYEventType::WMUY_YJETCR_REWEIGHTED;
    }
    else continue;

    if (event.LeptonID[0] == LeptonType::FAKEELECTRON) 
      event.LeptonID[0] = LeptonType::ELECTRON;
    else if (event.LeptonID[0] == LeptonType::FAKEPOSITRON) 
      event.LeptonID[0] = LeptonType::POSITRON;
    else if (event.LeptonID[0] == LeptonType::FAKEMUON) 
      event.LeptonID[0] = LeptonType::MUON;
    else if (event.LeptonID[0] == LeptonType::FAKEANTIMUON) 
      event.LeptonID[0] = LeptonType::ANTIMUON;
  
    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }
  MSGUser()->MSG_INFO("WEY Yjets After Loop Data : ", MyPlots->ElectronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral() + MyPlots->PositronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral());
  MSGUser()->MSG_INFO("WEY Yjets After Loop Data : ", MyPlots->MuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral() + MyPlots->AntiMuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral());
  
  for (int i = 0; i < AllSample->SignalVec.size(); i++)
  {
    event.Clone(AllSample->SignalVec.at(i));
    if (event.ID == WYEventType::WECY_YJETCR_LL)
    {
      event.ID = WYEventType::WECY_YJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_YJETCR_LL)
    {
      event.ID = WYEventType::WMUY_YJETCR_REWEIGHTED;
    }
    else continue;
    
    if (event.LeptonID[0] == LeptonType::FAKEELECTRON) 
      event.LeptonID[0] = LeptonType::ELECTRON;
    else if (event.LeptonID[0] == LeptonType::FAKEPOSITRON) 
      event.LeptonID[0] = LeptonType::POSITRON;
    else if (event.LeptonID[0] == LeptonType::FAKEMUON) 
      event.LeptonID[0] = LeptonType::MUON;
    else if (event.LeptonID[0] == LeptonType::FAKEANTIMUON) 
      event.LeptonID[0] = LeptonType::ANTIMUON;
   
    event.EventWeight = - event.EventWeight;
    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }
  MSGUser()->MSG_INFO("WEY Yjets After Loop Signal : ", MyPlots->ElectronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral() + MyPlots->PositronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral());
  MSGUser()->MSG_INFO("WUY Yjets After Loop Signal : ", MyPlots->MuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral() + MyPlots->AntiMuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral());

  for (int i = 0; i < AllSample->BKGVec.size(); i++)
  {
    event.Clone(AllSample->BKGVec.at(i));
     
    if (event.ID == WYEventType::WECY_YJETCR_LL)
    {
      if (event.GetChannelName() == "WPlusJet")
      {
        event.EventWeight = event.EventWeight * WYplotgroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      }
      event.ID = WYEventType::WECY_YJETCR_REWEIGHTED;
    }
    else if (event.ID == WYEventType::WMUY_YJETCR_LL)
    {
      if (event.GetChannelName() == "WPlusJet")
      {
        event.EventWeight = event.EventWeight * WYplotgroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetBinContent(WYplotgroup->WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D->GetNominalHist()->FindBin(event.PhotonEta[0], event.PhotonPt[0]));
      }
      event.ID = WYEventType::WMUY_YJETCR_REWEIGHTED;
    }
    else continue;
    
    if (event.LeptonID[0] == LeptonType::FAKEELECTRON) 
      event.LeptonID[0] = LeptonType::ELECTRON;
    else if (event.LeptonID[0] == LeptonType::FAKEPOSITRON) 
      event.LeptonID[0] = LeptonType::POSITRON;
    else if (event.LeptonID[0] == LeptonType::FAKEMUON) 
      event.LeptonID[0] = LeptonType::MUON;
    else if (event.LeptonID[0] == LeptonType::FAKEANTIMUON) 
      event.LeptonID[0] = LeptonType::ANTIMUON;

    event.EventWeight = - event.EventWeight;
    event.ChannelNumber = 1026;
    MyPlots->FillEvent(event);
  }
  MSGUser()->MSG_INFO("WEY Yjets After Loop BKG : ", MyPlots->ElectronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral() + MyPlots->PositronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral());
  MSGUser()->MSG_INFO("WUY Yjets After Loop BKG : ", MyPlots->MuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral() + MyPlots->AntiMuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral());

  // Get Reweight Factor
  double DataSumWEY = WYplotgroup->ElectronEta.Channel_Map["Data"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["Data"]->GetNominalHist()->Integral();
  double SignalSumWEY = WYplotgroup->ElectronEta.Channel_Map["Signal"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["Signal"]->GetNominalHist()->Integral();
  double BKGSumWEY = WYplotgroup->ElectronEta.Channel_Map["BKG"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["BKG"]->GetNominalHist()->Integral();
  double WjetsSumWEY = MyPlots->ElectronEta.Channel_Map["Wjets"]->GetNominalHist()->Integral() + MyPlots->PositronEta.Channel_Map["Wjets"]->GetNominalHist()->Integral();
  double YjetsSumWEYori = MyPlots->ElectronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral() + MyPlots->PositronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral();
  double YjetsSumWEY = DataSumWEY - SignalSumWEY - BKGSumWEY - WjetsSumWEY;
  double ScaleWEY = YjetsSumWEY/YjetsSumWEYori;
  MSGUser()->MSG_INFO(" SUMMARY");
  MSGUser()->MSG_INFO(" SUMMARY of WEY");
  MSGUser()->MSG_INFO(" Data    = ", DataSumWEY);
  MSGUser()->MSG_INFO(" Signal  = ", SignalSumWEY, "   (", SignalSumWEY/DataSumWEY, ")");
  MSGUser()->MSG_INFO(" Wjets   = ", WjetsSumWEY, "   (", WjetsSumWEY/DataSumWEY, ")");
  MSGUser()->MSG_INFO(" Yjets   = ", YjetsSumWEY, "   (", YjetsSumWEY/DataSumWEY, ")");
  MSGUser()->MSG_INFO(" BKG     = ", BKGSumWEY, "   (", BKGSumWEY/DataSumWEY, ") in which :");
  double ZeeSumWEY = WYplotgroup->ElectronEta.Channel_Map["Zee"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["Zee"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zee     = ", ZeeSumWEY, "   (", ZeeSumWEY/DataSumWEY, ")");
  double ZmumuSumWEY = WYplotgroup->ElectronEta.Channel_Map["Zmumu"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["Zmumu"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zmumu   = ", ZmumuSumWEY, "   (", ZmumuSumWEY/DataSumWEY, ")");
  double ZtautauSumWEY = WYplotgroup->ElectronEta.Channel_Map["Ztautau"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["Ztautau"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Ztautau = ", ZtautauSumWEY, "   (", ZtautauSumWEY/DataSumWEY, ")");
  double DibosonSumWEY = WYplotgroup->ElectronEta.Channel_Map["Diboson"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["Diboson"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Diboson = ", DibosonSumWEY, "   (", DibosonSumWEY/DataSumWEY, ")");
  double TopSumWEY = WYplotgroup->ElectronEta.Channel_Map["Top"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["Top"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Top     = ", TopSumWEY, "   (", TopSumWEY/DataSumWEY, ")");
  double ZYSumWEY = WYplotgroup->ElectronEta.Channel_Map["ZY"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["ZY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" ZY      = ", ZYSumWEY, "   (", ZYSumWEY/DataSumWEY, ")");
  double WtvYSumWEY = WYplotgroup->ElectronEta.Channel_Map["WtvY"]->GetNominalHist()->Integral() + WYplotgroup->PositronEta.Channel_Map["WtvY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" WtvY    = ", WtvYSumWEY, "   (", WtvYSumWEY/DataSumWEY, ")");
  
  double DataSumWUY = WYplotgroup->MuonEta.Channel_Map["Data"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["Data"]->GetNominalHist()->Integral();
  double SignalSumWUY = WYplotgroup->MuonEta.Channel_Map["Signal"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["Signal"]->GetNominalHist()->Integral();
  double BKGSumWUY = WYplotgroup->MuonEta.Channel_Map["BKG"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["BKG"]->GetNominalHist()->Integral();
  double WjetsSumWUY = MyPlots->MuonEta.Channel_Map["Wjets"]->GetNominalHist()->Integral() + MyPlots->AntiMuonEta.Channel_Map["Wjets"]->GetNominalHist()->Integral();
  double YjetsSumWUYori = MyPlots->MuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral() + MyPlots->AntiMuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral();
  double YjetsSumWUY = DataSumWUY - SignalSumWUY - BKGSumWUY - WjetsSumWUY;
  double ScaleWUY = YjetsSumWUY/YjetsSumWUYori;
  MSGUser()->MSG_INFO(" SUMMARY of WUY");
  MSGUser()->MSG_INFO(" Data    = ", DataSumWUY);
  MSGUser()->MSG_INFO(" Signal  = ", SignalSumWUY, "   (", SignalSumWUY/DataSumWUY, ")");
  MSGUser()->MSG_INFO(" Wjets   = ", WjetsSumWUY, "   (", WjetsSumWUY/DataSumWUY, ")");
  MSGUser()->MSG_INFO(" Yjets   = ", YjetsSumWUY, "   (", YjetsSumWUY/DataSumWUY, ")");
  MSGUser()->MSG_INFO(" BKG     = ", BKGSumWUY, "   (", BKGSumWUY/DataSumWUY, ") in which :");
  double ZeeSumWUY = WYplotgroup->MuonEta.Channel_Map["Zee"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["Zee"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zee     = ", ZeeSumWUY, "   (", ZeeSumWUY/DataSumWUY, ")");
  double ZmumuSumWUY = WYplotgroup->MuonEta.Channel_Map["Zmumu"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["Zmumu"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zmumu   = ", ZmumuSumWUY, "   (", ZmumuSumWUY/DataSumWUY, ")");
  double ZtautauSumWUY = WYplotgroup->MuonEta.Channel_Map["Ztautau"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["Ztautau"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Ztautau = ", ZtautauSumWUY, "   (", ZtautauSumWUY/DataSumWUY, ")");
  double DibosonSumWUY = WYplotgroup->MuonEta.Channel_Map["Diboson"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["Diboson"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Diboson = ", DibosonSumWUY, "   (", DibosonSumWUY/DataSumWUY, ")");
  double TopSumWUY = WYplotgroup->MuonEta.Channel_Map["Top"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["Top"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Top     = ", TopSumWUY, "   (", TopSumWUY/DataSumWUY, ")");
  double ZYSumWUY = WYplotgroup->MuonEta.Channel_Map["ZY"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["ZY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" ZY      = ", ZYSumWUY, "   (", ZYSumWUY/DataSumWUY, ")");
  double WtvYSumWUY = WYplotgroup->MuonEta.Channel_Map["WtvY"]->GetNominalHist()->Integral() + WYplotgroup->AntiMuonEta.Channel_Map["WtvY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" WtvY    = ", WtvYSumWUY, "   (", WtvYSumWUY/DataSumWUY, ")");
  
  double DataSumWplusEY = WYplotgroup->PositronEta.Channel_Map["Data"]->GetNominalHist()->Integral();
  double SignalSumWplusEY = WYplotgroup->PositronEta.Channel_Map["Signal"]->GetNominalHist()->Integral();
  double BKGSumWplusEY = WYplotgroup->PositronEta.Channel_Map["BKG"]->GetNominalHist()->Integral();
  double WjetsSumWplusEY = MyPlots->PositronEta.Channel_Map["Wjets"]->GetNominalHist()->Integral();
  double YjetsSumWplusEYori = MyPlots->PositronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral();
  double YjetsSumWplusEY = DataSumWplusEY - SignalSumWplusEY - BKGSumWplusEY - WjetsSumWplusEY;
  MSGUser()->MSG_INFO(" SUMMARY");
  MSGUser()->MSG_INFO(" W+ SUMMARY of WEY (Positron)");
  MSGUser()->MSG_INFO(" Data    = ", DataSumWplusEY, "   Data - Wjets - Yjets - BKG = ", DataSumWplusEY - WjetsSumWplusEY - YjetsSumWplusEYori*ScaleWEY - BKGSumWplusEY, "   (", (DataSumWplusEY - WjetsSumWplusEY - YjetsSumWplusEYori*ScaleWEY - BKGSumWplusEY)/DataSumWplusEY, ")");
  MSGUser()->MSG_INFO(" Signal  = ", SignalSumWplusEY, "   (", SignalSumWplusEY/DataSumWplusEY, ")");
  MSGUser()->MSG_INFO(" Wjets   = ", WjetsSumWplusEY, "   (", WjetsSumWplusEY/DataSumWplusEY, ")");
  MSGUser()->MSG_INFO(" Yjets   = ", YjetsSumWplusEYori*ScaleWEY, "   (", YjetsSumWplusEYori * ScaleWEY /DataSumWplusEY, ")     ideally : ", YjetsSumWplusEY, "   (", YjetsSumWplusEY/DataSumWplusEY, ")");
  MSGUser()->MSG_INFO(" BKG     = ", BKGSumWplusEY, "   (", BKGSumWplusEY/DataSumWplusEY, ") in which :");
  double ZeeSumWplusEY = WYplotgroup->PositronEta.Channel_Map["Zee"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zee     = ", ZeeSumWplusEY, "   (", ZeeSumWplusEY/DataSumWplusEY, ")");
  double ZmumuSumWplusEY = WYplotgroup->PositronEta.Channel_Map["Zmumu"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zmumu   = ", ZmumuSumWplusEY, "   (", ZmumuSumWplusEY/DataSumWplusEY, ")");
  double ZtautauSumWplusEY = WYplotgroup->PositronEta.Channel_Map["Ztautau"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Ztautau = ", ZtautauSumWplusEY, "   (", ZtautauSumWplusEY/DataSumWplusEY, ")");
  double DibosonSumWplusEY = WYplotgroup->PositronEta.Channel_Map["Diboson"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Diboson = ", DibosonSumWplusEY, "   (", DibosonSumWplusEY/DataSumWplusEY, ")");
  double TopSumWplusEY = WYplotgroup->PositronEta.Channel_Map["Top"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Top     = ", TopSumWplusEY, "   (", TopSumWplusEY/DataSumWplusEY, ")");
  double ZYSumWplusEY = WYplotgroup->PositronEta.Channel_Map["ZY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" ZY      = ", ZYSumWplusEY, "   (", ZYSumWplusEY/DataSumWplusEY, ")");
  double WtvYSumWplusEY = WYplotgroup->PositronEta.Channel_Map["WtvY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" WtvY    = ", WtvYSumWplusEY, "   (", WtvYSumWplusEY/DataSumWplusEY, ")");
  
  double DataSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["Data"]->GetNominalHist()->Integral();
  double SignalSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["Signal"]->GetNominalHist()->Integral();
  double BKGSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["BKG"]->GetNominalHist()->Integral();
  double WjetsSumWminusEY = MyPlots->ElectronEta.Channel_Map["Wjets"]->GetNominalHist()->Integral();
  double YjetsSumWminusEYori = MyPlots->ElectronEta.Channel_Map["Yjets"]->GetNominalHist()->Integral();
  double YjetsSumWminusEY = DataSumWminusEY - SignalSumWminusEY - BKGSumWminusEY - WjetsSumWminusEY;
  MSGUser()->MSG_INFO(" W- SUMMARY of WEY (Electron)");
  MSGUser()->MSG_INFO(" Data    = ", DataSumWminusEY, "   Data - Wjets - Yjets - BKG = ", DataSumWminusEY - WjetsSumWminusEY - YjetsSumWminusEYori*ScaleWEY - BKGSumWminusEY, "   (", (DataSumWminusEY - WjetsSumWminusEY - YjetsSumWminusEYori*ScaleWEY - BKGSumWminusEY)/DataSumWminusEY, ")");
  MSGUser()->MSG_INFO(" Signal  = ", SignalSumWminusEY, "   (", SignalSumWminusEY/DataSumWminusEY, ")");
  MSGUser()->MSG_INFO(" Wjets   = ", WjetsSumWminusEY, "   (", WjetsSumWminusEY/DataSumWminusEY, ")");
  MSGUser()->MSG_INFO(" Yjets   = ", YjetsSumWminusEYori*ScaleWEY, "   (", YjetsSumWminusEYori * ScaleWEY /DataSumWminusEY, ")     ideally : ", YjetsSumWminusEY, "   (", YjetsSumWminusEY/DataSumWminusEY, ")");
  MSGUser()->MSG_INFO(" BKG     = ", BKGSumWminusEY, "   (", BKGSumWminusEY/DataSumWminusEY, ") in which :");
  double ZeeSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["Zee"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zee     = ", ZeeSumWminusEY, "   (", ZeeSumWminusEY/DataSumWminusEY, ")");
  double ZmumuSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["Zmumu"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zmumu   = ", ZmumuSumWminusEY, "   (", ZmumuSumWminusEY/DataSumWminusEY, ")");
  double ZtautauSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["Ztautau"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Ztautau = ", ZtautauSumWminusEY, "   (", ZtautauSumWminusEY/DataSumWminusEY, ")");
  double DibosonSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["Diboson"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Diboson = ", DibosonSumWminusEY, "   (", DibosonSumWminusEY/DataSumWminusEY, ")");
  double TopSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["Top"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Top     = ", TopSumWminusEY, "   (", TopSumWminusEY/DataSumWminusEY, ")");
  double ZYSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["ZY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" ZY      = ", ZYSumWminusEY, "   (", ZYSumWminusEY/DataSumWminusEY, ")");
  double WtvYSumWminusEY = WYplotgroup->ElectronEta.Channel_Map["WtvY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" WtvY    = ", WtvYSumWminusEY, "   (", WtvYSumWminusEY/DataSumWminusEY, ")");
  
  double DataSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["Data"]->GetNominalHist()->Integral();
  double SignalSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["Signal"]->GetNominalHist()->Integral();
  double BKGSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["BKG"]->GetNominalHist()->Integral();
  double WjetsSumWplusUY = MyPlots->AntiMuonEta.Channel_Map["Wjets"]->GetNominalHist()->Integral();
  double YjetsSumWplusUYori = MyPlots->AntiMuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral();
  double YjetsSumWplusUY = DataSumWplusUY - SignalSumWplusUY - BKGSumWplusUY - WjetsSumWplusUY;
  MSGUser()->MSG_INFO(" W+ SUMMARY of WUY (AntiMuon)");
  MSGUser()->MSG_INFO(" Data    = ", DataSumWplusUY, "   Data - Wjets - Yjets - BKG = ", DataSumWplusUY - WjetsSumWplusUY - YjetsSumWplusUYori*ScaleWUY - BKGSumWplusUY, "   (", (DataSumWplusUY - WjetsSumWplusUY - YjetsSumWplusUYori*ScaleWUY - BKGSumWplusUY)/DataSumWplusUY, ")");
  MSGUser()->MSG_INFO(" Signal  = ", SignalSumWplusUY, "   (", SignalSumWplusUY/DataSumWplusUY, ")");
  MSGUser()->MSG_INFO(" Wjets   = ", WjetsSumWplusUY, "   (", WjetsSumWplusUY/DataSumWplusUY, ")");
  MSGUser()->MSG_INFO(" Yjets   = ", YjetsSumWplusUYori*ScaleWUY, "   (", YjetsSumWplusUYori * ScaleWUY /DataSumWplusUY, ")     ideally : ", YjetsSumWplusUY, "   (", YjetsSumWplusUY/DataSumWplusUY, ")");
  MSGUser()->MSG_INFO(" BKG     = ", BKGSumWplusUY, "   (", BKGSumWplusUY/DataSumWplusUY, ") in which :");
  double ZeeSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["Zee"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zee     = ", ZeeSumWplusUY, "   (", ZeeSumWplusUY/DataSumWplusUY, ")");
  double ZmumuSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["Zmumu"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zmumu   = ", ZmumuSumWplusUY, "   (", ZmumuSumWplusUY/DataSumWplusUY, ")");
  double ZtautauSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["Ztautau"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Ztautau = ", ZtautauSumWplusUY, "   (", ZtautauSumWplusUY/DataSumWplusUY, ")");
  double DibosonSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["Diboson"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Diboson = ", DibosonSumWplusUY, "   (", DibosonSumWplusUY/DataSumWplusUY, ")");
  double TopSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["Top"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Top     = ", TopSumWplusUY, "   (", TopSumWplusUY/DataSumWplusUY, ")");
  double ZYSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["ZY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" ZY      = ", ZYSumWplusUY, "   (", ZYSumWplusUY/DataSumWplusUY, ")");
  double WtvYSumWplusUY = WYplotgroup->AntiMuonEta.Channel_Map["WtvY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" WtvY    = ", WtvYSumWplusUY, "   (", WtvYSumWplusUY/DataSumWplusUY, ")");
  
  double DataSumWminusUY = WYplotgroup->MuonEta.Channel_Map["Data"]->GetNominalHist()->Integral();
  double SignalSumWminusUY = WYplotgroup->MuonEta.Channel_Map["Signal"]->GetNominalHist()->Integral();
  double BKGSumWminusUY = WYplotgroup->MuonEta.Channel_Map["BKG"]->GetNominalHist()->Integral();
  double WjetsSumWminusUY = MyPlots->MuonEta.Channel_Map["Wjets"]->GetNominalHist()->Integral();
  double YjetsSumWminusUYori = MyPlots->MuonEta.Channel_Map["Yjets"]->GetNominalHist()->Integral();
  double YjetsSumWminusUY = DataSumWminusUY - SignalSumWminusUY - BKGSumWminusUY - WjetsSumWminusUY;
  MSGUser()->MSG_INFO(" W- SUMMARY of WUY (Muon)");
  MSGUser()->MSG_INFO(" Data    = ", DataSumWminusUY, "   Data - Wjets - Yjets - BKG = ", DataSumWminusUY - WjetsSumWminusUY - YjetsSumWminusUYori*ScaleWUY - BKGSumWminusUY, "   (", (DataSumWminusUY - WjetsSumWminusUY - YjetsSumWminusUYori*ScaleWUY - BKGSumWminusUY)/DataSumWminusUY, ")");
  MSGUser()->MSG_INFO(" Signal  = ", SignalSumWminusUY, "   (", SignalSumWminusUY/DataSumWminusUY, ")");
  MSGUser()->MSG_INFO(" Wjets   = ", WjetsSumWminusUY, "   (", WjetsSumWminusUY/DataSumWminusUY, ")");
  MSGUser()->MSG_INFO(" Yjets   = ", YjetsSumWminusUYori*ScaleWUY, "   (", YjetsSumWminusUYori * ScaleWUY /DataSumWminusUY, ")     ideally : ", YjetsSumWminusUY, "   (", YjetsSumWminusUY/DataSumWminusUY, ")");
  MSGUser()->MSG_INFO(" BKG     = ", BKGSumWminusUY, "   (", BKGSumWminusUY/DataSumWminusUY, ") in which :");
  double ZeeSumWminusUY = WYplotgroup->MuonEta.Channel_Map["Zee"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zee     = ", ZeeSumWminusUY, "   (", ZeeSumWminusUY/DataSumWminusUY, ")");
  double ZmumuSumWminusUY = WYplotgroup->MuonEta.Channel_Map["Zmumu"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Zmumu   = ", ZmumuSumWminusUY, "   (", ZmumuSumWminusUY/DataSumWminusUY, ")");
  double ZtautauSumWminusUY = WYplotgroup->MuonEta.Channel_Map["Ztautau"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Ztautau = ", ZtautauSumWminusUY, "   (", ZtautauSumWminusUY/DataSumWminusUY, ")");
  double DibosonSumWminusUY = WYplotgroup->MuonEta.Channel_Map["Diboson"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Diboson = ", DibosonSumWminusUY, "   (", DibosonSumWminusUY/DataSumWminusUY, ")");
  double TopSumWminusUY = WYplotgroup->MuonEta.Channel_Map["Top"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" Top     = ", TopSumWminusUY, "   (", TopSumWminusUY/DataSumWminusUY, ")");
  double ZYSumWminusUY = WYplotgroup->MuonEta.Channel_Map["ZY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" ZY      = ", ZYSumWminusUY, "   (", ZYSumWminusUY/DataSumWminusUY, ")");
  double WtvYSumWminusUY = WYplotgroup->MuonEta.Channel_Map["WtvY"]->GetNominalHist()->Integral();
  MSGUser()->MSG_INFO(" WtvY    = ", WtvYSumWminusUY, "   (", WtvYSumWminusUY/DataSumWminusUY, ")");
  
  MSGUser()->MSG_INFO("Scale Yjets BKG plots of WEY by ", ScaleWEY, " & Scale Yjets BKG plots of WUY by ", ScaleWUY);
  MyPlots->RescaleYjets(ScaleWEY, ScaleWUY);
}

  MyPlots->WriteToFile(); 
  MSGUser()->MSG_INFO("Run Fake-LY-BKG Reweighting Finished");
}

