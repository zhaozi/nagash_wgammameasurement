#include "NAGASH.h"
#include "ZHZHTimer.h"
#include "WYPlotGroup.h"
#include "DrawWYPlots.h"

using namespace NAGASH;
using namespace std;

int main(int argc, char **argv)
{
  if (argc != 2)
  {
    cout << "Usage: " << argv[0] << " (config file)" << endl;
    return 0;
  }
  Analysis MyAnalysis(argv[1]);

  ZHZHTimer MyTimer(MyAnalysis.MSGUser());
  TString TimerRecord;
  TimerRecord = "(ROCK and ROLL)";
  MyTimer.FirstPR(TimerRecord);

  MyAnalysis.SubmitJob<DrawWYPlots>(false);
  MyAnalysis.Join();
  
  TimerRecord = "(To Be Continue)";
  MyTimer.PRD(TimerRecord);
  return 1;
}
