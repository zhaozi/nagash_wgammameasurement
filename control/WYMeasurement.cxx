#include "NAGASH.h"
#include "ZHZHTimer.h"
#include "WYLoopEvent.h"
#include "LoopForSumW.h"
#include "DrawWYPlots.h"
#include "LYFakeBKG.h"

#include <sys/stat.h>
#include <errno.h>

using namespace NAGASH;
using namespace std;

int main(int argc, char **argv)
{
  if (argc != 2)
  {
    cout << "Usage: " << argv[0] << " (config file)" << endl;
    return 0;
  }
  Analysis MyAnalysis(argv[1]);
  ZHZHTimer MyTimer(MyAnalysis.MSGUser());
  MyTimer.FirstPR(TString::Format("ROCK AND ROLL"));

  if (MyAnalysis.ConfigUser()->GetPar<bool>("LOOPFORSUMW"))
  {
    MyTimer.PRD(TString::Format("Start to Loop for Sum of Weights"));
    auto MyResultsForSumW = MyAnalysis.ProcessEventLoop<LoopForSumW>(MyAnalysis.ConfigUser()->GetPar<TString>("SampleList"));
    MyTimer.PRD(TString::Format("To Be Continued"));
    return 1;
  }
  else 
  {
    MyTimer.PRD(TString::Format("Start to Loop Events"));
    auto MyResults = MyAnalysis.ProcessEventLoop<WYLoopEvent>(MyAnalysis.ConfigUser()->GetPar<TString>("SampleList"));
    
    auto MyDataSet = MyResults->Get<WYDataSet>("AllEvents");
    auto MyPlotGroup = MyResults->Get<WYPlotGroup>("WYPlots");
    
    MyTimer.PRD(TString::Format("Start to Estimate Wjets & Yjets Backgroud"));
    auto BKGResults = MyAnalysis.SubmitJob<LYFakeBKG>(true, MyDataSet, MyPlotGroup);
    MyAnalysis.Join();
    
    MyTimer.PRD(TString::Format("Start to Draw Figures"));
    MyAnalysis.SubmitJob<DrawWYPlots>(false);
    MyAnalysis.Join();
  }
  MyTimer.PRD(TString::Format("To Be Continued"));
  return 1;
}
