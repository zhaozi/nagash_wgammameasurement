#ifndef LYFAKEBKG
#define LYFAKEBKG

#include "NAGASH.h"
#include "WYEvent.h"
#include "WYPlotGroup.h"

using namespace NAGASH;
using namespace std;

/*class LYFakeBKGHelper
{
public:
  LYFakeBKGHelper(const ConfigTool &config);
  
  bool doMUYYJetsBKGMode = 1; //  1-ABCD  2-Template Fitting
  GetReweightSF(double valx, double valy, std::shared_ptr<Plot2D> sf_2D)
  {
    MSG_DEBUG("valx = ", valx, " valy = ", valy, " BinIndex = ", sf_2D->FindBin(valx, valy), " SF = ", sf_2D->GetBinContent(sf_2D->FindBin(valx, valy)));
    return sf_2D->GetBinContent(sf_2D->FindBin(valx, valy));
  };
};*/

class LYFakeBKG : public Job
{
public:
  int EstimateYjetsMode = 2; // 1-ABCD  2-Template Fitting 
  std::shared_ptr<WYPlotGroup> MyPlots;
  
  LYFakeBKG(const ConfigTool &config) : Job(config) {};
  static StatusCode Process(const ConfigTool        &config,
                            shared_ptr<ResultGroup> result,
                            shared_ptr<WYDataSet>   AllSample,
                            shared_ptr<WYPlotGroup> WYplotgroup);
  void RunReweighting(shared_ptr<WYDataSet>   AllSample,
                      shared_ptr<WYPlotGroup> WYplotgroup);

  /*std::shared_ptr<STWDataSetSC> WECY_WJETCR_TLSampleSC;
  std::shared_ptr<STWDataSetSC> WMUY_WJETCR_TLSampleSC;
  std::shared_ptr<STWDataSetSC> WECY_YJETCR_LTSampleSC;
  std::shared_ptr<STWDataSetSC> WMUY_YJETCR_LTSampleSC;*/

};

#endif

