#ifndef ZHZHTIMER_HEADER
#define ZHZHTIMER_HEADER

#include "NAGASH.h"

using namespace std;
using namespace NAGASH;

class ZHZHTimer : public Timer
{
public:
    ZHZHTimer(std::shared_ptr<MSGTool> msg, const TString &name = "Timer") : Timer(msg, name) {}
    TString FirstRecord = "";
    void FirstPR(const TString &prefix = "")
    {
        FirstRecord = prefix;
        PrintCurrent(prefix);
        Record(prefix);
    };
    void PRD(const TString &prefix = "")
    {
        PrintCurrent(prefix);
        Record(prefix);
        Duration(FirstRecord, prefix);
    };
};

#endif
