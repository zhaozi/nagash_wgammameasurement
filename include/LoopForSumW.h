#ifndef LOOPFORSUMW_HEADER
#define LOOPFORSUMW_HEADER

#include "NAGASH.h"
#include "MCSetsInfo.h"

using namespace NAGASH;
using namespace std;

class SumW : public Result
{
public:
    SumW(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname)
        : Result(MSG, c, rname, fname) {}

    void Combine(std::shared_ptr<Result> result)
    {
        auto subsumw = std::dynamic_pointer_cast<std::remove_pointer<decltype(this)>::type>(result);
        if (subsumw != nullptr)
        {
            this->DataTotalEvents += subsumw->DataTotalEvents;
            this->Map_xAODFileName_SumOfWeight.insert(subsumw->Map_xAODFileName_SumOfWeight.begin(), subsumw->Map_xAODFileName_SumOfWeight.end());
            this->Map_xAODFileName_Channel.insert(subsumw->Map_xAODFileName_Channel.begin(), subsumw->Map_xAODFileName_Channel.end());
            this->Map_Channel_SumOfWeight.insert(subsumw->Map_Channel_SumOfWeight.begin(), subsumw->Map_Channel_SumOfWeight.end());
        }
    }

    void WriteToFile()
    {
        std::ofstream outfile;
        outfile.open(ConfigUser()->GetPar<TString>("OutputSumWFileName"), std::ios::out);

        if (!Map_Channel_SumOfWeight.empty())
        {
            std::map<int, double> Map_Channel_SumOfWeight_Me;
            // for each mc channel, get the sum of weights
            for (auto &t : Map_Channel_SumOfWeight)
            {
                //outfile << "  Channel : " << t.first << " Sum of Weight : " << std::setprecision(15) << t.second << " " << std::endl;
                Map_Channel_SumOfWeight_Me.insert(std::pair<int, double>(t.first, 0));
            }

            for (auto &t : Map_xAODFileName_Channel)
            {
                //outfile << "  xAODFileName : " << t.first << " Sum of Weight : " << std::setprecision(15) << Map_xAODFileName_SumOfWeight.find(t.first)->second << " " << std::endl;
                Map_Channel_SumOfWeight_Me.find(t.second)->second += Map_xAODFileName_SumOfWeight.find(t.first)->second;
            }

            for (auto &t : Map_Channel_SumOfWeight)
            {
                //outfile << "  Channel : " << t.first << " Sum of Weight : " << std::setprecision(15) << t.second << " "
                //        << " Input Sum of Weight : " << Map_Channel_SumOfWeight_Me.find(t.first)->second << std::endl;
                outfile << t.first << " "  << std::setprecision(15) << Map_Channel_SumOfWeight_Me.find(t.first)->second << std::endl;
            }
        }

        //outfile << "  Data : " << this->DataTotalEvents << " events" << std::endl;
        outfile.close();
    }

    std::map<std::string, double> Map_xAODFileName_SumOfWeight;
    std::map<std::string, int> Map_xAODFileName_Channel;
    std::map<int, double> Map_Channel_SumOfWeight;
    int DataTotalEvents = 0;
};

class LoopForSumW : public NAGASH::LoopEvent
{
public:
    int ChannelNumber = 0;
    double SumOfWeights_Channel = 0;

    LoopForSumW(const ConfigTool &config) : NAGASH::LoopEvent(config) {}
    virtual StatusCode InitializeData() override;
    virtual StatusCode InitializeUser() override;
    virtual StatusCode Execute() override;
    virtual StatusCode Finalize() override;
    bool IsUselessMC();

    bool isData = false;
    bool isMC = false;
    std::shared_ptr<SumW> MySumW;

   // Declaration of leaf types
   double          SumOfWeights_double;
   double          SumOfWeightsSquared_double;
   double          TotalEventProcessed_double;
   
   Float_t         SumOfWeights;
   Float_t         SumOfWeightsSquared;
   Float_t         TotalEventProcessed;
   std::string     *xAODFileName;

   // List of branches
   TBranch        *b_SumOfWeights;   //!
   TBranch        *b_SumOfWeightsSquared;   //!
   TBranch        *b_TotalEventProcessed;   //!
   TBranch        *b_xAODFileName;   //!
};

inline bool LoopForSumW::IsUselessMC()
{
   if (ChannelNumber == 361600 || ChannelNumber == 361601 || ChannelNumber == 361602 ||
       ChannelNumber == 361603 || ChannelNumber == 361604 || ChannelNumber == 361605 ||
       ChannelNumber == 361606 || ChannelNumber == 361607 || ChannelNumber == 361608 ||
       ChannelNumber == 361609 || ChannelNumber == 361610 ||
       ChannelNumber == 363360 || ChannelNumber == 363359 || ChannelNumber == 426131)
   {
      MSGUser()->MSG_INFO("Channel Number = ", ChannelNumber, " is Useless in STW analysis.");
      return true;
   }
   else if (ChannelNumber == 361100 || ChannelNumber == 361101 || ChannelNumber == 361102 ||
            ChannelNumber == 361103 || ChannelNumber == 361104 || ChannelNumber == 361105 || // W
            ChannelNumber == 361106 || ChannelNumber == 361107 || ChannelNumber == 361108 || // Z
            ChannelNumber == 363356 || ChannelNumber == 363357 || ChannelNumber == 363358 ||
            ChannelNumber == 363489 || ChannelNumber == 364253 || ChannelNumber == 364254 || // Diboson
            ChannelNumber == 364255 || ChannelNumber == 410470 || ChannelNumber == 410644 ||
            ChannelNumber == 410645 || ChannelNumber == 410646 || ChannelNumber == 410647 ||
            ChannelNumber == 410658 || ChannelNumber == 410659 || ChannelNumber == 364250 || // t
            ChannelNumber == 700402 || ChannelNumber == 700403) // WY
   {
      return false;
   }
   else if (ChannelNumber == 364100 || ChannelNumber == 364101 || ChannelNumber == 364102 ||
            ChannelNumber == 364103 || ChannelNumber == 364104 || ChannelNumber == 364105 ||
            ChannelNumber == 364114 || ChannelNumber == 364115 || ChannelNumber == 364116 ||
            ChannelNumber == 364117 || ChannelNumber == 364118 || ChannelNumber == 364119 ||
            ChannelNumber == 364128 || ChannelNumber == 364129 || ChannelNumber == 364130 ||
            ChannelNumber == 364131 || ChannelNumber == 364132 || ChannelNumber == 364133)
   {
      return false;
      MSGUser()->MSG_INFO("SHERPA Signal Channel Number = ", ChannelNumber, " is Useless.");
      return true;
   }
   else
   { 
      MSGUser()->MSG_INFO("Channel Number = ", ChannelNumber, " is Useless.");
      return true;
   } 
}

inline NAGASH::StatusCode LoopForSumW::InitializeData()
{
   // The InitializeData() function is used to open ROOT file, get TTree into
   // RootTreeUser. And call TTree::SetBranchAddress to set the address of variables
   // User need not to modify this function.

    RootFileUser()->GetObject("xAODInfo_Tree", RootTreeUser());
    if (RootTreeUser() == nullptr)
    {
        MSGUser()->MSG(NAGASH::MSGLevel::ERROR,"Wrong TTree name");
        return NAGASH::StatusCode::FAILURE;
    }

   // Set object pointer
   xAODFileName = 0;
   // Set branch addresses and branch pointers
   RootTreeUser()->SetBranchAddress("SumOfWeights", &SumOfWeights, &b_SumOfWeights);
   RootTreeUser()->SetBranchAddress("SumOfWeightsSquared", &SumOfWeightsSquared, &b_SumOfWeightsSquared);
   RootTreeUser()->SetBranchAddress("TotalEventProcessed", &TotalEventProcessed, &b_TotalEventProcessed);
   RootTreeUser()->SetBranchAddress("xAODFileName", &xAODFileName, &b_xAODFileName);

    return NAGASH::StatusCode::SUCCESS;
}

inline StatusCode LoopForSumW::InitializeUser()
{
    MCSetsInfo info(MSGUser(), ConfigUser()->GetPar<TString>("MCINFO_FILENAME").Data());
    ChannelNumber = info.GetChannelNumber(InputRootFileName().Data());
    
    //MSGUser()->MSG_INFO("ChannelNumber = ", ChannelNumber);
    IsUselessMC();
    
    if (InputRootFileName().Contains("MC16"))
        isMC = true;
    else
        isData = true;

    MySumW = ResultGroupUser()->BookResult<SumW>("MySumW", ConfigUser()->GetPar<TString>("OutputSumWFileName"));

    return StatusCode::SUCCESS;
}

inline StatusCode LoopForSumW::Execute()
{
    SumOfWeights_double = SumOfWeights;
    SumOfWeightsSquared_double = SumOfWeightsSquared;
    TotalEventProcessed_double = TotalEventProcessed;
    if (isMC)
    {
        //MSGUser()->MSG_INFO("ChannelNumber = ", ChannelNumber, " xAODFileName = ", *xAODFileName, " SumOfWeights = ", SumOfWeights);
        MySumW->Map_xAODFileName_SumOfWeight.insert(std::pair<std::string, double>(*xAODFileName, SumOfWeights_double));
        MySumW->Map_xAODFileName_Channel.insert(std::pair<std::string, int>(*xAODFileName, ChannelNumber));
        MySumW->Map_Channel_SumOfWeight.insert(std::pair<int, double>(ChannelNumber, SumOfWeights_Channel));
    }

    if (isData)
    {
        MySumW->DataTotalEvents += 1;
    }

    return StatusCode::SUCCESS;
}

inline StatusCode LoopForSumW::Finalize()
{
    MySumW->WriteToFile();
    return StatusCode::SUCCESS;
}

#endif



