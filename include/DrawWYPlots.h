#ifndef DRAWWYPLOTS
#define DRAWWYPLOTS

#include "NAGASH.h"
#include "WYPlotGroup.h"

#define PLOTSNUM 11

class DrawWYPlots : public Job
{
public:
 TString YEAR = "Run2";
 shared_ptr<WYPlotGroup> WYplotgroup;
 shared_ptr<WYPlotGroup> LYFakeplotgroup;
 DrawWYPlots(const ConfigTool &config) : Job(config){};
 static StatusCode Process(const ConfigTool &config,
                           shared_ptr<ResultGroup> result)
 {
  DrawWYPlots myjob(config);
  result = myjob.ResultGroupUser();
  myjob.Init();
  myjob.Draw();
  myjob.LYFakeDraw();
  return NAGASH::StatusCode::SUCCESS;
 };
 void Init()
 {
   YEAR = ConfigUser()->GetPar<TString>("YEAR");
   
   WYplotgroup = make_shared<WYPlotGroup>(MSGUser(), ConfigUser(), "WYPlots"); 
   TString WYPlotsFileName = ConfigUser()->GetPar<TString>("DRAW_WYPLOTS_FILENAME");
   shared_ptr<TFileHelper> WYinfile = make_shared<TFileHelper>(MSGUser(), WYPlotsFileName.Data());
   WYplotgroup->Recover(WYinfile);
   
   LYFakeplotgroup = make_shared<WYPlotGroup>(MSGUser(), ConfigUser(), "LYFake"); 
   TString LYFakePlotsFileName2 = ConfigUser()->GetPar<TString>("DRAW_LYFAKE_FILENAME");
   shared_ptr<TFileHelper> LYFakeinfile2 = make_shared<TFileHelper>(MSGUser(), LYFakePlotsFileName2.Data());   
   LYFakeplotgroup->Recover(LYFakeinfile2);    
 };

 void Draw()
 {
  TString FigureName[PLOTSNUM*4];

  TString XName[PLOTSNUM*4] = {"ElectronPt", "ElectronEta", "PhotonPt_WminusEY", "PhotonEta_WminusEY", "LYMass_WminusEY", "LYDeltaPhi_WminusEY", "LYDeltaR_WminusEY",
                                                                      "MET_WminusEY", "MT_WminusEY", "FabsEtaElectron_WminusLarger", "FabsEtaElectron_WminusSmaller",
                               "PositronPt", "PositronEta", "PhotonPt_WplusEY", "PhotonEta_WplusEY", "LYMass_WplusEY", "LYDeltaPhi_WplusEY", "LYDeltaR_WplusEY",
                                                                     "MET_WplusEY", "MT_WplusEY", "FabsEtaPositron_WplusLarger", "FabsEtaPositron_WplusSmaller",
                               "MuonPt", "MuonEta", "PhotonPt_WminusUY", "PhotonEta_WminusUY", "LYMass_WminusUY", "LYDeltaPhi_WminusUY", "LYDeltaR_WminusUY",
                                                                      "MET_WminusUY", "MT_WminusUY", "FabsEtaMuon_WminusLarger", "FabsEtaMuon_WminusSmaller",
                               "AntiMuonPt", "AntiMuonEta", "PhotonPt_WplusUY", "PhotonEta_WplusUY", "LYMass_WplusUY", "LYDeltaPhi_WplusUY", "LYDeltaR_WplusUY",
                                                                     "MET_WplusUY", "MT_WplusUY", "FabsEtaAntiMuon_WplusLarger", "FabsEtaAntiMuon_WplusSmaller"};

  TString YName = "Number of Events";
  TString ChannelNames[3] = {"Data", "Signal", "BKG"};
  #define CHANNELNUM 10
  TString MultiChannelNames[CHANNELNUM] = {"Data", "Signal", "WPlusJet", "Zee", "Zmumu", "Ztautau", "Diboson", "Top", "ZY", "WtvY"};
  TString MultiLegendNames[CHANNELNUM]  = {"Data", "Signal", "W+Jet", "Zee", "Z#mu#mu", "Z#tau#tau", "Diboson", "Top", "Z#gamma", "W#tau#nu#gamma"};
  TString FigureDir = "figure/oldBKG/";
  TString Eps = ".eps";
  shared_ptr<NGFigure> figure;

  for (int j = 0; j < PLOTSNUM*4; j++)
  {
   FigureName[j] = FigureDir + XName[j] + Eps;
   figure = ToolkitUser()->GetTool<NGFigure>(FigureName[j], XName[j], YName);
   for (int i = 0; i < CHANNELNUM; i++)
   {
    if (j == 0)       figure->SetInputHist(WYplotgroup->ElectronPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(WYplotgroup->ElectronEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(WYplotgroup->PhotonPt_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(WYplotgroup->PhotonEta_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(WYplotgroup->LYMass_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(WYplotgroup->LYDeltaPhi_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(WYplotgroup->LYDeltaR_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 7)  figure->SetInputHist(WYplotgroup->MET_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(WYplotgroup->MT_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(WYplotgroup->FabsEtaElectron_WminusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10)  figure->SetInputHist(WYplotgroup->FabsEtaElectron_WminusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 11)  figure->SetInputHist(WYplotgroup->PositronPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12)  figure->SetInputHist(WYplotgroup->PositronEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13)  figure->SetInputHist(WYplotgroup->PhotonPt_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 14)  figure->SetInputHist(WYplotgroup->PhotonEta_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15)  figure->SetInputHist(WYplotgroup->LYMass_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(WYplotgroup->LYDeltaPhi_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(WYplotgroup->LYDeltaR_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(WYplotgroup->MET_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(WYplotgroup->MT_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(WYplotgroup->FabsEtaPositron_WplusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 21) figure->SetInputHist(WYplotgroup->FabsEtaPositron_WplusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 22) figure->SetInputHist(WYplotgroup->MuonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 23) figure->SetInputHist(WYplotgroup->MuonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 24) figure->SetInputHist(WYplotgroup->PhotonPt_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 25) figure->SetInputHist(WYplotgroup->PhotonEta_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 26) figure->SetInputHist(WYplotgroup->LYMass_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 27) figure->SetInputHist(WYplotgroup->LYDeltaPhi_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 28) figure->SetInputHist(WYplotgroup->LYDeltaR_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 29) figure->SetInputHist(WYplotgroup->MET_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 30) figure->SetInputHist(WYplotgroup->MT_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 31) figure->SetInputHist(WYplotgroup->FabsEtaMuon_WminusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 32) figure->SetInputHist(WYplotgroup->FabsEtaMuon_WminusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 33) figure->SetInputHist(WYplotgroup->AntiMuonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 34) figure->SetInputHist(WYplotgroup->AntiMuonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 35) figure->SetInputHist(WYplotgroup->PhotonPt_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 36) figure->SetInputHist(WYplotgroup->PhotonEta_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 37) figure->SetInputHist(WYplotgroup->LYMass_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 38) figure->SetInputHist(WYplotgroup->LYDeltaPhi_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 39) figure->SetInputHist(WYplotgroup->LYDeltaR_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 40) figure->SetInputHist(WYplotgroup->MET_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 41) figure->SetInputHist(WYplotgroup->MT_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 42) figure->SetInputHist(WYplotgroup->FabsEtaAntiMuon_WplusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 43) figure->SetInputHist(WYplotgroup->FabsEtaAntiMuon_WplusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
   }
   figure->SetHeaderName("ATLAS Internal " + YEAR);
   //figure->SetSubYRange(0.91, 1.09);
   figure->SetMode("HSTACK", "RATIO", "#frac{Data}{Pred.}");
   //figure->SetShowChi2();
   figure->SetLegendColumn(3);
   figure->SetNotNorm();
   figure->DrawFigure();
   figure = 0;
  }
  
  TString WEY_YJETCR_XName[7*3] = {"WEY_YJETCR_LL_LeptonPt", "WEY_YJETCR_LL_LeptonEta", "WEY_YJETCR_LL_PhotonPt", "WEY_YJETCR_LL_PhotonEta", 
                                   "WEY_YJETCR_LL_MET", "WEY_YJETCR_LL_MT", "WEY_YJETCR_LL_LYMass",
                                   "WEY_YJETCR_LT_LeptonPt", "WEY_YJETCR_LT_LeptonEta", "WEY_YJETCR_LT_PhotonPt", "WEY_YJETCR_LT_PhotonEta", 
                                   "WEY_YJETCR_LT_MET", "WEY_YJETCR_LT_MT", "WEY_YJETCR_LT_LYMass",
                                   "WEY_YJETCR_TL_LeptonPt", "WEY_YJETCR_TL_LeptonEta", "WEY_YJETCR_TL_PhotonPt", "WEY_YJETCR_TL_PhotonEta",
                                   "WEY_YJETCR_TL_MET", "WEY_YJETCR_TL_MT", "WEY_YJETCR_TL_LYMass"}; 
  for (int j = 0; j < 7*3; j++)
  {
   FigureName[j] = FigureDir + WEY_YJETCR_XName[j] + Eps;
   figure = ToolkitUser()->GetTool<NGFigure>(FigureName[j], WEY_YJETCR_XName[j], YName);
   for (int i = 0; i < CHANNELNUM; i++)
   {
    if (j == 0)       figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 7)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10) figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 11) figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12) figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13) figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 14) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
   }
   figure->SetHeaderName("ATLAS Internal " + YEAR);
   //figure->SetSubYRange(0.91, 1.09);
   figure->SetMode("HSTACK", "RATIO", "#frac{Data}{Pred.}");
   //figure->SetShowChi2();
   figure->SetLegendColumn(3);
   figure->SetNotNorm();
   figure->DrawFigure();
   figure = 0;
  }

  TString WUY_YJETCR_XName[7*3] = {"WUY_YJETCR_LL_LeptonPt", "WUY_YJETCR_LL_LeptonEta", "WUY_YJETCR_LL_PhotonPt", "WUY_YJETCR_LL_PhotonEta", 
                                   "WUY_YJETCR_LL_MET", "WUY_YJETCR_LL_MT", "WUY_YJETCR_LL_LYMass",
                                   "WUY_YJETCR_LT_LeptonPt", "WUY_YJETCR_LT_LeptonEta", "WUY_YJETCR_LT_PhotonPt", "WUY_YJETCR_LT_PhotonEta", 
                                   "WUY_YJETCR_LT_MET", "WUY_YJETCR_LT_MT", "WUY_YJETCR_LT_LYMass",
                                   "WUY_YJETCR_TL_LeptonPt", "WUY_YJETCR_TL_LeptonEta", "WUY_YJETCR_TL_PhotonPt", "WUY_YJETCR_TL_PhotonEta",
                                   "WUY_YJETCR_TL_MET", "WUY_YJETCR_TL_MT", "WUY_YJETCR_TL_LYMass"}; 
  for (int j = 0; j < 7*3; j++)
  {
   FigureName[j] = FigureDir + WUY_YJETCR_XName[j] + Eps;
   figure = ToolkitUser()->GetTool<NGFigure>(FigureName[j], WUY_YJETCR_XName[j], YName);
   for (int i = 0; i < CHANNELNUM; i++)
   {
    if (j == 0)       figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 7)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10) figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 11) figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12) figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13) figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 14) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
   }
   figure->SetHeaderName("ATLAS Internal " + YEAR);
   //figure->SetSubYRange(0.91, 1.09);
   figure->SetMode("HSTACK", "RATIO", "#frac{Data}{Pred.}");
   //figure->SetShowChi2();
   figure->SetLegendColumn(3);
   figure->SetNotNorm();
   figure->DrawFigure();
   figure = 0;
  }


  TString FigureNameAsy[4];
  TString NameAsy[4] = {"AsymmetryFabsEtaElectron_Wminus",
                        "AsymmetryFabsEtaPositron_Wplus",
                        "AsymmetryFabsEtaMuon_Wminus",
                        "AsymmetryFabsEtaAntiMuon_Wplus"};
  TString XNameAsy = "#eta_{l}";
  TString YNameAsy = "Asymmetry";
  for (int j = 0; j < 4; j++)
  {
   FigureNameAsy[j] = FigureDir + NameAsy[j] + Eps;
   figure = ToolkitUser()->GetTool<NGFigure>(FigureNameAsy[j], XNameAsy, YNameAsy);
   if (j == 0) {
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->GetNominalHist(), TString::Format("Data"));
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaElectron_Wminus.Channel_Map["Signal"]->GetNominalHist(), TString::Format("Signal"));
   }
   if (j == 1) {
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->GetNominalHist(), TString::Format("Data"));
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaPositron_Wplus.Channel_Map["Signal"]->GetNominalHist(), TString::Format("Signal"));
   }
   if (j == 2) {
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->GetNominalHist(), TString::Format("Data"));
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaMuon_Wminus.Channel_Map["Signal"]->GetNominalHist(), TString::Format("Signal"));
   }
   if (j == 3) {
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->GetNominalHist(), TString::Format("Data"));
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Signal"]->GetNominalHist(), TString::Format("Signal"));
   }
   figure->SetHeaderName("ATLAS Internal " + YEAR);
   //figure->SetSubYRange(0.91, 1.09);
   figure->SetMode("MULTI", "PULL", "#frac{#Delta}{#sigma}");
   figure->SetShowChi2();
   figure->SetNotNorm();
   figure->DrawFigure();
   figure = 0;
  }
 };
 
 void LYFakeDraw()
 {
  TString FigureName[PLOTSNUM*4];

  TString XName[PLOTSNUM*4] = {"ElectronPt", "ElectronEta", "PhotonPt_WminusEY", "PhotonEta_WminusEY", "LYMass_WminusEY", "LYDeltaPhi_WminusEY", "LYDeltaR_WminusEY", "MET_WminusEY", "MT_WminusEY", "FabsEtaElectron_WminusLarger", "FabsEtaElectron_WminusSmaller",
                               "PositronPt", "PositronEta", "PhotonPt_WplusEY", "PhotonEta_WplusEY", "LYMass_WplusEY", "LYDeltaPhi_WplusEY", "LYDeltaR_WplusEY", "MET_WplusEY", "MT_WplusEY", "FabsEtaPositron_WplusLarger", "FabsEtaPositron_WplusSmaller",
                               "MuonPt", "MuonEta", "PhotonPt_WminusUY", "PhotonEta_WminusUY", "LYMass_WminusUY", "LYDeltaPhi_WminusUY", "LYDeltaR_WminusUY", "MET_WminusUY", "MT_WminusUY", "FabsEtaMuon_WminusLarger", "FabsEtaMuon_WminusSmaller",
                               "AntiMuonPt", "AntiMuonEta", "PhotonPt_WplusUY", "PhotonEta_WplusUY", "LYMass_WplusUY", "LYDeltaPhi_WplusUY", "LYDeltaR_WplusUY", "MET_WplusUY", "MT_WplusUY", "FabsEtaAntiMuon_WplusLarger", "FabsEtaAntiMuon_WplusSmaller"};
  TString YName = "Number of Events";
  TString ChannelNames[3]      = {"Data", "Signal", "BKG"};
  TString MultiChannelNames[CHANNELNUM+1] = {"Data", "Signal", "Wjets", "Yjets", "Zee", "Zmumu", "Ztautau", "Diboson", "Top", "ZY", "WtvY"};
  TString MultiLegendNames[CHANNELNUM+1] = {"Data", "Signal", "W+jets", "#gamma+jets", "Zee", "Z#mu#mu", "Z#tau#tau", "Diboson", "Top", "Z#gamma", "W#tau#nu#gamma"};
  TString FigureDir = "figure/LYFakeBKG/LYFake_";
  TString Eps = ".eps";
  shared_ptr<NGFigure> figure;

  for (int j = 0; j < PLOTSNUM*4; j++)
  {
   FigureName[j] = FigureDir + XName[j] + Eps;
   figure = ToolkitUser()->GetTool<NGFigure>(FigureName[j], XName[j], YName);
   for (int i = 0; i < CHANNELNUM+1; i++)
   {
  if (i != 2 && i != 3)
  {
    if (j == 0)       figure->SetInputHist(WYplotgroup->ElectronPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(WYplotgroup->ElectronEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(WYplotgroup->PhotonPt_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(WYplotgroup->PhotonEta_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(WYplotgroup->LYMass_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(WYplotgroup->LYDeltaPhi_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(WYplotgroup->LYDeltaR_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 7)  figure->SetInputHist(WYplotgroup->MET_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(WYplotgroup->MT_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(WYplotgroup->FabsEtaElectron_WminusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10)  figure->SetInputHist(WYplotgroup->FabsEtaElectron_WminusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 11)  figure->SetInputHist(WYplotgroup->PositronPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12)  figure->SetInputHist(WYplotgroup->PositronEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13)  figure->SetInputHist(WYplotgroup->PhotonPt_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 14)  figure->SetInputHist(WYplotgroup->PhotonEta_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15)  figure->SetInputHist(WYplotgroup->LYMass_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(WYplotgroup->LYDeltaPhi_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(WYplotgroup->LYDeltaR_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(WYplotgroup->MET_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(WYplotgroup->MT_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(WYplotgroup->FabsEtaPositron_WplusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 21) figure->SetInputHist(WYplotgroup->FabsEtaPositron_WplusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 22) figure->SetInputHist(WYplotgroup->MuonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 23) figure->SetInputHist(WYplotgroup->MuonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 24)  figure->SetInputHist(WYplotgroup->PhotonPt_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 25)  figure->SetInputHist(WYplotgroup->PhotonEta_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 26)  figure->SetInputHist(WYplotgroup->LYMass_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 27) figure->SetInputHist(WYplotgroup->LYDeltaPhi_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 28) figure->SetInputHist(WYplotgroup->LYDeltaR_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 29) figure->SetInputHist(WYplotgroup->MET_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 30) figure->SetInputHist(WYplotgroup->MT_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 31) figure->SetInputHist(WYplotgroup->FabsEtaMuon_WminusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 32) figure->SetInputHist(WYplotgroup->FabsEtaMuon_WminusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 33) figure->SetInputHist(WYplotgroup->AntiMuonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 34) figure->SetInputHist(WYplotgroup->AntiMuonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 35)  figure->SetInputHist(WYplotgroup->PhotonPt_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 36)  figure->SetInputHist(WYplotgroup->PhotonEta_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 37)  figure->SetInputHist(WYplotgroup->LYMass_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 38) figure->SetInputHist(WYplotgroup->LYDeltaPhi_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 39) figure->SetInputHist(WYplotgroup->LYDeltaR_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 40) figure->SetInputHist(WYplotgroup->MET_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 41) figure->SetInputHist(WYplotgroup->MT_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 42) figure->SetInputHist(WYplotgroup->FabsEtaAntiMuon_WplusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 43) figure->SetInputHist(WYplotgroup->FabsEtaAntiMuon_WplusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  }
  else if (i == 2 || i == 3)
  {
    if (j == 0)       figure->SetInputHist(LYFakeplotgroup->ElectronPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(LYFakeplotgroup->ElectronEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(LYFakeplotgroup->PhotonPt_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(LYFakeplotgroup->PhotonEta_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(LYFakeplotgroup->LYMass_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(LYFakeplotgroup->LYDeltaPhi_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(LYFakeplotgroup->LYDeltaR_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 7)  figure->SetInputHist(LYFakeplotgroup->MET_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(LYFakeplotgroup->MT_WminusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(LYFakeplotgroup->FabsEtaElectron_WminusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10) figure->SetInputHist(LYFakeplotgroup->FabsEtaElectron_WminusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 11) figure->SetInputHist(LYFakeplotgroup->PositronPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12) figure->SetInputHist(LYFakeplotgroup->PositronEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13) figure->SetInputHist(LYFakeplotgroup->PhotonPt_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 14) figure->SetInputHist(LYFakeplotgroup->PhotonEta_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15) figure->SetInputHist(LYFakeplotgroup->LYMass_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(LYFakeplotgroup->LYDeltaPhi_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(LYFakeplotgroup->LYDeltaR_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(LYFakeplotgroup->MET_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(LYFakeplotgroup->MT_WplusEY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(LYFakeplotgroup->FabsEtaPositron_WplusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 21) figure->SetInputHist(LYFakeplotgroup->FabsEtaPositron_WplusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 22) figure->SetInputHist(LYFakeplotgroup->MuonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 23) figure->SetInputHist(LYFakeplotgroup->MuonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 24) figure->SetInputHist(LYFakeplotgroup->PhotonPt_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 25) figure->SetInputHist(LYFakeplotgroup->PhotonEta_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 26) figure->SetInputHist(LYFakeplotgroup->LYMass_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 27) figure->SetInputHist(LYFakeplotgroup->LYDeltaPhi_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 28) figure->SetInputHist(LYFakeplotgroup->LYDeltaR_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 29) figure->SetInputHist(LYFakeplotgroup->MET_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 30) figure->SetInputHist(LYFakeplotgroup->MT_WminusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 31) figure->SetInputHist(LYFakeplotgroup->FabsEtaMuon_WminusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 32) figure->SetInputHist(LYFakeplotgroup->FabsEtaMuon_WminusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    
    else if (j == 33) figure->SetInputHist(LYFakeplotgroup->AntiMuonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 34) figure->SetInputHist(LYFakeplotgroup->AntiMuonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 35) figure->SetInputHist(LYFakeplotgroup->PhotonPt_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 36) figure->SetInputHist(LYFakeplotgroup->PhotonEta_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 37) figure->SetInputHist(LYFakeplotgroup->LYMass_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 38) figure->SetInputHist(LYFakeplotgroup->LYDeltaPhi_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 39) figure->SetInputHist(LYFakeplotgroup->LYDeltaR_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 40) figure->SetInputHist(LYFakeplotgroup->MET_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 41) figure->SetInputHist(LYFakeplotgroup->MT_WplusUY.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 42) figure->SetInputHist(LYFakeplotgroup->FabsEtaAntiMuon_WplusLarger.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 43) figure->SetInputHist(LYFakeplotgroup->FabsEtaAntiMuon_WplusSmaller.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  }
   }
   figure->SetHeaderName("ATLAS Internal " + YEAR);
   figure->SetSubYRange(0.75, 1.25);
   figure->SetMode("HSTACK", "RATIO", "#frac{Data}{Pred.}");
   //figure->SetShowChi2();
   figure->SetLegendColumn(3);
   figure->SetNotNorm();
   figure->DrawFigure();
   figure = 0;
  }
 
  TString WEY_YJETCR_XName[7*3] = {"WEY_YJETCR_LL_LeptonPt", "WEY_YJETCR_LL_LeptonEta", "WEY_YJETCR_LL_PhotonPt", "WEY_YJETCR_LL_PhotonEta", 
                                   "WEY_YJETCR_LL_MET", "WEY_YJETCR_LL_MT", "WEY_YJETCR_LL_LYMass",
                                   "WEY_YJETCR_LT_LeptonPt", "WEY_YJETCR_LT_LeptonEta", "WEY_YJETCR_LT_PhotonPt", "WEY_YJETCR_LT_PhotonEta", 
                                   "WEY_YJETCR_LT_MET", "WEY_YJETCR_LT_MT", "WEY_YJETCR_LT_LYMass",
                                   "WEY_YJETCR_TL_LeptonPt", "WEY_YJETCR_TL_LeptonEta", "WEY_YJETCR_TL_PhotonPt", "WEY_YJETCR_TL_PhotonEta",
                                   "WEY_YJETCR_TL_MET", "WEY_YJETCR_TL_MT", "WEY_YJETCR_TL_LYMass"}; 
  for (int j = 0; j < 7*3; j++)
  {
   FigureName[j] = FigureDir + WEY_YJETCR_XName[j] + Eps;
   figure = ToolkitUser()->GetTool<NGFigure>(FigureName[j], WEY_YJETCR_XName[j], YName);
   for (int i = 0; i < CHANNELNUM+1; i++)
   {
  if (i != 2 && i != 3)
  {
    if (j == 0)       figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 7)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10) figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 11) figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12) figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13) figure->SetInputHist(WYplotgroup->WEY_YJETCR_LT_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 14) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(WYplotgroup->WEY_YJETCR_TL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  }
  else if (i == 2)
  {
    if (j == 0)       figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 7)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LT_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LT_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LT_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LT_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 11) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LT_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LT_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_LT_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 14) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_TL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_TL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_TL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_TL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_TL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_TL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(LYFakeplotgroup->WEY_YJETCR_TL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  }
   }
   figure->SetHeaderName("ATLAS Internal " + YEAR);
   //figure->SetSubYRange(0.91, 1.09);
   figure->SetMode("HSTACK", "RATIO", "#frac{Data}{Pred.}");
   //figure->SetShowChi2();
   figure->SetLegendColumn(3);
   figure->SetNotNorm();
   figure->DrawFigure();
   figure = 0;
  }

  TString WUY_YJETCR_XName[7*3] = {"WUY_YJETCR_LL_LeptonPt", "WUY_YJETCR_LL_LeptonEta", "WUY_YJETCR_LL_PhotonPt", "WUY_YJETCR_LL_PhotonEta", 
                                   "WUY_YJETCR_LL_MET", "WUY_YJETCR_LL_MT", "WUY_YJETCR_LL_LYMass",
                                   "WUY_YJETCR_LT_LeptonPt", "WUY_YJETCR_LT_LeptonEta", "WUY_YJETCR_LT_PhotonPt", "WUY_YJETCR_LT_PhotonEta", 
                                   "WUY_YJETCR_LT_MET", "WUY_YJETCR_LT_MT", "WUY_YJETCR_LT_LYMass",
                                   "WUY_YJETCR_TL_LeptonPt", "WUY_YJETCR_TL_LeptonEta", "WUY_YJETCR_TL_PhotonPt", "WUY_YJETCR_TL_PhotonEta",
                                   "WUY_YJETCR_TL_MET", "WUY_YJETCR_TL_MT", "WUY_YJETCR_TL_LYMass"}; 
  for (int j = 0; j < 7*3; j++)
  {
   FigureName[j] = FigureDir + WUY_YJETCR_XName[j] + Eps;
   figure = ToolkitUser()->GetTool<NGFigure>(FigureName[j], WUY_YJETCR_XName[j], YName);
   for (int i = 0; i < CHANNELNUM+1; i++)
   {
  if (i != 2 && i != 3)
  {
    if (j == 0)       figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 7)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10) figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 11) figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12) figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13) figure->SetInputHist(WYplotgroup->WUY_YJETCR_LT_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 14) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(WYplotgroup->WUY_YJETCR_TL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  }
  if (i == 2)
  {
    if (j == 0)       figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 1)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 2)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 3)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 4)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 5)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 6)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 7)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LT_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 8)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LT_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 9)  figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LT_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 10) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LT_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 11) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LT_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 12) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LT_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 13) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_LT_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  
    else if (j == 14) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_TL_LeptonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 15) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_TL_LeptonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 16) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_TL_PhotonPt.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 17) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_TL_PhotonEta.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 18) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_TL_MET.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 19) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_TL_MT.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
    else if (j == 20) figure->SetInputHist(LYFakeplotgroup->WUY_YJETCR_TL_LYMass.Channel_Map[MultiChannelNames[i]]->GetNominalHist(), MultiLegendNames[i]);
  }
   }
   figure->SetHeaderName("ATLAS Internal " + YEAR);                                              
   //figure->SetSubYRange(0.91, 1.09);
   figure->SetMode("HSTACK", "RATIO", "#frac{Data}{Pred.}");
   //figure->SetShowChi2();
   figure->SetLegendColumn(3);
   figure->SetNotNorm();
   figure->DrawFigure();
   figure = 0;
  }


  WYplotgroup->AddLYFakePlots(LYFakeplotgroup);

  TString FigureNameAsy[4];
  TString NameAsy[4] = {"AsymmetryFabsEtaElectron_Wminus",
                        "AsymmetryFabsEtaPositron_Wplus",
                        "AsymmetryFabsEtaMuon_Wminus",
                        "AsymmetryFabsEtaAntiMuon_Wplus"};
  TString XNameAsy = "#eta_{l}";
  TString YNameAsy = "Asymmetry";
  for (int j = 0; j < 4; j++)
  {
   FigureNameAsy[j] = FigureDir + NameAsy[j] + Eps;
   figure = ToolkitUser()->GetTool<NGFigure>(FigureNameAsy[j], XNameAsy, YNameAsy);
   if (j == 0) {
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaElectron_Wminus.Channel_Map["Data"]->GetNominalHist(), TString::Format("Data"));
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaElectron_Wminus.Channel_Map["Signal"]->GetNominalHist(), TString::Format("Signal"));
   }
   if (j == 1) {
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaPositron_Wplus.Channel_Map["Data"]->GetNominalHist(), TString::Format("Data"));
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaPositron_Wplus.Channel_Map["Signal"]->GetNominalHist(), TString::Format("Signal"));
   }
   if (j == 2) {
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaMuon_Wminus.Channel_Map["Data"]->GetNominalHist(), TString::Format("Data"));
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaMuon_Wminus.Channel_Map["Signal"]->GetNominalHist(), TString::Format("Signal"));
   }
   if (j == 3) {
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Data"]->GetNominalHist(), TString::Format("Data"));
     figure->SetInputHist(WYplotgroup->AsymmetryFabsEtaAntiMuon_Wplus.Channel_Map["Signal"]->GetNominalHist(), TString::Format("Signal"));
   }
   figure->SetHeaderName("ATLAS Internal " + YEAR);
   //figure->SetSubYRange(0.91, 1.09);
   figure->SetMode("MULTI", "PULL", "#frac{#Delta}{#sigma}");
   figure->SetShowChi2();
   figure->SetNotNorm();
   figure->DrawFigure();
   figure = 0;
  }
 };
};

#endif

