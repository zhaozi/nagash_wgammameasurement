#ifndef NAGASH_WYEVENT_HEADER
#define NAGASH_WYEVENT_HEADER

using namespace std;

namespace NAGASH
{
        enum class WYEventType
        {
                UNKNOWN = 0,                         // Ignore
                WECY = 1,                            // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC
                WMUY = 2,                            // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC
                
                WECY_WJETCR_TL = 3,                  // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC     Photon: ID-Tight Iso-Loose
                WECY_WJETCR_LT = 4,                  // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC     Photon: ID-Loose Iso-Tight
                WECY_WJETCR_LL = 5,                  // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC     Photon: ID-Loose Iso-Loose 
                WECY_WJETCR_REWEIGHTED = 6,          // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC
                WMUY_WJETCR_TL = 7,                  // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC     Photon: ID-Tight Iso-Loose
                WMUY_WJETCR_LT = 8,                  // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC     Photon: ID-Loose Iso-Tight
                WMUY_WJETCR_LL = 9,                  // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC     Photon: ID-Loose Iso-Loose
                WMUY_WJETCR_REWEIGHTED = 10,         // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC
                
                WECY_YJETCR_TL = 11,                 // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC          e-Iso-Tight MET-Small(Loose)
                WECY_YJETCR_LT = 12,                 // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC          e-Iso-Loose MET-Large(Tight)
                WECY_YJETCR_LL = 13,                 // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC          e-Iso-Loose MET-Small(Loose)
                WECY_YJETCR_REWEIGHTED = 14,         // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC
                WMUY_YJETCR_TL = 15,                 // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC          e-Iso-Tight MET-Small(Loose)
                WMUY_YJETCR_LT = 16,                 // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC          e-Iso-Loose MET-Large(Tight)
                WMUY_YJETCR_LL = 17,                 // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC          e-Iso-Loose MET-Small(Loose)
                WMUY_YJETCR_REWEIGHTED = 18,         // Data  &  WY MC  &  WZ/ZZ/Wgamma/Zgamma MC
                
                AMOUNT = 19
        };

        enum class LeptonType
        {
                UNKNOWN = 0,
                ELECTRON = 1,
                POSITRON = 2,
                MUON = 3,
                ANTIMUON = 4,
                FWDELECTRON = 5,
                FAKEELECTRON = 6,
                FAKEPOSITRON = 7,
                FAKEMUON = 8,
                FAKEANTIMUON = 9,
                FAKEFWDELECTRON = 10
        };

        enum class PhotonType
        {
                UNKNOWN = 0,
                PHOTON = 1,
                FAKEPHOTON_TL = 2, // Tight ID & Loose Iso
                FAKEPHOTON_LT = 3, // Loose ID & Tight Iso
                FAKEPHOTON_LL = 4, // Loose ID & Loose Iso
        };

        class WYEvent
        {
        public:
                int ChannelNumber = 0;
                WYEventType ID = WYEventType::UNKNOWN;
                WYEventType SubID = WYEventType::UNKNOWN;
                vector<LeptonType> LeptonID;
                vector<float> LeptonPt;
                vector<float> LeptonEta;
                vector<float> LeptonPhi;
                vector<float> LeptonWeight;
                vector<float> LeptonLooseWeight;

                vector<LeptonType> Truth_LeptonID;
                vector<float> Truth_LeptonPt;
                vector<float> Truth_LeptonEta;
                vector<float> Truth_LeptonPhi;

                vector<bool> JetIsB;
                vector<float> JetPt;
                vector<float> JetEta;
                vector<float> JetPhi;
                vector<float> JetM;
                
                vector<PhotonType> PhotonID;
                vector<float> PhotonPt;
                vector<float> PhotonEta;
                vector<float> PhotonPhi;
                vector<float> PhotonWeight;

                float MET = 0;
                float METPhi = 0;

                float EventWeight = 1;

                bool WPlusJetReweighted = false;

                WYEvent();
                ~WYEvent();
                void ReweightedWPlusJet(){WPlusJetReweighted = true;};
                TString GetWYEventType() const;
                TString GetChannelName() const;
                WYEventType SetWYEventType(int id);
                LeptonType IntToLeptonType(int id);
                double GetWeight() const;
                double GetLooseWeight(int index) const;
                int GetBigType() const;
                int GetLeptonAmount() const;
                int GetPhotonAmount() const;
                int GetJetAmount() const;
                int GetBJetAmount() const;
                int GetBJetNumber(vector<int> &BJetNumber) const;
                double GetMET_Rel() const;
                double GetMET_RelPhi() const;
                double DeltaPhi(float Phi1, float Phi2) const;
                double GetLYDeltaPhi() const;
                double GetMETMPTDeltaPhi() const;
                double GetHT() const;
                double GetLYDeltaR() const;
                double GetLYMass() const;
                double GetMT() const;
                double GetMT_LY() const;
                void COut();
                void Clean();
                void Clone(WYEvent event);
                void SetLepton(double pt, double eta, double phi);
                void SetTruthLepton(double px, double py, double pz, double e);
                void SetJet(bool isB, double pt, double eta, double phi, double m);
                void SetPhoton(double pt, double eta, double phi);
                TLorentzVector LeptonP4(int index) const;
                TLorentzVector DileptonP4() const;
                TLorentzVector Truth_LeptonP4(int index) const;
                TLorentzVector Truth_DileptonP4() const;
                TLorentzVector JetP4(int index) const;
                TLorentzVector PhotonP4(int index) const;
                bool IsXMZMC() const;
                bool IsSignalMC() const;
                bool IsDibosonBKGMC() const;
                bool IsTRelatedMC() const;
                bool IsDrellYanMC() const;
                bool IsUselessMC() const;
        };

        inline WYEvent::WYEvent()
        {
                Clean();
        }

        inline WYEvent::~WYEvent()
        {
        }

        inline int WYEvent::GetBigType() const
        {
                return -1;
        }
        
        inline TString WYEvent::GetChannelName() const
        {
                TString name = "UNKNOWN";
                if (ChannelNumber == -1)
                {
                        name = "Data";
                }
                else if (ChannelNumber == 1026)
                {
                        if (ID == WYEventType::WECY_WJETCR_REWEIGHTED ||
                            ID == WYEventType::WMUY_WJETCR_REWEIGHTED)
                            name = "Wjets";
                        if (ID == WYEventType::WECY_YJETCR_REWEIGHTED ||
                            ID == WYEventType::WMUY_YJETCR_REWEIGHTED)
                            name = "Yjets";
                }
                else if (ChannelNumber == 700398 || // Zuu Y 
                         ChannelNumber == 700399 || // Zee Y 
                         ChannelNumber == 700400)   // Ztt Y 
                        name = "ZY";
                else if (ChannelNumber == 700402) // Wuv Y 
                        name = "WuvY";
                else if (ChannelNumber == 700403) // Wev Y
                        name = "WevY";
                else if (ChannelNumber == 700404) // Wtv Y
                        name = "WtvY";
                else if (ChannelNumber == 361100 || // W+ ev
                         ChannelNumber == 361101 || // W+ uv
                         ChannelNumber == 361102 || // W+ tv
                         ChannelNumber == 361103 || // W- ev
                         ChannelNumber == 361104 || // W- uv
                         ChannelNumber == 361105)   // W- tv
                {
                        if (ID == WYEventType::WECY_YJETCR_TL ||
                            ID == WYEventType::WECY_YJETCR_LT ||
                            ID == WYEventType::WECY_YJETCR_LL ||
                            ID == WYEventType::WMUY_YJETCR_TL ||
                            ID == WYEventType::WMUY_YJETCR_LT ||
                            ID == WYEventType::WMUY_YJETCR_LL)
                        {
                                if (WPlusJetReweighted == true)
                                  name = "Wjets";
                                else 
                                  name = "WPlusJet";
                        }
                        else if (ID == WYEventType::WECY ||
                                 ID == WYEventType::WMUY)
                        {
                                name = "WPlusJet";
                        }
                        else
                        {
                                name = "UNKNOWN";
                        }
                }
                else if (ChannelNumber == 361106) // Z ee
                        name = "Zee";
                else if (ChannelNumber == 361107) // Z uu
                        name = "Zmumu";
                else if (ChannelNumber == 361108) // Z tt
                        name = "Ztautau";
                else if (ChannelNumber == 364100 ||
                         ChannelNumber == 364101 ||
                         ChannelNumber == 364102 ||
                         ChannelNumber == 364103 ||
                         ChannelNumber == 364104 ||
                         ChannelNumber == 364105)
                        name = "Zmumu"; // Sherpa
                else if (ChannelNumber == 364114 || 
                         ChannelNumber == 364115 || 
                         ChannelNumber == 364116 ||                                     
                         ChannelNumber == 364117 || 
                         ChannelNumber == 364118 || 
                         ChannelNumber == 364119)
                        name = "Zee"; // Sherpa
                else if (ChannelNumber == 364128 || 
                         ChannelNumber == 364129 || 
                         ChannelNumber == 364130 || 
                         ChannelNumber == 364131 || 
                         ChannelNumber == 364132 || 
                         ChannelNumber == 364133) 
                        name = "Ztautau"; // Sherpa
                else if (ChannelNumber == 363356 || // ZqqZll - Sherpa
                         ChannelNumber == 363357 || // WqqZvv - Sherpa
                         ChannelNumber == 363358 || // WqqZll - Sherpa
                         ChannelNumber == 363489 || // WlvZqq - Sherpa
                         ChannelNumber == 364250 || // llll - Sherpa
                         ChannelNumber == 364253 || // lllv - Sherpa
                         ChannelNumber == 364254 || // llvv - Sherpa
                         ChannelNumber == 364255)   // lvvv - Sherpa
                        name = "Diboson";
                else if (ChannelNumber == 410470 || // ttbar
                         ChannelNumber == 410644 || // single-top s-ChannelNumber lepton top
                         ChannelNumber == 410645 || // single-top s-ChannelNumber lepton anti-top
                         ChannelNumber == 410646 || // Wt DR inclusive top
                         ChannelNumber == 410647 || // Wt DR inclusive anti-top
                         ChannelNumber == 410658 || // t-ChannelNumber BW50 lepton top
                         ChannelNumber == 410659)   // t-ChannelNumber BW50 lepton antitop
                        name = "Top";

                 return name;
        }

        inline TString WYEvent::GetWYEventType() const
        {
                TString TypeName = "UNKNOWN";
                int i = (int)ID;
                if (i == 0)
                        TypeName = "UNKNOWN";
                else if (i == 1)
                        TypeName = "WECY";
                else if (i == 2)
                        TypeName = "WMUY";
                return TypeName;
        }

        inline WYEventType WYEvent::SetWYEventType(int type)
        {
                if (type == 0)
                        ID = WYEventType::UNKNOWN;
                else if (type == 1)
                        ID = WYEventType::WECY;
                else if (type == 2)
                        ID = WYEventType::WMUY;
                return ID;
        }

        inline LeptonType WYEvent::IntToLeptonType(int type)
        {
                if (type == 0)
                        return LeptonType::UNKNOWN;
                else if (type == 1)
                        return LeptonType::ELECTRON;
                else if (type == 2)
                        return LeptonType::POSITRON;
                else if (type == 3)
                        return LeptonType::MUON;
                else if (type == 4)
                        return LeptonType::ANTIMUON;
                else if (type == 5)
                        return LeptonType::FWDELECTRON;
                else if (type == 6)
                        return LeptonType::FAKEELECTRON;
                else if (type == 7)
                        return LeptonType::FAKEMUON;
                else if (type == 8)
                        return LeptonType::FAKEFWDELECTRON;
                else
                        return LeptonType::UNKNOWN;
        }

        inline double WYEvent::GetWeight() const
        {
                return LeptonWeight.at(0) * PhotonWeight.at(0) * EventWeight;
        }

        inline double WYEvent::GetLooseWeight(int index) const
        {
                return -999;
                //return LeptonLooseWeight.at(0) * LeptonLooseWeight.at(1) * EventWeight;
        }

        inline int WYEvent::GetPhotonAmount() const
        {
                return PhotonPt.size();
        }

        inline int WYEvent::GetLeptonAmount() const
        {
                return LeptonPt.size();
        }

        inline int WYEvent::GetJetAmount() const
        {
                return JetPt.size();
        }

        inline int WYEvent::GetBJetAmount() const
        {
                int b = 0;
                for (int i = 0; i < JetIsB.size(); i++)
                {
                        if (JetIsB.at(i) == true)
                                b = b + 1;
                }
                return b;
        }

        inline int WYEvent::GetBJetNumber(vector<int> &BJetNumber) const
        {
                BJetNumber.clear();
                for (int i = 0; i < JetIsB.size(); i++)
                {
                        if (JetIsB.at(i) == true)
                                BJetNumber.emplace_back(i);
                }
                return BJetNumber.size();
        }

        inline double WYEvent::DeltaPhi(float Phi1, float Phi2) const
        {
                double DeltaPhi = Phi1 - Phi2;
                if (DeltaPhi > -M_PI && DeltaPhi <= M_PI)
                        return DeltaPhi;
                else if (DeltaPhi > M_PI)
                        return DeltaPhi - 2 * M_PI;
                else if (DeltaPhi <= -M_PI)
                        return DeltaPhi + 2 * M_PI;
                else
                        return 0;
        }

        inline double WYEvent::GetLYDeltaPhi() const
        {
                return fabs(DeltaPhi(LeptonPhi.at(0), PhotonPhi.at(0)));
        }

        inline double WYEvent::GetMET_Rel() const
        {
                double DeltaPhiL = M_PI / 2;
                for (int i = 0; i < LeptonPhi.size(); i++)
                {
                        if (fabs(DeltaPhi(LeptonPhi.at(i), METPhi)) < DeltaPhiL)
                                DeltaPhiL = fabs(DeltaPhi(LeptonPhi.at(i), METPhi));
                }
                if (DeltaPhiL < M_PI / 2)
                        return MET * sin(DeltaPhiL);
                else
                        return MET;
        }

        inline double WYEvent::GetMET_RelPhi() const
        {
                double DeltaPhiL = M_PI / 2;
                double NearestLeptonPhi = 0;
                for (int i = 0; i < LeptonPhi.size(); i++)
                {
                        if (fabs(DeltaPhi(LeptonPhi.at(i), METPhi)) < fabs(DeltaPhiL))
                        {
                                DeltaPhiL = DeltaPhi(LeptonPhi.at(i), METPhi);
                                NearestLeptonPhi = LeptonPhi.at(i);
                        }
                }
                if (fabs(DeltaPhiL) < M_PI / 2)
                {
                        if (DeltaPhiL > 0)
                        {
                                if (fabs(NearestLeptonPhi - M_PI / 2) < M_PI)
                                        return NearestLeptonPhi - M_PI / 2;
                                else
                                        return NearestLeptonPhi - M_PI / 2 + M_PI * 2;
                        }
                        else if (DeltaPhiL <= 0)
                        {
                                if (fabs(NearestLeptonPhi + M_PI / 2) < M_PI)
                                        return NearestLeptonPhi + M_PI / 2;
                                else
                                        return NearestLeptonPhi + M_PI / 2 - M_PI * 2;
                        }
                }
                else
                        return METPhi;
        }

        inline double WYEvent::GetMETMPTDeltaPhi() const
        {
                return DeltaPhi(METPhi, GetMET_RelPhi());
        }

        inline double WYEvent::GetHT() const
        {
                double HT = 0;
                for (int i = 0; i < LeptonPt.size(); i++)
                {
                        HT = HT + LeptonPt.at(i);
                }
                for (int i = 0; i < JetPt.size(); i++)
                {
                        HT = HT + JetPt.at(i);
                }
                return HT;
        }
        
        inline double WYEvent::GetLYDeltaR() const
        {
                return LeptonP4(0).DeltaR(PhotonP4(0));
        }
        
        inline double WYEvent::GetLYMass() const
        {
                return (LeptonP4(0) + PhotonP4(0)).M();
        }

        inline double WYEvent::GetMT() const
        {
                return sqrt(2 * LeptonPt.at(0) * MET * (1 - cos(DeltaPhi(LeptonPhi.at(0), METPhi))));
        }

        inline double WYEvent::GetMT_LY() const
        {
                return -999;
                //return sqrt(2 * LeptonPt.at(index) * MET * (1 - cos(DeltaPhi(LeptonPhi.at(index), METPhi))));
        }
        
        inline void WYEvent::COut()
        {
       cout << "ChannelNumber = " << ChannelNumber << endl;
       cout << "EventWeight = " << EventWeight << endl;
       cout << "MET = " << MET << endl;
       cout << "METPhi = " << METPhi << endl;
       for (int i = 0; i < LeptonPt.size(); i++)
       {
        cout << "LeptonID.at(" << i << ") = " << (int)LeptonID.at(i) << endl;
        cout << "LeptonPt.at(" << i << ") = " << LeptonPt.at(i) << endl;
        cout << "LeptonEta.at(" << i << ") = " << LeptonEta.at(i) << endl; 
        cout << "LeptonPhi.at(" << i << ") = " << LeptonPhi.at(i) << endl; 
        cout << "LeptonWeight.at(" << i << ") = " << LeptonWeight.at(i) << endl;
       }
       for (int i = 0; i < PhotonPt.size(); i++)
       {
        cout << "PhotonID.at(" << i << ") = " << (int)PhotonID.at(i) << endl;
        cout << "PhotonPt.at(" << i << ") = " << PhotonPt.at(i) << endl;
        cout << "PhotonEta.at(" << i << ") = " << PhotonEta.at(i) << endl;
        cout << "PhotonPhi.at(" << i << ") = " << PhotonPhi.at(i) << endl;
        cout << "PhotonWeight.at(" << i << ") = " << PhotonWeight.at(i) << endl;
       }
        }

        inline void WYEvent::Clean()
        {
                ChannelNumber = 0;
                ID = WYEventType::UNKNOWN;
                SubID = WYEventType::UNKNOWN;
                LeptonID.clear();
                LeptonID.shrink_to_fit();
                LeptonPt.clear();
                LeptonPt.shrink_to_fit();
                LeptonEta.clear();
                LeptonEta.shrink_to_fit();
                LeptonPhi.clear();
                LeptonPhi.shrink_to_fit();
                LeptonWeight.clear();
                LeptonWeight.shrink_to_fit();
                LeptonLooseWeight.clear();
                LeptonLooseWeight.shrink_to_fit();
                Truth_LeptonID.clear();
                Truth_LeptonID.shrink_to_fit();
                Truth_LeptonPt.clear();
                Truth_LeptonPt.shrink_to_fit();
                Truth_LeptonEta.clear();
                Truth_LeptonEta.shrink_to_fit();
                Truth_LeptonPhi.clear();
                Truth_LeptonPhi.shrink_to_fit();
                JetIsB.clear();
                JetIsB.shrink_to_fit();
                JetPt.clear();
                JetPt.shrink_to_fit();
                JetEta.clear();
                JetEta.shrink_to_fit();
                JetPhi.clear();
                JetPhi.shrink_to_fit();
                JetM.clear();
                JetM.shrink_to_fit();
                PhotonID.clear();
                PhotonID.shrink_to_fit();
                PhotonPt.clear();
                PhotonPt.shrink_to_fit();
                PhotonEta.clear();
                PhotonEta.shrink_to_fit();
                PhotonPhi.clear();
                PhotonPhi.shrink_to_fit();
                PhotonWeight.clear();
                PhotonWeight.shrink_to_fit();

                for (int i = 0; i < 1; i++)
                {
                        LeptonID.emplace_back(LeptonType::UNKNOWN);
                        LeptonPt.emplace_back(0);
                        LeptonEta.emplace_back(0);
                        LeptonPhi.emplace_back(0);
                        LeptonWeight.emplace_back(1);

                        PhotonID.emplace_back(PhotonType::UNKNOWN);
                        PhotonPt.emplace_back(0);
                        PhotonEta.emplace_back(0);
                        PhotonPhi.emplace_back(0);
                        PhotonWeight.emplace_back(1);

                        /*Truth_LeptonID.emplace_back(LeptonType::UNKNOWN);
                        Truth_LeptonPt.emplace_back(0);
                        Truth_LeptonEta.emplace_back(0);
                        Truth_LeptonPhi.emplace_back(0);*/
                }
                MET = 0;
                METPhi = 0;
                EventWeight = 1;
        }

        inline void WYEvent::Clone(WYEvent event)
        {
                WPlusJetReweighted = event.WPlusJetReweighted;
                ChannelNumber = event.ChannelNumber;
                ID = event.ID;
                LeptonID.clear();
                LeptonID.shrink_to_fit();
                LeptonPt.clear();
                LeptonPt.shrink_to_fit();
                LeptonEta.clear();
                LeptonEta.shrink_to_fit();
                LeptonPhi.clear();
                LeptonPhi.shrink_to_fit();
                LeptonWeight.clear();
                LeptonWeight.shrink_to_fit();
                LeptonLooseWeight.clear();
                LeptonLooseWeight.shrink_to_fit();
                Truth_LeptonID.clear();
                Truth_LeptonID.shrink_to_fit();
                Truth_LeptonPt.clear();
                Truth_LeptonPt.shrink_to_fit();
                Truth_LeptonEta.clear();
                Truth_LeptonEta.shrink_to_fit();
                Truth_LeptonPhi.clear();
                Truth_LeptonPhi.shrink_to_fit();
                JetIsB.clear();
                JetIsB.shrink_to_fit();
                JetPt.clear();
                JetPt.shrink_to_fit();
                JetEta.clear();
                JetEta.shrink_to_fit();
                JetPhi.clear();
                JetPhi.shrink_to_fit();
                JetM.clear();
                JetM.shrink_to_fit();
                PhotonID.clear();
                PhotonID.shrink_to_fit();
                PhotonPt.clear();
                PhotonPt.shrink_to_fit();
                PhotonEta.clear();
                PhotonEta.shrink_to_fit();
                PhotonPhi.clear();
                PhotonPhi.shrink_to_fit();
                PhotonWeight.clear();
                PhotonWeight.shrink_to_fit();

                for (int i = 0; i < event.LeptonLooseWeight.size(); i++)
                {
                        LeptonLooseWeight.emplace_back(event.LeptonLooseWeight.at(i));
                }
                for (int i = 0; i < event.LeptonID.size(); i++)
                {
                        LeptonID.emplace_back(event.LeptonID.at(i));
                        LeptonPt.emplace_back(event.LeptonPt.at(i));
                        LeptonEta.emplace_back(event.LeptonEta.at(i));
                        LeptonPhi.emplace_back(event.LeptonPhi.at(i));
                        LeptonWeight.emplace_back(event.LeptonWeight.at(i));
                }
                for (int i = 0; i < event.Truth_LeptonID.size(); i++)
                {
                        Truth_LeptonID.emplace_back(event.Truth_LeptonID.at(i));
                        Truth_LeptonPt.emplace_back(event.Truth_LeptonPt.at(i));
                        Truth_LeptonEta.emplace_back(event.Truth_LeptonEta.at(i));
                        Truth_LeptonPhi.emplace_back(event.Truth_LeptonPhi.at(i));
                }
                for (int i = 0; i < event.JetIsB.size(); i++)
                {
                        JetIsB.emplace_back(event.JetIsB.at(i));
                        JetPt.emplace_back(event.JetPt.at(i));
                        JetEta.emplace_back(event.JetEta.at(i));
                        JetPhi.emplace_back(event.JetPhi.at(i));
                        JetM.emplace_back(event.JetM.at(i));
                }
                for (int i = 0; i < event.PhotonID.size(); i++)
                {
                        PhotonID.emplace_back(event.PhotonID.at(i));
                        PhotonPt.emplace_back(event.PhotonPt.at(i));
                        PhotonEta.emplace_back(event.PhotonEta.at(i));
                        PhotonPhi.emplace_back(event.PhotonPhi.at(i));
                        PhotonWeight.emplace_back(event.PhotonWeight.at(i));
                }
                MET = event.MET;
                METPhi = event.METPhi;
                EventWeight = event.EventWeight;
        }

        inline void WYEvent::SetLepton(double pt, double eta, double phi)
        {
                LeptonPt.at(0) = pt;
                LeptonEta.at(0) = eta;
                LeptonPhi.at(0) = phi;
                /*LeptonPt.emplace_back(pt);
                LeptonEta.emplace_back(eta);
                LeptonPhi.emplace_back(phi);*/
        }

        inline void WYEvent::SetTruthLepton(double px, double py, double pz, double e)
        {
                TLorentzVector L;
                L.SetPxPyPzE(px, py, pz, e);
                Truth_LeptonPt.emplace_back(L.Pt());
                Truth_LeptonEta.emplace_back(L.Eta());
                Truth_LeptonPhi.emplace_back(L.Phi());
        }

        inline void WYEvent::SetJet(bool isB, double pt, double eta, double phi, double m)
        {
                JetIsB.emplace_back(isB);
                JetPt.emplace_back(pt);
                JetEta.emplace_back(eta);
                JetPhi.emplace_back(phi);
                JetM.emplace_back(m);
        }
        
        inline void WYEvent::SetPhoton(double pt, double eta, double phi)
        {
                PhotonPt.at(0) = pt;
                PhotonEta.at(0) = eta;
                PhotonPhi.at(0) = phi;
                /*PhotonPt.emplace_back(pt);
                PhotonEta.emplace_back(eta);
                PhotonPhi.emplace_back(phi);*/
        }

        inline TLorentzVector WYEvent::LeptonP4(int index) const
        {
                TLorentzVector vec(0, 0, 0, 0);
                if (index <= GetLeptonAmount())
                        vec.SetPtEtaPhiM(LeptonPt.at(index), LeptonEta.at(index), LeptonPhi.at(index), 0);
                return vec;
        }

        inline TLorentzVector WYEvent::DileptonP4() const
        {
                TLorentzVector vec1(0, 0, 0, 0);
                TLorentzVector vec2(0, 0, 0, 0);
                if (GetLeptonAmount() == 2)
                {
                        vec1.SetPtEtaPhiM(LeptonPt.at(0), LeptonEta.at(0), LeptonPhi.at(0), 0);
                        vec2.SetPtEtaPhiM(LeptonPt.at(1), LeptonEta.at(1), LeptonPhi.at(1), 0);
                        return vec1 + vec2;
                }
                else
                        return vec1;
        }

        inline TLorentzVector WYEvent::Truth_LeptonP4(int index) const
        {
                TLorentzVector vec(0, 0, 0, 0);
                if (index <= GetLeptonAmount())
                        vec.SetPtEtaPhiM(Truth_LeptonPt.at(index), Truth_LeptonEta.at(index), Truth_LeptonPhi.at(index), 0);
                return vec;
        }

        inline TLorentzVector WYEvent::Truth_DileptonP4() const
        {
                TLorentzVector vec1(0, 0, 0, 0);
                TLorentzVector vec2(0, 0, 0, 0);
                if (GetLeptonAmount() == 2)
                {
                        vec1.SetPtEtaPhiM(Truth_LeptonPt.at(0), Truth_LeptonEta.at(0), Truth_LeptonPhi.at(0), 0);
                        vec2.SetPtEtaPhiM(Truth_LeptonPt.at(1), Truth_LeptonEta.at(1), Truth_LeptonPhi.at(1), 0);
                        return vec1 + vec2;
                }
                else
                        return vec1;
        }

        inline TLorentzVector WYEvent::JetP4(int index) const
        {
                TLorentzVector vec(0, 0, 0, 0);
                if (index <= GetJetAmount())
                        vec.SetPtEtaPhiM(JetPt.at(index), JetEta.at(index), JetPhi.at(index), JetM.at(index));
                return vec;
        }

        inline TLorentzVector WYEvent::PhotonP4(int index) const
        {
                TLorentzVector vec(0, 0, 0, 0);
                if (index <= GetPhotonAmount())
                        vec.SetPtEtaPhiM(PhotonPt.at(index), PhotonEta.at(index), PhotonPhi.at(index), 0);
                return vec;
        }

        inline bool WYEvent::IsXMZMC() const
        {
                if (ChannelNumber == 361601 || ChannelNumber == 361603 || ChannelNumber == 361604 ||
                    ChannelNumber == 361602 || ChannelNumber == 361605 || ChannelNumber == 361606 ||
                    ChannelNumber == 361607 || ChannelNumber == 361608 || ChannelNumber == 361609 ||
                    ChannelNumber == 361610 || ChannelNumber == 361600)
                        return true;
                else
                        return false;
        }

        inline bool WYEvent::IsSignalMC() const
        {
                if (ChannelNumber == 700402 || ChannelNumber == 700403)
                        return true;
                else
                        return false;
        }

        inline bool WYEvent::IsDibosonBKGMC() const
        {
                if (ChannelNumber == 361601 || ChannelNumber == 361603 || ChannelNumber == 361604 ||
                    ChannelNumber == 361607 || ChannelNumber == 361610)
                        return true;
                else
                        return false;
        }

        inline bool WYEvent::IsTRelatedMC() const
        {
                if (ChannelNumber == 410470 || ChannelNumber == 410646 || ChannelNumber == 410647)
                        return true;
                else
                        return false;
        }

        inline bool WYEvent::IsDrellYanMC() const
        {
                if (ChannelNumber == 361106 || ChannelNumber == 361107 || ChannelNumber == 361108)
                        return true;
                else if (ChannelNumber == 364100 || ChannelNumber == 364103 || ChannelNumber == 364114 ||
                         ChannelNumber == 364117 || ChannelNumber == 364128 || ChannelNumber == 364131 ||   
                         ChannelNumber == 364101 || ChannelNumber == 364104 || ChannelNumber == 364115 ||
                         ChannelNumber == 364118 || ChannelNumber == 364129 || ChannelNumber == 364132 ||   
                         ChannelNumber == 364102 || ChannelNumber == 364105 || ChannelNumber == 364116 ||
                         ChannelNumber == 364119 || ChannelNumber == 364130 || ChannelNumber == 364133) // Sherpa Z pT 0_70 & 70_140
                        return true;
                else
                        return false;
        }

        inline bool WYEvent::IsUselessMC() const
        {
                /*if (ChannelNumber == 363489 || ChannelNumber == 363356 || ChannelNumber == 363360 ||
                    ChannelNumber == 363357 || ChannelNumber == 363358 || ChannelNumber == 364250 ||
                    ChannelNumber == 364253 || ChannelNumber == 364254 || ChannelNumber == 364255 ||
                    ChannelNumber == 363359 || ChannelNumber == 426131)
                {
                        //cout << "WARNING   Useless MC Channel Number = "<< ChannelNumber << endl;
                        return true;
                }
                else*/
                        return false;
        }

        class WYFullEvent
        {
                // This class is used to process Systematic Variation
                // Use Syst_Var instead of Var to process results for the variation
        public:
                vector<double> *Electron_Pt;
                vector<double> *Electron_Eta;
                vector<double> *Electron_Phi;
                vector<double> *Electron_Charge;
                vector<bool> *Electron_PassID;
                vector<bool> *Electron_PassIDLoose;
                vector<double> *Electron_Track_d0sig;
                vector<double> *Electron_Track_dz0;
                vector<bool> *Electron_PassIso;
                vector<bool> *Electron_MatchedTrigger;
                vector<double> *mc_Electron_RecoSF;
                vector<double> *mc_Electron_LooseIDSF;
                vector<double> *mc_Electron_IDSF;
                vector<double> *mc_Electron_IsoSF;
                vector<double> *mc_Electron_TriggerSF;
                vector<double> *mc_Electron_TriggerEff;
                vector<double> *ForwardElectron_Pt;
                vector<double> *ForwardElectron_Eta;
                vector<double> *ForwardElectron_Phi;
                vector<bool> *ForwardElectron_PassID;
                vector<bool> *ForwardElectron_PassIDLoose;
                vector<double> *mc_ForwardElectron_IDSF;
                vector<double> *Muon_Pt;
                vector<double> *Muon_Original_Pt;
                vector<double> *Muon_Eta;
                vector<double> *Muon_Phi;
                vector<double> *Muon_Charge;
                vector<double> *Muon_ptcone30;
                vector<bool> *Muon_PassID;
                vector<bool> *Muon_PassIDLoose;
                vector<double> *Muon_Track_dz0;
                vector<double> *Muon_Track_d0sig;
                vector<bool> *Muon_MatchedTrigger;
                vector<bool> *Muon_PassIso;
                vector<double> *mc_Muon_IsoSF;
                vector<double> *mc_Muon_RecoSF;
                vector<double> *mc_Muon_TriggerSF;
                vector<double> *mc_Muon_TriggerEff;
                vector<double> *mc_Muon_TTVASF;
                vector<double> *Jet_Pt;
                vector<double> *Jet_Eta;
                vector<double> *Jet_Phi;
                vector<double> *Jet_M;
                vector<int> *Jet_NumTrkPt500;
                vector<float> *Jet_SumPtTrkPt500;
                vector<float> *Jet_Jvt;
                vector<bool> *Jet_PassJvt;
                vector<float> *Jet_JvtSF;
                vector<bool> *Jet_isBJet;
                vector<float> *Jet_isBJetSF;
                vector<bool> *Jet_isBad;
                vector<double> *Photon_Pt;
                vector<double> *Photon_Eta;
                vector<double> *Photon_Phi;
                vector<bool> *Photon_PassCleaning;
                vector<bool> *Photon_PassID;
                vector<bool> *Photon_PassIDLoose;
                vector<bool> *Photon_PassIso;
                vector<double> *mc_Photon_IDSF;
                vector<double> *mc_Photon_IsoSF;
                vector<int> *Truth_ID;
                vector<int> *Truth_Status;
                vector<int> *Truth_MotherID;
                vector<double> *Truth_Px;
                vector<double> *Truth_Py;
                vector<double> *Truth_Pz;
                vector<double> *Truth_E;
                bool WithBadJet;
                int ChannelNumber;
                double MET;
                double MET_Phi;
                double mc_EventWeight;
                double mc_PileupWeight;
                void Reserve();
                void Clean();
                void COut();
        };
   
   inline void WYFullEvent::COut()
   {
       cout << "ChannelNumber = " << ChannelNumber << endl;
       cout << "mc_EventWeight = " << mc_EventWeight << endl;
       cout << "mc_PileupWeight = " << mc_PileupWeight << endl;
       cout << "MET = " << MET << endl;
       for (int i = 0; i < Electron_Pt->size(); i++)
       {
        cout << "Electron_Pt->at(" << i << ") = " << Electron_Pt->at(i) << endl;
        cout << "Electron_Eta->at(" << i << ") = " << Electron_Eta->at(i) << endl;
        cout << "Electron_Phi->at(" << i << ") = " << Electron_Phi->at(i) << endl;
        cout << "Electron_Charge->at(" << i << ") = " << Electron_Charge->at(i) << endl;
        cout << "Electron_PassIDLoose->at(" << i << ") = " << Electron_PassIDLoose->at(i) << endl;
        cout << "Electron_PassID->at(" << i << ") = " << Electron_PassID->at(i) << endl;
        cout << "Electron_PassIso->at(" << i << ") = " << Electron_PassIso->at(i) << endl;
        cout << "mc_Electron_RecoSF->at(" << i << ") = " << mc_Electron_RecoSF->at(i) << endl; 
        cout << "mc_Electron_IDSF->at(" << i << ") = " << mc_Electron_IDSF->at(i) << endl;
        cout << "mc_Electron_IsoSF->at(" << i << ") = " << mc_Electron_IsoSF->at(i) << endl;
        cout << "mc_Electron_TriggerSF->at(" << i << ") = " << mc_Electron_TriggerSF->at(i) << endl;
       }
       /*for (int i = 0; i < ForwardElectron_Pt->size(); i++)
       {
        cout << "ForwardElectron_Pt->at(" << i << ") = " << ForwardElectron_Pt->at(i) << endl;
        cout << "ForwardElectron_Eta->at(" << i << ") = " << ForwardElectron_Eta->at(i) << endl;
        cout << "ForwardElectron_Phi->at(" << i << ") = " << ForwardElectron_Phi->at(i) << endl;
        cout << "ForwardElectron_PassIDLoose->at(" << i << ") = " << ForwardElectron_PassIDLoose->at(i) << endl;
        cout << "ForwardElectron_PassID->at(" << i << ") = " << ForwardElectron_PassID->at(i) << endl;
        //cout << "ForwardElectron_LikelihoodValue->at(" << i << ") = " << ForwardElectron_LikelihoodValue->at(i) << endl;
       }*/
       for (int i = 0; i < Muon_Pt->size(); i++)
       {
        cout << "Muon_Pt->at(" << i << ") = " << Muon_Pt->at(i) << endl;
        cout << "Muon_Eta->at(" << i << ") = " << Muon_Eta->at(i) << endl;
        cout << "Muon_Charge->at(" << i << ") = " << Muon_Charge->at(i) << endl;
        cout << "Muon_ptcone30->at(" << i << ") = " << Muon_ptcone30->at(i) << endl;
        cout << "Muon_PassIDLoose->at(" << i << ") = " << Muon_PassIDLoose->at(i) << endl;
        cout << "Muon_PassID->at(" << i << ") = " << Muon_PassID->at(i) << endl;
        cout << "Muon_PassIso->at(" << i << ") = " << Muon_PassIso->at(i) << endl;
        cout << "Muon_Track_dz0->at(" << i << ") = " << Muon_Track_dz0->at(i) << endl;
        cout << "Muon_Track_d0sig->at(" << i << ") = " << Muon_Track_d0sig->at(i) << endl;
        cout << "mc_Muon_IsoSF->at(" << i << ") = " << mc_Muon_IsoSF->at(i) << endl;
        cout << "mc_Muon_RecoSF->at(" << i << ") = " << mc_Muon_RecoSF->at(i) << endl;
        cout << "mc_Muon_TriggerSF->at(" << i << ") = " << mc_Muon_TriggerSF->at(i) << endl;
       }
       for (int i = 0; i < Photon_Pt->size(); i++)
       {
        cout << "Photon_Pt->at(" << i << ") = " << Photon_Pt->at(i) << endl;
        cout << "Photon_Eta->at(" << i << ") = " << Photon_Eta->at(i) << endl;
        cout << "Photon_Phi->at(" << i << ") = " << Photon_Phi->at(i) << endl;
        cout << "Photon_PassIDLoose->at(" << i << ") = " << Photon_PassIDLoose->at(i) << endl;
        cout << "Photon_PassID->at(" << i << ") = " << Photon_PassID->at(i) << endl;
        cout << "Photon_PassIso->at(" << i << ") = " << Photon_PassIso->at(i) << endl;
        cout << "Photon_PassCleaning->at(" << i << ") = " << Photon_PassCleaning->at(i) << endl;
        cout << "mc_Photon_IDSF->at(" << i << ") = " << mc_Photon_IDSF->at(i) << endl;
        cout << "mc_Photon_IsoSF->at(" << i << ") = " << mc_Photon_IsoSF->at(i) << endl;
       }
   }

   inline void WYFullEvent::Reserve()
   {
      Electron_Pt = new vector<double>{};
      Electron_Eta = new vector<double>{};
      Electron_Phi = new vector<double>{};
      Electron_Charge = new vector<double>{};
      Electron_PassID = new vector<bool>{};
      Electron_PassIDLoose = new vector<bool>{};
      Electron_Track_d0sig = new vector<double>{};
      Electron_Track_dz0 = new vector<double>{};
      Electron_PassIso = new vector<bool>{};
      Electron_MatchedTrigger = new vector<bool>{};
      mc_Electron_RecoSF = new vector<double>{};
      mc_Electron_IDSF = new vector<double>{};
      mc_Electron_IsoSF = new vector<double>{};
      mc_Electron_TriggerSF = new vector<double>{};
      mc_Electron_TriggerEff = new vector<double>{};
      ForwardElectron_Pt = new vector<double>{};
      ForwardElectron_Eta = new vector<double>{};
      ForwardElectron_Phi = new vector<double>{};
      ForwardElectron_PassID = new vector<bool>{};
      ForwardElectron_PassIDLoose = new vector<bool>{};
      mc_ForwardElectron_IDSF = new vector<double>{};
      Muon_Pt = new vector<double>{};
      Muon_Original_Pt = new vector<double>{};
      Muon_Eta = new vector<double>{};
      Muon_Phi = new vector<double>{};
      Muon_Charge = new vector<double>{};
      Muon_ptcone30 = new vector<double>{};
      Muon_PassID = new vector<bool>{};
      Muon_PassIDLoose = new vector<bool>{};
      Muon_Track_dz0 = new vector<double>{};
      Muon_Track_d0sig = new vector<double>{};
      Muon_MatchedTrigger = new vector<bool>{};
      Muon_PassIso = new vector<bool>{};
      mc_Muon_IsoSF = new vector<double>{};
      mc_Muon_RecoSF = new vector<double>{};
      mc_Muon_TriggerSF = new vector<double>{};
      mc_Muon_TriggerEff = new vector<double>{};
      mc_Muon_TTVASF = new vector<double>{};
      Truth_ID = new vector<int>{};
      Truth_Status = new vector<int>{};
      Truth_MotherID = new vector<int>{};
      Truth_Px = new vector<double>{};
      Truth_Py = new vector<double>{};
      Truth_Pz = new vector<double>{};
      Truth_E = new vector<double>{};
      Jet_Pt = new vector<double>{};
      Jet_Eta = new vector<double>{};
      Jet_Phi = new vector<double>{};                           
      Jet_M = new vector<double>{};
      Jet_PassJvt = new vector<bool>{};
      Jet_JvtSF = new vector<float>{};
      Jet_isBad = new vector<bool>{};
      Photon_Pt = new vector<double>{};
      Photon_Eta = new vector<double>{};
      Photon_Phi = new vector<double>{};
      Photon_PassCleaning = new vector<bool>{};
      Photon_PassID = new vector<bool>{};
      Photon_PassIDLoose = new vector<bool>{};
      Photon_PassIso = new vector<bool>{};
      mc_Photon_IDSF = new vector<double>{};
      mc_Photon_IsoSF = new vector<double>{};

      Electron_Pt->reserve(2);
      Electron_Eta->reserve(2);
      Electron_Phi->reserve(2);
      Electron_Charge->reserve(2);
      Electron_PassID->reserve(2);
      Electron_PassIDLoose->reserve(2);
      Electron_Track_d0sig->reserve(2);
      Electron_Track_dz0->reserve(2);
      Electron_PassIso->reserve(2);
      Electron_MatchedTrigger->reserve(2);
      mc_Electron_RecoSF->reserve(2);
      mc_Electron_IDSF->reserve(2);
      mc_Electron_IsoSF->reserve(2);
      mc_Electron_TriggerSF->reserve(2);
      mc_Electron_TriggerEff->reserve(2);
      ForwardElectron_Pt->reserve(2);
      ForwardElectron_Eta->reserve(2);
      ForwardElectron_Phi->reserve(2);
      ForwardElectron_PassID->reserve(2);
      ForwardElectron_PassIDLoose->reserve(2);
      mc_ForwardElectron_IDSF->reserve(2);
      Muon_Pt->reserve(2);
      Muon_Original_Pt->reserve(2);
      Muon_Eta->reserve(2);
      Muon_Phi->reserve(2);
      Muon_Charge->reserve(2);
      Muon_ptcone30->reserve(2);
      Muon_PassID->reserve(2);
      Muon_PassIDLoose->reserve(2);
      Muon_Track_dz0->reserve(2);
      Muon_Track_d0sig->reserve(2);
      Muon_MatchedTrigger->reserve(2);
      Muon_PassIso->reserve(2);
      mc_Muon_IsoSF->reserve(2);
      mc_Muon_RecoSF->reserve(2);
      mc_Muon_TriggerSF->reserve(2);
      mc_Muon_TriggerEff->reserve(2);
      mc_Muon_TTVASF->reserve(2);
      Truth_ID->reserve(10);
      Truth_Status->reserve(10);              
      Truth_MotherID->reserve(10);
      Truth_Px->reserve(10);
      Truth_Py->reserve(10);
      Truth_Pz->reserve(10);
      Truth_E->reserve(10);
      Jet_Pt->reserve(10);
      Jet_Eta->reserve(10);
      Jet_Phi->reserve(10);
      Jet_M->reserve(10);
      Jet_PassJvt->reserve(10);
      Jet_JvtSF->reserve(10);
      Jet_isBad->reserve(10);
      Photon_Pt->reserve(2);
      Photon_Eta->reserve(2);
      Photon_Phi->reserve(2);
      Photon_PassCleaning->reserve(2);
      Photon_PassID->reserve(2);
      Photon_PassIDLoose->reserve(2);
      Photon_PassIso->reserve(2);
      mc_Photon_IDSF->reserve(2);
      mc_Photon_IsoSF->reserve(2);

      WithBadJet = 0;
      ChannelNumber = 0;
      MET = 0;
      MET_Phi = 0;
      mc_EventWeight = 0;
      mc_PileupWeight = 0;
   }

   inline void WYFullEvent::Clean()
   {
      Electron_Pt->clear();
      Electron_Eta->clear();
      Electron_Phi->clear();
      Electron_Charge->clear();
      Electron_PassID->clear();
      Electron_PassIDLoose->clear();
      Electron_Track_d0sig->clear();
      Electron_Track_dz0->clear();
      Electron_PassIso->clear();
      Electron_MatchedTrigger->clear();
      mc_Electron_RecoSF->clear();
      mc_Electron_IDSF->clear();
      mc_Electron_IsoSF->clear();
      mc_Electron_TriggerSF->clear();
      mc_Electron_TriggerEff->clear();
      ForwardElectron_Pt->clear();
      ForwardElectron_Eta->clear();
      ForwardElectron_Phi->clear();
      ForwardElectron_PassID->clear();
      ForwardElectron_PassIDLoose->clear();
      mc_ForwardElectron_IDSF->clear();
      Muon_Pt->clear();
      Muon_Original_Pt->clear();
      Muon_Eta->clear();
      Muon_Phi->clear();
      Muon_Charge->clear();
      Muon_ptcone30->clear();
      Muon_PassID->clear();
      Muon_PassIDLoose->clear();
      Muon_Track_dz0->clear();
      Muon_Track_d0sig->clear();
      Muon_MatchedTrigger->clear();
      Muon_PassIso->clear();
      mc_Muon_IsoSF->clear();
      mc_Muon_RecoSF->clear();
      mc_Muon_TriggerSF->clear();
      mc_Muon_TriggerEff->clear();
      mc_Muon_TTVASF->clear();
      Truth_ID->clear();
      Truth_Status->clear();
      Truth_MotherID->clear();
      Truth_Px->clear();
      Truth_Py->clear();
      Truth_Pz->clear();
      Truth_E->clear();
      Jet_Pt->clear();
      Jet_Eta->clear();
      Jet_Phi->clear();
      Jet_M->clear();
      Jet_PassJvt->clear();
      Jet_JvtSF->clear();
      Jet_isBad->clear();
      Photon_Pt->clear();
      Photon_Eta->clear();
      Photon_Phi->clear();
      Photon_PassCleaning->clear();
      Photon_PassID->clear();
      Photon_PassIDLoose->clear();
      Photon_PassIso->clear();
      mc_Photon_IDSF->clear();
      mc_Photon_IsoSF->clear();

      WithBadJet = 0;
      ChannelNumber = 0;
      MET = 0;
      MET_Phi = 0;
      mc_EventWeight = 0;
      mc_PileupWeight = 0;
   }

   class WYDataSet : public Result
   {
      // This class is the input for other procedures
      // Save all the events after the base line selection

   public:
      WYDataSet(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname = "") : Result(MSG, c, rname, fname){};
      vector<WYEvent> DataVec;
      vector<WYEvent> SignalVec;
      vector<WYEvent> BKGVec;

      virtual void Combine(std::shared_ptr<Result> result) override;
      virtual void PushBack(WYEvent event);
   };

   inline void WYDataSet::PushBack(WYEvent event)
   {
      if (event.ChannelNumber < 0)
         DataVec.push_back(event);
      else if (event.ChannelNumber == 700402 || event.ChannelNumber == 700403)
         SignalVec.push_back(event);
      else
      {
         BKGVec.push_back(event); // ZHZH TEST
      }
   }

   inline void WYDataSet::Combine(std::shared_ptr<Result> result)
   {
      auto subDataSet = std::dynamic_pointer_cast<std::remove_pointer<decltype(this)>::type>(result);
      if (subDataSet != nullptr)
      {
         DataVec.insert(DataVec.end(), subDataSet->DataVec.begin(), subDataSet->DataVec.end());
         SignalVec.insert(SignalVec.end(), subDataSet->SignalVec.begin(), subDataSet->SignalVec.end());
         BKGVec.insert(BKGVec.end(), subDataSet->BKGVec.begin(), subDataSet->BKGVec.end());
      }
   }

   class WYDataSetSC : public Result
   {
      // This class store the pointers to the events
   public:
      WYDataSetSC(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname = "") : Result(MSG, c, rname, fname){};
      vector<WYEvent *> DataVec;
      vector<WYEvent *> SignalVec;
      vector<WYEvent *> BKGVec;

      virtual void Combine(std::shared_ptr<Result> result) override;
      virtual void PushBack(WYEvent *event);
   };

   inline void WYDataSetSC::PushBack(WYEvent *event)
   {
      if (event->ChannelNumber < 0)
         DataVec.push_back(event);
      else if (event->ChannelNumber == 700402 || event->ChannelNumber == 700403)
         SignalVec.push_back(event);
      else
         BKGVec.push_back(event);
   }

   inline void WYDataSetSC::Combine(std::shared_ptr<Result> result)
   {
      auto subDataSet = std::dynamic_pointer_cast<std::remove_pointer<decltype(this)>::type>(result);
      if (subDataSet != nullptr)
      {
         DataVec.insert(DataVec.end(), subDataSet->DataVec.begin(), subDataSet->DataVec.end());
         SignalVec.insert(SignalVec.end(), subDataSet->SignalVec.begin(), subDataSet->SignalVec.end());
         BKGVec.insert(BKGVec.end(), subDataSet->BKGVec.begin(), subDataSet->BKGVec.end());
      }
   }

} //namespace NAGASH
#endif
