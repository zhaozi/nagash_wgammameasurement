#ifndef WYPLOTGROUP_HEADER
#define WYPLOTGROUP_HEADER

#include "NAGASH.h"
#include "WYEvent.h"

using namespace NAGASH;
using namespace std;

class WYBasicPlot1D
{
public:
    map<TString, shared_ptr<Plot1D>> Channel_Map;
    void Fill(WYEvent event, double val, double w)
    {
      if (event.GetChannelName() != "UNKNOWN")
      {
        Channel_Map[event.GetChannelName()]->Fill(val, w);
        if (event.ChannelNumber > 0)
        {
          if (event.ChannelNumber == 700402 || event.ChannelNumber == 700403)
          {
            Channel_Map["Signal"]->Fill(val, w);
          }
          else if (event.GetChannelName() != "WPlusJet")
            Channel_Map["BKG"]->Fill(val, w);
        }
      }
    };
};

class WYBasicPlot2D
{
public:
    map<TString, shared_ptr<Plot2D>> Channel_Map;
    void Fill(WYEvent event, double valx, double valy, double w)
    {
      if (event.GetChannelName() != "UNKNOWN")
      {
        Channel_Map[event.GetChannelName()]->Fill(valx, valy, w);
        if (event.ChannelNumber > 0)
        {
          if (event.ChannelNumber == 700402 || event.ChannelNumber == 700403)
          {
            Channel_Map["Signal"]->Fill(valx, valy, w);
          }
          else if (event.GetChannelName() != "WPlusJet")
            Channel_Map["BKG"]->Fill(valx, valy, w);
        }
      }
    };
};

class WYPlotGroup : public PlotGroup
{
public:
    WYPlotGroup(std::shared_ptr<MSGTool> MSG, std::shared_ptr<ConfigTool> c, const TString &rname, const TString &fname = "", int channel = -999);

    vector<double> ELECTRON_FABS_ETA_BINDIV;
    vector<double> ELECTRON_ETA_BINDIV;
    vector<double> MUON_FABS_ETA_BINDIV;
    vector<double> MUON_ETA_BINDIV;
    vector<double> PHOTON_FABS_ETA_BINDIV;
    vector<double> PHOTON_ETA_BINDIV;
    vector<double> LEPTON_FABS_ETA_BINDIV;
    vector<double> LEPTON_ETA_BINDIV;
    vector<double> LEPTON_PT_BINDIV;
    vector<double> PHOTON_PT_BINDIV;
    vector<double> YJETCR_MET_BINDIV;
    vector<double> YJETCR_MT_BINDIV;
    vector<double> MET_BINDIV;
    vector<double> MT_BINDIV;
    vector<double> PHI_BINDIV;
    vector<double> DELTAR_BINDIV;
    vector<double> LY_MASS_BINDIV;
    vector<double> WJET_PHOTON_ETA_BINDIV;
    vector<double> WJET_PHOTON_PT_BINDIV;
    vector<double> WEY_YJET_ELECTRON_ETA_BINDIV;
    vector<double> WEY_YJET_ELECTRON_PT_BINDIV;
    vector<double> WUY_YJET_MUON_ETA_BINDIV;
    vector<double> WUY_YJET_MUON_PT_BINDIV;
    bool BASELINE_PASS_WECY = true;
    bool BASELINE_PASS_WMUY = true;

    shared_ptr<Plot1D> ChannelNumber;

    WYBasicPlot1D ElectronPt;
    WYBasicPlot1D ElectronEta;
    WYBasicPlot1D PhotonPt_WminusEY;
    WYBasicPlot1D PhotonEta_WminusEY;
    WYBasicPlot1D LYMass_WminusEY;
    WYBasicPlot1D LYDeltaPhi_WminusEY;
    WYBasicPlot1D LYDeltaR_WminusEY;
    WYBasicPlot1D MET_WminusEY;
    WYBasicPlot1D MT_WminusEY;
    WYBasicPlot1D FabsEtaElectron_WminusLarger;
    WYBasicPlot1D FabsEtaElectron_WminusSmaller;
    shared_ptr<Plot1D> FabsEtaElectron_WminusLarger_WjetsRemoved;
    shared_ptr<Plot1D> FabsEtaElectron_WminusSmaller_WjetsRemoved;
    shared_ptr<Plot1D> FabsEtaElectron_WminusLarger_YjetsRemoved;
    shared_ptr<Plot1D> FabsEtaElectron_WminusSmaller_YjetsRemoved;
    shared_ptr<Plot1D> FabsEtaElectron_WminusLarger_BKGRemoved;
    shared_ptr<Plot1D> FabsEtaElectron_WminusSmaller_BKGRemoved;
    WYBasicPlot1D AsymmetryFabsEtaElectron_Wminus;
    
    WYBasicPlot1D PositronPt;
    WYBasicPlot1D PositronEta;
    WYBasicPlot1D PhotonPt_WplusEY;
    WYBasicPlot1D PhotonEta_WplusEY;
    WYBasicPlot1D LYMass_WplusEY;
    WYBasicPlot1D LYDeltaPhi_WplusEY;
    WYBasicPlot1D LYDeltaR_WplusEY;
    WYBasicPlot1D MET_WplusEY;
    WYBasicPlot1D MT_WplusEY;
    WYBasicPlot1D FabsEtaPositron_WplusLarger;
    WYBasicPlot1D FabsEtaPositron_WplusSmaller;
    shared_ptr<Plot1D> FabsEtaPositron_WplusLarger_WjetsRemoved;
    shared_ptr<Plot1D> FabsEtaPositron_WplusSmaller_WjetsRemoved;
    shared_ptr<Plot1D> FabsEtaPositron_WplusLarger_YjetsRemoved;
    shared_ptr<Plot1D> FabsEtaPositron_WplusSmaller_YjetsRemoved;
    shared_ptr<Plot1D> FabsEtaPositron_WplusLarger_BKGRemoved;
    shared_ptr<Plot1D> FabsEtaPositron_WplusSmaller_BKGRemoved;
    WYBasicPlot1D AsymmetryFabsEtaPositron_Wplus;

    WYBasicPlot1D MuonPt;
    WYBasicPlot1D MuonEta;
    WYBasicPlot1D PhotonPt_WminusUY;
    WYBasicPlot1D PhotonEta_WminusUY;
    WYBasicPlot1D LYMass_WminusUY;
    WYBasicPlot1D LYDeltaPhi_WminusUY;
    WYBasicPlot1D LYDeltaR_WminusUY;
    WYBasicPlot1D MET_WminusUY;
    WYBasicPlot1D MT_WminusUY;
    WYBasicPlot1D FabsEtaMuon_WminusSmaller;
    WYBasicPlot1D FabsEtaMuon_WminusLarger;
    shared_ptr<Plot1D> FabsEtaMuon_WminusSmaller_WjetsRemoved;
    shared_ptr<Plot1D> FabsEtaMuon_WminusLarger_WjetsRemoved;
    shared_ptr<Plot1D> FabsEtaMuon_WminusSmaller_YjetsRemoved;
    shared_ptr<Plot1D> FabsEtaMuon_WminusLarger_YjetsRemoved;
    shared_ptr<Plot1D> FabsEtaMuon_WminusSmaller_BKGRemoved;
    shared_ptr<Plot1D> FabsEtaMuon_WminusLarger_BKGRemoved;
    WYBasicPlot1D AsymmetryFabsEtaMuon_Wminus;

    WYBasicPlot1D AntiMuonPt;
    WYBasicPlot1D AntiMuonEta;
    WYBasicPlot1D PhotonPt_WplusUY;
    WYBasicPlot1D PhotonEta_WplusUY;
    WYBasicPlot1D LYMass_WplusUY;
    WYBasicPlot1D LYDeltaPhi_WplusUY;
    WYBasicPlot1D LYDeltaR_WplusUY;
    WYBasicPlot1D MET_WplusUY;
    WYBasicPlot1D MT_WplusUY;
    WYBasicPlot1D FabsEtaAntiMuon_WplusLarger;
    WYBasicPlot1D FabsEtaAntiMuon_WplusSmaller;
    shared_ptr<Plot1D> FabsEtaAntiMuon_WplusLarger_WjetsRemoved;
    shared_ptr<Plot1D> FabsEtaAntiMuon_WplusSmaller_WjetsRemoved;
    shared_ptr<Plot1D> FabsEtaAntiMuon_WplusLarger_YjetsRemoved;
    shared_ptr<Plot1D> FabsEtaAntiMuon_WplusSmaller_YjetsRemoved;
    shared_ptr<Plot1D> FabsEtaAntiMuon_WplusLarger_BKGRemoved;
    shared_ptr<Plot1D> FabsEtaAntiMuon_WplusSmaller_BKGRemoved;
    WYBasicPlot1D AsymmetryFabsEtaAntiMuon_Wplus;

    WYBasicPlot2D WJETCR_LT_PhotonEta_PhotonPt_2D;
    WYBasicPlot2D WJETCR_TL_PhotonEta_PhotonPt_2D;
    WYBasicPlot2D WJETCR_LL_PhotonEta_PhotonPt_2D;
    shared_ptr<Plot2D> WJETCR_LT_PhotonEta_PhotonPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WJETCR_TL_PhotonEta_PhotonPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WJETCR_LL_PhotonEta_PhotonPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WJETCR_LT_PhotonEta_PhotonPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WJETCR_TL_PhotonEta_PhotonPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WJETCR_LL_PhotonEta_PhotonPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WJETCR_SF_PhotonEta_PhotonPt_2D; // SF = LT / LL
    
    WYBasicPlot2D WEY_WJETCR_TT_PhotonEta_PhotonPt_2D;
    WYBasicPlot2D WUY_WJETCR_TT_PhotonEta_PhotonPt_2D;
    shared_ptr<Plot2D> WEY_WJETCR_MCSF_PhotonEta_PhotonPt_2D; // MCSF = TT_WPlusJet / TT_Data
    shared_ptr<Plot2D> WUY_WJETCR_MCSF_PhotonEta_PhotonPt_2D; // MCSF = TT_WPlusJet / TT_Data

    WYBasicPlot1D WEY_YJETCR_TL_LeptonPt;
    WYBasicPlot1D WEY_YJETCR_TL_LeptonEta;
    WYBasicPlot1D WEY_YJETCR_TL_PhotonPt;
    WYBasicPlot1D WEY_YJETCR_TL_PhotonEta;
    WYBasicPlot1D WEY_YJETCR_TL_MET;
    WYBasicPlot1D WEY_YJETCR_TL_MT;
    WYBasicPlot1D WEY_YJETCR_TL_LYMass;
    WYBasicPlot1D WEY_YJETCR_LT_LeptonPt;
    WYBasicPlot1D WEY_YJETCR_LT_LeptonEta;
    WYBasicPlot1D WEY_YJETCR_LT_PhotonPt;
    WYBasicPlot1D WEY_YJETCR_LT_PhotonEta;
    WYBasicPlot1D WEY_YJETCR_LT_MET;
    WYBasicPlot1D WEY_YJETCR_LT_MT;
    WYBasicPlot1D WEY_YJETCR_LT_LYMass;
    WYBasicPlot1D WEY_YJETCR_LL_LeptonPt;
    WYBasicPlot1D WEY_YJETCR_LL_LeptonEta;
    WYBasicPlot1D WEY_YJETCR_LL_PhotonPt;
    WYBasicPlot1D WEY_YJETCR_LL_PhotonEta;
    WYBasicPlot1D WEY_YJETCR_LL_MET;
    WYBasicPlot1D WEY_YJETCR_LL_MT;
    WYBasicPlot1D WEY_YJETCR_LL_LYMass;

    WYBasicPlot2D WEY_YJETCR_LT_ElectronEta_ElectronPt_2D;
    WYBasicPlot2D WEY_YJETCR_TL_ElectronEta_ElectronPt_2D;
    WYBasicPlot2D WEY_YJETCR_LL_ElectronEta_ElectronPt_2D;
    shared_ptr<Plot2D> WEY_YJETCR_LT_ElectronEta_ElectronPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_TL_ElectronEta_ElectronPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_LL_ElectronEta_ElectronPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_LT_ElectronEta_ElectronPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_TL_ElectronEta_ElectronPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_LL_ElectronEta_ElectronPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_LT_ElectronEta_ElectronPt_WjetsRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_TL_ElectronEta_ElectronPt_WjetsRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_LL_ElectronEta_ElectronPt_WjetsRemoved_2D;
    shared_ptr<Plot2D> WEY_YJETCR_SF_ElectronEta_ElectronPt_2D; //  SF = TL / LL (different from 7TeV Note)

    WYBasicPlot1D WUY_YJETCR_TL_LeptonPt;
    WYBasicPlot1D WUY_YJETCR_TL_LeptonEta;
    WYBasicPlot1D WUY_YJETCR_TL_PhotonPt;
    WYBasicPlot1D WUY_YJETCR_TL_PhotonEta;
    WYBasicPlot1D WUY_YJETCR_TL_MET;
    WYBasicPlot1D WUY_YJETCR_TL_MT;
    WYBasicPlot1D WUY_YJETCR_TL_LYMass;
    WYBasicPlot1D WUY_YJETCR_LT_LeptonPt;
    WYBasicPlot1D WUY_YJETCR_LT_LeptonEta;
    WYBasicPlot1D WUY_YJETCR_LT_PhotonPt;
    WYBasicPlot1D WUY_YJETCR_LT_PhotonEta;
    WYBasicPlot1D WUY_YJETCR_LT_MET;
    WYBasicPlot1D WUY_YJETCR_LT_MT;
    WYBasicPlot1D WUY_YJETCR_LT_LYMass;
    WYBasicPlot1D WUY_YJETCR_LL_LeptonPt;
    WYBasicPlot1D WUY_YJETCR_LL_LeptonEta;
    WYBasicPlot1D WUY_YJETCR_LL_PhotonPt;
    WYBasicPlot1D WUY_YJETCR_LL_PhotonEta;
    WYBasicPlot1D WUY_YJETCR_LL_MET;
    WYBasicPlot1D WUY_YJETCR_LL_MT;
    WYBasicPlot1D WUY_YJETCR_LL_LYMass;

    WYBasicPlot2D WUY_YJETCR_LT_MuonEta_MuonPt_2D;
    WYBasicPlot2D WUY_YJETCR_TL_MuonEta_MuonPt_2D;
    WYBasicPlot2D WUY_YJETCR_LL_MuonEta_MuonPt_2D;
    shared_ptr<Plot2D> WUY_YJETCR_LT_MuonEta_MuonPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_TL_MuonEta_MuonPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_LL_MuonEta_MuonPt_SignalRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_LT_MuonEta_MuonPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_TL_MuonEta_MuonPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_LL_MuonEta_MuonPt_BKGRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_LT_MuonEta_MuonPt_WjetsRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_TL_MuonEta_MuonPt_WjetsRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_LL_MuonEta_MuonPt_WjetsRemoved_2D;
    shared_ptr<Plot2D> WUY_YJETCR_SF_MuonEta_MuonPt_2D; //  SF = TL / LL (different from 7TeV Note)
    // Could use YJETCR_LL for template fitting method for the Yjets BKG

    void BookWminusEYChannelPlots(int channel);
    void BookWplusEYChannelPlots(int channel);
    void BookWminusUYChannelPlots(int channel);
    void BookWplusUYChannelPlots(int channel);

    void FillWminusEYChannelEvent(const WYEvent event);
    void FillWplusEYChannelEvent(const WYEvent event);
    void FillWminusUYChannelEvent(const WYEvent event);
    void FillWplusUYChannelEvent(const WYEvent event);
    void FillWJETCRChannelEvent(const WYEvent event);
    void FillWEYWPlusJetChannelEvent(const WYEvent event);
    void FillWUYWPlusJetChannelEvent(const WYEvent event);
    void FillWEYYJETCRChannelEvent(const WYEvent event);
    void FillWUYYJETCRChannelEvent(const WYEvent event);
  
    WYBasicPlot1D BookBasicPlot1D(TString name, vector<double> bindivx, int channel);
    WYBasicPlot2D BookBasicPlot2D(TString name, vector<double> bindivx, vector<double> bindivy, int channel);
    void FillEvent(WYEvent &event);
    TString GetChannelName(int channel);
    
    void AddLYFakePlots(shared_ptr<WYPlotGroup> LYFakePlotGroup);
    void ResetWPlusJetMCSF(shared_ptr<WYPlotGroup> LYFakePlotGroup);
    void ResetYJETCRSF(shared_ptr<WYPlotGroup> LYFakePlotGroup);
    void RescaleYjets(double scaleWEY, double scaleWUY);
};

#endif
