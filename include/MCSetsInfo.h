// Get the info of MC sets from mcsetsinfo.csv
// User can simply input the filename and get the xsec of correspond mc set
// e.g.
//  MCSetsInfo info(std::make_shared<MSGTool>(), "/Data2/MC16a_ForData1516/mcsetsinfo.csv");
//  std::cout << info.GetXsecPb("/Data2/MC16a_ForData1516/V20210624/user.chenw.COMMON_MC16a_WZSkim_V20210727.363356.e5525_s3126_r9364_r9315_p4512_myOutput.root/user.chenw.26338972._000001.myOutput.root") << std::endl;
//
//
//
#ifndef MCSETSINFO_HEADER
#define MCSETSINFO_HEADER

#include "Tool.h"

class MCSetsInfo : public NAGASH::Tool
{
public:
    MCSetsInfo(std::shared_ptr<NAGASH::MSGTool> msg, const std::string &filename) : NAGASH::Tool(msg)
    {
        std::ifstream infile;
        infile.open(filename.c_str());

        std::vector<std::vector<std::string>> info;
        // read all the data
        std::string line;
        std::getline(infile, line); // dump the first line
        while (std::getline(infile, line))
        {
            std::stringstream lineStream(line);
            std::string cell;
            std::vector<std::string> result;
            while (std::getline(lineStream, cell, ','))
                result.emplace_back(cell);

            info.emplace_back(result);
        }

        infile.close();

        // prepare the XsecMap
        for (auto &i : info)
        {
            auto findresult = XsecMap.find(i[8]);
            double xsec, kfactor = 1, filtereff = 1;
            xsec = std::atof(i[6].c_str());
            if (i[11] != "")
                kfactor = std::atof(i[11].c_str());
            if (i[12] != "")
                filtereff = std::atof(i[12].c_str());

            if (findresult == XsecMap.end())
            {
                std::map<std::string, double> tempmap;
                tempmap.emplace(std::make_pair(i[5], xsec * kfactor * filtereff * 1000));
                XsecMap.emplace(std::make_pair(i[8], tempmap));
            }
            else
                findresult->second.emplace(std::make_pair(i[5], xsec * kfactor * filtereff * 1000));
        }
    }

    double GetXsecPb(const std::string &channelnumber, const std::string &version)
    {
        auto findresult = XsecMap.find(channelnumber);
        if (findresult != XsecMap.end())
        {
            auto findresult2 = findresult->second.find(version);
            if (findresult2 != findresult->second.end())
                return findresult2->second;
        }

        auto titleguard = MSGUser()->StartTitleWithGuard("MCSetInfo::GetXsecPb");
        MSGUser()->MSG_WARNING("No channel number or version is found, returning zero (input channe number : ", channelnumber, " version : ", version, ")");
        return 0;
    }

    // this function trys to parser filename to get channelnumber and version
    double GetXsecPb(const std::string &filename)
    {
        return GetXsecPb(GetChannel(filename), GetVersion(filename));
    }

    std::string GetChannel(const std::string &filename) const
    {
        std::regex channelregex("(\\.[0-9]{6}\\.)");
        auto word_begin = std::sregex_iterator(filename.begin(), filename.end(), channelregex);
        std::string channelnumber = (*word_begin).str();
        channelnumber.erase(0, 1);
        channelnumber.pop_back();

        return channelnumber;
    }

    std::string GetVersion(const std::string &filename) const
    {
        std::regex versionregex("(\\.e.+p[0-9]{4})");
        auto word_begin = std::sregex_iterator(filename.begin(), filename.end(), versionregex);
        std::string version = (*word_begin).str();
        version.erase(0, 1);

        return version;
    }

    int GetChannelNumber(const std::string &filename) const
    {
        return std::atoi(GetChannel(filename).c_str());
    }

private:
    std::map<std::string, std::map<std::string, double>> XsecMap;
};

#endif
