#ifndef WYLoopEvent_HEADER
#define WYLoopEvent_HEADER

#include "NAGASH.h"
#include "WYEvent.h"
#include "WYPlotGroup.h"

using namespace NAGASH;
using namespace std;

class WYLoopEvent : public NAGASH::LoopEvent
{
public:
   WYLoopEvent(const NAGASH::ConfigTool &c) : NAGASH::LoopEvent(c) {} // Constructor
   virtual NAGASH::StatusCode InitializeData() override;
   virtual NAGASH::StatusCode InitializeUser() override;
   virtual NAGASH::StatusCode Execute() override;
   virtual NAGASH::StatusCode Finalize() override;
   virtual NAGASH::StatusCode InitializeParameters();
   virtual NAGASH::StatusCode InitializeNomalization();

   // Functions used for selection
   bool BaseLineSelection(WYFullEvent fullevent, WYEvent &event);
   bool ElectronBaseLineSelection(double pt, double eta, double dz0, double d0sig, bool passid);
   bool MuonBaseLineSelection(double pt, double eta, double dz0, double d0sig, bool passid);
   bool ForwardElectronBaseLineSelection(double pt, double eta);
   bool JetBaseLineSelection(double pt, double eta, bool passjvt);
   bool PhotonBaseLineSelection(double pt, double eta, bool passcleaning, bool passid);
   bool TriggerMatch(vector<bool> *el, vector<bool> *mu);
   WYEventType EventIdentification(int nelectron, int npositron, int nmuon, int nantimuon, int nfwd, int njet, int nphoton);

   LeptonType ElectronSelection(int charge, bool passid, bool passiso);
   LeptonType MuonSelection(int charge, bool passid, bool passiso, double iso);
   LeptonType ForwardElectronSelection(bool passid);
   PhotonType PhotonSelection(bool passid, bool passiso);
   double GetElectronWeight(LeptonType type, double recosf, double idsf, double isosf);
   double GetMuonWeight(LeptonType type, double recosf, double ttvasf, double isosf);
   double GetPhotonWeight(PhotonType type, double idsf, double isosf); // all the photon triggers are for multi-photon
   double GetEventWeight(double evt, double pileup, WYEventType type, double sf);
   bool LeptonTruthMatch(WYEvent event);
   bool LeptonTruthMatch(TLorentzVector lepton, TLorentzVector truthlepton);

   bool IsUselessMC(int channelnum);

   // Functions needed for new sample
   bool PreSelection();
   void CountParticleNumbers();
   void RebuildFullEvent();

   // auxiliary variable to storage event info
   int NElectronsLast = 0;
   int NForwardElectronsLast = 0;
   int NMuonsLast = 0;
   int NJetsLast = 0;
   int NPhotonsLast = 0;
   int NTruthJetsLast = 0;
   int NTruthParticlesLast = 0;
   int NVertexLast = 0;

   // constant used in the selection
   double MUON_BASELINE_PTMIN = 15;
   double MUON_BASELINE_FABS_ETAMAX = 2.5;
   double MUON_BASELINE_FABS_DZ0MAX = 0.5;
   double MUON_BASELINE_FABS_D0SIGMAX = 3;
   double MUON_BASELINE_ISOMAX = 0.06;

   double ELEC_BASELINE_PTMIN;
   double ELEC_BASELINE_FABS_ETAMAX = 2.5;
   double ELEC_BASELINE_CRACK_FABS_ETAMAX = 1.52;
   double ELEC_BASELINE_CRACK_FABS_ETAMIN = 1.37;
   double ELEC_BASELINE_FABS_DZ0MAX = 0.5;
   double ELEC_BASELINE_FABS_D0SIGMAX = 5;

   double FWDELEC_BASELINE_PTMIN = 15;
   double FWDELEC_BASELINE_FABS_ETAMAX = 4.9;
   double FWDELEC_BASELINE_FABS_ETAMIN = 2.5;
   double FWDELEC_BASELINE_CRACK_FABS_ETAMAX = 3.35;
   double FWDELEC_BASELINE_CRACK_FABS_ETAMIN = 3.16;

   double JET_BASELINE_FABS_ETAMAX = 4.5;
   double JET_BASELINE_PTMIN = 35;
   double JET_BASELINE_OVERLEP = 0.6;

   double PHOTON_BASELINE_PTMIN = 15;
   double PHOTON_BASELINE_FABS_ETAMAX = 2.37;
   double PHOTON_BASELINE_CRACK_FABS_ETAMAX = 1.52;
   double PHOTON_BASELINE_CRACK_FABS_ETAMIN = 1.37;

   double WECY_BASELINE_ZVETO_DELTAMASSMIN = 10;
   double WMUY_BASELINE_ZVETO_DELTAMASSMIN = 0;
   double WECY_BASELINE_ELEC_PTMIN = 25;
   double WMUY_BASELINE_MUON_PTMIN = 25;
   double WY_BASELINE_METMIN = 25;
   double WY_BASELINE_MWTMIN = 40;
   double WY_BASELINE_LY_DELTARMIN = 0.7;

   bool BASELINE_PASS_WECY = true;
   //bool BASELINE_PASS_WEFY;
   bool BASELINE_PASS_WMUY = true;

   int EstimateYjetsMode = 2;

   double NormalizationFactor = 1;
   int ChannelNumber_FromFile = -1;
   double SumOfWeights_FromFile = -1;
   WYFullEvent DefaultEvent;
   std::shared_ptr<WYDataSet> MyDataSet;
   std::shared_ptr<WYPlotGroup> MyPlots;

   // Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Bool_t PassTrigger;
   Bool_t PassElectronTrigger;
   Bool_t PassMuonTrigger;
   Bool_t TriggerMatched;
   Bool_t WithBadJet;
   Bool_t IsBadBatman;
   Int_t NElectrons;
   Int_t NLooseElectrons;
   Int_t NMediumElectrons;
   Int_t NTightElectrons;
   Int_t NMuons;
   Int_t NLooseMuons;
   Int_t NMediumMuons;
   Int_t NTightMuons;
   Int_t NHighPtMuons;
   Int_t NForwardElectrons;
   Int_t NJets;
   Int_t NBJets_Eff60;
   Int_t NBJets_Eff70;
   Int_t NBJets_Eff77;
   Int_t NBJets_Eff85;
   Int_t NPhotons;
   Int_t NLoosePhotons;
   Int_t NTightPhotons;
   Int_t NTruthParticles;
   Int_t NTruthJets;

   // List of branches
   TBranch *b_PassTrigger;         //!
   TBranch *b_PassElectronTrigger; //!
   TBranch *b_PassMuonTrigger;     //!
   TBranch *b_TriggerMatched;      //!
   TBranch *b_WithBadJet;          //!
   TBranch *b_IsBadBatman;         //!
   TBranch *b_NElectrons;          //!
   TBranch *b_NLooseElectrons;     //!
   TBranch *b_NMediumElectrons;    //!
   TBranch *b_NTightElectrons;     //!
   TBranch *b_NMuons;              //!
   TBranch *b_NLooseMuons;         //!
   TBranch *b_NMediumMuons;        //!
   TBranch *b_NTightMuons;         //!
   TBranch *b_NHighPtMuons;        //!
   TBranch *b_NForwardElectrons;   //!
   TBranch *b_NJets;               //!
   TBranch *b_NBJets_Eff60;        //!
   TBranch *b_NBJets_Eff70;        //!
   TBranch *b_NBJets_Eff77;        //!
   TBranch *b_NBJets_Eff85;        //!
   TBranch *b_NPhotons;              //!
   TBranch *b_NLoosePhotons;         //!
   TBranch *b_NTightPhotons;         //!
   TBranch *b_NTruthParticles;     //!
   TBranch *b_NTruthJets;          //!

   // Electron_Calibration_Tree
   TTree *Electron_Calibration_Tree = nullptr;
   Float_t Electron_Original_Pt;
   Float_t Electron_Calibrated_Pt_S12Off;

   TBranch *b_Electron_Original_Pt;          //!
   TBranch *b_Electron_Calibrated_Pt_S12Off; //!

   // Electron_Cluster_Tree
   TTree *Electron_Cluster_Tree = nullptr;
   Int_t Electron_nCluster;
   Float_t Electron_Cluster_ETACALOFRAME;
   Float_t Electron_Cluster_PHICALOFRAME;
   Float_t Electron_Cluster_ETA1CALOFRAME;
   Float_t Electron_Cluster_PHI1CALOFRAME;
   Float_t Electron_Cluster_ETA2CALOFRAME;
   Float_t Electron_Cluster_PHI2CALOFRAME;

   TBranch *b_Electron_nCluster;              //!
   TBranch *b_Electron_Cluster_ETACALOFRAME;  //!
   TBranch *b_Electron_Cluster_PHICALOFRAME;  //!
   TBranch *b_Electron_Cluster_ETA1CALOFRAME; //!
   TBranch *b_Electron_Cluster_PHI1CALOFRAME; //!
   TBranch *b_Electron_Cluster_ETA2CALOFRAME; //!
   TBranch *b_Electron_Cluster_PHI2CALOFRAME; //!

   // Electron_Isolation_Tree
   TTree *Electron_Isolation_Tree = nullptr;
   Float_t Electron_Isolation_ptcone20;
   Float_t Electron_Isolation_ptcone30;
   Float_t Electron_Isolation_ptcone40;
   Float_t Electron_Isolation_ptvarcone20;
   Float_t Electron_Isolation_ptvarcone30_TightTTVA_pt1000;
   Float_t Electron_Isolation_ptcone20_TightTTVALooseCone_pt500;
   Float_t Electron_Isolation_ptcone20_TightTTVALooseCone_pt1000;
   Float_t Electron_Isolation_ptvarcone20_TightTTVALooseCone_pt1000;
   Float_t Electron_Isolation_ptvarcone30_TightTTVALooseCone_pt1000;
   Float_t Electron_Isolation_ptvarcone40_TightTTVALooseCone_pt1000;
   Float_t Electron_Isolation_topoetcone20;
   Float_t Electron_Isolation_topoetcone30;
   Float_t Electron_Isolation_topoetcone40;
   Float_t Electron_Isolation_neflowisol20;

   TBranch *b_Electron_Isolation_ptcone20;                              //!
   TBranch *b_Electron_Isolation_ptcone30;                              //!
   TBranch *b_Electron_Isolation_ptcone40;                              //!
   TBranch *b_Electron_Isolation_ptvarcone20;                           //!
   TBranch *b_Electron_Isolation_ptvarcone30_TightTTVA_pt1000;          //!
   TBranch *b_Electron_Isolation_ptcone20_TightTTVALooseCone_pt500;     //!
   TBranch *b_Electron_Isolation_ptcone20_TightTTVALooseCone_pt1000;    //!
   TBranch *b_Electron_Isolation_ptvarcone20_TightTTVALooseCone_pt1000; //!
   TBranch *b_Electron_Isolation_ptvarcone30_TightTTVALooseCone_pt1000; //!
   TBranch *b_Electron_Isolation_ptvarcone40_TightTTVALooseCone_pt1000; //!
   TBranch *b_Electron_Isolation_topoetcone20;                          //!
   TBranch *b_Electron_Isolation_topoetcone30;                          //!
   TBranch *b_Electron_Isolation_topoetcone40;                          //!
   TBranch *b_Electron_Isolation_neflowisol20;                          //!

   // Electron_MediumID_ChargeMisIDSF_Syst_Tree
   TTree *Electron_MediumID_ChargeMisIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;
   Float_t mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;
   Float_t mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down;
   Float_t mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;

   TBranch *b_mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;     //!
   TBranch *b_mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;       //!
   TBranch *b_mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down; //!
   TBranch *b_mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;   //!

   // Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree
   TTree *Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;

   TBranch *b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;     //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down; //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;   //!

   // Electron_MediumID_FCLooseIsoSF_Syst_Tree
   TTree *Electron_MediumID_FCLooseIsoSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree
   TTree *Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree
   TTree *Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree
   TTree *Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;

   TBranch *b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;     //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down; //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;   //!

   // Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree
   TTree *Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;

   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;     //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down; //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;   //!

   // Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree
   TTree *Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_MediumID_FCTightIsoSF_Syst_Tree
   TTree *Electron_MediumID_FCTightIsoSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree
   TTree *Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree
   TTree *Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_MediumIDSF_Syst_Tree
   TTree *Electron_MediumIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down;      //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up;        //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down;      //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up;        //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down;      //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up;        //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down;      //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up;        //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down;      //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up;        //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_Pt_Syst_Tree
   TTree *Electron_Pt_Syst_Tree = nullptr;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down;
   Float_t Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up;

   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1down;                //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up;                  //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up;           //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1down;                     //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1up;                       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down; //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up;   //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down; //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up;   //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down; //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up;   //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1down;                      //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1up;                        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down;                  //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up;                    //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down;                  //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up;                    //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up;              //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down;  //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up;    //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down;  //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up;    //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down; //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up;   //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down; //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up;   //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down;        //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down;          //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up;           //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up;           //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down;                //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up;                  //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down;           //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down;             //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up;               //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up;              //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up;              //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up;              //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up;              //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down;            //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up;              //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down;       //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up;         //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down;                  //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up;                    //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down;                 //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up;                   //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down;                 //!
   TBranch *b_Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up;                   //!

   // Electron_RecoSF_Syst_Tree
   TTree *Electron_RecoSF_Syst_Tree = nullptr;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_SF_Tree
   TTree *Electron_SF_Tree = nullptr;
   Float_t mc_Electron_RecoSF;
   Float_t mc_Electron_MediumIDSF;
   Float_t mc_Electron_TightIDSF;
   Float_t mc_Electron_MediumID_FCLooseIsoSF;
   Float_t mc_Electron_TightID_FCLooseIsoSF;
   Float_t mc_Electron_MediumID_FCTightIsoSF;
   Float_t mc_Electron_TightID_FCTightIsoSF;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerSF;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerSF;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF;
   Float_t mc_Electron_MediumID_FCLooseIso_TriggerEff;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff;
   Float_t mc_Electron_MediumID_FCTightIso_TriggerEff;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLooseSF;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF;
   Float_t mc_Electron_MediumID_ChargeMisIDSF;
   Float_t mc_Electron_TightID_ChargeMisIDSF;
   Float_t mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF;
   Float_t mc_Electron_TightID_FCLooseIso_ChargeMisIDSF;
   Float_t mc_Electron_MediumID_FCTightIso_ChargeMisIDSF;
   Float_t mc_Electron_TightID_FCTightIso_ChargeMisIDSF;
   Float_t mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF;

   TBranch *b_mc_Electron_RecoSF;                                       //!
   TBranch *b_mc_Electron_MediumIDSF;                                   //!
   TBranch *b_mc_Electron_TightIDSF;                                    //!
   TBranch *b_mc_Electron_MediumID_FCLooseIsoSF;                        //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF;                         //!
   TBranch *b_mc_Electron_MediumID_FCTightIsoSF;                        //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF;                         //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerSF;                //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF;                 //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerSF;                //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF;                 //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_TriggerEff;               //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff;                //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_TriggerEff;               //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff;                //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF;             //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF;              //!
   TBranch *b_mc_Electron_MediumID_ChargeMisIDSF;                       //!
   TBranch *b_mc_Electron_TightID_ChargeMisIDSF;                        //!
   TBranch *b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF;            //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF;             //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF;            //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF;             //!
   TBranch *b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF; //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF;  //!

   // Electron_ShowerShape_Tree
   TTree *Electron_ShowerShape_Tree = nullptr;
   Float_t Electron_ShowerShape_f1;
   Float_t Electron_ShowerShape_f3;
   Float_t Electron_ShowerShape_e277;
   Float_t Electron_ShowerShape_weta1;
   Float_t Electron_ShowerShape_weta2;
   Float_t Electron_ShowerShape_fracs1;
   Float_t Electron_ShowerShape_wtots1;
   Float_t Electron_ShowerShape_Reta;
   Float_t Electron_ShowerShape_Rphi;
   Float_t Electron_ShowerShape_Eratio;
   Float_t Electron_ShowerShape_Rhad;
   Float_t Electron_ShowerShape_Rhad1;
   Float_t Electron_ShowerShape_DeltaE;

   TBranch *b_Electron_ShowerShape_f1;     //!
   TBranch *b_Electron_ShowerShape_f3;     //!
   TBranch *b_Electron_ShowerShape_e277;   //!
   TBranch *b_Electron_ShowerShape_weta1;  //!
   TBranch *b_Electron_ShowerShape_weta2;  //!
   TBranch *b_Electron_ShowerShape_fracs1; //!
   TBranch *b_Electron_ShowerShape_wtots1; //!
   TBranch *b_Electron_ShowerShape_Reta;   //!
   TBranch *b_Electron_ShowerShape_Rphi;   //!
   TBranch *b_Electron_ShowerShape_Eratio; //!
   TBranch *b_Electron_ShowerShape_Rhad;   //!
   TBranch *b_Electron_ShowerShape_Rhad1;  //!
   TBranch *b_Electron_ShowerShape_DeltaE; //!

   // Electron_TightID_ChargeMisIDSF_Syst_Tree
   TTree *Electron_TightID_ChargeMisIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;
   Float_t mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;
   Float_t mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down;
   Float_t mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;

   TBranch *b_mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;     //!
   TBranch *b_mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;       //!
   TBranch *b_mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down; //!
   TBranch *b_mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;   //!

   // Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree
   TTree *Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;
   Float_t mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;
   Float_t mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down;
   Float_t mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;

   TBranch *b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;     //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down; //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;   //!

   // Electron_TightID_FCLooseIsoSF_Syst_Tree
   TTree *Electron_TightID_FCLooseIsoSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree
   TTree *Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree
   TTree *Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree
   TTree *Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;
   Float_t mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;
   Float_t mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down;
   Float_t mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;

   TBranch *b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;     //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down; //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;   //!

   // Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree
   TTree *Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;

   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down;     //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down; //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up;   //!

   // Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree
   TTree *Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_TightID_FCTightIsoSF_Syst_Tree
   TTree *Electron_TightID_FCTightIsoSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_TightID_FCTightIso_TriggerEff_Syst_Tree
   TTree *Electron_TightID_FCTightIso_TriggerEff_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_TightID_FCTightIso_TriggerSF_Syst_Tree
   TTree *Electron_TightID_FCTightIso_TriggerSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_TightIDSF_Syst_Tree
   TTree *Electron_TightIDSF_Syst_Tree = nullptr;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down;
   Float_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up;
   Int_t mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex;

   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down;      //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up;        //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down;      //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up;        //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down;      //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up;        //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down;      //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up;        //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down;      //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up;        //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down;      //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up;        //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down;       //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up;         //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down;   //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up;     //!
   TBranch *b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex; //!

   // Electron_Track_Tree
   TTree *Electron_Track_Tree = nullptr;
   Float_t Electron_Track_Pt;
   Float_t Electron_Track_Eta;
   Float_t Electron_Track_Phi;
   Float_t Electron_Track_M;
   Float_t Electron_Track_E;
   Float_t Electron_Track_Rapidity;
   Float_t Electron_Track_Charge;
   Float_t Electron_Track_d0;
   Float_t Electron_Track_z0;
   Float_t Electron_Track_Phi0;
   Float_t Electron_Track_Theta;
   Float_t Electron_Track_qOverP;
   Float_t Electron_Track_Chi2;
   Float_t Electron_Track_NDoF;

   TBranch *b_Electron_Track_Pt;       //!
   TBranch *b_Electron_Track_Eta;      //!
   TBranch *b_Electron_Track_Phi;      //!
   TBranch *b_Electron_Track_M;        //!
   TBranch *b_Electron_Track_E;        //!
   TBranch *b_Electron_Track_Rapidity; //!
   TBranch *b_Electron_Track_Charge;   //!
   TBranch *b_Electron_Track_d0;       //!
   TBranch *b_Electron_Track_z0;       //!
   TBranch *b_Electron_Track_Phi0;     //!
   TBranch *b_Electron_Track_Theta;    //!
   TBranch *b_Electron_Track_qOverP;   //!
   TBranch *b_Electron_Track_Chi2;     //!
   TBranch *b_Electron_Track_NDoF;     //!

   // Electron_Tree
   TTree *Electron_Tree = nullptr;
   Float_t Electron_Calibrated_Pt;
   Float_t Electron_Eta;
   Float_t Electron_Phi;
   Float_t Electron_Charge;
   Bool_t Electron_PassIDMedium;
   Bool_t Electron_PassIDLoose;
   Bool_t Electron_PassIDTight;
   Float_t Electron_Track_dz0;
   Float_t Electron_Track_d0sig;
   Bool_t Electron_MatchedTrigger;
   Bool_t Electron_PassECIDSLoose;
   Bool_t Electron_PassIso_FCLoose;
   Bool_t Electron_PassIso_FCTight;

   TBranch *b_Electron_Calibrated_Pt;   //!
   TBranch *b_Electron_Eta;             //!
   TBranch *b_Electron_Phi;             //!
   TBranch *b_Electron_Charge;          //!
   TBranch *b_Electron_PassIDMedium;    //!
   TBranch *b_Electron_PassIDLoose;     //!
   TBranch *b_Electron_PassIDTight;     //!
   TBranch *b_Electron_Track_dz0;       //!
   TBranch *b_Electron_Track_d0sig;     //!
   TBranch *b_Electron_MatchedTrigger;  //!
   TBranch *b_Electron_PassECIDSLoose;  //!
   TBranch *b_Electron_PassIso_FCLoose; //!
   TBranch *b_Electron_PassIso_FCTight; //!

   // Electron_Trigger_Tree
   TTree *Electron_Trigger_Tree = nullptr;
   Bool_t MatchedTrigger_HLT_e24_lhmedium_L1EM20VH;
   Bool_t MatchedTrigger_HLT_e60_lhmedium;
   Bool_t MatchedTrigger_HLT_e120_lhloose;
   Bool_t MatchedTrigger_HLT_e26_lhtight_nod0_ivarloose;
   Bool_t MatchedTrigger_HLT_e60_lhmedium_nod0;
   Bool_t MatchedTrigger_HLT_e140_lhloose_nod0;
   Bool_t MatchedTrigger_HLT_e300_etcut;

   TBranch *b_MatchedTrigger_HLT_e24_lhmedium_L1EM20VH;      //!
   TBranch *b_MatchedTrigger_HLT_e60_lhmedium;               //!
   TBranch *b_MatchedTrigger_HLT_e120_lhloose;               //!
   TBranch *b_MatchedTrigger_HLT_e26_lhtight_nod0_ivarloose; //!
   TBranch *b_MatchedTrigger_HLT_e60_lhmedium_nod0;          //!
   TBranch *b_MatchedTrigger_HLT_e140_lhloose_nod0;          //!
   TBranch *b_MatchedTrigger_HLT_e300_etcut;                 //!

   // Electron_Useless_Tree
   TTree *Electron_Useless_Tree = nullptr;
   Float_t Electron_ECIDS;

   TBranch *b_Electron_ECIDS; //!

   // EventInfo_Tree
   TTree *EventInfo_Tree = nullptr;
   Float_t SumOfWeights_Channel;
   Float_t TotalEventProcessed_Channel;
   Long64_t EventNumber;
   Int_t RunNumber;
   Int_t ChannelNumber;
   Int_t BCID;
   Int_t LumiBlockNumber;
   Float_t Mu;
   Float_t Average_Mu;
   Float_t Corrected_Mu;
   Float_t Corrected_Average_Mu;
   Float_t CorrectedAndScaled_Mu;
   Float_t CorrectedAndScaled_Average_Mu;
   Int_t MuIndependent_RunNumber;
   Float_t BeamPosition_X;
   Float_t BeamPosition_Y;
   Float_t BeamPosition_Z;
   Float_t BeamPosition_SigmaX;
   Float_t BeamPosition_SigmaY;
   Float_t BeamPosition_SigmaZ;
   Float_t mc_evtwt;
   Float_t mc_pileupwt;
   Int_t NVertex;
   Int_t NPrimaryVertex;
   Int_t NPileUpVertex;
   Float_t MET_Px;
   Float_t MET_Py;
   Float_t MET_MET;
   Float_t MET_Phi;
   Float_t MET_SumET;

   TBranch *b_SumOfWeights_Channel;          //!
   TBranch *b_TotalEventProcessed_Channel;   //!
   TBranch *b_EventNumber;                   //!
   TBranch *b_RunNumber;                     //!
   TBranch *b_ChannelNumber;                 //!
   TBranch *b_BCID;                          //!
   TBranch *b_LumiBlockNumber;               //!
   TBranch *b_Mu;                            //!
   TBranch *b_Average_Mu;                    //!
   TBranch *b_Corrected_Mu;                  //!
   TBranch *b_Corrected_Average_Mu;          //!
   TBranch *b_CorrectedAndScaled_Mu;         //!
   TBranch *b_CorrectedAndScaled_Average_Mu; //!
   TBranch *b_MuIndependent_RunNumber;       //!
   TBranch *b_BeamPosition_X;                //!
   TBranch *b_BeamPosition_Y;                //!
   TBranch *b_BeamPosition_Z;                //!
   TBranch *b_BeamPosition_SigmaX;           //!
   TBranch *b_BeamPosition_SigmaY;           //!
   TBranch *b_BeamPosition_SigmaZ;           //!
   TBranch *b_mc_evtwt;                      //!
   TBranch *b_mc_pileupwt;                   //!
   TBranch *b_NVertex;                       //!
   TBranch *b_NPrimaryVertex;                       //!
   TBranch *b_NPileUpVertex;                       //!
   TBranch *b_MET_Px;                        //!
   TBranch *b_MET_Py;                        //!
   TBranch *b_MET_MET;                       //!
   TBranch *b_MET_Phi;                       //!
   TBranch *b_MET_SumET;                     //!

   // Vertex_Tree
   TTree *Vertex_Tree = nullptr;
   Int_t Vertex_Accociated_Tracks;

   TBranch *b_Vertex_Accociated_Tracks; //!

   // ForwardElectron_Calibration_Tree
   TTree *ForwardElectron_Calibration_Tree = nullptr;
   Float_t ForwardElectron_Original_Pt;
   Float_t ForwardElectron_Calibrated_Pt_S12Off;

   TBranch *b_ForwardElectron_Original_Pt;          //!
   TBranch *b_ForwardElectron_Calibrated_Pt_S12Off; //!

   // ForwardElectron_Cluster_Tree
   TTree *ForwardElectron_Cluster_Tree = nullptr;
    Int_t           ForwardElectron_nCluster;
   Float_t         ForwardElectron_Cluster_E;
   Float_t         ForwardElectron_Cluster_Eta;
   Float_t         ForwardElectron_Cluster_EmaxSamplRatio;
   Float_t         ForwardElectron_Cluster_Phi;
   Float_t         ForwardElectron_Cluster_FIRST_PHI;
   Float_t         ForwardElectron_Cluster_FIRST_ETA;
   Float_t         ForwardElectron_Cluster_SECOND_R;
   Float_t         ForwardElectron_Cluster_SECOND_LAMBDA;
   Float_t         ForwardElectron_Cluster_DELTA_PHI;
   Float_t         ForwardElectron_Cluster_DELTA_THETA;
   Float_t         ForwardElectron_Cluster_DELTA_ALPHA;
   Float_t         ForwardElectron_Cluster_CENTER_X;
   Float_t         ForwardElectron_Cluster_CENTER_Y;
   Float_t         ForwardElectron_Cluster_CENTER_Z;
   Float_t         ForwardElectron_Cluster_CENTER_MAG;
   Float_t         ForwardElectron_Cluster_CENTER_LAMBDA;
   Float_t         ForwardElectron_Cluster_LATERAL;
   Float_t         ForwardElectron_Cluster_LONGITUDINAL;
   Float_t         ForwardElectron_Cluster_ENG_FRAC_EM;
   Float_t         ForwardElectron_Cluster_ENG_FRAC_MAX;
   Float_t         ForwardElectron_Cluster_ENG_FRAC_CORE;
   Float_t         ForwardElectron_Cluster_FIRST_ENG_DENS;
   Float_t         ForwardElectron_Cluster_SECOND_ENG_DENS;
   Float_t         ForwardElectron_Cluster_ISOLATION;
   Float_t         ForwardElectron_Cluster_ENG_BAD_CELLS;
   Float_t         ForwardElectron_Cluster_N_BAD_CELLS;
   Float_t         ForwardElectron_Cluster_BAD_CELLS_CORR_E;
   Float_t         ForwardElectron_Cluster_BADLARQ_FRAC;
   Float_t         ForwardElectron_Cluster_ENG_POS;
   Float_t         ForwardElectron_Cluster_SIGNIFICANCE;
   Float_t         ForwardElectron_Cluster_CELL_SIGNIFICANCE;
   Float_t         ForwardElectron_Cluster_CELL_SIG_SAMPLING;
   Float_t         ForwardElectron_Cluster_AVG_LAR_Q;
   Float_t         ForwardElectron_Cluster_AVG_TILE_Q;
   Float_t         ForwardElectron_Cluster_ENG_BAD_HV_CELLS;
   Float_t         ForwardElectron_Cluster_N_BAD_HV_CELLS;
   Float_t         ForwardElectron_Cluster_PTD;
   Float_t         ForwardElectron_Cluster_EM_PROBABILITY;
   Float_t         ForwardElectron_Cluster_HAD_WEIGHT;
   Float_t         ForwardElectron_Cluster_OOC_WEIGHT;
   Float_t         ForwardElectron_Cluster_DM_WEIGHT;

   TBranch        *b_ForwardElectron_nCluster;   //!
   TBranch        *b_ForwardElectron_Cluster_E;   //!
   TBranch        *b_ForwardElectron_Cluster_Eta;   //!
   TBranch        *b_ForwardElectron_Cluster_EmaxSamplRatio;   //!
   TBranch        *b_ForwardElectron_Cluster_Phi;   //!
   TBranch        *b_ForwardElectron_Cluster_FIRST_PHI;   //!
   TBranch        *b_ForwardElectron_Cluster_FIRST_ETA;   //!
   TBranch        *b_ForwardElectron_Cluster_SECOND_R;   //!
   TBranch        *b_ForwardElectron_Cluster_SECOND_LAMBDA;   //!
   TBranch        *b_ForwardElectron_Cluster_DELTA_PHI;   //!
   TBranch        *b_ForwardElectron_Cluster_DELTA_THETA;   //!
   TBranch        *b_ForwardElectron_Cluster_DELTA_ALPHA;   //!
   TBranch        *b_ForwardElectron_Cluster_CENTER_X;   //!
   TBranch        *b_ForwardElectron_Cluster_CENTER_Y;   //!
   TBranch        *b_ForwardElectron_Cluster_CENTER_Z;   //!
   TBranch        *b_ForwardElectron_Cluster_CENTER_MAG;   //!
   TBranch        *b_ForwardElectron_Cluster_CENTER_LAMBDA;   //!
   TBranch        *b_ForwardElectron_Cluster_LATERAL;   //!
   TBranch        *b_ForwardElectron_Cluster_LONGITUDINAL;   //!
   TBranch        *b_ForwardElectron_Cluster_ENG_FRAC_EM;   //!
   TBranch        *b_ForwardElectron_Cluster_ENG_FRAC_MAX;   //!
   TBranch        *b_ForwardElectron_Cluster_ENG_FRAC_CORE;   //!
   TBranch        *b_ForwardElectron_Cluster_FIRST_ENG_DENS;   //!
   TBranch        *b_ForwardElectron_Cluster_SECOND_ENG_DENS;   //!
   TBranch        *b_ForwardElectron_Cluster_ISOLATION;   //!
   TBranch        *b_ForwardElectron_Cluster_ENG_BAD_CELLS;   //!
   TBranch        *b_ForwardElectron_Cluster_N_BAD_CELLS;   //!
   TBranch        *b_ForwardElectron_Cluster_BAD_CELLS_CORR_E;   //!
   TBranch        *b_ForwardElectron_Cluster_BADLARQ_FRAC;   //!
   TBranch        *b_ForwardElectron_Cluster_ENG_POS;   //!
   TBranch        *b_ForwardElectron_Cluster_SIGNIFICANCE;   //!
   TBranch        *b_ForwardElectron_Cluster_CELL_SIGNIFICANCE;   //!
   TBranch        *b_ForwardElectron_Cluster_CELL_SIG_SAMPLING;   //!
   TBranch        *b_ForwardElectron_Cluster_AVG_LAR_Q;   //!
   TBranch        *b_ForwardElectron_Cluster_AVG_TILE_Q;   //!
   TBranch        *b_ForwardElectron_Cluster_ENG_BAD_HV_CELLS;   //!
   TBranch        *b_ForwardElectron_Cluster_N_BAD_HV_CELLS;   //!
   TBranch        *b_ForwardElectron_Cluster_PTD;   //!
   TBranch        *b_ForwardElectron_Cluster_EM_PROBABILITY;   //!
   TBranch        *b_ForwardElectron_Cluster_HAD_WEIGHT;   //!
   TBranch        *b_ForwardElectron_Cluster_OOC_WEIGHT;   //!
   TBranch        *b_ForwardElectron_Cluster_DM_WEIGHT;   //!

   // ForwardElectron_SF_Tree
   TTree *ForwardElectron_SF_Tree = nullptr;
   Float_t mc_ForwardElectron_LooseIDSF;
   Float_t mc_ForwardElectron_MediumIDSF;
   Float_t mc_ForwardElectron_TightIDSF;

   TBranch *b_mc_ForwardElectron_LooseIDSF;  //!
   TBranch *b_mc_ForwardElectron_MediumIDSF; //!
   TBranch *b_mc_ForwardElectron_TightIDSF;  //!

   // ForwardElectron_ShowerShape_Tree
   TTree *ForwardElectron_ShowerShape_Tree = nullptr;
   Float_t ForwardElectron_ShowerShape_f1;
   Float_t ForwardElectron_ShowerShape_f3;
   Float_t ForwardElectron_ShowerShape_f1core;
   Float_t ForwardElectron_ShowerShape_f3core;
   Float_t ForwardElectron_ShowerShape_e277;
   Float_t ForwardElectron_ShowerShape_weta1;
   Float_t ForwardElectron_ShowerShape_weta2;
   Float_t ForwardElectron_ShowerShape_fracs1;
   Float_t ForwardElectron_ShowerShape_wtots1;
   Float_t ForwardElectron_ShowerShape_Reta;
   Float_t ForwardElectron_ShowerShape_Rphi;
   Float_t ForwardElectron_ShowerShape_Eratio;
   Float_t ForwardElectron_ShowerShape_Rhad;
   Float_t ForwardElectron_ShowerShape_Rhad1;
   Float_t ForwardElectron_ShowerShape_DeltaE;

   TBranch *b_ForwardElectron_ShowerShape_f1;     //!
   TBranch *b_ForwardElectron_ShowerShape_f3;     //!
   TBranch *b_ForwardElectron_ShowerShape_f1core; //!
   TBranch *b_ForwardElectron_ShowerShape_f3core; //!
   TBranch *b_ForwardElectron_ShowerShape_e277;   //!
   TBranch *b_ForwardElectron_ShowerShape_weta1;  //!
   TBranch *b_ForwardElectron_ShowerShape_weta2;  //!
   TBranch *b_ForwardElectron_ShowerShape_fracs1; //!
   TBranch *b_ForwardElectron_ShowerShape_wtots1; //!
   TBranch *b_ForwardElectron_ShowerShape_Reta;   //!
   TBranch *b_ForwardElectron_ShowerShape_Rphi;   //!
   TBranch *b_ForwardElectron_ShowerShape_Eratio; //!
   TBranch *b_ForwardElectron_ShowerShape_Rhad;   //!
   TBranch *b_ForwardElectron_ShowerShape_Rhad1;  //!
   TBranch *b_ForwardElectron_ShowerShape_DeltaE; //!

   // ForwardElectron_Tree
   TTree *ForwardElectron_Tree = nullptr;
   Float_t ForwardElectron_Calibrated_Pt;
   Float_t ForwardElectron_Eta;
   Float_t ForwardElectron_Phi;
   Float_t ForwardElectron_Charge;
   Bool_t ForwardElectron_PassIDMedium;
   Bool_t ForwardElectron_PassIDLoose;
   Bool_t ForwardElectron_PassIDTight;
   Float_t ForwardElectron_LikelihoodValue;

   TBranch *b_ForwardElectron_Calibrated_Pt; //!
   TBranch *b_ForwardElectron_Eta;           //!
   TBranch *b_ForwardElectron_Phi;           //!
   TBranch *b_ForwardElectron_Charge;        //!
   TBranch *b_ForwardElectron_PassIDMedium;  //!
   TBranch *b_ForwardElectron_PassIDLoose;   //!
   TBranch *b_ForwardElectron_PassIDTight;   //!
   TBranch *b_ForwardElectron_LikelihoodValue; //!

   // Jet_BJet_Eff60_Syst_Tree
/*   TTree *Jet_BJet_Eff60_Syst_Tree = nullptr;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff60;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff60;

   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff60;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff60;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff60;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff60;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff60;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff60;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff60;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff60;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff60;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff60;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff60; //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff60;   //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff60;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff60;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff60;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff60;              //!

   // Jet_BJet_Eff70_Syst_Tree
   TTree *Jet_BJet_Eff70_Syst_Tree = nullptr;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff70;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff70;

   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff70;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff70;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff70;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff70;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff70;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff70;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff70;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff70;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff70;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff70;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff70; //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff70;   //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff70;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff70;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff70;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff70;              //!

   // Jet_BJet_Eff77_Syst_Tree
   TTree *Jet_BJet_Eff77_Syst_Tree = nullptr;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff77;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff77;

   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff77;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff77;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff77;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff77;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff77;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff77;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff77;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff77;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff77;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff77;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff77; //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff77;   //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff77;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff77;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff77;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff77;              //!

   // Jet_BJet_Eff85_Syst_Tree
   TTree *Jet_BJet_Eff85_Syst_Tree = nullptr;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff85;
   Float_t Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff85;

   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff85;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff85;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff85;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff85;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff85;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff85;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff85;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff85;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff85;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff85;              //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff85; //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff85;   //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff85;                //!
   TBranch *b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff85;                  //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff85;            //!
   TBranch *b_Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff85;              //!

   // Jet_Jvt_Syst_Tree
   TTree *Jet_Jvt_Syst_Tree = nullptr;
   Float_t Jet_JvtSF_CentralSYS_DOWN;
   Float_t Jet_JvtSF_CentralSYS_UP;
   Float_t Jet_JvtSF_ForwardSYS_DOWN;
   Float_t Jet_JvtSF_ForwardSYS_UP;

   TBranch *b_Jet_JvtSF_CentralSYS_DOWN; //!
   TBranch *b_Jet_JvtSF_CentralSYS_UP;   //!
   TBranch *b_Jet_JvtSF_ForwardSYS_DOWN; //!
   TBranch *b_Jet_JvtSF_ForwardSYS_UP;   //!

   // Jet_Pt_Syst_Tree
   TTree *Jet_Pt_Syst_Tree = nullptr;
   Float_t Jet_Calibrated_Pt_JET_JER_DataVsMC_MC16__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_central__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_forward__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_central__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_forward__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_N_fit_error_up_central__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_fit_error_up_forward__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_central__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_forward__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_central__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_forward__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_central__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_forward__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_central__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_forward__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_closure__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_jesnp1__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_jesnp2__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_jesnp3__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_jv__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_mcgenerator__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_pt3dphi__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat10__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat10__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat11__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat11__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat12__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat12__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat1__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat1__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat2__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat2__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat3__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat3__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat4__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat4__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat5__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat5__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat6__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat6__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat7__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat7__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat8__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat8__1up;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat9__1down;
   Float_t Jet_Calibrated_Pt_JET_JER_dijet_stat9__1up;
   Float_t Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1down;
   Float_t Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1up;
   Float_t Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1down;
   Float_t Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1up;
   Float_t Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1down;
   Float_t Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1up;
   Float_t Jet_Calibrated_Pt_JET_Flavor_Composition__1down;
   Float_t Jet_Calibrated_Pt_JET_Flavor_Composition__1up;
   Float_t Jet_Calibrated_Pt_JET_Flavor_Response__1down;
   Float_t Jet_Calibrated_Pt_JET_Flavor_Response__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_GamESZee__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_GamESZee__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Generator__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Generator__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Jvt__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Jvt__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Purity__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Purity__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat10__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat10__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat11__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat11__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat12__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat12__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat13__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat13__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat14__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat14__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat15__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat15__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat16__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat16__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat1__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat1__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat2__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat2__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat3__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat3__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat4__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat4__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat5__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat5__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat6__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat6__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat7__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat7__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat8__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat8__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat9__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Stat9__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Veto__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_Veto__1up;
   Float_t Jet_Calibrated_Pt_JET_Gjet_dPhi__1down;
   Float_t Jet_Calibrated_Pt_JET_Gjet_dPhi__1up;
   Float_t Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1down;
   Float_t Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1up;
   Float_t Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1down;
   Float_t Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1up;
   Float_t Jet_Calibrated_Pt_JET_Pileup_PtTerm__1down;
   Float_t Jet_Calibrated_Pt_JET_Pileup_PtTerm__1up;
   Float_t Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1down;
   Float_t Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_Jvt__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_Jvt__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MC__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MC__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuScale__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuScale__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat10__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat10__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat11__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat11__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat12__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat12__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat13__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat13__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat14__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat14__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat1__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat1__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat2__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat2__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat3__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat3__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat4__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat4__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat5__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat5__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat6__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat6__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat7__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat7__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat8__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat8__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat9__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_MuStat9__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_Veto__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_Veto__1up;
   Float_t Jet_Calibrated_Pt_JET_Zjet_dPhi__1down;
   Float_t Jet_Calibrated_Pt_JET_Zjet_dPhi__1up;

   TBranch *b_Jet_Calibrated_Pt_JET_JER_DataVsMC_MC16__1down;                             //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_central__1up;               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_forward__1up;               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_central__1down;   //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_forward__1down;   //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_fit_error_up_central__1up;                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_fit_error_up_forward__1up;                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_central__1down;                //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_forward__1down;                //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_central__1up;          //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_forward__1up;          //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_central__1up; //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_forward__1up; //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_central__1up;                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_forward__1up;                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_closure__1up;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_jesnp1__1up;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_jesnp2__1up;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_jesnp3__1up;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_jv__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_mcgenerator__1up;                           //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_pt3dphi__1up;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat10__1down;                              //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat10__1up;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat11__1down;                              //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat11__1up;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat12__1down;                              //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat12__1up;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat1__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat1__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat2__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat2__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat3__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat3__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat4__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat4__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat5__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat5__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat6__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat6__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat7__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat7__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat8__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat8__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat9__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_JER_dijet_stat9__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1down;                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1up;                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1down;       //!
   TBranch *b_Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1up;         //!
   TBranch *b_Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1down;                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1up;                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Flavor_Composition__1down;                            //!
   TBranch *b_Jet_Calibrated_Pt_JET_Flavor_Composition__1up;                              //!
   TBranch *b_Jet_Calibrated_Pt_JET_Flavor_Response__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Flavor_Response__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_GamESZee__1down;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_GamESZee__1up;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Generator__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Generator__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Jvt__1down;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Jvt__1up;                                        //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Purity__1down;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Purity__1up;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1down;                           //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1up;                             //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat10__1down;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat10__1up;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat11__1down;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat11__1up;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat12__1down;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat12__1up;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat13__1down;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat13__1up;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat14__1down;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat14__1up;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat15__1down;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat15__1up;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat16__1down;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat16__1up;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat1__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat1__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat2__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat2__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat3__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat3__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat4__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat4__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat5__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat5__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat6__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat6__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat7__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat7__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat8__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat8__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat9__1down;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Stat9__1up;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Veto__1down;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_Veto__1up;                                       //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_dPhi__1down;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Gjet_dPhi__1up;                                       //!
   TBranch *b_Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1down;                              //!
   TBranch *b_Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1up;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Pileup_PtTerm__1down;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Pileup_PtTerm__1up;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1down;                            //!
   TBranch *b_Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1up;                              //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1down;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1up;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_Jvt__1down;                                      //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_Jvt__1up;                                        //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MC__1down;                                       //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MC__1up;                                         //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1down;                             //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1up;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1down;                             //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1up;                               //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuScale__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuScale__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1down;                                //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1up;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat10__1down;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat10__1up;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat11__1down;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat11__1up;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat12__1down;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat12__1up;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat13__1down;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat13__1up;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat14__1down;                                 //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat14__1up;                                   //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat1__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat1__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat2__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat2__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat3__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat3__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat4__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat4__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat5__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat5__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat6__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat6__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat7__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat7__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat8__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat8__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat9__1down;                                  //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_MuStat9__1up;                                    //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1down;                           //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1up;                             //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_Veto__1down;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_Veto__1up;                                       //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_dPhi__1down;                                     //!
   TBranch *b_Jet_Calibrated_Pt_JET_Zjet_dPhi__1up;                                       //!
*/
   // Jet_SF_Tree
   TTree *Jet_SF_Tree = nullptr;
   Float_t Jet_JvtSF;
   Float_t Jet_isBJetSF_Eff60;
   Float_t Jet_isBJetSF_Eff70;
   Float_t Jet_isBJetSF_Eff77;
   Float_t Jet_isBJetSF_Eff85;

   TBranch *b_Jet_JvtSF;          //!
   TBranch *b_Jet_isBJetSF_Eff60; //!
   TBranch *b_Jet_isBJetSF_Eff70; //!
   TBranch *b_Jet_isBJetSF_Eff77; //!
   TBranch *b_Jet_isBJetSF_Eff85; //!

   // Jet_Tree
   TTree *Jet_Tree = nullptr;
   Float_t Jet_Original_Pt;
   Float_t Jet_Calibrated_Pt;
   Int_t Jet_NumTrkPt500;
   Float_t Jet_SumPtTrkPt500;
   Float_t Jet_Eta;
   Float_t Jet_Phi;
   Float_t Jet_M;
   Float_t Jet_E;
   Float_t Jet_Rapidity;
   Float_t Jet_Jvt;
   // Int_t Jet_TruthLabel;
   Float_t Jet_isBJetScore;
   Bool_t Jet_PassJvt;
   Bool_t Jet_isBad;
   Bool_t Jet_isBJet_Eff60;
   Bool_t Jet_isBJet_Eff70;
   Bool_t Jet_isBJet_Eff77;
   Bool_t Jet_isBJet_Eff85;

   TBranch *b_Jet_Original_Pt;   //!
   TBranch *b_Jet_Calibrated_Pt; //!
   TBranch *b_Jet_NumTrkPt500;   //!
   TBranch *b_Jet_SumPtTrkPt500; //!
   TBranch *b_Jet_Eta;           //!
   TBranch *b_Jet_Phi;           //!
   TBranch *b_Jet_M;             //!
   TBranch *b_Jet_E;             //!
   TBranch *b_Jet_Rapidity;      //!
   TBranch *b_Jet_Jvt;           //!
   // TBranch *b_Jet_TruthLabel;    //!
   TBranch *b_Jet_isBJetScore;  //!
   TBranch *b_Jet_PassJvt;      //!
   TBranch *b_Jet_isBad;        //!
   TBranch *b_Jet_isBJet_Eff60; //!
   TBranch *b_Jet_isBJet_Eff70; //!
   TBranch *b_Jet_isBJet_Eff77; //!
   TBranch *b_Jet_isBJet_Eff85; //!

   // JetTruth_Tree
   TTree *JetTruth_Tree = nullptr;
   Int_t Jet_TruthLabel;
   Float_t Jet_TruthPt;
   Float_t Jet_TruthEta;
   Float_t Jet_TruthPhi;
   Float_t Jet_TruthM;
   Float_t Jet_TruthE;

   TBranch *b_Jet_TruthLabel; //!
   TBranch *b_Jet_TruthPt;    //!
   TBranch *b_Jet_TruthEta;   //!
   TBranch *b_Jet_TruthPhi;   //!
   TBranch *b_Jet_TruthM;     //!
   TBranch *b_Jet_TruthE;     //!

   // METSyst_Tree
   TTree *METSyst_Tree = nullptr;
   Float_t MET_Px_Syst_MET_SoftTrk_ResoPara;
   Float_t MET_Py_Syst_MET_SoftTrk_ResoPara;
   Float_t MET_MET_Syst_MET_SoftTrk_ResoPara;
   Float_t MET_Phi_Syst_MET_SoftTrk_ResoPara;
   Float_t MET_SumET_Syst_MET_SoftTrk_ResoPara;
   Float_t MET_Px_Syst_MET_SoftTrk_ResoPerp;
   Float_t MET_Py_Syst_MET_SoftTrk_ResoPerp;
   Float_t MET_MET_Syst_MET_SoftTrk_ResoPerp;
   Float_t MET_Phi_Syst_MET_SoftTrk_ResoPerp;
   Float_t MET_SumET_Syst_MET_SoftTrk_ResoPerp;
   Float_t MET_Px_Syst_MET_SoftTrk_Scale__1up;
   Float_t MET_Py_Syst_MET_SoftTrk_Scale__1up;
   Float_t MET_MET_Syst_MET_SoftTrk_Scale__1up;
   Float_t MET_Phi_Syst_MET_SoftTrk_Scale__1up;
   Float_t MET_SumET_Syst_MET_SoftTrk_Scale__1up;
   Float_t MET_Px_Syst_MET_SoftTrk_Scale__1down;
   Float_t MET_Py_Syst_MET_SoftTrk_Scale__1down;
   Float_t MET_MET_Syst_MET_SoftTrk_Scale__1down;
   Float_t MET_Phi_Syst_MET_SoftTrk_Scale__1down;
   Float_t MET_SumET_Syst_MET_SoftTrk_Scale__1down;

   TBranch *b_MET_Px_Syst_MET_SoftTrk_ResoPara;        //!
   TBranch *b_MET_Py_Syst_MET_SoftTrk_ResoPara;        //!
   TBranch *b_MET_MET_Syst_MET_SoftTrk_ResoPara;       //!
   TBranch *b_MET_Phi_Syst_MET_SoftTrk_ResoPara;       //!
   TBranch *b_MET_SumET_Syst_MET_SoftTrk_ResoPara;     //!
   TBranch *b_MET_Px_Syst_MET_SoftTrk_ResoPerp;        //!
   TBranch *b_MET_Py_Syst_MET_SoftTrk_ResoPerp;        //!
   TBranch *b_MET_MET_Syst_MET_SoftTrk_ResoPerp;       //!
   TBranch *b_MET_Phi_Syst_MET_SoftTrk_ResoPerp;       //!
   TBranch *b_MET_SumET_Syst_MET_SoftTrk_ResoPerp;     //!
   TBranch *b_MET_Px_Syst_MET_SoftTrk_Scale__1up;      //!
   TBranch *b_MET_Py_Syst_MET_SoftTrk_Scale__1up;      //!
   TBranch *b_MET_MET_Syst_MET_SoftTrk_Scale__1up;     //!
   TBranch *b_MET_Phi_Syst_MET_SoftTrk_Scale__1up;     //!
   TBranch *b_MET_SumET_Syst_MET_SoftTrk_Scale__1up;   //!
   TBranch *b_MET_Px_Syst_MET_SoftTrk_Scale__1down;    //!
   TBranch *b_MET_Py_Syst_MET_SoftTrk_Scale__1down;    //!
   TBranch *b_MET_MET_Syst_MET_SoftTrk_Scale__1down;   //!
   TBranch *b_MET_Phi_Syst_MET_SoftTrk_Scale__1down;   //!
   TBranch *b_MET_SumET_Syst_MET_SoftTrk_Scale__1down; //!

   // Muon_FCLooseIsoSF_Syst_Tree
   TTree *Muon_FCLooseIsoSF_Syst_Tree = nullptr;
   Float_t mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down;
   Float_t mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up;
   Float_t mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down;
   Float_t mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up;

   TBranch *b_mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down; //!
   TBranch *b_mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up;   //!
   TBranch *b_mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down;  //!
   TBranch *b_mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up;    //!

   // Muon_FCTightIsoSF_Syst_Tree
   TTree *Muon_FCTightIsoSF_Syst_Tree = nullptr;
   Float_t mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down;
   Float_t mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up;
   Float_t mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down;
   Float_t mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up;

   TBranch *b_mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down; //!
   TBranch *b_mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up;   //!
   TBranch *b_mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down;  //!
   TBranch *b_mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up;    //!

   // Muon_FixedCutPflowLooseIsoSF_Syst_Tree
   TTree *Muon_FixedCutPflowLooseIsoSF_Syst_Tree = nullptr;
   Float_t mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down;
   Float_t mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up;
   Float_t mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down;
   Float_t mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up;

   TBranch *b_mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down; //!
   TBranch *b_mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up;   //!
   TBranch *b_mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down;  //!
   TBranch *b_mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up;    //!

   // Muon_HighPt_Syst_Tree
   TTree *Muon_HighPt_Syst_Tree = nullptr;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_ID__1down;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_ID__1up;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_MS__1down;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_MS__1up;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1down;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1up;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1down;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1up;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_SCALE__1down;
   Float_t Muon_Calibrated_HighPt_Syst_MUON_SCALE__1up;

   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_ID__1down;              //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_ID__1up;                //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_MS__1down;              //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_MS__1up;                //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1down; //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1down;     //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1up;       //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_SCALE__1down;           //!
   TBranch *b_Muon_Calibrated_HighPt_Syst_MUON_SCALE__1up;             //!

   // Muon_Isolation_Tree
   TTree *Muon_Isolation_Tree = nullptr;
   Float_t Muon_Isolation_etcone20;
   Float_t Muon_Isolation_etcone30;
   Float_t Muon_Isolation_etcone40;
   Float_t Muon_Isolation_ptcone20;
   Float_t Muon_Isolation_ptcone30;
   Float_t Muon_Isolation_ptcone40;
   Float_t Muon_Isolation_ptcone50;
   Float_t Muon_Isolation_ptvarcone20;
   Float_t Muon_Isolation_ptvarcone30;
   Float_t Muon_Isolation_ptvarcone40;
   Float_t Muon_Isolation_ptcone20_TightTTVA_pt500;
   Float_t Muon_Isolation_ptcone30_TightTTVA_pt500;
   Float_t Muon_Isolation_ptcone40_TightTTVA_pt500;
   Float_t Muon_Isolation_ptcone20_TightTTVA_pt1000;
   Float_t Muon_Isolation_ptcone30_TightTTVA_pt1000;
   Float_t Muon_Isolation_ptcone40_TightTTVA_pt1000;
   Float_t Muon_Isolation_ptvarcone20_TightTTVA_pt500;
   Float_t Muon_Isolation_ptvarcone30_TightTTVA_pt500;
   Float_t Muon_Isolation_ptvarcone40_TightTTVA_pt500;
   Float_t Muon_Isolation_ptvarcone20_TightTTVA_pt1000;
   Float_t Muon_Isolation_ptvarcone30_TightTTVA_pt1000;
   Float_t Muon_Isolation_ptvarcone40_TightTTVA_pt1000;
   Float_t Muon_Isolation_ptcone20_TightTTVALooseCone_pt500;
   Float_t Muon_Isolation_ptcone30_TightTTVALooseCone_pt500;
   Float_t Muon_Isolation_ptcone40_TightTTVALooseCone_pt500;
   Float_t Muon_Isolation_ptcone20_TightTTVALooseCone_pt1000;
   Float_t Muon_Isolation_ptcone30_TightTTVALooseCone_pt1000;
   Float_t Muon_Isolation_ptcone40_TightTTVALooseCone_pt1000;
   Float_t Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt500;
   Float_t Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt500;
   Float_t Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt500;
   Float_t Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt1000;
   Float_t Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt1000;
   Float_t Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt1000;
   Float_t Muon_Isolation_topoetcone20;
   Float_t Muon_Isolation_topoetcone30;
   Float_t Muon_Isolation_topoetcone40;
   Float_t Muon_Isolation_neflowisol20;
   Float_t Muon_Isolation_neflowisol30;
   Float_t Muon_Isolation_neflowisol40;

   TBranch *b_Muon_Isolation_etcone20;                              //!
   TBranch *b_Muon_Isolation_etcone30;                              //!
   TBranch *b_Muon_Isolation_etcone40;                              //!
   TBranch *b_Muon_Isolation_ptcone20;                              //!
   TBranch *b_Muon_Isolation_ptcone30;                              //!
   TBranch *b_Muon_Isolation_ptcone40;                              //!
   TBranch *b_Muon_Isolation_ptcone50;                              //!
   TBranch *b_Muon_Isolation_ptvarcone20;                           //!
   TBranch *b_Muon_Isolation_ptvarcone30;                           //!
   TBranch *b_Muon_Isolation_ptvarcone40;                           //!
   TBranch *b_Muon_Isolation_ptcone20_TightTTVA_pt500;              //!
   TBranch *b_Muon_Isolation_ptcone30_TightTTVA_pt500;              //!
   TBranch *b_Muon_Isolation_ptcone40_TightTTVA_pt500;              //!
   TBranch *b_Muon_Isolation_ptcone20_TightTTVA_pt1000;             //!
   TBranch *b_Muon_Isolation_ptcone30_TightTTVA_pt1000;             //!
   TBranch *b_Muon_Isolation_ptcone40_TightTTVA_pt1000;             //!
   TBranch *b_Muon_Isolation_ptvarcone20_TightTTVA_pt500;           //!
   TBranch *b_Muon_Isolation_ptvarcone30_TightTTVA_pt500;           //!
   TBranch *b_Muon_Isolation_ptvarcone40_TightTTVA_pt500;           //!
   TBranch *b_Muon_Isolation_ptvarcone20_TightTTVA_pt1000;          //!
   TBranch *b_Muon_Isolation_ptvarcone30_TightTTVA_pt1000;          //!
   TBranch *b_Muon_Isolation_ptvarcone40_TightTTVA_pt1000;          //!
   TBranch *b_Muon_Isolation_ptcone20_TightTTVALooseCone_pt500;     //!
   TBranch *b_Muon_Isolation_ptcone30_TightTTVALooseCone_pt500;     //!
   TBranch *b_Muon_Isolation_ptcone40_TightTTVALooseCone_pt500;     //!
   TBranch *b_Muon_Isolation_ptcone20_TightTTVALooseCone_pt1000;    //!
   TBranch *b_Muon_Isolation_ptcone30_TightTTVALooseCone_pt1000;    //!
   TBranch *b_Muon_Isolation_ptcone40_TightTTVALooseCone_pt1000;    //!
   TBranch *b_Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt500;  //!
   TBranch *b_Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt500;  //!
   TBranch *b_Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt500;  //!
   TBranch *b_Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt1000; //!
   TBranch *b_Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt1000; //!
   TBranch *b_Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt1000; //!
   TBranch *b_Muon_Isolation_topoetcone20;                          //!
   TBranch *b_Muon_Isolation_topoetcone30;                          //!
   TBranch *b_Muon_Isolation_topoetcone40;                          //!
   TBranch *b_Muon_Isolation_neflowisol20;                          //!
   TBranch *b_Muon_Isolation_neflowisol30;                          //!
   TBranch *b_Muon_Isolation_neflowisol40;                          //!

   // Muon_PLVLooseIsoSF_Syst_Tree
   TTree *Muon_PLVLooseIsoSF_Syst_Tree = nullptr;
   Float_t mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down;
   Float_t mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up;
   Float_t mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down;
   Float_t mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up;

   TBranch *b_mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down; //!
   TBranch *b_mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up;   //!
   TBranch *b_mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down;  //!
   TBranch *b_mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up;    //!

   // Muon_PLVTightIsoSF_Syst_Tree
   TTree *Muon_PLVTightIsoSF_Syst_Tree = nullptr;
   Float_t mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down;
   Float_t mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up;
   Float_t mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down;
   Float_t mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up;

   TBranch *b_mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down; //!
   TBranch *b_mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up;   //!
   TBranch *b_mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down;  //!
   TBranch *b_mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up;    //!

   // Muon_Pt_Syst_Tree
   TTree *Muon_Pt_Syst_Tree = nullptr;
   Float_t Muon_Calibrated_Pt_Syst_MUON_ID__1down;
   Float_t Muon_Calibrated_Pt_Syst_MUON_ID__1up;
   Float_t Muon_Calibrated_Pt_Syst_MUON_MS__1down;
   Float_t Muon_Calibrated_Pt_Syst_MUON_MS__1up;
   Float_t Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1down;
   Float_t Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1up;
   Float_t Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1down;
   Float_t Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1up;
   Float_t Muon_Calibrated_Pt_Syst_MUON_SCALE__1down;
   Float_t Muon_Calibrated_Pt_Syst_MUON_SCALE__1up;

   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_ID__1down;              //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_ID__1up;                //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_MS__1down;              //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_MS__1up;                //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1down; //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1up;   //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1down;     //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1up;       //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_SCALE__1down;           //!
   TBranch *b_Muon_Calibrated_Pt_Syst_MUON_SCALE__1up;             //!

   // Muon_RecoHighPt_Syst_Tree
   TTree *Muon_RecoHighPt_Syst_Tree = nullptr;
   Float_t mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down;
   Float_t mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up;
   Float_t mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1down;
   Float_t mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1up;
   Float_t mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down;
   Float_t mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up;
   Float_t mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1down;
   Float_t mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1up;
   Float_t mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up;
   Float_t mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up;

   TBranch *b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down;              //!
   TBranch *b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up;                //!
   TBranch *b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1down;                    //!
   TBranch *b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1up;                      //!
   TBranch *b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down;               //!
   TBranch *b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up;                 //!
   TBranch *b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1down;                     //!
   TBranch *b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1up;                       //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down;  //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up;    //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down;  //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up;    //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down; //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down; //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up;   //!

   // Muon_RecoLoose_Syst_Tree
   TTree *Muon_RecoLoose_Syst_Tree = nullptr;
   Float_t mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down;
   Float_t mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up;
   Float_t mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1down;
   Float_t mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1up;
   Float_t mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down;
   Float_t mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up;
   Float_t mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1down;
   Float_t mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1up;
   Float_t mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up;
   Float_t mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up;

   TBranch *b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down;              //!
   TBranch *b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up;                //!
   TBranch *b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1down;                    //!
   TBranch *b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1up;                      //!
   TBranch *b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down;               //!
   TBranch *b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up;                 //!
   TBranch *b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1down;                     //!
   TBranch *b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1up;                       //!
   TBranch *b_mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down;  //!
   TBranch *b_mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up;    //!
   TBranch *b_mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down;  //!
   TBranch *b_mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up;    //!
   TBranch *b_mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down; //!
   TBranch *b_mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch *b_mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down; //!
   TBranch *b_mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up;   //!

   // Muon_RecoMedium_Syst_Tree
   TTree *Muon_RecoMedium_Syst_Tree = nullptr;
   Float_t mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down;
   Float_t mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up;
   Float_t mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1down;
   Float_t mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1up;
   Float_t mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down;
   Float_t mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up;
   Float_t mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1down;
   Float_t mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1up;
   Float_t mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up;
   Float_t mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up;

   TBranch *b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down;              //!
   TBranch *b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up;                //!
   TBranch *b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1down;                    //!
   TBranch *b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1up;                      //!
   TBranch *b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down;               //!
   TBranch *b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up;                 //!
   TBranch *b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1down;                     //!
   TBranch *b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1up;                       //!
   TBranch *b_mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down;  //!
   TBranch *b_mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up;    //!
   TBranch *b_mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down;  //!
   TBranch *b_mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up;    //!
   TBranch *b_mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down; //!
   TBranch *b_mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch *b_mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down; //!
   TBranch *b_mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up;   //!

   // Muon_RecoTight_Syst_Tree
   TTree *Muon_RecoTight_Syst_Tree = nullptr;
   Float_t mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down;
   Float_t mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up;
   Float_t mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1down;
   Float_t mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1up;
   Float_t mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down;
   Float_t mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up;
   Float_t mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1down;
   Float_t mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1up;
   Float_t mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up;
   Float_t mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down;
   Float_t mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up;
   Float_t mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down;
   Float_t mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up;

   TBranch *b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down;              //!
   TBranch *b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up;                //!
   TBranch *b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1down;                    //!
   TBranch *b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1up;                      //!
   TBranch *b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down;               //!
   TBranch *b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up;                 //!
   TBranch *b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1down;                     //!
   TBranch *b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1up;                       //!
   TBranch *b_mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down;  //!
   TBranch *b_mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up;    //!
   TBranch *b_mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down;  //!
   TBranch *b_mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up;    //!
   TBranch *b_mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down; //!
   TBranch *b_mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up;   //!
   TBranch *b_mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down; //!
   TBranch *b_mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up;   //!

   // Muon_SF_Tree
   TTree *Muon_SF_Tree = nullptr;
   Float_t mc_Muon_FixedCutPflowLooseIsoSF;
   Float_t mc_Muon_FCLooseIsoSF;
   Float_t mc_Muon_FCTightIsoSF;
   Float_t mc_Muon_PLVLooseIsoSF;
   Float_t mc_Muon_PLVTightIsoSF;
   Float_t mc_Muon_RecoLoose_TriggerSF;
   Float_t mc_Muon_RecoLoose_TriggerEff;
   Float_t mc_Muon_RecoMedium_TriggerSF;
   Float_t mc_Muon_RecoMedium_TriggerEff;
   Float_t mc_Muon_RecoTight_TriggerSF;
   Float_t mc_Muon_RecoTight_TriggerEff;
   Float_t mc_Muon_RecoHighPt_TriggerSF;
   Float_t mc_Muon_RecoHighPt_TriggerEff;
   Float_t mc_Muon_LooseRecoSF;
   Float_t mc_Muon_MediumRecoSF;
   Float_t mc_Muon_TightRecoSF;
   Float_t mc_Muon_TTVASF;

   TBranch *b_mc_Muon_FixedCutPflowLooseIsoSF; //!
   TBranch *b_mc_Muon_FCLooseIsoSF;            //!
   TBranch *b_mc_Muon_FCTightIsoSF;            //!
   TBranch *b_mc_Muon_PLVLooseIsoSF;           //!
   TBranch *b_mc_Muon_PLVTightIsoSF;           //!
   TBranch *b_mc_Muon_RecoLoose_TriggerSF;     //!
   TBranch *b_mc_Muon_RecoLoose_TriggerEff;    //!
   TBranch *b_mc_Muon_RecoMedium_TriggerSF;    //!
   TBranch *b_mc_Muon_RecoMedium_TriggerEff;   //!
   TBranch *b_mc_Muon_RecoTight_TriggerSF;     //!
   TBranch *b_mc_Muon_RecoTight_TriggerEff;    //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerSF;    //!
   TBranch *b_mc_Muon_RecoHighPt_TriggerEff;   //!
   TBranch *b_mc_Muon_LooseRecoSF;             //!
   TBranch *b_mc_Muon_MediumRecoSF;            //!
   TBranch *b_mc_Muon_TightRecoSF;             //!
   TBranch *b_mc_Muon_TTVASF;                  //!

   // Muon_Track_Tree
   TTree *Muon_Track_Tree = nullptr;
   Float_t Muon_Track_Pt;
   Float_t Muon_Track_Eta;
   Float_t Muon_Track_Phi;
   Float_t Muon_Track_M;
   Float_t Muon_Track_E;
   Float_t Muon_Track_Rapidity;
   Float_t Muon_Track_Charge;
   Float_t Muon_Track_d0;
   Float_t Muon_Track_z0;
   Float_t Muon_Track_Phi0;
   Float_t Muon_Track_Theta;
   Float_t Muon_Track_qOverP;
   Float_t Muon_Track_Chi2;
   Float_t Muon_Track_NDoF;

   TBranch *b_Muon_Track_Pt;       //!
   TBranch *b_Muon_Track_Eta;      //!
   TBranch *b_Muon_Track_Phi;      //!
   TBranch *b_Muon_Track_M;        //!
   TBranch *b_Muon_Track_E;        //!
   TBranch *b_Muon_Track_Rapidity; //!
   TBranch *b_Muon_Track_Charge;   //!
   TBranch *b_Muon_Track_d0;       //!
   TBranch *b_Muon_Track_z0;       //!
   TBranch *b_Muon_Track_Phi0;     //!
   TBranch *b_Muon_Track_Theta;    //!
   TBranch *b_Muon_Track_qOverP;   //!
   TBranch *b_Muon_Track_Chi2;     //!
   TBranch *b_Muon_Track_NDoF;     //!

   // Muon_Tree
   TTree *Muon_Tree = nullptr;
   Float_t Muon_Original_Pt;
   Float_t Muon_Calibrated_Pt;
   Float_t Muon_Calibrated_HighPt;
   Float_t Muon_Eta;
   Float_t Muon_Phi;
   Float_t Muon_M;
   Float_t Muon_E;
   Float_t Muon_Charge;
   Bool_t Muon_PassIDHighPt;
   Bool_t Muon_PassIDMedium;
   Bool_t Muon_PassIDTight;
   Bool_t Muon_PassIDLoose;
   Bool_t Muon_MatchedTrigger;
   Bool_t Muon_PassIso_FixedCutPflowLoose;
   Bool_t Muon_PassIso_FCLoose;
   Bool_t Muon_PassIso_FCTight;
   Bool_t Muon_PassIso_PLVLoose;
   Bool_t Muon_PassIso_PLVTight;
   Float_t Muon_Track_dz0;
   Float_t Muon_Track_d0Significance;

   TBranch *b_Muon_Original_Pt;                //!
   TBranch *b_Muon_Calibrated_Pt;              //!
   TBranch *b_Muon_Calibrated_HighPt;          //!
   TBranch *b_Muon_Eta;                        //!
   TBranch *b_Muon_Phi;                        //!
   TBranch *b_Muon_M;                          //!
   TBranch *b_Muon_E;                          //!
   TBranch *b_Muon_Charge;                     //!
   TBranch *b_Muon_PassIDHighPt;               //!
   TBranch *b_Muon_PassIDMedium;               //!
   TBranch *b_Muon_PassIDTight;                //!
   TBranch *b_Muon_PassIDLoose;                //!
   TBranch *b_Muon_MatchedTrigger;             //!
   TBranch *b_Muon_PassIso_FixedCutPflowLoose; //!
   TBranch *b_Muon_PassIso_FCLoose;            //!
   TBranch *b_Muon_PassIso_FCTight;            //!
   TBranch *b_Muon_PassIso_PLVLoose;           //!
   TBranch *b_Muon_PassIso_PLVTight;           //!
   TBranch *b_Muon_Track_dz0;                  //!
   TBranch *b_Muon_Track_d0Significance;       //!

   // Muon_Trigger_Tree
   TTree *Muon_Trigger_Tree = nullptr;
   Bool_t MatchedTrigger_HLT_mu26_imedium;
   Bool_t MatchedTrigger_HLT_mu50;
   Bool_t MatchedTrigger_HLT_mu26_ivarmedium;

   TBranch *b_MatchedTrigger_HLT_mu26_imedium;    //!
   TBranch *b_MatchedTrigger_HLT_mu50;            //!
   TBranch *b_MatchedTrigger_HLT_mu26_ivarmedium; //!

   // Muon_TTVASF_Syst_Tree
   TTree *Muon_TTVASF_Syst_Tree = nullptr;
   Float_t mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1down;
   Float_t mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1up;
   Float_t mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1down;
   Float_t mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1up;

   TBranch *b_mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1down; //!
   TBranch *b_mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1up;   //!
   TBranch *b_mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1down;  //!
   TBranch *b_mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1up;    //!

   // Photon_Tree
   TTree *Photon_Tree = nullptr;
   float Photon_Calibrated_Pt;  //!
   float Photon_Eta;         //!
   float Photon_Phi;         //!
   bool Photon_PassCleaning;  //!
   bool Photon_PassIDTight;                             //!
   bool Photon_PassIDLoose;                             //!
   bool Photon_PassIso_FCLoose;                          //!
   bool Photon_PassIso_FCTight;                          //!
   
   TBranch *b_Photon_Calibrated_Pt;  //!
   TBranch *b_Photon_Eta;         //!
   TBranch *b_Photon_Phi;         //!
   TBranch *b_Photon_PassCleaning;  //!
   TBranch *b_Photon_PassIDTight;                             //!
   TBranch *b_Photon_PassIDLoose;                             //!
   TBranch *b_Photon_PassIso_FCLoose;                          //!
   TBranch *b_Photon_PassIso_FCTight;                          //!

   // Photon_SF_Tree
   TTree *Photon_SF_Tree = nullptr;
   //bool mc_Photon_LooseIDSF;                            //!
   float mc_Photon_TightIDSF;                            //!
   float mc_Photon_FCLooseIsoSF;                   //!
   float mc_Photon_FCTightIsoSF;                   //!
   
   TBranch *b_mc_Photon_TightIDSF;                            //!
   TBranch *b_mc_Photon_FCLooseIsoSF;                   //!
   TBranch *b_mc_Photon_FCTightIsoSF;                   //!

   // Photon Pt Systematics
   TTree *Photon_Pt_Syst_Tree = nullptr;
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1down;            //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up;              //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down;       //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up;         //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down;        //!
   float Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1down;                //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1up;                  //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down; //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up;   //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down; //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up;   //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down; //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up;   //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1down;                 //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1up;                  //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down;              //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up;               //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down;              //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up;               //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up;           //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down;  //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up;   //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down;  //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up;   //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down; //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up;   //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down; //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up;   //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down;        //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up;              //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down;          //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up;            //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up;           //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up;           //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down;         //!                          
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up;           //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up;           //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down;         //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up;           //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down;      //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up;       //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down;              //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up;               //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down;             //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up;               //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down;             //!
   float Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up;               //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1down;             //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1up;               //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1down;             //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1up;               //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1down;             //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1up;               //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1down;             //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1up;               //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1down;             //!
   float Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1up;               //!

   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down;   //!   
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1down;   //!                      
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1up;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1down;   //!
   TBranch        *b_Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1up;   //!

   //float mc_Photon_LooseIDSF_Syst_PH_EFF_ID_Uncertainty__1down;               //!
   //float mc_Photon_LooseIDSF_Syst_PH_EFF_ID_Uncertainty__1up;               //!

   // Photon_TightIDSF_Syst_Tree
   TTree *Photon_TightIDSF_Syst_Tree = nullptr;
   float mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1down;               //!
   float mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1up;               //!
   
   TBranch *b_mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1down;               //!
   TBranch *b_mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1up;               //!
   
   // Photon_FCLooseIsoSF_Syst_Tree
   TTree *Photon_FCLooseIsoSF_Syst_Tree = nullptr;
   float mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down;               //!
   float mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up;               //!
   
   TBranch *b_mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down;               //!
   TBranch *b_mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up;               //!

   // Photon_FCTightIsoSF_Syst_Tree
   TTree *Photon_FCTightIsoSF_Syst_Tree = nullptr;
   float mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down;               //!
   float mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up;               //!

   TBranch *b_mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down;               //!
   TBranch *b_mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up;               //!

   // ParticleTruth_Tree
   TTree *ParticleTruth_Tree = nullptr;
   Int_t Truth_ID;
   Int_t Truth_Barcode;
   Int_t Truth_Status;
   Int_t Truth_MotherID;
   Float_t Truth_Px;
   Float_t Truth_Py;
   Float_t Truth_Pz;
   Float_t Truth_E;
   Float_t Truth_M;

   TBranch *b_Truth_ID;       //!
   TBranch *b_Truth_Barcode;  //!
   TBranch *b_Truth_Status;   //!
   TBranch *b_Truth_MotherID; //!
   TBranch *b_Truth_Px;       //!
   TBranch *b_Truth_Py;       //!
   TBranch *b_Truth_Pz;       //!
   TBranch *b_Truth_E;        //!
   TBranch *b_Truth_M;        //!

   // PDFInfo_Tree
   TTree *PDFInfo_Tree = nullptr;
   Int_t PDGID1;
   Int_t PDGID2;
   Int_t PDFID1;
   Int_t PDFID2;
   Float_t x1;
   Float_t x2;
   Float_t Q;
   Float_t xf1;
   Float_t xf2;

   TBranch *b_PDGID1; //!
   TBranch *b_PDGID2; //!
   TBranch *b_PDFID1; //!
   TBranch *b_PDFID2; //!
   TBranch *b_x1;     //!
   TBranch *b_x2;     //!
   TBranch *b_Q;      //!
   TBranch *b_xf1;    //!
   TBranch *b_xf2;    //!

   // TriggerInfo_Tree
   TTree *TriggerInfo_Tree = nullptr;
   Bool_t PassTrigger_HLT_mu26_imedium;
   Bool_t PassTrigger_HLT_e24_lhmedium_L1EM20VH;
   Bool_t PassTrigger_HLT_e60_lhmedium;
   Bool_t PassTrigger_HLT_e120_lhloose;
   Bool_t PassTrigger_HLT_mu50;
   Bool_t PassTrigger_HLT_mu26_ivarmedium;
   Bool_t PassTrigger_HLT_e26_lhtight_nod0_ivarloose;
   Bool_t PassTrigger_HLT_e60_lhmedium_nod0;
   Bool_t PassTrigger_HLT_e140_lhloose_nod0;
   Bool_t PassTrigger_HLT_e300_etcut;

   TBranch *b_PassTrigger_HLT_mu26_imedium;               //!
   TBranch *b_PassTrigger_HLT_e24_lhmedium_L1EM20VH;      //!
   TBranch *b_PassTrigger_HLT_e60_lhmedium;               //!
   TBranch *b_PassTrigger_HLT_e120_lhloose;               //!
   TBranch *b_PassTrigger_HLT_mu50;                       //!
   TBranch *b_PassTrigger_HLT_mu26_ivarmedium;            //!
   TBranch *b_PassTrigger_HLT_e26_lhtight_nod0_ivarloose; //!
   TBranch *b_PassTrigger_HLT_e60_lhmedium_nod0;          //!
   TBranch *b_PassTrigger_HLT_e140_lhloose_nod0;          //!
   TBranch *b_PassTrigger_HLT_e300_etcut;                 //!

   // Functions of getting entries
   void RetrieveTriggerInfo()
   {
      if (TriggerInfo_Tree)
         TriggerInfo_Tree->GetEntry(CurrentEntry());
   }

   void RetrieveEventInfo()
   {
      if (EventInfo_Tree)
         EventInfo_Tree->GetEntry(CurrentEntry());
   }

   void RetrievePDFInfo()
   {
      if (PDFInfo_Tree)
         PDFInfo_Tree->GetEntry(CurrentEntry());
   }

   void RetrieveMETSyst()
   {
      if (METSyst_Tree)
         METSyst_Tree->GetEntry(CurrentEntry());
   }

   void RetrieveVertex(int number)
   {
      if (number < 0 || number > NVertex - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveVertex");
         MSGUser()->MSG_WARNING("input number not legal (number of vertex is ", NVertex, ", but ", number, " is given)");
         return;
      }

      if (Vertex_Tree)
         Vertex_Tree->GetEntry(VertexCount + number);
   }

   void RetrieveParticleTruth(int number)
   {
      if (number < 0 || number > NTruthParticles - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveParticleTruth");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NTruthParticles, ", but ", number, " is given)");
         return;
      }

      if (ParticleTruth_Tree)
         ParticleTruth_Tree->GetEntry(TruthParticleCount + number);
   }

   void RetrieveElectron(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectron");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_Tree)
         Electron_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronCalibration(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronCalibration");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_Calibration_Tree)
         Electron_Calibration_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronUseless(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronUseless");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_Useless_Tree)
         Electron_Useless_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTrack(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTrack");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_Track_Tree)
         Electron_Track_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTrigger(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTrigger");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_Trigger_Tree)
         Electron_Trigger_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronIsolation(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronIsolation");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_Isolation_Tree)
         Electron_Isolation_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronCluster(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronCluster");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_Cluster_Tree)
         Electron_Cluster_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronShowerShape(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronShowerShape");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_ShowerShape_Tree)
         Electron_ShowerShape_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronSF(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronSF");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_SF_Tree)
         Electron_SF_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronPtSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronPtSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_Pt_Syst_Tree)
         Electron_Pt_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDChargeMisIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDChargeMisIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_ChargeMisIDSF_Syst_Tree)
         Electron_MediumID_ChargeMisIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumIDSF_Syst_Tree)
         Electron_MediumIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDChargeMisIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDChargeMisIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_ChargeMisIDSF_Syst_Tree)
         Electron_TightID_ChargeMisIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightIDSF_Syst_Tree)
         Electron_TightIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCLooseIsoChargeMisIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCLooseIsoChargeMisIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree)
         Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCLooseIsoSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCLooseIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCLooseIsoSF_Syst_Tree)
         Electron_MediumID_FCLooseIsoSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCLooseIsoTriggerEffSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCLooseIsoTriggerEffSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree)
         Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCLooseIsoTriggerSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCLooseIsoTriggerSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree)
         Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCLooseIsoChargeMisIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCLooseIsoChargeMisIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree)
         Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCLooseIsoSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCLooseIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCLooseIsoSF_Syst_Tree)
         Electron_TightID_FCLooseIsoSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCLooseIsoTriggerEffSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCLooseIsoTriggerEffSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree)
         Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCLooseIsoTriggerSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCLooseIsoTriggerSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree)
         Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCTightIsoChargeMisIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCTightIsoChargeMisIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree)
         Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCTightIsoSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCTightIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCTightIsoSF_Syst_Tree)
         Electron_MediumID_FCTightIsoSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCTightIsoTriggerEffSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCTightIsoTriggerEffSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree)
         Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCTightIsoTriggerSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCTightIsoTriggerSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree)
         Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCTightIsoECIDSLooseSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCTightIsoECIDSLooseSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree)
         Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronMediumIDFCTightIsoECIDSLooseChargeMisIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronMediumIDFCTightIsoECIDSLooseChargeMisIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree)
         Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCTightIsoChargeMisIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCTightIsoChargeMisIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree)
         Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCTightIsoSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCTightIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCTightIsoSF_Syst_Tree)
         Electron_TightID_FCTightIsoSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCTightIsoTriggerEffSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCTightIsoTriggerEffSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCTightIso_TriggerEff_Syst_Tree)
         Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCTightIsoTriggerSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCTightIsoTriggerSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCTightIso_TriggerSF_Syst_Tree)
         Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCTightIsoECIDSLooseSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCTightIsoECIDSLooseSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree)
         Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronTightIDFCTightIsoECIDSLooseChargeMisIDSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronTightIDFCTightIsoECIDSLooseChargeMisIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree)
         Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveElectronRecoSFSyst(int number)
   {
      if (number < 0 || number > NElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveElectronRecoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NElectrons, ", but ", number, " is given)");
         return;
      }

      if (Electron_RecoSF_Syst_Tree)
         Electron_RecoSF_Syst_Tree->GetEntry(ElectronCount + number);
   }

   void RetrieveForwardElectron(int number)
   {
      if (number < 0 || number > NForwardElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveForwardElectron");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NForwardElectrons, ", but ", number, " is given)");
         return;
      }

      if (ForwardElectron_Tree)
         ForwardElectron_Tree->GetEntry(ForwardElectronCount + number);
   }

   void RetrieveForwardElectronCalibration(int number)
   {
      if (number < 0 || number > NForwardElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveForwardElectronCalibration");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NForwardElectrons, ", but ", number, " is given)");
         return;
      }

      if (ForwardElectron_Calibration_Tree)
         ForwardElectron_Calibration_Tree->GetEntry(ForwardElectronCount + number);
   }

   void RetrieveForwardElectronShowerShape(int number)
   {
      if (number < 0 || number > NForwardElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveForwardElectronShowerShape");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NForwardElectrons, ", but ", number, " is given)");
         return;
      }

      if (ForwardElectron_ShowerShape_Tree)
         ForwardElectron_ShowerShape_Tree->GetEntry(ForwardElectronCount + number);
   }

   void RetrieveForwardElectronCluster(int number)
   {
      if (number < 0 || number > NForwardElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveForwardElectronCluster");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NForwardElectrons, ", but ", number, " is given)");
         return;
      }

      if (ForwardElectron_Cluster_Tree)
         ForwardElectron_Cluster_Tree->GetEntry(ForwardElectronCount + number);
   }

   void RetrieveForwardElectronSF(int number)
   {
      if (number < 0 || number > NForwardElectrons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveForwardElectronSF");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NForwardElectrons, ", but ", number, " is given)");
         return;
      }

      if (ForwardElectron_SF_Tree)
         ForwardElectron_SF_Tree->GetEntry(ForwardElectronCount + number);
   }

   void RetrieveMuon(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuon");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_Tree)
         Muon_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonTrack(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonTrack");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_Track_Tree)
         Muon_Track_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonTrigger(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonTrigger");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_Trigger_Tree)
         Muon_Trigger_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonIsolation(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonIsolation");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }
      if (Muon_Isolation_Tree)
         Muon_Isolation_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonSF(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonSF");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_SF_Tree)
         Muon_SF_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonPtSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonPtSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_Pt_Syst_Tree)
         Muon_Pt_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonHighPtSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonHighPtSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_HighPt_Syst_Tree)
         Muon_HighPt_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonFixedCutPflowLooseIsoSFSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonFixedCutPflowLooseIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_FixedCutPflowLooseIsoSF_Syst_Tree)
         Muon_FixedCutPflowLooseIsoSF_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonFCLooseIsoSFSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonFCLooseIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_FCLooseIsoSF_Syst_Tree)
         Muon_FCLooseIsoSF_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonFCTightIsoSFSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonFCTightIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_FCTightIsoSF_Syst_Tree)
         Muon_FCTightIsoSF_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonPLVLooseIsoSFSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonPLVLooseIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_PLVLooseIsoSF_Syst_Tree)
         Muon_PLVLooseIsoSF_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonPLVTightIsoSFSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonPLVTightIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_PLVTightIsoSF_Syst_Tree)
         Muon_PLVTightIsoSF_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonRecoLooseSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonRecoLooseSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_RecoLoose_Syst_Tree)
         Muon_RecoLoose_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonRecoMediumSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonRecoMediumSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_RecoMedium_Syst_Tree)
         Muon_RecoMedium_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonRecoTightSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonRecoTightSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_RecoTight_Syst_Tree)
         Muon_RecoTight_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonRecoHighPtSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonRecoHighPtSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_RecoHighPt_Syst_Tree)
         Muon_RecoHighPt_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveMuonTTVASFSyst(int number)
   {
      if (number < 0 || number > NMuons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveMuonTTVASFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NMuons, ", but ", number, " is given)");
         return;
      }

      if (Muon_TTVASF_Syst_Tree)
         Muon_TTVASF_Syst_Tree->GetEntry(MuonCount + number);
   }

   void RetrieveJet(int number)
   {
      if (number < 0 || number > NJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJet");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NJets, ", but ", number, " is given)");
         return;
      }

      if (Jet_Tree)
         Jet_Tree->GetEntry(JetCount + number);
   }

   void RetrieveJetSF(int number)
   {
      if (number < 0 || number > NJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJetSF");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NJets, ", but ", number, " is given)");
         return;
      }

      if (Jet_SF_Tree)
         Jet_SF_Tree->GetEntry(JetCount + number);
   }

   void RetrieveJetTruth(int number)
   {
      if (number < 0 || number > NTruthJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJetTruth");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NTruthJets, ", but ", number, " is given)");
         return;
      }

      if (JetTruth_Tree)
         JetTruth_Tree->GetEntry(TruthJetCount + number);
   }
/*
   void RetrieveJetPtSyst(int number)
   {
      if (number < 0 || number > NJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJetPtSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NJets, ", but ", number, " is given)");
         return;
      }

      if (Jet_Pt_Syst_Tree)
         Jet_Pt_Syst_Tree->GetEntry(JetCount + number);
   }

   void RetrieveJetJvtSyst(int number)
   {
      if (number < 0 || number > NJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJetJvtSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NJets, ", but ", number, " is given)");
         return;
      }

      if (Jet_Jvt_Syst_Tree)
         Jet_Jvt_Syst_Tree->GetEntry(JetCount + number);
   }

   void RetrieveJetBJetEff60Syst(int number)
   {
      if (number < 0 || number > NJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJetBJetEff60Syst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NJets, ", but ", number, " is given)");
         return;
      }

      if (Jet_BJet_Eff60_Syst_Tree)
         Jet_BJet_Eff60_Syst_Tree->GetEntry(JetCount + number);
   }

   void RetrieveJetBJetEff70Syst(int number)
   {
      if (number < 0 || number > NJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJetBJetEff70Syst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NJets, ", but ", number, " is given)");
         return;
      }

      if (Jet_BJet_Eff70_Syst_Tree)
         Jet_BJet_Eff70_Syst_Tree->GetEntry(JetCount + number);
   }

   void RetrieveJetBJetEff77Syst(int number)
   {
      if (number < 0 || number > NJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJetBJetEff77Syst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NJets, ", but ", number, " is given)");
         return;
      }

      if (Jet_BJet_Eff77_Syst_Tree)
         Jet_BJet_Eff77_Syst_Tree->GetEntry(JetCount + number);
   }

   void RetrieveJetBJetEff85Syst(int number)
   {
      if (number < 0 || number > NJets - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrieveJetBJetEff85Syst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NJets, ", but ", number, " is given)");
         return;
      }

      if (Jet_BJet_Eff85_Syst_Tree)
         Jet_BJet_Eff85_Syst_Tree->GetEntry(JetCount + number);
   }
*/
   void RetrievePhoton(int number)
   {
      if (number < 0 || number > NPhotons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrievePhoton");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NPhotons, ", but ", number, " is given)");
         return;
      }

      if (Photon_Tree)
         Photon_Tree->GetEntry(PhotonCount + number);
   }

   void RetrievePhotonSF(int number)
   {
      if (number < 0 || number > NPhotons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrievePhotonSF");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NPhotons, ", but ", number, " is given)");
         return;
      }

      if (Photon_SF_Tree)
         Photon_SF_Tree->GetEntry(PhotonCount + number);
   }

   void RetrievePhotonPtSyst(int number)
   {
      if (number < 0 || number > NPhotons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrievePhotonPtSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NPhotons, ", but ", number, " is given)");
         return;
      }

      if (Photon_Pt_Syst_Tree)
         Photon_Pt_Syst_Tree->GetEntry(PhotonCount + number);
   }
   
   void RetrievePhotonTightIDSFSyst(int number)
   {
      if (number < 0 || number > NPhotons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrievePhotonTightIDSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NPhotons, ", but ", number, " is given)");
         return;
      }

      if (Photon_TightIDSF_Syst_Tree)
         Photon_TightIDSF_Syst_Tree->GetEntry(PhotonCount + number);
   }

   void RetrievePhotonFCLooseIsoSFSyst(int number)
   {
      if (number < 0 || number > NPhotons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrievePhotonFCLooseIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NPhotons, ", but ", number, " is given)");
         return;
      }

      if (Photon_FCLooseIsoSF_Syst_Tree)
         Photon_FCLooseIsoSF_Syst_Tree->GetEntry(PhotonCount + number);
   }

   void RetrievePhotonFCTightIsoSFSyst(int number)
   {
      if (number < 0 || number > NPhotons - 1)
      {
         auto guard = MSGUser()->StartTitleWithGuard("LoopV20220820::RetrievePhotonFCTightIsoSFSyst");
         MSGUser()->MSG_WARNING("input number not legal (number of truth particles is ", NPhotons, ", but ", number, " is given)");
         return;
      }

      if (Photon_FCTightIsoSF_Syst_Tree)
         Photon_FCTightIsoSF_Syst_Tree->GetEntry(PhotonCount + number);
   }

private:
   uint64_t ElectronCount = 0;
   uint64_t ForwardElectronCount = 0;
   uint64_t MuonCount = 0;
   uint64_t JetCount = 0;
   uint64_t PhotonCount = 0;
   uint64_t TruthJetCount = 0;
   uint64_t TruthParticleCount = 0;
   uint64_t VertexCount = 0;

   std::map<TString, TTree *> SubTree_Map;
   std::map<TString, long> SubTree_Counter_Map;
   std::map<TString, Int_t *> SubTree_SubCounter_Map;
};

inline NAGASH::StatusCode WYLoopEvent::InitializeData()
{
   // The InitializeData() function is used to open ROOT file, get TTree into
   // RootTreeUser. And call TTree::SetBranchAddress to set the address of variables
   // User need not to modify this function.

   RootFileUser()->GetObject("EventSave_Tree", RootTreeUser());
   if (RootTreeUser() == nullptr)
   {
      MSGUser()->MSG(NAGASH::MSGLevel::ERROR, "Wrong TTree name");
      return NAGASH::StatusCode::FAILURE;
   }

   // Set branch addresses and branch pointers
   RootTreeUser()->SetBranchAddress("PassTrigger", &PassTrigger, &b_PassTrigger);
   RootTreeUser()->SetBranchAddress("PassElectronTrigger", &PassElectronTrigger, &b_PassElectronTrigger);
   RootTreeUser()->SetBranchAddress("PassMuonTrigger", &PassMuonTrigger, &b_PassMuonTrigger);
   RootTreeUser()->SetBranchAddress("TriggerMatched", &TriggerMatched, &b_TriggerMatched);
   RootTreeUser()->SetBranchAddress("WithBadJet", &WithBadJet, &b_WithBadJet);
   RootTreeUser()->SetBranchAddress("IsBadBatman", &IsBadBatman, &b_IsBadBatman);
   RootTreeUser()->SetBranchAddress("NElectrons", &NElectrons, &b_NElectrons);
   RootTreeUser()->SetBranchAddress("NLooseElectrons", &NLooseElectrons, &b_NLooseElectrons);
   RootTreeUser()->SetBranchAddress("NMediumElectrons", &NMediumElectrons, &b_NMediumElectrons);
   RootTreeUser()->SetBranchAddress("NTightElectrons", &NTightElectrons, &b_NTightElectrons);
   RootTreeUser()->SetBranchAddress("NMuons", &NMuons, &b_NMuons);
   RootTreeUser()->SetBranchAddress("NLooseMuons", &NLooseMuons, &b_NLooseMuons);
   RootTreeUser()->SetBranchAddress("NMediumMuons", &NMediumMuons, &b_NMediumMuons);
   RootTreeUser()->SetBranchAddress("NTightMuons", &NTightMuons, &b_NTightMuons);
   RootTreeUser()->SetBranchAddress("NHighPtMuons", &NHighPtMuons, &b_NHighPtMuons);
   RootTreeUser()->SetBranchAddress("NForwardElectrons", &NForwardElectrons, &b_NForwardElectrons);
   RootTreeUser()->SetBranchAddress("NJets", &NJets, &b_NJets);
   RootTreeUser()->SetBranchAddress("NBJets_Eff60", &NBJets_Eff60, &b_NBJets_Eff60);
   RootTreeUser()->SetBranchAddress("NBJets_Eff70", &NBJets_Eff70, &b_NBJets_Eff70);
   RootTreeUser()->SetBranchAddress("NBJets_Eff77", &NBJets_Eff77, &b_NBJets_Eff77);
   RootTreeUser()->SetBranchAddress("NBJets_Eff85", &NBJets_Eff85, &b_NBJets_Eff85);
   RootTreeUser()->SetBranchAddress("NPhotons", &NPhotons, &b_NPhotons);
   RootTreeUser()->SetBranchAddress("NLoosePhotons", &NLoosePhotons, &b_NLoosePhotons);
   RootTreeUser()->SetBranchAddress("NTightPhotons", &NTightPhotons, &b_NTightPhotons);
   RootTreeUser()->SetBranchAddress("NTruthParticles", &NTruthParticles, &b_NTruthParticles);
   RootTreeUser()->SetBranchAddress("NTruthJets", &NTruthJets, &b_NTruthJets);

   // Electron_Calibration_Tree
   RootFileUser()->GetObject("Electron_Calibration_Tree", Electron_Calibration_Tree);
   if (Electron_Calibration_Tree)
   {
      Electron_Calibration_Tree->SetBranchAddress("Electron_Original_Pt", &Electron_Original_Pt, &b_Electron_Original_Pt);
      Electron_Calibration_Tree->SetBranchAddress("Electron_Calibrated_Pt_S12Off", &Electron_Calibrated_Pt_S12Off, &b_Electron_Calibrated_Pt_S12Off);
   }

   // Electron_Cluster_Tree
   RootFileUser()->GetObject("Electron_Cluster_Tree", Electron_Cluster_Tree);
   if (Electron_Cluster_Tree)
   {
      Electron_Cluster_Tree->SetBranchAddress("Electron_nCluster", &Electron_nCluster, &b_Electron_nCluster);
      Electron_Cluster_Tree->SetBranchAddress("Electron_Cluster_ETACALOFRAME", &Electron_Cluster_ETACALOFRAME, &b_Electron_Cluster_ETACALOFRAME);
      Electron_Cluster_Tree->SetBranchAddress("Electron_Cluster_PHICALOFRAME", &Electron_Cluster_PHICALOFRAME, &b_Electron_Cluster_PHICALOFRAME);
      Electron_Cluster_Tree->SetBranchAddress("Electron_Cluster_ETA1CALOFRAME", &Electron_Cluster_ETA1CALOFRAME, &b_Electron_Cluster_ETA1CALOFRAME);
      Electron_Cluster_Tree->SetBranchAddress("Electron_Cluster_PHI1CALOFRAME", &Electron_Cluster_PHI1CALOFRAME, &b_Electron_Cluster_PHI1CALOFRAME);
      Electron_Cluster_Tree->SetBranchAddress("Electron_Cluster_ETA2CALOFRAME", &Electron_Cluster_ETA2CALOFRAME, &b_Electron_Cluster_ETA2CALOFRAME);
      Electron_Cluster_Tree->SetBranchAddress("Electron_Cluster_PHI2CALOFRAME", &Electron_Cluster_PHI2CALOFRAME, &b_Electron_Cluster_PHI2CALOFRAME);
   }

   // Electron_Isolation_Tree
   RootFileUser()->GetObject("Electron_Isolation_Tree", Electron_Isolation_Tree);
   if (Electron_Isolation_Tree)
   {
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptcone20", &Electron_Isolation_ptcone20, &b_Electron_Isolation_ptcone20);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptcone30", &Electron_Isolation_ptcone30, &b_Electron_Isolation_ptcone30);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptcone40", &Electron_Isolation_ptcone40, &b_Electron_Isolation_ptcone40);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptvarcone20", &Electron_Isolation_ptvarcone20, &b_Electron_Isolation_ptvarcone20);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptvarcone30_TightTTVA_pt1000", &Electron_Isolation_ptvarcone30_TightTTVA_pt1000, &b_Electron_Isolation_ptvarcone30_TightTTVA_pt1000);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptcone20_TightTTVALooseCone_pt500", &Electron_Isolation_ptcone20_TightTTVALooseCone_pt500, &b_Electron_Isolation_ptcone20_TightTTVALooseCone_pt500);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptcone20_TightTTVALooseCone_pt1000", &Electron_Isolation_ptcone20_TightTTVALooseCone_pt1000, &b_Electron_Isolation_ptcone20_TightTTVALooseCone_pt1000);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptvarcone20_TightTTVALooseCone_pt1000", &Electron_Isolation_ptvarcone20_TightTTVALooseCone_pt1000, &b_Electron_Isolation_ptvarcone20_TightTTVALooseCone_pt1000);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptvarcone30_TightTTVALooseCone_pt1000", &Electron_Isolation_ptvarcone30_TightTTVALooseCone_pt1000, &b_Electron_Isolation_ptvarcone30_TightTTVALooseCone_pt1000);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_ptvarcone40_TightTTVALooseCone_pt1000", &Electron_Isolation_ptvarcone40_TightTTVALooseCone_pt1000, &b_Electron_Isolation_ptvarcone40_TightTTVALooseCone_pt1000);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_topoetcone20", &Electron_Isolation_topoetcone20, &b_Electron_Isolation_topoetcone20);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_topoetcone30", &Electron_Isolation_topoetcone30, &b_Electron_Isolation_topoetcone30);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_topoetcone40", &Electron_Isolation_topoetcone40, &b_Electron_Isolation_topoetcone40);
      Electron_Isolation_Tree->SetBranchAddress("Electron_Isolation_neflowisol20", &Electron_Isolation_neflowisol20, &b_Electron_Isolation_neflowisol20);
   }

   // Electron_MediumID_ChargeMisIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_ChargeMisIDSF_Syst_Tree", Electron_MediumID_ChargeMisIDSF_Syst_Tree);
   if (Electron_MediumID_ChargeMisIDSF_Syst_Tree)
   {
      Electron_MediumID_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down", &mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down, &b_mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down);
      Electron_MediumID_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up", &mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up, &b_mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up);
      Electron_MediumID_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down", &mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down, &b_mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down);
      Electron_MediumID_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up", &mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up, &b_mc_Electron_MediumID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up);
   }

   // Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree", Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree);
   if (Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree)
   {
      Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down", &mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down, &b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down);
      Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up", &mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up, &b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up);
      Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down", &mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down, &b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down);
      Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up", &mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up, &b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up);
   }

   // Electron_MediumID_FCLooseIsoSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCLooseIsoSF_Syst_Tree", Electron_MediumID_FCLooseIsoSF_Syst_Tree);
   if (Electron_MediumID_FCLooseIsoSF_Syst_Tree)
   {
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up);
      Electron_MediumID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex", &mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_MediumID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree", Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree);
   if (Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree)
   {
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up);
      Electron_MediumID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex", &mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree", Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree);
   if (Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree)
   {
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up);
      Electron_MediumID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex", &mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree", Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree);
   if (Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree)
   {
      Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down", &mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down, &b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down);
      Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up", &mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up, &b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up);
      Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down", &mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down, &b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down);
      Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up", &mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up, &b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up);
   }

   // Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree", Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree);
   if (Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree)
   {
      Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down);
      Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up);
      Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down);
      Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up);
   }

   // Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree", Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree);
   if (Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree)
   {
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up);
      Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_MediumID_FCTightIsoSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCTightIsoSF_Syst_Tree", Electron_MediumID_FCTightIsoSF_Syst_Tree);
   if (Electron_MediumID_FCTightIsoSF_Syst_Tree)
   {
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up);
      Electron_MediumID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex", &mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_MediumID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree", Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree);
   if (Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree)
   {
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up);
      Electron_MediumID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex", &mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_MediumID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree", Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree);
   if (Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree)
   {
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up);
      Electron_MediumID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex", &mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_MediumID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_MediumIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_MediumIDSF_Syst_Tree", Electron_MediumIDSF_Syst_Tree);
   if (Electron_MediumIDSF_Syst_Tree)
   {
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up);
      Electron_MediumIDSF_Syst_Tree->SetBranchAddress("mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex", &mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_MediumIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_Pt_Syst_Tree
   RootFileUser()->GetObject("Electron_Pt_Syst_Tree", Electron_Pt_Syst_Tree);
   if (Electron_Pt_Syst_Tree)
   {
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up", &Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up, &b_Electron_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_AF2__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_G4__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down", &Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down);
      Electron_Pt_Syst_Tree->SetBranchAddress("Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up", &Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up, &b_Electron_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up);
   }

   // Electron_RecoSF_Syst_Tree
   RootFileUser()->GetObject("Electron_RecoSF_Syst_Tree", Electron_RecoSF_Syst_Tree);
   if (Electron_RecoSF_Syst_Tree)
   {
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1down", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1down, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1down);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1up", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1up, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP0__1up);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1down", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1down, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1down);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1up", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1up, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP1__1up);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1down", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1down, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1down);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1up", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1up, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP2__1up);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1down", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1down, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1down);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1up", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1up, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP3__1up);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1down", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1down, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1down);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1up", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1up, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP4__1up);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1down", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1down, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1down);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1up", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1up, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP5__1up);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1down", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1down, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1down);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1up", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1up, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_CorrUncertaintyNP6__1up);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1down", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1down, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1down);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1up", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1up, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty__1up);
      Electron_RecoSF_Syst_Tree->SetBranchAddress("mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty_BinIndex", &mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_RecoSF_Syst_EL_EFF_Reco_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_SF_Tree
   RootFileUser()->GetObject("Electron_SF_Tree", Electron_SF_Tree);
   if (Electron_SF_Tree)
   {
      Electron_SF_Tree->SetBranchAddress("mc_Electron_RecoSF", &mc_Electron_RecoSF, &b_mc_Electron_RecoSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumIDSF", &mc_Electron_MediumIDSF, &b_mc_Electron_MediumIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightIDSF", &mc_Electron_TightIDSF, &b_mc_Electron_TightIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIsoSF", &mc_Electron_MediumID_FCLooseIsoSF, &b_mc_Electron_MediumID_FCLooseIsoSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF", &mc_Electron_TightID_FCLooseIsoSF, &b_mc_Electron_TightID_FCLooseIsoSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIsoSF", &mc_Electron_MediumID_FCTightIsoSF, &b_mc_Electron_MediumID_FCTightIsoSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF", &mc_Electron_TightID_FCTightIsoSF, &b_mc_Electron_TightID_FCTightIsoSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerSF", &mc_Electron_MediumID_FCLooseIso_TriggerSF, &b_mc_Electron_MediumID_FCLooseIso_TriggerSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF", &mc_Electron_TightID_FCLooseIso_TriggerSF, &b_mc_Electron_TightID_FCLooseIso_TriggerSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerSF", &mc_Electron_MediumID_FCTightIso_TriggerSF, &b_mc_Electron_MediumID_FCTightIso_TriggerSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF", &mc_Electron_TightID_FCTightIso_TriggerSF, &b_mc_Electron_TightID_FCTightIso_TriggerSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_TriggerEff", &mc_Electron_MediumID_FCLooseIso_TriggerEff, &b_mc_Electron_MediumID_FCLooseIso_TriggerEff);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff", &mc_Electron_TightID_FCLooseIso_TriggerEff, &b_mc_Electron_TightID_FCLooseIso_TriggerEff);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_TriggerEff", &mc_Electron_MediumID_FCTightIso_TriggerEff, &b_mc_Electron_MediumID_FCTightIso_TriggerEff);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff", &mc_Electron_TightID_FCTightIso_TriggerEff, &b_mc_Electron_TightID_FCTightIso_TriggerEff);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLooseSF", &mc_Electron_MediumID_FCTightIso_ECIDSLooseSF, &b_mc_Electron_MediumID_FCTightIso_ECIDSLooseSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_ChargeMisIDSF", &mc_Electron_MediumID_ChargeMisIDSF, &b_mc_Electron_MediumID_ChargeMisIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_ChargeMisIDSF", &mc_Electron_TightID_ChargeMisIDSF, &b_mc_Electron_TightID_ChargeMisIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF", &mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF, &b_mc_Electron_MediumID_FCLooseIso_ChargeMisIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_ChargeMisIDSF", &mc_Electron_TightID_FCLooseIso_ChargeMisIDSF, &b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ChargeMisIDSF", &mc_Electron_MediumID_FCTightIso_ChargeMisIDSF, &b_mc_Electron_MediumID_FCTightIso_ChargeMisIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ChargeMisIDSF", &mc_Electron_TightID_FCTightIso_ChargeMisIDSF, &b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF", &mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF, &b_mc_Electron_MediumID_FCTightIso_ECIDSLoose_ChargeMisIDSF);
      Electron_SF_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF", &mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF, &b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF);
   }

   // Electron_ShowerShape_Tree
   RootFileUser()->GetObject("Electron_ShowerShape_Tree", Electron_ShowerShape_Tree);
   if (Electron_ShowerShape_Tree)
   {
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_f1", &Electron_ShowerShape_f1, &b_Electron_ShowerShape_f1);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_f3", &Electron_ShowerShape_f3, &b_Electron_ShowerShape_f3);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_e277", &Electron_ShowerShape_e277, &b_Electron_ShowerShape_e277);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_weta1", &Electron_ShowerShape_weta1, &b_Electron_ShowerShape_weta1);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_weta2", &Electron_ShowerShape_weta2, &b_Electron_ShowerShape_weta2);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_fracs1", &Electron_ShowerShape_fracs1, &b_Electron_ShowerShape_fracs1);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_wtots1", &Electron_ShowerShape_wtots1, &b_Electron_ShowerShape_wtots1);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_Reta", &Electron_ShowerShape_Reta, &b_Electron_ShowerShape_Reta);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_Rphi", &Electron_ShowerShape_Rphi, &b_Electron_ShowerShape_Rphi);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_Eratio", &Electron_ShowerShape_Eratio, &b_Electron_ShowerShape_Eratio);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_Rhad", &Electron_ShowerShape_Rhad, &b_Electron_ShowerShape_Rhad);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_Rhad1", &Electron_ShowerShape_Rhad1, &b_Electron_ShowerShape_Rhad1);
      Electron_ShowerShape_Tree->SetBranchAddress("Electron_ShowerShape_DeltaE", &Electron_ShowerShape_DeltaE, &b_Electron_ShowerShape_DeltaE);
   }

   // Electron_TightID_ChargeMisIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_ChargeMisIDSF_Syst_Tree", Electron_TightID_ChargeMisIDSF_Syst_Tree);
   if (Electron_TightID_ChargeMisIDSF_Syst_Tree)
   {
      Electron_TightID_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down", &mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down, &b_mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down);
      Electron_TightID_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up", &mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up, &b_mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up);
      Electron_TightID_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down", &mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down, &b_mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down);
      Electron_TightID_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up", &mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up, &b_mc_Electron_TightID_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up);
   }

   // Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree", Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree);
   if (Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree)
   {
      Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down", &mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down, &b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down);
      Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up", &mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up, &b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up);
      Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down", &mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down, &b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down);
      Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up", &mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up, &b_mc_Electron_TightID_FCLooseIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up);
   }

   // Electron_TightID_FCLooseIsoSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCLooseIsoSF_Syst_Tree", Electron_TightID_FCLooseIsoSF_Syst_Tree);
   if (Electron_TightID_FCLooseIsoSF_Syst_Tree)
   {
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up);
      Electron_TightID_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex", &mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_TightID_FCLooseIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree", Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree);
   if (Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree)
   {
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up);
      Electron_TightID_FCLooseIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex", &mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_TightID_FCLooseIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree", Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree);
   if (Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree)
   {
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up);
      Electron_TightID_FCLooseIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex", &mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_TightID_FCLooseIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree", Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree);
   if (Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree)
   {
      Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down", &mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down, &b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down);
      Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up", &mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up, &b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up);
      Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down", &mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down, &b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down);
      Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up", &mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up, &b_mc_Electron_TightID_FCTightIso_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up);
   }

   // Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree", Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree);
   if (Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree)
   {
      Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down", &mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1down);
      Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up", &mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_STAT__1up);
      Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down", &mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1down);
      Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up", &mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLoose_ChargeMisIDSF_Syst_EL_CHARGEID_SYStotal__1up);
   }

   // Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree", Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree);
   if (Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree)
   {
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP0__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP10__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP1__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP2__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP3__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP4__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP5__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP6__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP7__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP8__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_CorrUncertaintyNP9__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1down);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty__1up);
      Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex", &mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_TightID_FCTightIso_ECIDSLooseSF_Syst_EL_EFF_ChargeIDSel_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_TightID_FCTightIsoSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCTightIsoSF_Syst_Tree", Electron_TightID_FCTightIsoSF_Syst_Tree);
   if (Electron_TightID_FCTightIsoSF_Syst_Tree)
   {
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP0__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP10__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP1__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP2__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP3__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP4__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP5__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP6__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP7__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP8__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_CorrUncertaintyNP9__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1down);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty__1up);
      Electron_TightID_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex", &mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_TightID_FCTightIsoSF_Syst_EL_EFF_Iso_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_TightID_FCTightIso_TriggerEff_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCTightIso_TriggerEff_Syst_Tree", Electron_TightID_FCTightIso_TriggerEff_Syst_Tree);
   if (Electron_TightID_FCTightIso_TriggerEff_Syst_Tree)
   {
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP0__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP10__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP1__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP2__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP3__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP4__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP5__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP6__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP7__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP8__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_CorrUncertaintyNP9__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1down);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty__1up);
      Electron_TightID_FCTightIso_TriggerEff_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex", &mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_TightID_FCTightIso_TriggerEff_Syst_EL_EFF_TriggerEff_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_TightID_FCTightIso_TriggerSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightID_FCTightIso_TriggerSF_Syst_Tree", Electron_TightID_FCTightIso_TriggerSF_Syst_Tree);
   if (Electron_TightID_FCTightIso_TriggerSF_Syst_Tree)
   {
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP0__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP10__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP1__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP2__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP3__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP4__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP5__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP6__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP7__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP8__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_CorrUncertaintyNP9__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1down);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty__1up);
      Electron_TightID_FCTightIso_TriggerSF_Syst_Tree->SetBranchAddress("mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex", &mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_TightID_FCTightIso_TriggerSF_Syst_EL_EFF_Trigger_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_TightIDSF_Syst_Tree
   RootFileUser()->GetObject("Electron_TightIDSF_Syst_Tree", Electron_TightIDSF_Syst_Tree);
   if (Electron_TightIDSF_Syst_Tree)
   {
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP0__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP10__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP11__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP12__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP13__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP14__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP15__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP1__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP2__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP3__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP4__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP5__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP6__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP7__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP8__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_CorrUncertaintyNP9__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1down);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty__1up);
      Electron_TightIDSF_Syst_Tree->SetBranchAddress("mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex", &mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex, &b_mc_Electron_TightIDSF_Syst_EL_EFF_ID_FULL_UncorrUncertainty_BinIndex);
   }

   // Electron_Track_Tree
   RootFileUser()->GetObject("Electron_Track_Tree", Electron_Track_Tree);
   if (Electron_Track_Tree)
   {
      Electron_Track_Tree->SetBranchAddress("Electron_Track_Pt", &Electron_Track_Pt, &b_Electron_Track_Pt);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_Eta", &Electron_Track_Eta, &b_Electron_Track_Eta);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_Phi", &Electron_Track_Phi, &b_Electron_Track_Phi);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_M", &Electron_Track_M, &b_Electron_Track_M);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_E", &Electron_Track_E, &b_Electron_Track_E);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_Rapidity", &Electron_Track_Rapidity, &b_Electron_Track_Rapidity);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_Charge", &Electron_Track_Charge, &b_Electron_Track_Charge);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_d0", &Electron_Track_d0, &b_Electron_Track_d0);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_z0", &Electron_Track_z0, &b_Electron_Track_z0);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_Phi0", &Electron_Track_Phi0, &b_Electron_Track_Phi0);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_Theta", &Electron_Track_Theta, &b_Electron_Track_Theta);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_qOverP", &Electron_Track_qOverP, &b_Electron_Track_qOverP);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_Chi2", &Electron_Track_Chi2, &b_Electron_Track_Chi2);
      Electron_Track_Tree->SetBranchAddress("Electron_Track_NDoF", &Electron_Track_NDoF, &b_Electron_Track_NDoF);
   }

   // Electron_Tree
   RootFileUser()->GetObject("Electron_Tree", Electron_Tree);
   if (Electron_Tree)
   {
      Electron_Tree->SetBranchAddress("Electron_Calibrated_Pt", &Electron_Calibrated_Pt, &b_Electron_Calibrated_Pt);
      Electron_Tree->SetBranchAddress("Electron_Eta", &Electron_Eta, &b_Electron_Eta);
      Electron_Tree->SetBranchAddress("Electron_Phi", &Electron_Phi, &b_Electron_Phi);
      Electron_Tree->SetBranchAddress("Electron_Charge", &Electron_Charge, &b_Electron_Charge);
      Electron_Tree->SetBranchAddress("Electron_PassIDMedium", &Electron_PassIDMedium, &b_Electron_PassIDMedium);
      Electron_Tree->SetBranchAddress("Electron_PassIDLoose", &Electron_PassIDLoose, &b_Electron_PassIDLoose);
      Electron_Tree->SetBranchAddress("Electron_PassIDTight", &Electron_PassIDTight, &b_Electron_PassIDTight);
      Electron_Tree->SetBranchAddress("Electron_Track_dz0", &Electron_Track_dz0, &b_Electron_Track_dz0);
      Electron_Tree->SetBranchAddress("Electron_Track_d0sig", &Electron_Track_d0sig, &b_Electron_Track_d0sig);
      Electron_Tree->SetBranchAddress("Electron_MatchedTrigger", &Electron_MatchedTrigger, &b_Electron_MatchedTrigger);
      Electron_Tree->SetBranchAddress("Electron_PassECIDSLoose", &Electron_PassECIDSLoose, &b_Electron_PassECIDSLoose);
      Electron_Tree->SetBranchAddress("Electron_PassIso_FCLoose", &Electron_PassIso_FCLoose, &b_Electron_PassIso_FCLoose);
      Electron_Tree->SetBranchAddress("Electron_PassIso_FCTight", &Electron_PassIso_FCTight, &b_Electron_PassIso_FCTight);
   }

   // Electron_Trigger_Tree
   RootFileUser()->GetObject("Electron_Trigger_Tree", Electron_Trigger_Tree);
   if (Electron_Trigger_Tree)
   {
      Electron_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_e24_lhmedium_L1EM20VH", &MatchedTrigger_HLT_e24_lhmedium_L1EM20VH, &b_MatchedTrigger_HLT_e24_lhmedium_L1EM20VH);
      Electron_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_e60_lhmedium", &MatchedTrigger_HLT_e60_lhmedium, &b_MatchedTrigger_HLT_e60_lhmedium);
      Electron_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_e120_lhloose", &MatchedTrigger_HLT_e120_lhloose, &b_MatchedTrigger_HLT_e120_lhloose);
      Electron_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_e26_lhtight_nod0_ivarloose", &MatchedTrigger_HLT_e26_lhtight_nod0_ivarloose, &b_MatchedTrigger_HLT_e26_lhtight_nod0_ivarloose);
      Electron_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_e60_lhmedium_nod0", &MatchedTrigger_HLT_e60_lhmedium_nod0, &b_MatchedTrigger_HLT_e60_lhmedium_nod0);
      Electron_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_e140_lhloose_nod0", &MatchedTrigger_HLT_e140_lhloose_nod0, &b_MatchedTrigger_HLT_e140_lhloose_nod0);
      Electron_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_e300_etcut", &MatchedTrigger_HLT_e300_etcut, &b_MatchedTrigger_HLT_e300_etcut);
   }

   // Electron_Useless_Tree
   RootFileUser()->GetObject("Electron_Useless_Tree", Electron_Useless_Tree);
   if (Electron_Useless_Tree)
   {
      Electron_Useless_Tree->SetBranchAddress("Electron_ECIDS", &Electron_ECIDS, &b_Electron_ECIDS);
   }

   // EventInfo_Tree
   RootFileUser()->GetObject("EventInfo_Tree", EventInfo_Tree);
   if (EventInfo_Tree)
   {
      EventInfo_Tree->SetBranchAddress("SumOfWeights_Channel", &SumOfWeights_Channel, &b_SumOfWeights_Channel);
      EventInfo_Tree->SetBranchAddress("TotalEventProcessed_Channel", &TotalEventProcessed_Channel, &b_TotalEventProcessed_Channel);
      EventInfo_Tree->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
      EventInfo_Tree->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
      EventInfo_Tree->SetBranchAddress("ChannelNumber", &ChannelNumber, &b_ChannelNumber);
      EventInfo_Tree->SetBranchAddress("BCID", &BCID, &b_BCID);
      EventInfo_Tree->SetBranchAddress("LumiBlockNumber", &LumiBlockNumber, &b_LumiBlockNumber);
      EventInfo_Tree->SetBranchAddress("Mu", &Mu, &b_Mu);
      EventInfo_Tree->SetBranchAddress("Average_Mu", &Average_Mu, &b_Average_Mu);
      EventInfo_Tree->SetBranchAddress("Corrected_Mu", &Corrected_Mu, &b_Corrected_Mu);
      EventInfo_Tree->SetBranchAddress("Corrected_Average_Mu", &Corrected_Average_Mu, &b_Corrected_Average_Mu);
      EventInfo_Tree->SetBranchAddress("CorrectedAndScaled_Mu", &CorrectedAndScaled_Mu, &b_CorrectedAndScaled_Mu);
      EventInfo_Tree->SetBranchAddress("CorrectedAndScaled_Average_Mu", &CorrectedAndScaled_Average_Mu, &b_CorrectedAndScaled_Average_Mu);
      EventInfo_Tree->SetBranchAddress("MuIndependent_RunNumber", &MuIndependent_RunNumber, &b_MuIndependent_RunNumber);
      EventInfo_Tree->SetBranchAddress("BeamPosition_X", &BeamPosition_X, &b_BeamPosition_X);
      EventInfo_Tree->SetBranchAddress("BeamPosition_Y", &BeamPosition_Y, &b_BeamPosition_Y);
      EventInfo_Tree->SetBranchAddress("BeamPosition_Z", &BeamPosition_Z, &b_BeamPosition_Z);
      EventInfo_Tree->SetBranchAddress("BeamPosition_SigmaX", &BeamPosition_SigmaX, &b_BeamPosition_SigmaX);
      EventInfo_Tree->SetBranchAddress("BeamPosition_SigmaY", &BeamPosition_SigmaY, &b_BeamPosition_SigmaY);
      EventInfo_Tree->SetBranchAddress("BeamPosition_SigmaZ", &BeamPosition_SigmaZ, &b_BeamPosition_SigmaZ);
      EventInfo_Tree->SetBranchAddress("mc_evtwt", &mc_evtwt, &b_mc_evtwt);
      EventInfo_Tree->SetBranchAddress("mc_pileupwt", &mc_pileupwt, &b_mc_pileupwt);
      EventInfo_Tree->SetBranchAddress("NVertex", &NVertex, &b_NVertex);
      EventInfo_Tree->SetBranchAddress("NPrimaryVertex", &NPrimaryVertex, &b_NPrimaryVertex);
      EventInfo_Tree->SetBranchAddress("NPileUpVertex", &NPileUpVertex, &b_NPileUpVertex);
      EventInfo_Tree->SetBranchAddress("MET_Px", &MET_Px, &b_MET_Px);
      EventInfo_Tree->SetBranchAddress("MET_Py", &MET_Py, &b_MET_Py);
      EventInfo_Tree->SetBranchAddress("MET_MET", &MET_MET, &b_MET_MET);
      EventInfo_Tree->SetBranchAddress("MET_Phi", &MET_Phi, &b_MET_Phi);
      EventInfo_Tree->SetBranchAddress("MET_SumET", &MET_SumET, &b_MET_SumET);
   }

   RootFileUser()->GetObject("Vertex_Tree", Vertex_Tree);
   if (Vertex_Tree)
   {
      Vertex_Tree->SetBranchAddress("Vertex_Accociated_Tracks", &Vertex_Accociated_Tracks, &b_Vertex_Accociated_Tracks);
   }

   // ForwardElectron_Calibration_Tree
   RootFileUser()->GetObject("ForwardElectron_Calibration_Tree", ForwardElectron_Calibration_Tree);
   if (ForwardElectron_Calibration_Tree)
   {
      ForwardElectron_Calibration_Tree->SetBranchAddress("ForwardElectron_Original_Pt", &ForwardElectron_Original_Pt, &b_ForwardElectron_Original_Pt);
      ForwardElectron_Calibration_Tree->SetBranchAddress("ForwardElectron_Calibrated_Pt_S12Off", &ForwardElectron_Calibrated_Pt_S12Off, &b_ForwardElectron_Calibrated_Pt_S12Off);
   }

   // ForwardElectron_Cluster_Tree
   RootFileUser()->GetObject("ForwardElectron_Cluster_Tree", ForwardElectron_Cluster_Tree);
   if (ForwardElectron_Cluster_Tree)
   {
     ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_nCluster", &ForwardElectron_nCluster, &b_ForwardElectron_nCluster);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_E", &ForwardElectron_Cluster_E, &b_ForwardElectron_Cluster_E);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_Eta", &ForwardElectron_Cluster_Eta, &b_ForwardElectron_Cluster_Eta);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_EmaxSamplRatio", &ForwardElectron_Cluster_EmaxSamplRatio, &b_ForwardElectron_Cluster_EmaxSamplRatio);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_Phi", &ForwardElectron_Cluster_Phi, &b_ForwardElectron_Cluster_Phi);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_FIRST_PHI", &ForwardElectron_Cluster_FIRST_PHI, &b_ForwardElectron_Cluster_FIRST_PHI);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_FIRST_ETA", &ForwardElectron_Cluster_FIRST_ETA, &b_ForwardElectron_Cluster_FIRST_ETA);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_SECOND_R", &ForwardElectron_Cluster_SECOND_R, &b_ForwardElectron_Cluster_SECOND_R);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_SECOND_LAMBDA", &ForwardElectron_Cluster_SECOND_LAMBDA, &b_ForwardElectron_Cluster_SECOND_LAMBDA);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_DELTA_PHI", &ForwardElectron_Cluster_DELTA_PHI, &b_ForwardElectron_Cluster_DELTA_PHI);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_DELTA_THETA", &ForwardElectron_Cluster_DELTA_THETA, &b_ForwardElectron_Cluster_DELTA_THETA);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_DELTA_ALPHA", &ForwardElectron_Cluster_DELTA_ALPHA, &b_ForwardElectron_Cluster_DELTA_ALPHA);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_CENTER_X", &ForwardElectron_Cluster_CENTER_X, &b_ForwardElectron_Cluster_CENTER_X);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_CENTER_Y", &ForwardElectron_Cluster_CENTER_Y, &b_ForwardElectron_Cluster_CENTER_Y);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_CENTER_Z", &ForwardElectron_Cluster_CENTER_Z, &b_ForwardElectron_Cluster_CENTER_Z);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_CENTER_MAG", &ForwardElectron_Cluster_CENTER_MAG, &b_ForwardElectron_Cluster_CENTER_MAG);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_CENTER_LAMBDA", &ForwardElectron_Cluster_CENTER_LAMBDA, &b_ForwardElectron_Cluster_CENTER_LAMBDA);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_LATERAL", &ForwardElectron_Cluster_LATERAL, &b_ForwardElectron_Cluster_LATERAL);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_LONGITUDINAL", &ForwardElectron_Cluster_LONGITUDINAL, &b_ForwardElectron_Cluster_LONGITUDINAL);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_ENG_FRAC_EM", &ForwardElectron_Cluster_ENG_FRAC_EM, &b_ForwardElectron_Cluster_ENG_FRAC_EM);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_ENG_FRAC_MAX", &ForwardElectron_Cluster_ENG_FRAC_MAX, &b_ForwardElectron_Cluster_ENG_FRAC_MAX);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_ENG_FRAC_CORE", &ForwardElectron_Cluster_ENG_FRAC_CORE, &b_ForwardElectron_Cluster_ENG_FRAC_CORE);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_FIRST_ENG_DENS", &ForwardElectron_Cluster_FIRST_ENG_DENS, &b_ForwardElectron_Cluster_FIRST_ENG_DENS);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_SECOND_ENG_DENS", &ForwardElectron_Cluster_SECOND_ENG_DENS, &b_ForwardElectron_Cluster_SECOND_ENG_DENS);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_ISOLATION", &ForwardElectron_Cluster_ISOLATION, &b_ForwardElectron_Cluster_ISOLATION);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_ENG_BAD_CELLS", &ForwardElectron_Cluster_ENG_BAD_CELLS, &b_ForwardElectron_Cluster_ENG_BAD_CELLS);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_N_BAD_CELLS", &ForwardElectron_Cluster_N_BAD_CELLS, &b_ForwardElectron_Cluster_N_BAD_CELLS);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_BAD_CELLS_CORR_E", &ForwardElectron_Cluster_BAD_CELLS_CORR_E, &b_ForwardElectron_Cluster_BAD_CELLS_CORR_E);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_BADLARQ_FRAC", &ForwardElectron_Cluster_BADLARQ_FRAC, &b_ForwardElectron_Cluster_BADLARQ_FRAC);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_ENG_POS", &ForwardElectron_Cluster_ENG_POS, &b_ForwardElectron_Cluster_ENG_POS);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_SIGNIFICANCE", &ForwardElectron_Cluster_SIGNIFICANCE, &b_ForwardElectron_Cluster_SIGNIFICANCE);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_CELL_SIGNIFICANCE", &ForwardElectron_Cluster_CELL_SIGNIFICANCE, &b_ForwardElectron_Cluster_CELL_SIGNIFICANCE);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_CELL_SIG_SAMPLING", &ForwardElectron_Cluster_CELL_SIG_SAMPLING, &b_ForwardElectron_Cluster_CELL_SIG_SAMPLING);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_AVG_LAR_Q", &ForwardElectron_Cluster_AVG_LAR_Q, &b_ForwardElectron_Cluster_AVG_LAR_Q);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_AVG_TILE_Q", &ForwardElectron_Cluster_AVG_TILE_Q, &b_ForwardElectron_Cluster_AVG_TILE_Q);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_ENG_BAD_HV_CELLS", &ForwardElectron_Cluster_ENG_BAD_HV_CELLS, &b_ForwardElectron_Cluster_ENG_BAD_HV_CELLS);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_N_BAD_HV_CELLS", &ForwardElectron_Cluster_N_BAD_HV_CELLS, &b_ForwardElectron_Cluster_N_BAD_HV_CELLS);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_PTD", &ForwardElectron_Cluster_PTD, &b_ForwardElectron_Cluster_PTD);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_EM_PROBABILITY", &ForwardElectron_Cluster_EM_PROBABILITY, &b_ForwardElectron_Cluster_EM_PROBABILITY);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_HAD_WEIGHT", &ForwardElectron_Cluster_HAD_WEIGHT, &b_ForwardElectron_Cluster_HAD_WEIGHT);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_OOC_WEIGHT", &ForwardElectron_Cluster_OOC_WEIGHT, &b_ForwardElectron_Cluster_OOC_WEIGHT);
   ForwardElectron_Cluster_Tree->SetBranchAddress("ForwardElectron_Cluster_DM_WEIGHT", &ForwardElectron_Cluster_DM_WEIGHT, &b_ForwardElectron_Cluster_DM_WEIGHT);
   }

   // ForwardElectron_SF_Tree
   RootFileUser()->GetObject("ForwardElectron_SF_Tree", ForwardElectron_SF_Tree);
   if (ForwardElectron_SF_Tree)
   {
      ForwardElectron_SF_Tree->SetBranchAddress("mc_ForwardElectron_LooseIDSF", &mc_ForwardElectron_LooseIDSF, &b_mc_ForwardElectron_LooseIDSF);
      ForwardElectron_SF_Tree->SetBranchAddress("mc_ForwardElectron_MediumIDSF", &mc_ForwardElectron_MediumIDSF, &b_mc_ForwardElectron_MediumIDSF);
      ForwardElectron_SF_Tree->SetBranchAddress("mc_ForwardElectron_TightIDSF", &mc_ForwardElectron_TightIDSF, &b_mc_ForwardElectron_TightIDSF);
   }

   // ForwardElectron_ShowerShape_Tree
   RootFileUser()->GetObject("ForwardElectron_ShowerShape_Tree", ForwardElectron_ShowerShape_Tree);
   if (ForwardElectron_ShowerShape_Tree)
   {
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_f1", &ForwardElectron_ShowerShape_f1, &b_ForwardElectron_ShowerShape_f1);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_f3", &ForwardElectron_ShowerShape_f3, &b_ForwardElectron_ShowerShape_f3);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_f1core", &ForwardElectron_ShowerShape_f1core, &b_ForwardElectron_ShowerShape_f1core);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_f3core", &ForwardElectron_ShowerShape_f3core, &b_ForwardElectron_ShowerShape_f3core);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_e277", &ForwardElectron_ShowerShape_e277, &b_ForwardElectron_ShowerShape_e277);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_weta1", &ForwardElectron_ShowerShape_weta1, &b_ForwardElectron_ShowerShape_weta1);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_weta2", &ForwardElectron_ShowerShape_weta2, &b_ForwardElectron_ShowerShape_weta2);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_fracs1", &ForwardElectron_ShowerShape_fracs1, &b_ForwardElectron_ShowerShape_fracs1);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_wtots1", &ForwardElectron_ShowerShape_wtots1, &b_ForwardElectron_ShowerShape_wtots1);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_Reta", &ForwardElectron_ShowerShape_Reta, &b_ForwardElectron_ShowerShape_Reta);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_Rphi", &ForwardElectron_ShowerShape_Rphi, &b_ForwardElectron_ShowerShape_Rphi);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_Eratio", &ForwardElectron_ShowerShape_Eratio, &b_ForwardElectron_ShowerShape_Eratio);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_Rhad", &ForwardElectron_ShowerShape_Rhad, &b_ForwardElectron_ShowerShape_Rhad);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_Rhad1", &ForwardElectron_ShowerShape_Rhad1, &b_ForwardElectron_ShowerShape_Rhad1);
      ForwardElectron_ShowerShape_Tree->SetBranchAddress("ForwardElectron_ShowerShape_DeltaE", &ForwardElectron_ShowerShape_DeltaE, &b_ForwardElectron_ShowerShape_DeltaE);
   }

   // ForwardElectron_Tree
   RootFileUser()->GetObject("ForwardElectron_Tree", ForwardElectron_Tree);
   if (ForwardElectron_Tree)
   {
      ForwardElectron_Tree->SetBranchAddress("ForwardElectron_Calibrated_Pt", &ForwardElectron_Calibrated_Pt, &b_ForwardElectron_Calibrated_Pt);
      ForwardElectron_Tree->SetBranchAddress("ForwardElectron_Eta", &ForwardElectron_Eta, &b_ForwardElectron_Eta);
      ForwardElectron_Tree->SetBranchAddress("ForwardElectron_Phi", &ForwardElectron_Phi, &b_ForwardElectron_Phi);
      ForwardElectron_Tree->SetBranchAddress("ForwardElectron_Charge", &ForwardElectron_Charge, &b_ForwardElectron_Charge);
      ForwardElectron_Tree->SetBranchAddress("ForwardElectron_PassIDMedium", &ForwardElectron_PassIDMedium, &b_ForwardElectron_PassIDMedium);
      ForwardElectron_Tree->SetBranchAddress("ForwardElectron_PassIDLoose", &ForwardElectron_PassIDLoose, &b_ForwardElectron_PassIDLoose);
      ForwardElectron_Tree->SetBranchAddress("ForwardElectron_PassIDTight", &ForwardElectron_PassIDTight, &b_ForwardElectron_PassIDTight);
      //ForwardElectron_Tree->SetBranchAddress("ForwardElectron_LikelihoodValue", &ForwardElectron_LikelihoodValue, &b_ForwardElectron_LikelihoodValue); 
   }
/*
   // Jet_BJet_Eff60_Syst_Tree
   RootFileUser()->GetObject("Jet_BJet_Eff60_Syst_Tree", Jet_BJet_Eff60_Syst_Tree);
   if (Jet_BJet_Eff60_Syst_Tree)
   {
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff60", &Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff60", &Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff60", &Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff60", &Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff60, &b_Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff60);
      Jet_BJet_Eff60_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff60", &Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff60, &b_Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff60);
   }

   // Jet_BJet_Eff70_Syst_Tree
   RootFileUser()->GetObject("Jet_BJet_Eff70_Syst_Tree", Jet_BJet_Eff70_Syst_Tree);
   if (Jet_BJet_Eff70_Syst_Tree)
   {
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff70", &Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff70", &Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff70", &Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff70", &Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff70, &b_Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff70);
      Jet_BJet_Eff70_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff70", &Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff70, &b_Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff70);
   }

   // Jet_BJet_Eff77_Syst_Tree
   RootFileUser()->GetObject("Jet_BJet_Eff77_Syst_Tree", Jet_BJet_Eff77_Syst_Tree);
   if (Jet_BJet_Eff77_Syst_Tree)
   {
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff77", &Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff77", &Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff77", &Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff77", &Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff77, &b_Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff77);
      Jet_BJet_Eff77_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff77", &Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff77, &b_Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff77);
   }

   // Jet_BJet_Eff85_Syst_Tree
   RootFileUser()->GetObject("Jet_BJet_Eff85_Syst_Tree", Jet_BJet_Eff85_Syst_Tree);
   if (Jet_BJet_Eff85_Syst_Tree)
   {
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_0__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_1__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_2__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_3__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_C_4__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_0__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_1__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_2__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_3__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_Light_4__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff85", &Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff85", &Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_extrapolation_from_charm__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_0__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_1__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_2__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_3__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_4__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_5__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_6__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_7__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff85", &Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_Eigen_B_8__1up_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff85", &Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff85, &b_Jet_isBJetSF_FT_EFF_extrapolation__1down_Eff85);
      Jet_BJet_Eff85_Syst_Tree->SetBranchAddress("Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff85", &Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff85, &b_Jet_isBJetSF_FT_EFF_extrapolation__1up_Eff85);
   }

   // Jet_Jvt_Syst_Tree
   RootFileUser()->GetObject("Jet_Jvt_Syst_Tree", Jet_Jvt_Syst_Tree);
   if (Jet_Jvt_Syst_Tree)
   {
      Jet_Jvt_Syst_Tree->SetBranchAddress("Jet_JvtSF_CentralSYS_DOWN", &Jet_JvtSF_CentralSYS_DOWN, &b_Jet_JvtSF_CentralSYS_DOWN);
      Jet_Jvt_Syst_Tree->SetBranchAddress("Jet_JvtSF_CentralSYS_UP", &Jet_JvtSF_CentralSYS_UP, &b_Jet_JvtSF_CentralSYS_UP);
      Jet_Jvt_Syst_Tree->SetBranchAddress("Jet_JvtSF_ForwardSYS_DOWN", &Jet_JvtSF_ForwardSYS_DOWN, &b_Jet_JvtSF_ForwardSYS_DOWN);
      Jet_Jvt_Syst_Tree->SetBranchAddress("Jet_JvtSF_ForwardSYS_UP", &Jet_JvtSF_ForwardSYS_UP, &b_Jet_JvtSF_ForwardSYS_UP);
   }

   // Jet_Pt_Syst_Tree
   RootFileUser()->GetObject("Jet_Pt_Syst_Tree", Jet_Pt_Syst_Tree);
   if (Jet_Pt_Syst_Tree)
   {
      /*Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_DataVsMC_MC16__1down", &Jet_Calibrated_Pt_JET_JER_DataVsMC_MC16__1down, &b_Jet_Calibrated_Pt_JET_JER_DataVsMC_MC16__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_central__1up", &Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_central__1up, &b_Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_central__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_forward__1up", &Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_forward__1up, &b_Jet_Calibrated_Pt_JET_JER_N_constscale_noise_up_forward__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_central__1down", &Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_central__1down, &b_Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_central__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_forward__1down", &Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_forward__1down, &b_Jet_Calibrated_Pt_JET_JER_N_fit_conversion_hist_variation_forward__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_fit_error_up_central__1up", &Jet_Calibrated_Pt_JET_JER_N_fit_error_up_central__1up, &b_Jet_Calibrated_Pt_JET_JER_N_fit_error_up_central__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_fit_error_up_forward__1up", &Jet_Calibrated_Pt_JET_JER_N_fit_error_up_forward__1up, &b_Jet_Calibrated_Pt_JET_JER_N_fit_error_up_forward__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_central__1down", &Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_central__1down, &b_Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_central__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_forward__1down", &Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_forward__1down, &b_Jet_Calibrated_Pt_JET_JER_N_fitrange_high_up_forward__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_central__1up", &Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_central__1up, &b_Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_central__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_forward__1up", &Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_forward__1up, &b_Jet_Calibrated_Pt_JET_JER_N_mc_non_closure_variation_forward__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_central__1up", &Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_central__1up, &b_Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_central__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_forward__1up", &Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_forward__1up, &b_Jet_Calibrated_Pt_JET_JER_N_no_pu_N_fit_instability_variation_forward__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_central__1up", &Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_central__1up, &b_Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_central__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_forward__1up", &Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_forward__1up, &b_Jet_Calibrated_Pt_JET_JER_N_no_pu_N_unc_up_forward__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_closure__1up", &Jet_Calibrated_Pt_JET_JER_dijet_closure__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_closure__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_jesnp1__1up", &Jet_Calibrated_Pt_JET_JER_dijet_jesnp1__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_jesnp1__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_jesnp2__1up", &Jet_Calibrated_Pt_JET_JER_dijet_jesnp2__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_jesnp2__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_jesnp3__1up", &Jet_Calibrated_Pt_JET_JER_dijet_jesnp3__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_jesnp3__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_jv__1up", &Jet_Calibrated_Pt_JET_JER_dijet_jv__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_jv__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_mcgenerator__1up", &Jet_Calibrated_Pt_JET_JER_dijet_mcgenerator__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_mcgenerator__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_pt3dphi__1up", &Jet_Calibrated_Pt_JET_JER_dijet_pt3dphi__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_pt3dphi__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat10__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat10__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat10__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat10__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat10__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat10__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat11__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat11__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat11__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat11__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat11__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat11__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat12__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat12__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat12__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat12__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat12__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat12__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat1__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat1__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat1__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat1__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat1__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat1__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat2__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat2__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat2__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat2__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat2__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat2__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat3__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat3__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat3__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat3__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat3__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat3__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat4__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat4__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat4__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat4__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat4__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat4__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat5__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat5__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat5__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat5__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat5__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat5__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat6__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat6__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat6__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat6__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat6__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat6__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat7__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat7__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat7__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat7__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat7__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat7__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat8__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat8__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat8__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat8__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat8__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat8__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat9__1down", &Jet_Calibrated_Pt_JET_JER_dijet_stat9__1down, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat9__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_JER_dijet_stat9__1up", &Jet_Calibrated_Pt_JET_JER_dijet_stat9__1up, &b_Jet_Calibrated_Pt_JET_JER_dijet_stat9__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1down", &Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1down, &b_Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1up", &Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1up, &b_Jet_Calibrated_Pt_JET_EtaIntercalibration_Modelling__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1down", &Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1down, &b_Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1up", &Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1up, &b_Jet_Calibrated_Pt_JET_EtaIntercalibration_NonClosure_2018data__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1down", &Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1down, &b_Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1up", &Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1up, &b_Jet_Calibrated_Pt_JET_EtaIntercalibration_TotalStat__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Flavor_Composition__1down", &Jet_Calibrated_Pt_JET_Flavor_Composition__1down, &b_Jet_Calibrated_Pt_JET_Flavor_Composition__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Flavor_Composition__1up", &Jet_Calibrated_Pt_JET_Flavor_Composition__1up, &b_Jet_Calibrated_Pt_JET_Flavor_Composition__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Flavor_Response__1down", &Jet_Calibrated_Pt_JET_Flavor_Response__1down, &b_Jet_Calibrated_Pt_JET_Flavor_Response__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Flavor_Response__1up", &Jet_Calibrated_Pt_JET_Flavor_Response__1up, &b_Jet_Calibrated_Pt_JET_Flavor_Response__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_GamESZee__1down", &Jet_Calibrated_Pt_JET_Gjet_GamESZee__1down, &b_Jet_Calibrated_Pt_JET_Gjet_GamESZee__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_GamESZee__1up", &Jet_Calibrated_Pt_JET_Gjet_GamESZee__1up, &b_Jet_Calibrated_Pt_JET_Gjet_GamESZee__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1down", &Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1down, &b_Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1up", &Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1up, &b_Jet_Calibrated_Pt_JET_Gjet_GamEsmear__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Generator__1down", &Jet_Calibrated_Pt_JET_Gjet_Generator__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Generator__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Generator__1up", &Jet_Calibrated_Pt_JET_Gjet_Generator__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Generator__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Jvt__1down", &Jet_Calibrated_Pt_JET_Gjet_Jvt__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Jvt__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Jvt__1up", &Jet_Calibrated_Pt_JET_Gjet_Jvt__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Jvt__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Purity__1down", &Jet_Calibrated_Pt_JET_Gjet_Purity__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Purity__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Purity__1up", &Jet_Calibrated_Pt_JET_Gjet_Purity__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Purity__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1down", &Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1down, &b_Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1up", &Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1up, &b_Jet_Calibrated_Pt_JET_Gjet_ShowerTopology__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat10__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat10__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat10__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat10__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat10__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat10__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat11__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat11__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat11__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat11__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat11__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat11__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat12__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat12__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat12__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat12__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat12__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat12__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat13__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat13__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat13__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat13__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat13__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat13__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat14__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat14__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat14__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat14__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat14__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat14__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat15__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat15__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat15__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat15__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat15__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat15__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat16__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat16__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat16__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat16__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat16__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat16__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat1__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat1__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat1__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat1__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat1__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat1__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat2__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat2__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat2__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat2__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat2__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat2__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat3__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat3__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat3__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat3__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat3__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat3__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat4__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat4__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat4__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat4__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat4__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat4__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat5__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat5__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat5__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat5__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat5__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat5__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat6__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat6__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat6__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat6__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat6__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat6__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat7__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat7__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat7__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat7__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat7__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat7__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat8__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat8__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat8__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat8__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat8__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat8__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat9__1down", &Jet_Calibrated_Pt_JET_Gjet_Stat9__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Stat9__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Stat9__1up", &Jet_Calibrated_Pt_JET_Gjet_Stat9__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Stat9__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Veto__1down", &Jet_Calibrated_Pt_JET_Gjet_Veto__1down, &b_Jet_Calibrated_Pt_JET_Gjet_Veto__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_Veto__1up", &Jet_Calibrated_Pt_JET_Gjet_Veto__1up, &b_Jet_Calibrated_Pt_JET_Gjet_Veto__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_dPhi__1down", &Jet_Calibrated_Pt_JET_Gjet_dPhi__1down, &b_Jet_Calibrated_Pt_JET_Gjet_dPhi__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Gjet_dPhi__1up", &Jet_Calibrated_Pt_JET_Gjet_dPhi__1up, &b_Jet_Calibrated_Pt_JET_Gjet_dPhi__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1down", &Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1down, &b_Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1up", &Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1up, &b_Jet_Calibrated_Pt_JET_Pileup_OffsetMu__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1down", &Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1down, &b_Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1up", &Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1up, &b_Jet_Calibrated_Pt_JET_Pileup_OffsetNPV__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Pileup_PtTerm__1down", &Jet_Calibrated_Pt_JET_Pileup_PtTerm__1down, &b_Jet_Calibrated_Pt_JET_Pileup_PtTerm__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Pileup_PtTerm__1up", &Jet_Calibrated_Pt_JET_Pileup_PtTerm__1up, &b_Jet_Calibrated_Pt_JET_Pileup_PtTerm__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1down", &Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1down, &b_Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1up", &Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1up, &b_Jet_Calibrated_Pt_JET_Pileup_RhoTopology__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecESZee__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecEsmear__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat10__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat11__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat12__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat13__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat14__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat1__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat2__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat3__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat4__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat5__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat6__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat7__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat8__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1down", &Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1up", &Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ElecStat9__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_Jvt__1down", &Jet_Calibrated_Pt_JET_Zjet_Jvt__1down, &b_Jet_Calibrated_Pt_JET_Zjet_Jvt__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_Jvt__1up", &Jet_Calibrated_Pt_JET_Zjet_Jvt__1up, &b_Jet_Calibrated_Pt_JET_Zjet_Jvt__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MC__1down", &Jet_Calibrated_Pt_JET_Zjet_MC__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MC__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MC__1up", &Jet_Calibrated_Pt_JET_Zjet_MC__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MC__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1down", &Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1up", &Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuSagittaRes__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1down", &Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1up", &Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuSagittaRho__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuScale__1down", &Jet_Calibrated_Pt_JET_Zjet_MuScale__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuScale__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuScale__1up", &Jet_Calibrated_Pt_JET_Zjet_MuScale__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuScale__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1down", &Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1up", &Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuSmearID__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1down", &Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1up", &Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuSmearMS__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat10__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat10__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat10__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat10__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat10__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat10__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat11__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat11__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat11__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat11__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat11__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat11__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat12__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat12__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat12__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat12__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat12__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat12__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat13__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat13__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat13__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat13__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat13__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat13__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat14__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat14__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat14__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat14__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat14__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat14__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat1__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat1__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat1__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat1__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat1__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat1__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat2__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat2__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat2__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat2__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat2__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat2__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat3__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat3__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat3__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat3__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat3__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat3__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat4__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat4__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat4__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat4__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat4__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat4__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat5__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat5__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat5__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat5__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat5__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat5__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat6__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat6__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat6__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat6__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat6__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat6__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat7__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat7__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat7__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat7__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat7__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat7__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat8__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat8__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat8__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat8__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat8__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat8__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat9__1down", &Jet_Calibrated_Pt_JET_Zjet_MuStat9__1down, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat9__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_MuStat9__1up", &Jet_Calibrated_Pt_JET_Zjet_MuStat9__1up, &b_Jet_Calibrated_Pt_JET_Zjet_MuStat9__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1down", &Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1down, &b_Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1up", &Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1up, &b_Jet_Calibrated_Pt_JET_Zjet_ShowerTopology__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_Veto__1down", &Jet_Calibrated_Pt_JET_Zjet_Veto__1down, &b_Jet_Calibrated_Pt_JET_Zjet_Veto__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_Veto__1up", &Jet_Calibrated_Pt_JET_Zjet_Veto__1up, &b_Jet_Calibrated_Pt_JET_Zjet_Veto__1up);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_dPhi__1down", &Jet_Calibrated_Pt_JET_Zjet_dPhi__1down, &b_Jet_Calibrated_Pt_JET_Zjet_dPhi__1down);
      Jet_Pt_Syst_Tree->SetBranchAddress("Jet_Calibrated_Pt_JET_Zjet_dPhi__1up", &Jet_Calibrated_Pt_JET_Zjet_dPhi__1up, &b_Jet_Calibrated_Pt_JET_Zjet_dPhi__1up);
   }
*/
   // Jet_SF_Tree
   RootFileUser()->GetObject("Jet_SF_Tree", Jet_SF_Tree);
   if (Jet_SF_Tree)
   {
      Jet_SF_Tree->SetBranchAddress("Jet_JvtSF", &Jet_JvtSF, &b_Jet_JvtSF);
      Jet_SF_Tree->SetBranchAddress("Jet_isBJetSF_Eff60", &Jet_isBJetSF_Eff60, &b_Jet_isBJetSF_Eff60);
      Jet_SF_Tree->SetBranchAddress("Jet_isBJetSF_Eff70", &Jet_isBJetSF_Eff70, &b_Jet_isBJetSF_Eff70);
      Jet_SF_Tree->SetBranchAddress("Jet_isBJetSF_Eff77", &Jet_isBJetSF_Eff77, &b_Jet_isBJetSF_Eff77);
      Jet_SF_Tree->SetBranchAddress("Jet_isBJetSF_Eff85", &Jet_isBJetSF_Eff85, &b_Jet_isBJetSF_Eff85);
   }

   // Jet_Tree
   RootFileUser()->GetObject("Jet_Tree", Jet_Tree);
   if (Jet_Tree)
   {
      Jet_Tree->SetBranchAddress("Jet_Original_Pt", &Jet_Original_Pt, &b_Jet_Original_Pt);
      Jet_Tree->SetBranchAddress("Jet_Calibrated_Pt", &Jet_Calibrated_Pt, &b_Jet_Calibrated_Pt);
      Jet_Tree->SetBranchAddress("Jet_NumTrkPt500", &Jet_NumTrkPt500, &b_Jet_NumTrkPt500);
      Jet_Tree->SetBranchAddress("Jet_SumPtTrkPt500", &Jet_SumPtTrkPt500, &b_Jet_SumPtTrkPt500);
      Jet_Tree->SetBranchAddress("Jet_Eta", &Jet_Eta, &b_Jet_Eta);
      Jet_Tree->SetBranchAddress("Jet_Phi", &Jet_Phi, &b_Jet_Phi);
      Jet_Tree->SetBranchAddress("Jet_M", &Jet_M, &b_Jet_M);
      Jet_Tree->SetBranchAddress("Jet_E", &Jet_E, &b_Jet_E);
      Jet_Tree->SetBranchAddress("Jet_Rapidity", &Jet_Rapidity, &b_Jet_Rapidity);
      Jet_Tree->SetBranchAddress("Jet_Jvt", &Jet_Jvt, &b_Jet_Jvt);
      // Jet_Tree->SetBranchAddress("Jet_TruthLabel", &Jet_TruthLabel, &b_Jet_TruthLabel);
      Jet_Tree->SetBranchAddress("Jet_isBJetScore", &Jet_isBJetScore, &b_Jet_isBJetScore);
      Jet_Tree->SetBranchAddress("Jet_PassJvt", &Jet_PassJvt, &b_Jet_PassJvt);
      Jet_Tree->SetBranchAddress("Jet_isBad", &Jet_isBad, &b_Jet_isBad);
      Jet_Tree->SetBranchAddress("Jet_isBJet_Eff60", &Jet_isBJet_Eff60, &b_Jet_isBJet_Eff60);
      Jet_Tree->SetBranchAddress("Jet_isBJet_Eff70", &Jet_isBJet_Eff70, &b_Jet_isBJet_Eff70);
      Jet_Tree->SetBranchAddress("Jet_isBJet_Eff77", &Jet_isBJet_Eff77, &b_Jet_isBJet_Eff77);
      Jet_Tree->SetBranchAddress("Jet_isBJet_Eff85", &Jet_isBJet_Eff85, &b_Jet_isBJet_Eff85);
   }

   // JetTruth_Tree
   RootFileUser()->GetObject("JetTruth_Tree", JetTruth_Tree);
   if (JetTruth_Tree)
   {
      JetTruth_Tree->SetBranchAddress("Jet_TruthLabel", &Jet_TruthLabel, &b_Jet_TruthLabel);
      JetTruth_Tree->SetBranchAddress("Jet_TruthPt", &Jet_TruthPt, &b_Jet_TruthPt);
      JetTruth_Tree->SetBranchAddress("Jet_TruthEta", &Jet_TruthEta, &b_Jet_TruthEta);
      JetTruth_Tree->SetBranchAddress("Jet_TruthPhi", &Jet_TruthPhi, &b_Jet_TruthPhi);
      JetTruth_Tree->SetBranchAddress("Jet_TruthM", &Jet_TruthM, &b_Jet_TruthM);
      JetTruth_Tree->SetBranchAddress("Jet_TruthE", &Jet_TruthE, &b_Jet_TruthE);
   }

   // METSyst_Tree
   RootFileUser()->GetObject("METSyst_Tree", METSyst_Tree);
   if (METSyst_Tree)
   {
      METSyst_Tree->SetBranchAddress("MET_Px_Syst_MET_SoftTrk_ResoPara", &MET_Px_Syst_MET_SoftTrk_ResoPara, &b_MET_Px_Syst_MET_SoftTrk_ResoPara);
      METSyst_Tree->SetBranchAddress("MET_Py_Syst_MET_SoftTrk_ResoPara", &MET_Py_Syst_MET_SoftTrk_ResoPara, &b_MET_Py_Syst_MET_SoftTrk_ResoPara);
      METSyst_Tree->SetBranchAddress("MET_MET_Syst_MET_SoftTrk_ResoPara", &MET_MET_Syst_MET_SoftTrk_ResoPara, &b_MET_MET_Syst_MET_SoftTrk_ResoPara);
      METSyst_Tree->SetBranchAddress("MET_Phi_Syst_MET_SoftTrk_ResoPara", &MET_Phi_Syst_MET_SoftTrk_ResoPara, &b_MET_Phi_Syst_MET_SoftTrk_ResoPara);
      METSyst_Tree->SetBranchAddress("MET_SumET_Syst_MET_SoftTrk_ResoPara", &MET_SumET_Syst_MET_SoftTrk_ResoPara, &b_MET_SumET_Syst_MET_SoftTrk_ResoPara);
      METSyst_Tree->SetBranchAddress("MET_Px_Syst_MET_SoftTrk_ResoPerp", &MET_Px_Syst_MET_SoftTrk_ResoPerp, &b_MET_Px_Syst_MET_SoftTrk_ResoPerp);
      METSyst_Tree->SetBranchAddress("MET_Py_Syst_MET_SoftTrk_ResoPerp", &MET_Py_Syst_MET_SoftTrk_ResoPerp, &b_MET_Py_Syst_MET_SoftTrk_ResoPerp);
      METSyst_Tree->SetBranchAddress("MET_MET_Syst_MET_SoftTrk_ResoPerp", &MET_MET_Syst_MET_SoftTrk_ResoPerp, &b_MET_MET_Syst_MET_SoftTrk_ResoPerp);
      METSyst_Tree->SetBranchAddress("MET_Phi_Syst_MET_SoftTrk_ResoPerp", &MET_Phi_Syst_MET_SoftTrk_ResoPerp, &b_MET_Phi_Syst_MET_SoftTrk_ResoPerp);
      METSyst_Tree->SetBranchAddress("MET_SumET_Syst_MET_SoftTrk_ResoPerp", &MET_SumET_Syst_MET_SoftTrk_ResoPerp, &b_MET_SumET_Syst_MET_SoftTrk_ResoPerp);
      METSyst_Tree->SetBranchAddress("MET_Px_Syst_MET_SoftTrk_Scale__1up", &MET_Px_Syst_MET_SoftTrk_Scale__1up, &b_MET_Px_Syst_MET_SoftTrk_Scale__1up);
      METSyst_Tree->SetBranchAddress("MET_Py_Syst_MET_SoftTrk_Scale__1up", &MET_Py_Syst_MET_SoftTrk_Scale__1up, &b_MET_Py_Syst_MET_SoftTrk_Scale__1up);
      METSyst_Tree->SetBranchAddress("MET_MET_Syst_MET_SoftTrk_Scale__1up", &MET_MET_Syst_MET_SoftTrk_Scale__1up, &b_MET_MET_Syst_MET_SoftTrk_Scale__1up);
      METSyst_Tree->SetBranchAddress("MET_Phi_Syst_MET_SoftTrk_Scale__1up", &MET_Phi_Syst_MET_SoftTrk_Scale__1up, &b_MET_Phi_Syst_MET_SoftTrk_Scale__1up);
      METSyst_Tree->SetBranchAddress("MET_SumET_Syst_MET_SoftTrk_Scale__1up", &MET_SumET_Syst_MET_SoftTrk_Scale__1up, &b_MET_SumET_Syst_MET_SoftTrk_Scale__1up);
      METSyst_Tree->SetBranchAddress("MET_Px_Syst_MET_SoftTrk_Scale__1down", &MET_Px_Syst_MET_SoftTrk_Scale__1down, &b_MET_Px_Syst_MET_SoftTrk_Scale__1down);
      METSyst_Tree->SetBranchAddress("MET_Py_Syst_MET_SoftTrk_Scale__1down", &MET_Py_Syst_MET_SoftTrk_Scale__1down, &b_MET_Py_Syst_MET_SoftTrk_Scale__1down);
      METSyst_Tree->SetBranchAddress("MET_MET_Syst_MET_SoftTrk_Scale__1down", &MET_MET_Syst_MET_SoftTrk_Scale__1down, &b_MET_MET_Syst_MET_SoftTrk_Scale__1down);
      METSyst_Tree->SetBranchAddress("MET_Phi_Syst_MET_SoftTrk_Scale__1down", &MET_Phi_Syst_MET_SoftTrk_Scale__1down, &b_MET_Phi_Syst_MET_SoftTrk_Scale__1down);
      METSyst_Tree->SetBranchAddress("MET_SumET_Syst_MET_SoftTrk_Scale__1down", &MET_SumET_Syst_MET_SoftTrk_Scale__1down, &b_MET_SumET_Syst_MET_SoftTrk_Scale__1down);
   }

   // Muon_FCLooseIsoSF_Syst_Tree
   RootFileUser()->GetObject("Muon_FCLooseIsoSF_Syst_Tree", Muon_FCLooseIsoSF_Syst_Tree);
   if (Muon_FCLooseIsoSF_Syst_Tree)
   {
      Muon_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down", &mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down, &b_mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down);
      Muon_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up", &mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up, &b_mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up);
      Muon_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down", &mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down, &b_mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down);
      Muon_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up", &mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up, &b_mc_Muon_FCLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up);
   }

   // Muon_FCTightIsoSF_Syst_Tree
   RootFileUser()->GetObject("Muon_FCTightIsoSF_Syst_Tree", Muon_FCTightIsoSF_Syst_Tree);
   if (Muon_FCTightIsoSF_Syst_Tree)
   {
      Muon_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down", &mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down, &b_mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down);
      Muon_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up", &mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up, &b_mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up);
      Muon_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down", &mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down, &b_mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down);
      Muon_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up", &mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up, &b_mc_Muon_FCTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up);
   }

   // Muon_FixedCutPflowLooseIsoSF_Syst_Tree
   RootFileUser()->GetObject("Muon_FixedCutPflowLooseIsoSF_Syst_Tree", Muon_FixedCutPflowLooseIsoSF_Syst_Tree);
   if (Muon_FixedCutPflowLooseIsoSF_Syst_Tree)
   {
      Muon_FixedCutPflowLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down", &mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down, &b_mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down);
      Muon_FixedCutPflowLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up", &mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up, &b_mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up);
      Muon_FixedCutPflowLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down", &mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down, &b_mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down);
      Muon_FixedCutPflowLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up", &mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up, &b_mc_Muon_FixedCutPflowLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up);
   }

   // Muon_HighPt_Syst_Tree
   RootFileUser()->GetObject("Muon_HighPt_Syst_Tree", Muon_HighPt_Syst_Tree);
   if (Muon_HighPt_Syst_Tree)
   {
      /*Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_ID__1down", &Muon_Calibrated_HighPt_Syst_MUON_ID__1down, &b_Muon_Calibrated_HighPt_Syst_MUON_ID__1down);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_ID__1up", &Muon_Calibrated_HighPt_Syst_MUON_ID__1up, &b_Muon_Calibrated_HighPt_Syst_MUON_ID__1up);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_MS__1down", &Muon_Calibrated_HighPt_Syst_MUON_MS__1down, &b_Muon_Calibrated_HighPt_Syst_MUON_MS__1down);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_MS__1up", &Muon_Calibrated_HighPt_Syst_MUON_MS__1up, &b_Muon_Calibrated_HighPt_Syst_MUON_MS__1up);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1down", &Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1down, &b_Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1down);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1up", &Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1up, &b_Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RESBIAS__1up);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1down", &Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1down, &b_Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1down);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1up", &Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1up, &b_Muon_Calibrated_HighPt_Syst_MUON_SAGITTA_RHO__1up);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_SCALE__1down", &Muon_Calibrated_HighPt_Syst_MUON_SCALE__1down, &b_Muon_Calibrated_HighPt_Syst_MUON_SCALE__1down);
      Muon_HighPt_Syst_Tree->SetBranchAddress("Muon_Calibrated_HighPt_Syst_MUON_SCALE__1up", &Muon_Calibrated_HighPt_Syst_MUON_SCALE__1up, &b_Muon_Calibrated_HighPt_Syst_MUON_SCALE__1up);
   */}

   // Muon_Isolation_Tree
   RootFileUser()->GetObject("Muon_Isolation_Tree", Muon_Isolation_Tree);
   if (Muon_Isolation_Tree)
   {
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone20", &Muon_Isolation_ptcone20, &b_Muon_Isolation_ptcone20);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone30", &Muon_Isolation_ptcone30, &b_Muon_Isolation_ptcone30);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone40", &Muon_Isolation_ptcone40, &b_Muon_Isolation_ptcone40);
      /*Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone50", &Muon_Isolation_ptcone50, &b_Muon_Isolation_ptcone50);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_etcone20", &Muon_Isolation_etcone20, &b_Muon_Isolation_etcone20);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_etcone30", &Muon_Isolation_etcone30, &b_Muon_Isolation_etcone30);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_etcone40", &Muon_Isolation_etcone40, &b_Muon_Isolation_etcone40);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone20", &Muon_Isolation_ptvarcone20, &b_Muon_Isolation_ptvarcone20);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone30", &Muon_Isolation_ptvarcone30, &b_Muon_Isolation_ptvarcone30);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone40", &Muon_Isolation_ptvarcone40, &b_Muon_Isolation_ptvarcone40);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone20_TightTTVA_pt500", &Muon_Isolation_ptcone20_TightTTVA_pt500, &b_Muon_Isolation_ptcone20_TightTTVA_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone30_TightTTVA_pt500", &Muon_Isolation_ptcone30_TightTTVA_pt500, &b_Muon_Isolation_ptcone30_TightTTVA_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone40_TightTTVA_pt500", &Muon_Isolation_ptcone40_TightTTVA_pt500, &b_Muon_Isolation_ptcone40_TightTTVA_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone20_TightTTVA_pt1000", &Muon_Isolation_ptcone20_TightTTVA_pt1000, &b_Muon_Isolation_ptcone20_TightTTVA_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone30_TightTTVA_pt1000", &Muon_Isolation_ptcone30_TightTTVA_pt1000, &b_Muon_Isolation_ptcone30_TightTTVA_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone40_TightTTVA_pt1000", &Muon_Isolation_ptcone40_TightTTVA_pt1000, &b_Muon_Isolation_ptcone40_TightTTVA_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone20_TightTTVA_pt500", &Muon_Isolation_ptvarcone20_TightTTVA_pt500, &b_Muon_Isolation_ptvarcone20_TightTTVA_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone30_TightTTVA_pt500", &Muon_Isolation_ptvarcone30_TightTTVA_pt500, &b_Muon_Isolation_ptvarcone30_TightTTVA_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone40_TightTTVA_pt500", &Muon_Isolation_ptvarcone40_TightTTVA_pt500, &b_Muon_Isolation_ptvarcone40_TightTTVA_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone20_TightTTVA_pt1000", &Muon_Isolation_ptvarcone20_TightTTVA_pt1000, &b_Muon_Isolation_ptvarcone20_TightTTVA_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone30_TightTTVA_pt1000", &Muon_Isolation_ptvarcone30_TightTTVA_pt1000, &b_Muon_Isolation_ptvarcone30_TightTTVA_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone40_TightTTVA_pt1000", &Muon_Isolation_ptvarcone40_TightTTVA_pt1000, &b_Muon_Isolation_ptvarcone40_TightTTVA_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone20_TightTTVALooseCone_pt500", &Muon_Isolation_ptcone20_TightTTVALooseCone_pt500, &b_Muon_Isolation_ptcone20_TightTTVALooseCone_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone30_TightTTVALooseCone_pt500", &Muon_Isolation_ptcone30_TightTTVALooseCone_pt500, &b_Muon_Isolation_ptcone30_TightTTVALooseCone_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone40_TightTTVALooseCone_pt500", &Muon_Isolation_ptcone40_TightTTVALooseCone_pt500, &b_Muon_Isolation_ptcone40_TightTTVALooseCone_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone20_TightTTVALooseCone_pt1000", &Muon_Isolation_ptcone20_TightTTVALooseCone_pt1000, &b_Muon_Isolation_ptcone20_TightTTVALooseCone_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone30_TightTTVALooseCone_pt1000", &Muon_Isolation_ptcone30_TightTTVALooseCone_pt1000, &b_Muon_Isolation_ptcone30_TightTTVALooseCone_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptcone40_TightTTVALooseCone_pt1000", &Muon_Isolation_ptcone40_TightTTVALooseCone_pt1000, &b_Muon_Isolation_ptcone40_TightTTVALooseCone_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt500", &Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt500, &b_Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt500", &Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt500, &b_Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt500", &Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt500, &b_Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt500);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt1000", &Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt1000, &b_Muon_Isolation_ptvarcone20_TightTTVALooseCone_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt1000", &Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt1000, &b_Muon_Isolation_ptvarcone30_TightTTVALooseCone_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt1000", &Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt1000, &b_Muon_Isolation_ptvarcone40_TightTTVALooseCone_pt1000);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_topoetcone20", &Muon_Isolation_topoetcone20, &b_Muon_Isolation_topoetcone20);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_topoetcone30", &Muon_Isolation_topoetcone30, &b_Muon_Isolation_topoetcone30);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_topoetcone40", &Muon_Isolation_topoetcone40, &b_Muon_Isolation_topoetcone40);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_neflowisol20", &Muon_Isolation_neflowisol20, &b_Muon_Isolation_neflowisol20);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_neflowisol30", &Muon_Isolation_neflowisol30, &b_Muon_Isolation_neflowisol30);
      Muon_Isolation_Tree->SetBranchAddress("Muon_Isolation_neflowisol40", &Muon_Isolation_neflowisol40, &b_Muon_Isolation_neflowisol40);*/
   }

   // Muon_PLVLooseIsoSF_Syst_Tree
   RootFileUser()->GetObject("Muon_PLVLooseIsoSF_Syst_Tree", Muon_PLVLooseIsoSF_Syst_Tree);
   if (Muon_PLVLooseIsoSF_Syst_Tree)
   {
      Muon_PLVLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down", &mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down, &b_mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1down);
      Muon_PLVLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up", &mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up, &b_mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_STAT__1up);
      Muon_PLVLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down", &mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down, &b_mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1down);
      Muon_PLVLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up", &mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up, &b_mc_Muon_PLVLooseIsoSF_Syst_MUON_EFF_ISO_SYS__1up);
   }

   // Muon_PLVTightIsoSF_Syst_Tree
   RootFileUser()->GetObject("Muon_PLVTightIsoSF_Syst_Tree", Muon_PLVTightIsoSF_Syst_Tree);
   if (Muon_PLVTightIsoSF_Syst_Tree)
   {
      Muon_PLVTightIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down", &mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down, &b_mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1down);
      Muon_PLVTightIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up", &mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up, &b_mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_STAT__1up);
      Muon_PLVTightIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down", &mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down, &b_mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1down);
      Muon_PLVTightIsoSF_Syst_Tree->SetBranchAddress("mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up", &mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up, &b_mc_Muon_PLVTightIsoSF_Syst_MUON_EFF_ISO_SYS__1up);
   }

   // Muon_Pt_Syst_Tree
   RootFileUser()->GetObject("Muon_Pt_Syst_Tree", Muon_Pt_Syst_Tree);
   if (Muon_Pt_Syst_Tree)
   {
      /*Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_ID__1down", &Muon_Calibrated_Pt_Syst_MUON_ID__1down, &b_Muon_Calibrated_Pt_Syst_MUON_ID__1down);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_ID__1up", &Muon_Calibrated_Pt_Syst_MUON_ID__1up, &b_Muon_Calibrated_Pt_Syst_MUON_ID__1up);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_MS__1down", &Muon_Calibrated_Pt_Syst_MUON_MS__1down, &b_Muon_Calibrated_Pt_Syst_MUON_MS__1down);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_MS__1up", &Muon_Calibrated_Pt_Syst_MUON_MS__1up, &b_Muon_Calibrated_Pt_Syst_MUON_MS__1up);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1down", &Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1down, &b_Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1down);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1up", &Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1up, &b_Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RESBIAS__1up);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1down", &Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1down, &b_Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1down);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1up", &Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1up, &b_Muon_Calibrated_Pt_Syst_MUON_SAGITTA_RHO__1up);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_SCALE__1down", &Muon_Calibrated_Pt_Syst_MUON_SCALE__1down, &b_Muon_Calibrated_Pt_Syst_MUON_SCALE__1down);
      Muon_Pt_Syst_Tree->SetBranchAddress("Muon_Calibrated_Pt_Syst_MUON_SCALE__1up", &Muon_Calibrated_Pt_Syst_MUON_SCALE__1up, &b_Muon_Calibrated_Pt_Syst_MUON_SCALE__1up);
   */}

   // Muon_RecoHighPt_Syst_Tree
   RootFileUser()->GetObject("Muon_RecoHighPt_Syst_Tree", Muon_RecoHighPt_Syst_Tree);
   if (Muon_RecoHighPt_Syst_Tree)
   {
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down", &mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down, &b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up", &mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up, &b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1down", &mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1down, &b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1down);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1up", &mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1up, &b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_STAT__1up);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down", &mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down, &b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up", &mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up, &b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1down", &mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1down, &b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1down);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1up", &mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1up, &b_mc_Muon_HighPtRecoSF_Syst_MUON_EFF_RECO_SYS__1up);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down", &mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down, &b_mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up", &mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up, &b_mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down", &mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down, &b_mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up", &mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up, &b_mc_Muon_RecoHighPt_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down", &mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down, &b_mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up", &mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up, &b_mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down", &mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down, &b_mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down);
      Muon_RecoHighPt_Syst_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up", &mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up, &b_mc_Muon_RecoHighPt_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up);
   }

   // Muon_RecoLoose_Syst_Tree
   RootFileUser()->GetObject("Muon_RecoLoose_Syst_Tree", Muon_RecoLoose_Syst_Tree);
   if (Muon_RecoLoose_Syst_Tree)
   {
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down", &mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down, &b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up", &mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up, &b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1down", &mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1down, &b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1down);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1up", &mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1up, &b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_STAT__1up);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down", &mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down, &b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up", &mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up, &b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1down", &mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1down, &b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1down);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1up", &mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1up, &b_mc_Muon_LooseRecoSF_Syst_MUON_EFF_RECO_SYS__1up);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down", &mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down, &b_mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up", &mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up, &b_mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down", &mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down, &b_mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up", &mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up, &b_mc_Muon_RecoLoose_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down", &mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down, &b_mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up", &mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up, &b_mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down", &mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down, &b_mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down);
      Muon_RecoLoose_Syst_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up", &mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up, &b_mc_Muon_RecoLoose_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up);
   }

   // Muon_RecoMedium_Syst_Tree
   RootFileUser()->GetObject("Muon_RecoMedium_Syst_Tree", Muon_RecoMedium_Syst_Tree);
   if (Muon_RecoMedium_Syst_Tree)
   {
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down", &mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down, &b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up", &mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up, &b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1down", &mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1down, &b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1down);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1up", &mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1up, &b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_STAT__1up);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down", &mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down, &b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up", &mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up, &b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1down", &mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1down, &b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1down);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1up", &mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1up, &b_mc_Muon_MediumRecoSF_Syst_MUON_EFF_RECO_SYS__1up);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down", &mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down, &b_mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up", &mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up, &b_mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down", &mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down, &b_mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up", &mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up, &b_mc_Muon_RecoMedium_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down", &mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down, &b_mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up", &mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up, &b_mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down", &mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down, &b_mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down);
      Muon_RecoMedium_Syst_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up", &mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up, &b_mc_Muon_RecoMedium_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up);
   }

   // Muon_RecoTight_Syst_Tree
   RootFileUser()->GetObject("Muon_RecoTight_Syst_Tree", Muon_RecoTight_Syst_Tree);
   if (Muon_RecoTight_Syst_Tree)
   {
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down", &mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down, &b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1down);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up", &mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up, &b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT_LOWPT__1up);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1down", &mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1down, &b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1down);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1up", &mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1up, &b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_STAT__1up);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down", &mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down, &b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1down);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up", &mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up, &b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS_LOWPT__1up);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1down", &mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1down, &b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1down);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1up", &mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1up, &b_mc_Muon_TightRecoSF_Syst_MUON_EFF_RECO_SYS__1up);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down", &mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down, &b_mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1down);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up", &mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up, &b_mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigStatUncertainty__1up);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down", &mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down, &b_mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1down);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up", &mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up, &b_mc_Muon_RecoTight_TriggerSF_Syst_MUON_EFF_TrigSystUncertainty__1up);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down", &mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down, &b_mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1down);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up", &mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up, &b_mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigStatUncertainty__1up);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down", &mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down, &b_mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1down);
      Muon_RecoTight_Syst_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up", &mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up, &b_mc_Muon_RecoTight_TriggerEff_Syst_MUON_EFF_TrigSystUncertainty__1up);
   }

   // Muon_SF_Tree
   RootFileUser()->GetObject("Muon_SF_Tree", Muon_SF_Tree);
   if (Muon_SF_Tree)
   {
      Muon_SF_Tree->SetBranchAddress("mc_Muon_FixedCutPflowLooseIsoSF", &mc_Muon_FixedCutPflowLooseIsoSF, &b_mc_Muon_FixedCutPflowLooseIsoSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_FCLooseIsoSF", &mc_Muon_FCLooseIsoSF, &b_mc_Muon_FCLooseIsoSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_FCTightIsoSF", &mc_Muon_FCTightIsoSF, &b_mc_Muon_FCTightIsoSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_PLVLooseIsoSF", &mc_Muon_PLVLooseIsoSF, &b_mc_Muon_PLVLooseIsoSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_PLVTightIsoSF", &mc_Muon_PLVTightIsoSF, &b_mc_Muon_PLVTightIsoSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerSF", &mc_Muon_RecoLoose_TriggerSF, &b_mc_Muon_RecoLoose_TriggerSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_RecoLoose_TriggerEff", &mc_Muon_RecoLoose_TriggerEff, &b_mc_Muon_RecoLoose_TriggerEff);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerSF", &mc_Muon_RecoMedium_TriggerSF, &b_mc_Muon_RecoMedium_TriggerSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_RecoMedium_TriggerEff", &mc_Muon_RecoMedium_TriggerEff, &b_mc_Muon_RecoMedium_TriggerEff);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerSF", &mc_Muon_RecoTight_TriggerSF, &b_mc_Muon_RecoTight_TriggerSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_RecoTight_TriggerEff", &mc_Muon_RecoTight_TriggerEff, &b_mc_Muon_RecoTight_TriggerEff);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerSF", &mc_Muon_RecoHighPt_TriggerSF, &b_mc_Muon_RecoHighPt_TriggerSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_RecoHighPt_TriggerEff", &mc_Muon_RecoHighPt_TriggerEff, &b_mc_Muon_RecoHighPt_TriggerEff);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_LooseRecoSF", &mc_Muon_LooseRecoSF, &b_mc_Muon_LooseRecoSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_MediumRecoSF", &mc_Muon_MediumRecoSF, &b_mc_Muon_MediumRecoSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_TightRecoSF", &mc_Muon_TightRecoSF, &b_mc_Muon_TightRecoSF);
      Muon_SF_Tree->SetBranchAddress("mc_Muon_TTVASF", &mc_Muon_TTVASF, &b_mc_Muon_TTVASF);
   }

   // Muon_Track_Tree
   RootFileUser()->GetObject("Muon_Track_Tree", Muon_Track_Tree);
   if (Muon_Track_Tree)
   {
      Muon_Track_Tree->SetBranchAddress("Muon_Track_Pt", &Muon_Track_Pt, &b_Muon_Track_Pt);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_Eta", &Muon_Track_Eta, &b_Muon_Track_Eta);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_Phi", &Muon_Track_Phi, &b_Muon_Track_Phi);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_M", &Muon_Track_M, &b_Muon_Track_M);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_E", &Muon_Track_E, &b_Muon_Track_E);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_Rapidity", &Muon_Track_Rapidity, &b_Muon_Track_Rapidity);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_Charge", &Muon_Track_Charge, &b_Muon_Track_Charge);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_d0", &Muon_Track_d0, &b_Muon_Track_d0);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_z0", &Muon_Track_z0, &b_Muon_Track_z0);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_Phi0", &Muon_Track_Phi0, &b_Muon_Track_Phi0);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_Theta", &Muon_Track_Theta, &b_Muon_Track_Theta);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_qOverP", &Muon_Track_qOverP, &b_Muon_Track_qOverP);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_Chi2", &Muon_Track_Chi2, &b_Muon_Track_Chi2);
      Muon_Track_Tree->SetBranchAddress("Muon_Track_NDoF", &Muon_Track_NDoF, &b_Muon_Track_NDoF);
   }

   // Muon_Tree
   RootFileUser()->GetObject("Muon_Tree", Muon_Tree);
   if (Muon_Tree)
   {
      Muon_Tree->SetBranchAddress("Muon_Original_Pt", &Muon_Original_Pt, &b_Muon_Original_Pt);
      Muon_Tree->SetBranchAddress("Muon_Calibrated_Pt", &Muon_Calibrated_Pt, &b_Muon_Calibrated_Pt);
      Muon_Tree->SetBranchAddress("Muon_Calibrated_HighPt", &Muon_Calibrated_HighPt, &b_Muon_Calibrated_HighPt);
      Muon_Tree->SetBranchAddress("Muon_Eta", &Muon_Eta, &b_Muon_Eta);
      Muon_Tree->SetBranchAddress("Muon_Phi", &Muon_Phi, &b_Muon_Phi);
      Muon_Tree->SetBranchAddress("Muon_M", &Muon_M, &b_Muon_M);
      Muon_Tree->SetBranchAddress("Muon_E", &Muon_E, &b_Muon_E);
      Muon_Tree->SetBranchAddress("Muon_Charge", &Muon_Charge, &b_Muon_Charge);
      Muon_Tree->SetBranchAddress("Muon_PassIDHighPt", &Muon_PassIDHighPt, &b_Muon_PassIDHighPt);
      Muon_Tree->SetBranchAddress("Muon_PassIDMedium", &Muon_PassIDMedium, &b_Muon_PassIDMedium);
      Muon_Tree->SetBranchAddress("Muon_PassIDTight", &Muon_PassIDTight, &b_Muon_PassIDTight);
      Muon_Tree->SetBranchAddress("Muon_PassIDLoose", &Muon_PassIDLoose, &b_Muon_PassIDLoose);
      Muon_Tree->SetBranchAddress("Muon_MatchedTrigger", &Muon_MatchedTrigger, &b_Muon_MatchedTrigger);
      Muon_Tree->SetBranchAddress("Muon_PassIso_FixedCutPflowLoose", &Muon_PassIso_FixedCutPflowLoose, &b_Muon_PassIso_FixedCutPflowLoose);
      Muon_Tree->SetBranchAddress("Muon_PassIso_FCLoose", &Muon_PassIso_FCLoose, &b_Muon_PassIso_FCLoose);
      Muon_Tree->SetBranchAddress("Muon_PassIso_FCTight", &Muon_PassIso_FCTight, &b_Muon_PassIso_FCTight);
      Muon_Tree->SetBranchAddress("Muon_PassIso_PLVLoose", &Muon_PassIso_PLVLoose, &b_Muon_PassIso_PLVLoose);
      Muon_Tree->SetBranchAddress("Muon_PassIso_PLVTight", &Muon_PassIso_PLVTight, &b_Muon_PassIso_PLVTight);
      Muon_Tree->SetBranchAddress("Muon_Track_dz0", &Muon_Track_dz0, &b_Muon_Track_dz0);
      Muon_Tree->SetBranchAddress("Muon_Track_d0Significance", &Muon_Track_d0Significance, &b_Muon_Track_d0Significance);
   }

   // Muon_Trigger_Tree
   RootFileUser()->GetObject("Muon_Trigger_Tree", Muon_Trigger_Tree);
   if (Muon_Trigger_Tree)
   {
      Muon_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_mu26_imedium", &MatchedTrigger_HLT_mu26_imedium, &b_MatchedTrigger_HLT_mu26_imedium);
      Muon_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_mu50", &MatchedTrigger_HLT_mu50, &b_MatchedTrigger_HLT_mu50);
      Muon_Trigger_Tree->SetBranchAddress("MatchedTrigger_HLT_mu26_ivarmedium", &MatchedTrigger_HLT_mu26_ivarmedium, &b_MatchedTrigger_HLT_mu26_ivarmedium);
   }

   // Muon_TTVASF_Syst_Tree
   RootFileUser()->GetObject("Muon_TTVASF_Syst_Tree", Muon_TTVASF_Syst_Tree);
   if (Muon_TTVASF_Syst_Tree)
   {
      Muon_TTVASF_Syst_Tree->SetBranchAddress("mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1down", &mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1down, &b_mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1down);
      Muon_TTVASF_Syst_Tree->SetBranchAddress("mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1up", &mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1up, &b_mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_STAT__1up);
      Muon_TTVASF_Syst_Tree->SetBranchAddress("mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1down", &mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1down, &b_mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1down);
      Muon_TTVASF_Syst_Tree->SetBranchAddress("mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1up", &mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1up, &b_mc_Muon_TTVASF_Syst_MUON_EFF_TTVA_SYS__1up);
   }

   // Photon_Tree
   RootFileUser()->GetObject("Photon_Tree", Photon_Tree);
   if (Photon_Tree)
   {
      Photon_Tree->SetBranchAddress("Photon_Calibrated_Pt", &Photon_Calibrated_Pt, &b_Photon_Calibrated_Pt);
      Photon_Tree->SetBranchAddress("Photon_Eta", &Photon_Eta, &b_Photon_Eta);
      Photon_Tree->SetBranchAddress("Photon_Phi", &Photon_Phi, &b_Photon_Phi);
      Photon_Tree->SetBranchAddress("Photon_PassCleaning", &Photon_PassCleaning, &b_Photon_PassCleaning);
      Photon_Tree->SetBranchAddress("Photon_PassIDTight", &Photon_PassIDTight, &b_Photon_PassIDTight);
      Photon_Tree->SetBranchAddress("Photon_PassIDLoose", &Photon_PassIDLoose, &b_Photon_PassIDLoose);
      Photon_Tree->SetBranchAddress("Photon_PassIso_FCLoose", &Photon_PassIso_FCLoose, &b_Photon_PassIso_FCLoose);
      Photon_Tree->SetBranchAddress("Photon_PassIso_FCTight", &Photon_PassIso_FCTight, &b_Photon_PassIso_FCTight);
   }

   // Photon_SF_Tree
   RootFileUser()->GetObject("Photon_SF_Tree", Photon_SF_Tree);
   if (Photon_SF_Tree)
   {
      Photon_SF_Tree->SetBranchAddress("mc_Photon_TightIDSF", &mc_Photon_TightIDSF, &b_mc_Photon_TightIDSF);
      Photon_SF_Tree->SetBranchAddress("mc_Photon_FCLooseIsoSF", &mc_Photon_FCLooseIsoSF, &b_mc_Photon_FCLooseIsoSF);
      Photon_SF_Tree->SetBranchAddress("mc_Photon_FCTightIsoSF", &mc_Photon_FCTightIsoSF, &b_mc_Photon_FCTightIsoSF);
   }

   // Photon_Pt_Syst_Tree
   RootFileUser()->GetObject("Photon_Pt_Syst_Tree", Photon_Pt_Syst_Tree);
   if (Photon_Pt_Syst_Tree)
   {
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_AF2__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCALO__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALCRYO__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALGAP__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALIBL__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALID__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_MATERIALPP0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_PILEUP__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_SAMPLINGTERM__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up", &Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up, &b_Photon_Calibrated_Pt_Syst_EG_RESOLUTION_ZSMEARING__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_AF2__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_E4SCINTILLATOR__ETABIN2__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_G4__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_L1GAIN__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_L2GAIN__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARCALIB__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECCALIB__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARELECUNCONV__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1down); 
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_LARUNCONVCALIB__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN10__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN11__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN2__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN3__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN4__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN5__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN6__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN7__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN8__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCALO__ETABIN9__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN10__1up); 
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN11__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN2__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN3__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN4__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN5__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN6__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN7__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN8__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATCRYO__ETABIN9__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN2__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATID__ETABIN3__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_MATPP0__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PEDESTAL__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS_BARREL_B12__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN2__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN3__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN4__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN5__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN6__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN7__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_PS__ETABIN8__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN0__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN2__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN3__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_S12__ETABIN4__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_TOPOCLUSTER_THRES__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_WTOTS1__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESTAT__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down", &Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up", &Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up, &b_Photon_Calibrated_Pt_Syst_EG_SCALE_ZEESYST__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1down", &Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1down, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1up", &Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1up, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVEFFICIENCY__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1down", &Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1down, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1up", &Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1up, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVFAKERATE__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1down", &Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1down, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1up", &Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1up, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_CONVRADIUS__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1down", &Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1down, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1up", &Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1up, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGECONV__1up);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1down", &Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1down, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1down);
   Photon_Pt_Syst_Tree->SetBranchAddress("Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1up", &Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1up, &b_Photon_Calibrated_Pt_Syst_PH_SCALE_LEAKAGEUNCONV__1up);
   }

   // Photon_TightIDSF_Syst_Tree
   RootFileUser()->GetObject("Photon_TightIDSF_Syst_Tree", Photon_TightIDSF_Syst_Tree);
   if (Photon_TightIDSF_Syst_Tree)
   {
      Photon_TightIDSF_Syst_Tree->SetBranchAddress("mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1down", &mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1down, &b_mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1down);
      Photon_TightIDSF_Syst_Tree->SetBranchAddress("mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1up", &mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1up, &b_mc_Photon_TightIDSF_Syst_PH_EFF_ID_Uncertainty__1up);
   }

   // Photon_FCLooseIsoSF_Syst_Tree
   RootFileUser()->GetObject("Photon_FCLooseIsoSF_Syst_Tree", Photon_FCLooseIsoSF_Syst_Tree);
   if (Photon_FCLooseIsoSF_Syst_Tree)
   {
      Photon_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down", &mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down, &b_mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down);
      Photon_FCLooseIsoSF_Syst_Tree->SetBranchAddress("mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up", &mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up, &b_mc_Photon_FCLooseIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up);
   }

   // Photon_FCTightIsoSF_Syst_Tree
   RootFileUser()->GetObject("Photon_FCTightIsoSF_Syst_Tree", Photon_FCTightIsoSF_Syst_Tree);
   if (Photon_FCTightIsoSF_Syst_Tree)
   {
      Photon_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down", &mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down, &b_mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1down);
      Photon_FCTightIsoSF_Syst_Tree->SetBranchAddress("mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up", &mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up, &b_mc_Photon_FCTightIsoSF_Syst_PH_EFF_ISO_Uncertainty__1up);
   }

   // ParticleTruth_Tree
   RootFileUser()->GetObject("ParticleTruth_Tree", ParticleTruth_Tree);
   if (ParticleTruth_Tree)
   {
      ParticleTruth_Tree->SetBranchAddress("Truth_ID", &Truth_ID, &b_Truth_ID);
      ParticleTruth_Tree->SetBranchAddress("Truth_Barcode", &Truth_Barcode, &b_Truth_Barcode);
      ParticleTruth_Tree->SetBranchAddress("Truth_Status", &Truth_Status, &b_Truth_Status);
      //ParticleTruth_Tree->SetBranchAddress("Truth_MotherID", &Truth_MotherID, &b_Truth_MotherID);
      ParticleTruth_Tree->SetBranchAddress("Truth_Px", &Truth_Px, &b_Truth_Px);
      ParticleTruth_Tree->SetBranchAddress("Truth_Py", &Truth_Py, &b_Truth_Py);
      ParticleTruth_Tree->SetBranchAddress("Truth_Pz", &Truth_Pz, &b_Truth_Pz);
      ParticleTruth_Tree->SetBranchAddress("Truth_E", &Truth_E, &b_Truth_E);
      ParticleTruth_Tree->SetBranchAddress("Truth_M", &Truth_M, &b_Truth_M);
   }

   // PDFInfo_Tree
   RootFileUser()->GetObject("PDFInfo_Tree", PDFInfo_Tree);
   if (PDFInfo_Tree)
   {
      PDFInfo_Tree->SetBranchAddress("PDGID1", &PDGID1, &b_PDGID1);
      PDFInfo_Tree->SetBranchAddress("PDGID2", &PDGID2, &b_PDGID2);
      PDFInfo_Tree->SetBranchAddress("PDFID1", &PDFID1, &b_PDFID1);
      PDFInfo_Tree->SetBranchAddress("PDFID2", &PDFID2, &b_PDFID2);
      PDFInfo_Tree->SetBranchAddress("x1", &x1, &b_x1);
      PDFInfo_Tree->SetBranchAddress("x2", &x2, &b_x2);
      PDFInfo_Tree->SetBranchAddress("Q", &Q, &b_Q);
      PDFInfo_Tree->SetBranchAddress("xf1", &xf1, &b_xf1);
      PDFInfo_Tree->SetBranchAddress("xf2", &xf2, &b_xf2);
   }

   // TriggerInfo_Tree
   RootFileUser()->GetObject("TriggerInfo_Tree", TriggerInfo_Tree);
   if (TriggerInfo_Tree)
   {
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_mu26_imedium", &PassTrigger_HLT_mu26_imedium, &b_PassTrigger_HLT_mu26_imedium);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_e24_lhmedium_L1EM20VH", &PassTrigger_HLT_e24_lhmedium_L1EM20VH, &b_PassTrigger_HLT_e24_lhmedium_L1EM20VH);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_e60_lhmedium", &PassTrigger_HLT_e60_lhmedium, &b_PassTrigger_HLT_e60_lhmedium);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_e120_lhloose", &PassTrigger_HLT_e120_lhloose, &b_PassTrigger_HLT_e120_lhloose);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_mu50", &PassTrigger_HLT_mu50, &b_PassTrigger_HLT_mu50);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_mu26_ivarmedium", &PassTrigger_HLT_mu26_ivarmedium, &b_PassTrigger_HLT_mu26_ivarmedium);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_e26_lhtight_nod0_ivarloose", &PassTrigger_HLT_e26_lhtight_nod0_ivarloose, &b_PassTrigger_HLT_e26_lhtight_nod0_ivarloose);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_e60_lhmedium_nod0", &PassTrigger_HLT_e60_lhmedium_nod0, &b_PassTrigger_HLT_e60_lhmedium_nod0);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_e140_lhloose_nod0", &PassTrigger_HLT_e140_lhloose_nod0, &b_PassTrigger_HLT_e140_lhloose_nod0);
      TriggerInfo_Tree->SetBranchAddress("PassTrigger_HLT_e300_etcut", &PassTrigger_HLT_e300_etcut, &b_PassTrigger_HLT_e300_etcut);
   }

   return NAGASH::StatusCode::SUCCESS;
}

#endif
